#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(logdie infof info warning);
use strict;
use Getopt::Std;
use DBIFunc;
use CommonFunc;
use File::Basename;
use Logger;

use vars qw($opt_M $opt_p $opt_n $opt_I $opt_E $opt_i $opt_s $opt_k $opt_U $opt_S $opt_P $opt_D $opt_v $opt_e $opt_l $opt_d $opt_h %CONFIG);

sub usage
{
   print @_;
   print "USAGE: $0 -UUSER -SSERVER -PPASS [-DDB]
   -p samplepct   adds 'with sampling = samplepct' (sybase 12.5.0.3+)
   -D DB_LIST     (pipe separated list with wildcards of databases)
   -E TABLES      (comma separated list of tables to EXCLUDE)
   -I TABLES      (comma separated list of tables to INCLUDE)
   -s             (silent mode)
	-k					keep old stats (ie no delete stats)
   -d             (debug mode)
   -v		  			number of steps/values to use (suggest 60 - dflt 20)
   -h             (html output)
   -i             update index stats
   -n 		  		noexec
   -e errorlog    (optional)
   -l sessionlog  (optional)";

   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List") unless getopts('nM:sU:D:S:P:kl:e:p:dhiE:I:v:');

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  undef,
            -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

use Sys::Hostname;
info( "update_stats.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

logdie usage("Must pass sa password\n" )  unless defined $opt_P;
logdie usage("Must pass server\n")			unless defined $opt_S;
logdie usage("Must pass sa username\n" )  unless defined $opt_U;

my($show_output);
$show_output=1 unless defined $opt_s;

if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}
#
# CONNECT TO DB
#
info "update_stats.pl version 2\n";
info "started at ".localtime(time)."\n";
info "Connecting to database",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-type=>$opt_M,-password=>$opt_P,-debug=>$opt_d) or logdie "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");
my($server_type,@databaselist)=dbi_parse_opt_D($opt_D,1);

info("Server Type=$server_type\n");
if( $#databaselist<0 ) {
	warning "No Databases Found On Server Matching Spec $opt_D\n";
	my($server_type,@databaselist)=dbi_parse_opt_D("%",1);
	warning "Allowed Databases:",join(" ",@databaselist),"\n";
	exit(0);
}

dbi_set_option( -croak=>\&mycr, -print=> \&mypr);

foreach my $db (@databaselist) {
  	next if $db eq "master"
                or $db eq "model"
                or $db eq "sybsystemprocs"
                or $db eq "sybsystemdb"
                or $db eq "sybsecurity"
                or $db eq "tempdb";

	info "Working On Database $db\n";
	my($cmd)="update statistics";
	$cmd="update index statistics" if defined $opt_i;
	$cmd.= " OBJECT using $opt_v values" if defined $opt_v;
	$cmd.= " with sampling=$opt_p" if defined $opt_p;

	if( $server_type eq "SQLSERVER" ) {
   	dbi_eachobject(
				-db=>$db,
				-cmd=>"update statistics",
				-cmd2=>"exec sp_recompile",
				-no_exec=>$opt_n,
				-type=>"U",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
	} elsif( $opt_k )  {
   	dbi_eachobject(
				-db=>$db,
				-cmd=>$cmd,
				-cmd1=>"exec sp_recompile",
				-no_exec=>$opt_n,
				-type=>"U",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
	} else {
   	dbi_eachobject(
				-db=>$db,
				-cmd=>"delete statistics",
				-cmd1=>$cmd,
				-cmd2=>"exec sp_recompile",
				-no_exec=>$opt_n,
				-type=>"U",
				-do_print=>$show_output,
				-debug=>$opt_d,
				-no_results=>1,
				-include=>$opt_I,
				-exclude=>$opt_E);
	}

   	#dbi_eachobject(-db=>$db,-cmd=>"exec sp_recompile",-type=>"U",-do_print=>$show_output, -debug=>$opt_d, -include=>$opt_I, -exclude=>$opt_E);
	info "Completed Database $db\n";
}

info "completed at ".localtime(time)."\n";
dbi_disconnect();
exit(0);

sub mypr {
	$_[0]=~s/^\t//;
	info( localtime(time).": ".join("",@_) )
		unless $_[0] =~ /Each stored procedure and trigger that/;
}
sub mycr {	info( "CR",@_ ); }

__END__

=head1 NAME

update_stats.pl - update statistics on a database

=head2 AUTHOR

By Ed Barlow

=head2 DESCRIPTION

update statistics on all tables in db.  Also recompiles.  If Db Not Passed,
will work on all user databases in the server.  Pass -s if you want no output.

=head2 USAGE

USAGE: update_stats.pl -UUSER -SSERVER -PPASS [-DDB]

   -D DB_LIST     (pipe separated list with wildcards of databases)
   -E TABLES      (comma separated list of tables to EXCLUDE)
   -I TABLES      (comma separated list of tables to INCLUDE)
   -s             (silent mode)
	-k					keep old stats (ie no delete stats)
   -d             (debug mode)
   -v		  number of steps/values to use (suggest 60 - dflt 20)
   -h             (html output)
   -i             update index stats
   -n 		  noexec
   -e errorlog    (optional)
   -l sessionlog  (optional)

=head2 ARGUMENTS

You can control pretty much everything here.  Will do a delete stats, update stats, and then an sp_recompile
on the object in question.

=cut
