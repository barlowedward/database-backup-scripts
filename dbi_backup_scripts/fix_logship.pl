#!/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use Getopt::Std;
use File::Basename;
use CommonFunc;
use File::Copy;
use Do_Time;
use Logger;
use Repository;
use MlpAlarm;
use Sys::Hostname;
use Net::myFTP;

use vars qw($opt_T $opt_S %CONFIG $opt_p $opt_J $opt_D $opt_U $opt_N $opt_d $opt_F $opt_f $opt_P $opt_u $opt_s $opt_y $opt_t $opt_h);

# Copyright (c) 2003-6 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
   print $0.' -Jjob -Ssourcedir -hsourceUnixHostname -Ttargetdir -ttargetUnixHostname -Ddatabase -UN -u/usr/bin/gunzip

 -f full dumps resync (will load dumps too)
 -y since yesterday (24 hours)
 -s since time - yyyymmdd.hhmmss
 -p purge target only files
 -N nocopy - just rename .done extensions (must pass -U)
 -U is used to move .done to unnamed extensions\n";
 -P print differences
 -u uncompress program for source side
 -D database name - comma separated list

 -F filespec - match patern for files.  Otherwise the following will be used
	.$db.logdump.\d+.\d+	if database name is passed
	logdump.\d+.\d+		if no database is passed

where \d+ is the perl string for a set of digits
';
	return join("",@_),"\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List\n") unless getopts('S:T:J:D:UNF:Pdu:s:yt:h:fp');

if( defined $opt_y and ! defined $opt_s ) {
	print "Defaulting sync to 1 day : change sync time with use -syyyymmdd.hhmmss\n";
	$opt_s= do_time(-fmt=>"yyyymmdd.hhmiss",-time=>(time-24*60*60))
		if defined $opt_y and ! defined $opt_s;
}

my($since_day,$since_time) = split(/\./,$opt_s) if defined $opt_s;
print "fix_logship.pl v1.0 : run at ".localtime(time)."\n";
print "run on hostname=",hostname(),"\n";

if( defined $opt_J ) {
	MlpBatchStart(-monitor_program=>'FixLogship', -system=>$opt_J, -batchjob=>1);
	%CONFIG=read_configfile( -job=>$opt_J );
	if( defined $opt_d ) {
		foreach (sort keys %CONFIG) { print "debug: CONFIG{".$_."}=".$CONFIG{$_}."\n"; }
	}

	logger_init(
		-logfile	=> undef,
            	-errfile	=> undef,
            	-database	=> $opt_D,
            	-debug	 	=> $opt_d,
            	-system 	=> $opt_J,
            	-subsystem 	=> $opt_J,
            	-command_run 	=> $CONFIG{COMMAND_RUN},
            	-mail_host   	=> $CONFIG{SUCCESS_MAIL_TO},
            	-mail_to     	=> $CONFIG{SUCCESS_MAIL_TO},
            	-success_mail_to=> $CONFIG{SUCCESS_MAIL_TO} );

        #
        # GET LOG SHIPPING VARIABLES
        #
        #die "Error - Can not pass job name if IS_REMOTE != n - it is currently $CONFIG{IS_REMOTE}.  You should pass -h or -t."
        #	if $CONFIG{IS_REMOTE} ne "n" and ( ! defined $opt_h or ! defined $opt_t);

        if( ! defined $opt_S ) {
        	if( $CONFIG{IS_REMOTE} eq "n" ) {
        		$opt_S=$CONFIG{LOCAL_DIR};
        		if( defined $opt_f ) {
        			$opt_S.="/".$CONFIG{DSQUERY}."/dbdumps"
        				if -d "$CONFIG{LOCAL_DIR}/$CONFIG{DSQUERY}/dbdumps";
        		} else {
        			$opt_S.="/".$CONFIG{DSQUERY}."/logdumps"
        				if -d "$CONFIG{LOCAL_DIR}/$CONFIG{DSQUERY}/logdumps";
        		}
        	} else {
        		$opt_S=$CONFIG{REMOTE_DIR};
        		if( $opt_f ) {
        			$opt_S.="/".$CONFIG{DSQUERY}."/dbdumps" ;
        			$opt_S = $CONFIG{REMOTE_DUMP_DIR} if defined $CONFIG{REMOTE_DUMP_DIR};
        		} else {
        			$opt_S.="/".$CONFIG{DSQUERY}."/logdumps";
        			$opt_S = $CONFIG{REMOTE_LOG_DIR} if defined $CONFIG{REMOTE_LOG_DIR};
        		}

        	}
        }

        $opt_T=$CONFIG{XFER_TO_DIR} unless defined $opt_T;

        #if( ! defined $opt_F ) {
        	if( !  defined $CONFIG{XFER_TO_SERVER} ) {
        		foreach (sort keys %CONFIG) { print "$_ $CONFIG{$_} \n"; }
        		die "XFER_TO_SERVER does not exist\n";
        	}

        #	my($user,$password)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sybase",-debug=>$opt_d);
        #	my($srvtype);
        #	if( defined $user ) {
        #		$srvtype="sybase";
        #		print "USER=$user\n";
        #		die "Error - must pass file spec with -F for unix" unless defined $opt_F;
        #	} else {
        #		($user,$password)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sqlsvr");
        #		$srvtype="sqlsvr";
        #		$opt_F="_tlog_" unless defined $opt_F;
        #	}
        #}

        if( $CONFIG{IS_REMOTE} ne "n" ) {
        	$opt_h=$CONFIG{REMOTE_HOST} unless defined $opt_h;
        	$opt_t=$CONFIG{XFER_TO_HOST} unless defined $opt_t;
        }

        print "Arguments For fix_logship.pl set to:\n";
        print "\tSOURCE DIR:  $opt_S\n";
        print "\tSOURCE HOST: $opt_h\n" if defined $opt_h;
        print "\tTARGET DIR:  $opt_T\n";
        print "\tTARGET HOST: $opt_t\n" if defined $opt_t;

        #if( $CONFIG{IS_REMOTE} ne "n" ) {
        #	print "Fixing log shipping on remote system.\n";
        #	print "The following variables will be used:\n";
        #	foreach (sort keys %CONFIG) { print "$_ $CONFIG{$_} \n"; }
        #	die "Done";
        #}

        #print "\t-F $opt_F\n";

        # print "-SOURCESERVER=>$CONFIG{DSQUERY}\n";
        # print "-TARGETSERVER=>$CONFIG{XFER_TO_SERVER}\n";
        # print "-INDIR=>$CONFIG{XFER_TO_DIR}\n";
        # print "user=$user password=$password";
        # print "-S=>$opt_S\n";
        # print "-T=>$opt_T\n";
} else {
	MlpBatchStart(-monitor_program=>'FixLogship', -system=>$opt_S, -batchjob=>1);
	logger_init(-logfile=>  undef,
            -errfile=>  undef,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  $opt_S,
            -subsystem =>$opt_D,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );
}

my($hlog,$hpass,$hmethod,$tlog,$tpass,$tmethod);
# h should be JOB_REMOTE_HOST and t should be JOB_XFER_TO_HOST
if( defined $opt_t and defined $opt_h ) {
	($hlog,$hpass)=get_password(-name=>$opt_h,-type=>"unix");
	if( ! defined $hlog or ! defined $hpass ) {
		die "Cant find password for $opt_h\n";
	}

	my(%args)=get_password_info(-type=>"unix",-name=>$opt_h);
	if( is_nt() ) {
		$hmethod=$args{WIN32_COMM_METHOD} if is_nt();
	} else {
		$hmethod=$args{UNIX_COMM_METHOD};
	}
	die "COMM_METHOD NOT DEFINED for SERVER $opt_h\n" unless $hmethod;
	die "host $opt_h: METHOD=NONE - aborting\n" if $hmethod eq "NONE";	# hmm cant talk

	($tlog,$tpass)=get_password(-name=>$opt_t,-type=>"unix");
	if( ! defined $tlog or ! defined $tpass ) {
		die "Cant find password for $opt_t\n";
	}

	my(%args)=get_password_info(-type=>"unix",-name=>$opt_t);
	if( is_nt() ) {
		$tmethod=$args{WIN32_COMM_METHOD} if is_nt();
	} else {
		$tmethod=$args{UNIX_COMM_METHOD};
	}
	die "COMM_METHOD NOT DEFINED for SERVER $opt_t\n" unless $tmethod;
	die "host $opt_t: METHOD=NONE - aborting\n" if $tmethod eq "NONE";	# hmm cant talk
}

logdie usage("Must pass source dir\n") unless defined $opt_S;
logdie usage("Must pass target dir\n") unless defined $opt_T;

logdie usage("Bad source dir $opt_S\n") unless -d $opt_S or $opt_h;
logdie usage("Bad target dir $opt_T\n") unless -d $opt_T or $opt_t;

my(%config);
$config{Debug} = $opt_d;
$config{Timeout}=90000;
if( defined $tlog and defined $tpass ) {
	$config{IS_REMOTE}="y";
} else {
	$config{IS_REMOTE}="n";
}

my($ftptgt)= Net::myFTP->new($opt_t,METHOD=>$tmethod,%config)
        or logdie "Failed To Create FTP Connection to $opt_t : $@";
my($r,$err)=$ftptgt->login($tlog,$tpass);
logdie( "Cant FTP To host ($opt_t) As $tlog : $err \n") unless $r;
$ftptgt->binary();

my($ftpsrc)= Net::myFTP->new($opt_h,METHOD=>$hmethod,%config)
         or logdie "Failed To Create FTP Connection to $opt_h : $@";
my($r2,$err2)=$ftpsrc->login($hlog,$hpass);
logdie( "Cant FTP To host ($opt_h) As $hlog : $err\n") unless $r2;

$ftpsrc->binary();

my(@dblist)=split(/[\,\|]/,$opt_D);
push @dblist,"" if ! defined $opt_D;
foreach my $dbname (@dblist ) {
	print "Reading Source ($opt_S) \n";
	my(@sourcefiles)=get_files($ftpsrc,$opt_S,$dbname );
	die "No Files Found In Directory $opt_S Matching Spec\n"
		if $#sourcefiles<0;
	print " ... ",($#sourcefiles+1)," Files Found\n";
	print "Reading Target ($opt_T)\n";
	my(@targetfiles)=get_files($ftptgt,$opt_T,$dbname);
	print " ... ",($#targetfiles+1)," Files Found\n";
	print "Done File Reads\n";

        my(%found);
        my(%bestdate,%besttime);
        foreach ( @targetfiles )  {
        	my($f)=$_;
        	$f=~s/\.done$//;
        	$f=~s/\.gz$//;
        	$f=~s/\.done$//;
        	$f=~s/\.gz$//;
        	$found{$f}="TARGET ONLY";

        	next if $f eq $_;
		my($db,$dt,$tm);
		if( defined $opt_f ) {
			$f=~ /\w+.(\w+).dbdump.(\d+).(\d+)/;
        		($db,$dt,$tm)=($1,$2,$3);
        	} else {
        		$f=~ /\w+.(\w+).logdump.(\d+).(\d+)/;
        		($db,$dt,$tm)=($1,$2,$3);
        	}

        	if( defined $opt_s ) {
        		next if $dt<$since_day;
        		next if $dt eq $since_day and int($tm)<int($since_time);
        	}

#print "File = $f\n";
#print "DBG: copy Date COmpare $tm, $since_time $dt $since_day\n" ;
#print "db=$db dt=$dt tm=$tm\n";
        	if( ! defined $bestdate{$db} ) {
        		$bestdate{$db}=0;
        		$besttime{$db}=0;
        	}
        	next if $bestdate{$db} > $dt;
        	next if $bestdate{$db} == $dt and $besttime{$db}>$tm;
        	$bestdate{$db}=$dt;
        	$besttime{$db}=$tm;
        }
#print "\n";
#foreach (sort keys %bestdate) {
       	#print $_," ",$bestdate{$_}," ",$besttime{$_},"\n";
#}

        foreach ( @sourcefiles )  {
        	my($f)=$_;
        	$f=~s/\.done$//;
        	$f=~s/\.gz$//;
        	$f=~s/\.done$//;
        	$f=~s/\.gz$//;

        	if( defined $found{$f} ) {
        		$found{$f} = "OK";
        	} else {
        		$found{$f} = "SOURCE ONLY";
        	}

		my($db,$dt,$tm);
		if( defined $opt_f ) {
			/\w+.(\w+).dbdump.(\d+).(\d+)/;
        		($db,$dt,$tm)=($1,$2,$3);
        	} else {
        		/\w+.(\w+).logdump.(\d+).(\d+)/;
        		($db,$dt,$tm)=($1,$2,$3);
        	}

        	if( defined $opt_s ) {
        		delete $found{$f} if $dt<$since_day;
        		next if $dt<$since_day;
        		delete $found{$f} if $dt eq $since_day
				and int($tm)<int($since_time);
        		next if $dt eq $since_day
				and int($tm)<int($since_time);
        	}

        	$found{$f}="SKIP" if $bestdate{$db} > $dt;
        	$found{$f}="SKIP" if $bestdate{$db} == $dt and $besttime{$db}>$tm;
        }

        my($num_ok,$num_source,$num_target,$num_skip)=(0,0,0,0);
        foreach (keys %found) {
        	$num_ok++ 	if $found{$_} eq "OK";
        	$num_source++ 	if $found{$_} eq "SOURCE ONLY";
        	$num_target++ 	if $found{$_} eq "TARGET ONLY";
        	$num_skip++ 	if $found{$_} eq "SKIP";
#print "Found $found{$_} $_ \n";
        }

        print "Files On Source Only = $num_source (these need copying)\n";
        print "Files On Target Only = $num_target (source purged)\n";
        print "Files On Both        = $num_ok\n";
        print "Files Too Early      = $num_skip (.done file exists)\n";
#die "done";

        if( ! defined $opt_N ) {
        	if( defined $opt_f or $opt_p ) {	# remove target only stuff if and only if its a full dump
        		foreach( keys %found ) {
        			next unless $found{$_} eq "TARGET ONLY";
        			$ftptgt->delete($_);
        			print "removing $_\n";
        		}
        	}

		print "Sourcefiles=",($#sourcefiles+1)."\n";
		foreach ( sort @sourcefiles )  {
        		my($f)=$_;
        		s/\.done$//;
        		s/\.gz$//;
        		 #s/\.done$//;
        		 #s/\.gz$//;

        		next if $found{$_} eq "SKIP" or $found{$_} eq "OK";
        		next unless $found{$_} eq "SOURCE ONLY";
        		print $found{$_}," ",$f," (copying)\n";
        		if( defined $opt_u and $f =~ /\.gz$/) {
        			print "... uncompressing: $opt_u $opt_S/$f \n";
        			system( $opt_u." ".$opt_S."/".$f );
        			$f =~ s/.gz$//;
        		}

        		if( $CONFIG{IS_REMOTE} eq "n" ) {
        			copy($opt_S."/".$f, $opt_T."/".$f) or warn "Cant Copy $f : $!\n";
        		} else {
        			my($tmpfile);

        			if( defined $CONFIG{XFER_SCRATCH_DIR}
					and -d $CONFIG{XFER_SCRATCH_DIR} ) {
        				if( ! -w  $CONFIG{XFER_SCRATCH_DIR} ) {
        					logdie("ERROR: Scratch Dir Not Writable $CONFIG{XFER_SCRATCH_DIR}\n");
        				}
        				$tmpfile="$CONFIG{XFER_SCRATCH_DIR}/$f";
        				print "Scratch File $tmpfile\n";
        			} elsif( defined $CONFIG{XFER_SCRATCH_DIR} ) {
        					logdie("ERROR: Scratch Dir Does Not Exist $CONFIG{XFER_SCRATCH_DIR}\n");
        			} else {
	        			$tmpfile = "/tmp/$f" if -d "/tmp";
						$tmpfile = "$f" if ! $tmpfile;
					}

				if( $opt_f ) {
					$ftpsrc->close();
					$ftpsrc= Net::myFTP->new($opt_h,METHOD=>$hmethod,%config)
         					or logdie "Failed To Create FTP Connection to $opt_h : $@";
					my($rc,$err)=$ftpsrc->login($hlog,$hpass);
					logdie( "Cant FTP To host ($opt_h) As $hlog : $err \n") unless $rc;
					$ftpsrc->binary();
				}

        			#$ftpsrc->cwd($opt_S);	# for reconnect
        			my($rc)=$ftpsrc->get($opt_S."/".$f,$tmpfile);
        			#print "Fetched $rc\n";
        			if( -r $tmpfile ) {
        				if( $opt_f ) {	# explicit reconnect on full dumps
        					$ftptgt->close();
						$ftptgt= Net::myFTP->new($opt_t,METHOD=>$tmethod,%config)
        						or logdie "Failed To Create FTP Connection to $opt_t : $@";
						my($rc,$err)=$ftptgt->login($tlog,$tpass);
						logdie( "Cant FTP To host ($opt_t) As $tlog : $err \n") unless $rc;
						$ftptgt->binary();
					}
					$ftptgt->cwd($opt_T);	# for reconnect
        				$rc=$ftptgt->put($tmpfile,$opt_T."/".$f);
        				#print "Put $rc\n";
        				unlink $tmpfile;
        			} else {
        				print "File $tmpfile Not Found After Copy\n";
        				warn  "File $tmpfile Not Found After Copy\n";
        			}
        		}
        	}
        }

        if( defined $opt_U and ! $opt_f ) {
        	foreach ( @targetfiles ) {
        		next unless /\.done$/ or /\.done\./;
        		s/.done//;
        		print "Renaming $_\n";
        		$ftptgt->rename($opt_T."/".$_.".done",$opt_T."/".$_)
        			or print "\nWarning $!\n";
        	}
        }

        if( $opt_f ) {
        	die "No Target Server identified" unless $CONFIG{XFER_TO_SERVER};
        	my($login,$password)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>'sybase');
        	($login,$password)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>'sqlsvr')
        		unless $login and $password;
        	die "No password for  $CONFIG{XFER_TO_SERVER}\n" unless $login and $password;
        	my($full_dump_file_name);
        	foreach ( sort @sourcefiles )  {	# get the latest - should sort correctly
			$full_dump_file_name=$CONFIG{XFER_TO_DIR_BY_TARGET}."/".$_;
		}
        	die "No Full Dump name\n" unless $full_dump_file_name;
			print "No Databases Selected - Not Loading\n" if $dbname eq "";
			next if $dbname eq "";
        	die "No DBNAME\n" unless $dbname;

        	my($cmd)="load_database.pl -U$login -S".$CONFIG{XFER_TO_SERVER}." -D".$dbname." -i".$full_dump_file_name." -O ";

        	print $cmd." -Pxxx\n";
        	$cmd .= " -d " if $opt_d;
        	$cmd .= " -P$password |";
        	open( LD, $cmd) or die "Cant run $cmd\n";
        	foreach( <LD> ) {
        		chomp;
        		print $_,"\n";
        	}
        	close(LD);

        }

        print "fix_logship.pl: Completed Database $dbname\n";
}
print "fix_logship.pl: Program Completed at ".localtime(time)."\n";
MlpBatchDone(-monitor_program=>'FixLogship', -system=>$opt_J);

exit(0);

sub get_files
{
	my($ftpcon,$rootdir,$db)=@_;
	print "get_files(dir=$rootdir,db=$db)\n" if defined $opt_d;
	my(@dirlist);

	info "Grabbing The File List $rootdir\n";

	$ftpcon->cwd($rootdir);
	my(@files)=$ftpcon->ls();
        #opendir(DIR,$rootdir) or die("Can't open directory $rootdir : $!\n");
	#my(@files)=grep(!/^\./,readdir(DIR));
	#closedir(DIR);

	#if( defined $opt_F ) {
		#print "filter all files must have $opt_F\n";
		#@dirlist = grep(/$opt_F/,@files);
	if( $opt_f ) {
		print 'Filtering for dump_\d+.dmp or dbdump.\d+.\d+\n' if $opt_d;
		@dirlist = grep( (/dump_\d+.dmp/i or /_db_\d+.BAK$/i or /_db_\d+\.dump/ or /dbdump.\d+.\d+/), @files);
		if( defined $db and $db !~ /^\s*$/) {
			print "db filter all files must have = $db.dbdump.\n";
			@dirlist = grep((/\.$db\./ or /^${db}_/),@dirlist);
		}

	} else {
		print 'Filtering for /tlog_\d+.trn/i or /logdump.\d+.\d+/\n' if $opt_d;
		@dirlist = grep( (/tlog_\d+.trn/i or /logdump.\d+.\d+/), @files);
		if( defined $db and $db !~ /^\s*$/) {
			print "db filter all files must have = $db.logdump.\n";
			@dirlist = grep((/\.$db\./ or /^${db}_/),@dirlist);
		}
	}

	return @dirlist;
}


__END__

=head1 NAME

fix_logship.pl - sync two directories

=head2 USAGE

fix_logship.pl -Jjob -Ssourcedir -hsourceUnixHostname -Ttargetdir -ttargetUnixHostname -Ddatabase -UN -u/usr/bin/gunzip

 -f full dumps resync (will load dumps too)
 -y since yesterday (24 hours)
 -s since time - yyyymmdd.hhmmss
 -p purge target only files
 -N nocopy - just rename .done extensions (must pass -U)
 -U is used to move .done to unnamed extensions\n";
 -P print differences
 -u uncompress program for source side
 -D database name - comma separated list

 -F filespec - match patern for files.  Otherwise the following will be used
	.$db.logdump.\d+.\d+	if database name is passed
	logdump.\d+.\d+		if no database is passed

where \d+ is the perl string for a set of digits

=head2 SYNOPSIS

Works on both unix and windows.  Will sync two directories.  By default it only copies from the source to
the target (no removes on target) but with full dumps or if -p is called it will purge the target too.

The only caveat of this program is that the way it copies files is to use a temporary local directory.  This
means that if you running from machine a to copy SYBA to SYBA_DR, it copies SYBA->a and then a->SYBA_DR.  Two
copies.  Might be slow with large databases.  Recommend that you run locally (on either SYBA or SYBA_DR) if
your databases are larger than a 100 gigs or so.

=cut
