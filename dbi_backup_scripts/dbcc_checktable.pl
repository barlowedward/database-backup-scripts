#!/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use Getopt::Std;
use DBIFunc;
use File::Basename;
use vars qw($opt_M $opt_F $opt_i $opt_s $opt_U $opt_S $opt_P $opt_D $opt_e $opt_E $opt_l $opt_d $opt_h $opt_T $opt_D);
use CommonFunc;
use Do_Time;


sub usage
{
   print @_;
   print "USAGE: $0 -UUSER -SSERVER -PPASS -FFILE -TMINUTES -DDATABASE_LIST -EESTIMATEFILE

If you pass a database list it will create the file listing in FILE.
MINUTES is the number of minutes to run before stopping (estimate).
ESTIMATEFILE is optional parameter - to keep track of time estimates.";
   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless getopts('M:sU:D:S:P:l:e:dhiF:T:D:E:');


my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

die usage("Must pass sa password\n" )  unless defined $opt_P;
die usage("Must pass server\n")			unless defined $opt_S;
die usage("Must pass sa username\n" )  unless defined $opt_U;

die "FATAL ERROR: Must pass -F " unless $opt_F;
die "FATAL ERROR: FILE $opt_F does not exist" if ! -r $opt_F and ! defined $opt_D;
die "FATAL ERROR: Must pass -T " unless $opt_T;

if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}

#
# CONNECT TO DB
#
print "Connecting to server",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-lsybaseogin=>$opt_U,-password=>$opt_P,-debug=>$opt_d,-type=>$opt_M)
	or die "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($server_type)="SQLSERVER";
foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
	($server_type)=dbi_decode_row($_);
}
info("Server Type=$server_type\n");

# We Parse $opt_D and Create Ordered List of db,object,size ordered by size
# across all your passed in databases.
if( defined $opt_D ) {
	print "Creating File $opt_F\n";
my($query)="select db=db_name(),
	obj=object_name(id),
	size=sum(reserved_pgs(i.id,i.doampg)+reserved_pgs(i.id,i.ioampg))
into #tmp
from sysindexes i having 1=2";
	dbi_query(-db=>"master",-query=>$query, -no_results=>1);

	$opt_D=~s/\s//g;

	my(@dbs)=split(/,/,$opt_D);

	$query="insert #tmp
select db_name(),object_name(id), sum(reserved_pgs(i.id,i.doampg)+reserved_pgs(i.id,i.ioampg))
from sysindexes i
where id>=100
group by id";
	foreach (@dbs) {
		dbi_query(-db=>$_,-query=>$query, -no_results=>1);
	}
	$query="select * from #tmp order by size";
	my(@rc)=dbi_query(-db=>"master",-query=>$query);
	open( OUTFILE,">".$opt_F) or die "Cant write $opt_F : $!\n";
	foreach (@rc) {
		my(@x)=dbi_decode_row($_);
		print OUTFILE join("\t",@x),"\n";
	}
	close(OUTFILE);
}

my(@data);
open( IN,$opt_F) or die "Cant read $opt_F : $!\n";
while(<IN>) { chop; push @data, $_; }
close(IN);

my(%time_estimate_pg,%time_estimate_sec);
if( defined $opt_E and -r $opt_E ) {
	open( IN,$opt_F) or die "Cant read $opt_F : $!\n";
	while(<IN>) { chop; my($maxpg,$numpg,$seconds)=split; $time_estimate_pg{$maxpg}=$numpg; $time_estimate_sec{$maxpg}=$seconds; }
	close(IN);
} else {
	for( 10,100,1000,10000,100000,1000000,10000000,100000000,1000000000,10000000000,100000000000 ) {
		$time_estimate_pg{$_}=0;
		$time_estimate_sec{$_}=0;
	}
}
my($lineno)=0;
my($total_secs_taken,$total_pgs_done)=(time,0);
my(@realout);
my($current_est_max)=10;
foreach(@data) {
	$lineno++;
	my($db,$tbl,$pages) = split;
	my($t)=time;

	# FIGURE OUT IF YOU CAN FINISH IN TIME
	my($est_secs);
	if( $total_secs_taken != $t ) {
		my($est_rate)= $total_pgs_done / ($t-$total_secs_taken);	# pages/sec
		$est_secs = $pages / $est_rate;
		if( time+$est_secs > $opt_T*60+$total_secs_taken )  {
			print "This program has run to completion - Allowed time $opt_T minutes\n";
			last;
		}
	}
	if( $pages>$current_est_max ) {
		$current_est_max*=10;
	}

	my($est2_secs);
	if( $time_estimate_sec{$current_est_max} > 0 ) {
		$est2_secs = $pages * $time_estimate_sec{$current_est_max} /$time_estimate_pg{$current_est_max};
	}

	print $lineno.") ".do_time(-fmt=>'hh:mi:ss')." object=$db..$tbl pages=$pages est=".int($est_secs)." or ".int($est2_secs)." secs\n";

	my($query)="dbcc checktable($tbl)";
	my(@rc)=dbi_query(-db=>$db,-query=>$query, -no_results=>1);
	foreach ( @rc ) {
		my($x)=join(" ",dbi_decode_row($_));
		next if $x=~/^2536/ or $x=~/^2579/ or $x=~/^7929/ or $x=~/^7928/ or $x=~/^2528/ or $x=~/^9909/
				or $x=~/^12907/;
		chomp $x;
		push @realout,"obj=$tbl $x";
		print $x,"\n";
	}
	$time_estimate_pg{$current_est_max}+=$pages;
	$time_estimate_sec{$current_est_max}+= (time-$t);
	$total_pgs_done += $pages;
	print "-- done $pages pages in ".(time-$t)." seconds (total pages=$total_pgs_done in ".(time-$total_secs_taken)." secs)\n";
}

print "-- FINAL OUTPUT --\n";
foreach (@realout) {
	print $_,"\n";
}

if( defined $opt_E  ) {
	open(E,">".$opt_E) or die "cant write $opt_E : $!\n";
	foreach( sort keys %time_estimate_pg ) {
		print E $_," ",$time_estimate_pg{$_}," ",$time_estimate_sec{$_},"\n"
	}
	close(E);
}

dbi_disconnect();
exit(0);

__END__

=head1 NAME

dbcc_checktable.pl - dbcc for larger databases

=head2 AUTHOR

By Ed Barlow

=head2 DESCRIPTION

An inteligent dbcc that works on table sizes and tries to keep running only for a specified
amount of minutes.  Note that DBCC does not stop if it might go over that limit, so we run until
the total time taken exceeds the MINUTES passed in.

If you pass a database list it will create the file listing in FILE.  This consists of database,
object, and pages.  This list is run through sequentially.  The system calculates an estimate of
the number of seconds that the table will take.  If time_taken + estimate > minutes_parameter, then
the dbcc checktable will not run.

If you pass in an ESTIMATEFILE as an optional parameter - better statistics are automatically created
and managed.  Default statistics are based on total pages processed and total time taken *in this run*.
Better statistics (from the ESTIMATEFILE) are based on the historical average of rows per second for
a single table and grouped into categories based on table size.  The default estimate will be conservative
because there is overhead factored in.  The better statistics are required if you wish to work on
files other than in straight linear order - smallest to fastest.

Say you did the first 500 tables (smallest) on one saturday. The use of estimated statistics could allow you to do
the next 40 (largest) on the following saturday.  You could not do this safely with default stats.

=head2 USAGE

USAGE: dbcc_checktable.pl -UUSER -SSERVER -PPASS -FFILE -TMINUTES -DDATABASE_LIST -EESTIMATEFILE

If you pass a database list it will create the file listing in FILE.
MINUTES is the number of minutes to run before stopping (estimate).
ESTIMATEFILE is optional parameter - to keep track of time estimates.

=cut

