#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use vars qw($opt_d $opt_U $opt_S $opt_P $opt_o $opt_h $opt_Q $opt_l $opt_n $opt_e $opt_b %CONFIG $opt_v $opt_B $opt_R);

use strict;
use Do_Time;
use Getopt::Std;
use Logger;
use CommonFunc;
use DBIFunc;
use File::Basename;
use MlpAlarm;
use Repository;

sub usage
{
	print(@_);
	print("\nconfig_report.pl -USA_USER -SSERVER -PSA_PASS -oFILE -BBatchDesignator -Rsybase|sqlsvr
      -o FILE       (output file)
      -Q make the output file be in standard location
      -R run on all available servers sybase|sqlsvr|all
                will skip if a current audit file exists
      -n no optdiag output
      -h            (output in html format)
      -b bcp out system tables too (into outfile.tablename.bcp)
      -e errorlog   (optional)
      -l sessionlog (optional)
      -v versions to keep (default=7)

   Creates a configuration report for a server that can be used
   to recreate that server from scratch.  \n");
	return "\n";
}

$| =1;
MlpAgentStart( -monitor_program=>$opt_B ) if $opt_B;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";
chdir "C:/" if is_nt();

die(usage("")) if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die(usage("Bad Parameter List")) unless getopts('U:S:P:o:he:l:Qbdv:B:R:n');
if( ! $opt_R ) {
	die(usage("Must pass sa password\n"))  unless defined $opt_P;
	die(usage("Must pass server\n")    )   unless defined $opt_S;
	die(usage("Must pass sa username\n"))  unless defined $opt_U;
	die(usage("Must pass -o if -b is passed\n")) if defined $opt_b and ! defined $opt_o;
} else {
	die "Cant pass -o outfile if you pass -Rtype\n" if $opt_o and ! $opt_S;
}

$opt_v=7 unless $opt_v;

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            # -database=> undef,
            -debug =>   $opt_d,
            # -system =>  undef,
            # -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

my($STHEAD,$ENDHEAD)=("","\n");
($STHEAD,$ENDHEAD)=("<H2>","</H2>\n") if defined $opt_h;
if( $opt_Q and $opt_S and ! $opt_o ) {
	my($rootdir)= get_gem_root_dir()."/data/BACKUP_LOGS/";
	mkdir($rootdir."$opt_S",0777) unless -d $rootdir."$opt_S";
	mkdir($rootdir."$opt_S/audits",0777) unless -d $rootdir."$opt_S/audits";
	$opt_o=$rootdir."$opt_S/audits/$opt_S.".today();
}

if( defined $opt_S and ! $opt_U and ! $opt_P ) {
	($opt_U,$opt_P)=get_password(-type=>'sqlsvr',-name=>$opt_S) unless $opt_U;
	($opt_U,$opt_P)=get_password(-type=>'sybase',-name=>$opt_S) unless $opt_U;
	run_an_audit($opt_S,$opt_U,$opt_P);
} elsif( $opt_R ) {
	my($rootdir)= get_gem_root_dir()."/data/BACKUP_LOGS/";
	die "Root directory for backupscripts is $rootdir and doenst exist\n" unless -d $rootdir;
	if( is_nt() and ($opt_R eq "sqlsvr" or $opt_R eq "all")) {
		foreach my $srv (get_password(-type=>'sqlsvr')) {
			mkdir($rootdir."$srv",0777) unless -d $rootdir."$srv";
			mkdir($rootdir."$srv/audits",0777) unless -d $rootdir."$srv/audits";
			$opt_o=$rootdir."$srv/audits/$srv.".today();
			my($login,$password)=get_password(-type=>'sqlsvr',-name=>$srv);
			run_an_audit($srv,$login,$password);
		}
	}
	if( $opt_R eq "sybase" or $opt_R eq "all" ) {
		foreach my $srv (get_password(-type=>'sybase')) {
			mkdir($rootdir."$srv",0777) unless -d $rootdir."$srv";
			mkdir($rootdir."$srv/audits",0777) unless -d $rootdir."$srv/audits";
			$opt_o=$rootdir."$srv/audits/$srv.".today();
			my($login,$password)=get_password(-type=>'sybase',-name=>$srv);
			#print "Auditing $srv/$login/$password/$opt_o\n";
			#next;
			run_an_audit($srv,$login,$password);
		}
	}
} else {
	run_an_audit($opt_S,$opt_U,$opt_P);
}
MlpAgentDone() if $opt_B;
exit(0);

sub run_an_audit {
	my($server,$login,$password)=@_;
	if( ! $server ) {
		warn "Must pass server to audit server\n";
		return;
	}
	if( ! $password ) {
		warn "Must pass password to audit server $server\n";
		return;
	}
	info("Starting Audit of Server $server\n");

#print "DBG DBG Start dbi_connect\n";
	my($rc)=dbi_connect(-srv=>$server,-login=>$login,-password=>$password,-debug=>$opt_d,-die_on_error=>0);
	#print "DBG DBG End dbi_connect $rc\n";
	if( ! $rc ) {
		print("Cant connect to $server as $login\n");
		return;
	}
	dbi_set_mode("INLINE");

	#my($SERVER_TYPE,$VERSION)=dbi_db_version();

	# GET DB INFO
	my($server_type,@databaselist)=dbi_parse_opt_D("%",1);
	print "DB LIST=",join(" ",@databaselist),"\n" if $opt_d;

	my(%alldb);
	foreach ( dbi_query(-db=>"master",-query=>"select name from sysdatabases",-debug=>$opt_d) ) {
		my($nm)=dbi_decode_row($_);
		$alldb{$nm}="BAD";
	}
	foreach(@databaselist) { $alldb{$_}="OK"; }

	# ok i guess we can log in and use databases.... so... start output file
	if( defined $opt_o ) {
		archive_file($opt_o, $opt_v);
		my($rc)=open(OUTFILE,">$opt_o");
		if( ! $rc ) { print("Cant open $opt_o: $!\n"); return; }
		info("Server $server Output Will Be Saved\n\tInto Audit File=$opt_o\n");
	}

	output( "<H2>" ) if defined $opt_h;
	output( "$server_type SERVER CONFIGURATION REPORT" );
	output( "</H2>" ) if defined $opt_h;
	output( "\n" );
	output( "Created by: config_report.pl ".$NL );
	output( "Server: $server",$NL );
	output( "Server Type=$server_type" ,$NL);
	#output( "Server Version=$VERSION",$NL);
	output( "Report Run At: ".localtime().$NL );
	output( "Output File $opt_o",$NL ) if defined $opt_o;
	output( $NL );
	dbi_msg_ok(1000);

	pr_query("master","select 'version='+\@\@version","$server_type VERSION");

	pr_query("master","exec sp__helpdb \@dont_format=\'Y\'","DATABASE LAYOUT");
	#dbi_msg_exclude(1000);

	pr_query("master","exec sp__configure","CONFIGURATION OPTIONS");
	pr_query("master","exec sp__helpmirror","MIRRORING INFORMATION")
		if $server_type eq "SYBASE";
	pr_query("master","exec sp__vdevno","VIRTUAL DEVICE LAYOUT")
		unless $server_type eq "SQLSERVER";
	pr_query("master","exec sp__diskdevice","DISK LAYOUT");
	pr_query("master","exec sp__dumpdevice","DUMP DEVICE LAYOUT");
	pr_query("master","exec sp__helplogin \@filter=\"A\"","SPECIAL SYSTEM LOGINS")
		if $server_type eq "SYBASE";
	pr_query("master","exec sp__helplogin \@dont_format='Y'","SYSTEM LOGINS")
		if $server_type eq "SQLSERVER";

	line();
	output( $STHEAD,"REVERSE ENGINEERING PROCEDURES",$ENDHEAD  );
	line();
	pr_query("master","exec sp__revdb");
	pr_query("master","exec sp__revdevice");
	pr_query("master","exec sp__revmirror")
		unless $server_type eq "SQLSERVER";
	pr_query("master","exec sp__revlogin");
	pr_query("master","exec sp__revrole")
		unless $server_type eq "SQLSERVER";

	foreach my $db (@databaselist) {
		if( ! dbi_use_database($db) ) {
			warn("Cant Use Database $db on $server\n");
			next;
		}
		pr_query($db,"exec sp__revsegment") if $server_type eq "SYBASE";
		pr_query($db,"exec sp__revgroup") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revuser") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revalias") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revtype") 	if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revbindings") if $server_type eq "SYBASE";;
		pr_query($db,"exec sp__revrule") 	if $server_type eq "SYBASE";;
	}

	#
	# PRINT TABLES
	#
	line();
	output( $STHEAD,"SELECTS FROM SYSTEM TABLES",$ENDHEAD  );
	line();
	pr_query("master","select type,id,csid,status,name,description from syscharsets");
	pr_query("master","select * from sysconfigures");
	if( 	$server_type eq "SQLSERVER" ) {
		pr_query("master","select name,dbid,mode,status,status2,crdate,reserved,category,cmptlevel,filename,version from sysdatabases");
	} else {
		pr_query("master","select * from sysdatabases");
	}

	pr_query("master","select * from sysdevices");

	if( 	$server_type eq "SQLSERVER" ) {
		pr_query("master","select STATUS,CREATEDATE,UPDATEDATE,ACCDATE,TOTCPU,TOTIO,SPACELIMIT,TIMELIMIT,RESULTLIMIT,NAME,DBNAME,LANGUAGE,DENYLOGIN,HASACCESS,ISNTNAME,ISNTGROUP,ISNTUSER,SYSADMIN,SECURITYADMIN,SERVERADMIN,SETUPADMIN,PROCESSADMIN,DISKADMIN,DBCREATOR,BULKADMIN,LOGINNAME from syslogins");
	} else {
		pr_query("master","select * from syslogins");
	}
	pr_query("master","select * from sysloginroles") if $server_type eq "SYBASE";;
	pr_query("master","select * from sysremotelogins");
	pr_query("master","select * from sysservers");
	pr_query("master","select * from sysusages order by vstart") if $server_type eq "SYBASE";
	pr_query("master","select * from sysusages order by dbid, lstart") if $server_type eq "SYBASE";

	if( defined $opt_b ) {
		bcp_table("master","syscharsets",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","sysconfigures",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","sysdatabases",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","sysdevices",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","syslogins",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","sysloginroles",$opt_o,$server,$login,$password,$server_type) if $server_type eq "SYBASE";
		bcp_table("master","sysremotelogins",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","sysservers",$opt_o,$server,$login,$password,$server_type);
		bcp_table("master","sysusages",$opt_o,$server,$login,$password,$server_type) if $server_type eq "SYBASE";
	}

	my($rc1151)=dbi_query(-db=>"master",-query=>"select name from sysobjects where name='systimeranges'",-debug=>$opt_d);
	my(@is_1151)=dbi_decode_row($rc1151);
	if( $#is_1151>=0 ) {
		pr_query("master","select * from sysresourcelimits") if $server_type eq "SYBASE";
		pr_query("master","select * from systimeranges ") if $server_type eq "SYBASE";
		bcp_table("master","sysresourcelimits",$opt_o,$server,$login,$password,$server_type) 	if defined $opt_b and $server_type eq "SYBASE";
		bcp_table("master","systimeranges",$opt_o,$server,$login,$password,$server_type) 		if defined $opt_b and $server_type eq "SYBASE";
	}
	dbi_disconnect();

	if( defined $opt_o and ! defined $opt_n and $server_type eq "SYBASE" ) {
		#my($d)=dirname($opt_o);
		#my($b)=basename($opt_o);
		foreach my $db (@databaselist) {
			next if $db eq "master" or $db eq "model" or $db eq "tempdb"  or $db eq "sybsystemdb";
			my($f)=$opt_o.".OPTDIAG.".$db;
			print "**> OPTDIAG OUTPUT TO $f\n";
			output( "**> OPTDIAG OUTPUT TO $f\n" );
			archive_file($f, $opt_v);
			my($cmd)="optdiag statistics $db -o$f -U$login -S$server -P$password";
			my($cmdnopass)="optdiag statistics $db -o$f -U$login -S$server -PXXX";
			output( "running $cmdnopass\n" );
			print $cmdnopass."\n";
			open( CMD, $cmd." |") or warn "Cant run $cmdnopass : $!\n";
			foreach (<CMD>) {
				print $_,"\n";
			}
			close(CMD);
		}
	} else {
		output("Not running optdiag as server type != SYBASE ($server_type)\n")
			if $server_type ne "SYBASE";
		output("Not running optdiag as no output file was passed in with $opt_o\n")
			unless $opt_o;
	}

	close(OUTFILE) if $opt_o;
	info("Audit of $server Completed\n");
}

sub line
{
	if( defined $opt_h ) {
		# output( "<hr>\n" );
	} else {
		output( "---------------------------------------------\n");
	}
}
my($bcp);
my($loginpass);

sub bcp_table
{
	my($db,$table,$outfile,$server,$login,$password,$server_type)=@_;

	# modify $opt_o for filename
	if( ! defined $bcp ) {
		if( $server_type eq "SYBASE" ) {
			$bcp=$ENV{SYBASE}."/ASE-15_0/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/ASE-15_0/bin/bcp";
			$bcp=$ENV{SYBASE}."/OCS-15_0/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/OCS-15_0/bin/bcp";
			$bcp=$ENV{SYBASE}."/ASE-12_5/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/ASE-12_5/bin/bcp";
			$bcp=$ENV{SYBASE}."/OCS-12_5/bin/bcp" if ! $bcp and  -x $ENV{SYBASE}."/OCS-12_5/bin/bcp";
			$bcp=$ENV{SYBASE}."/bin/bcp" if ! $bcp and -x $ENV{SYBASE}."/bin/bcp";

			# we could be on nt
			if( ! $bcp ) {
				$bcp=$ENV{SYBASE}."/ASE-15_0/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/ASE-15_0/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/OCS-15_0/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/OCS-15_0/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/ASE-12_5/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/ASE-12_5/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/OCS-12_5/bin/bcp.exe" if ! $bcp and  -x $ENV{SYBASE}."/OCS-12_5/bin/bcp.exe";
				$bcp=$ENV{SYBASE}."/bin/bcp.exe" if ! $bcp and -x $ENV{SYBASE}."/bin/bcp.exe";
			}

			if( $bcp eq "" and -d $ENV{SYBASE} ) {
				opendir(DIR,$ENV{SYBASE})
					or die("Can't open directory (\$SYBASE) $ENV{SYBASE} : $!\n");
				my(@dirlist) = grep((!/^\./ and -d "$ENV{SYBASE}/$_") ,readdir(DIR));
				closedir(DIR);
				foreach ( @dirlist ) {
					if( -x "$_/bin/bcp" ) {
						$bcp = "$_/bin/bcp";
						last;
					}
				}
			}
			$loginpass="-U$login -PXXX";
		} else {
			foreach ( split(/;/,$ENV{PATH} )) {
				print "TESTING PATH $_\n" if $opt_d;
				next unless -d $_;
				if( -e "$_/osql.exe" ) {
					print "FOUND OSQL in $_\n" if $opt_d;
					$bcp="./bcp.exe";
					#$bcp=~s.\\.\/.g;
					my($rc)=chdir($_);
					if( ! $rc ) {
						warn "Cant chdir $_ $!\n";
						next;
					}
					last;
				}
			}
			$loginpass="-T ";
		}
	}

	if( ! defined $bcp ) {
		output("BCP NOT FOUND - CANT COPY TABLE $table\n");
		return;
	}

	my($filename)=$opt_o.".".$table."_native.bcp";
	my($cmd)="$bcp $db..$table out $filename -n  -S$server $loginpass\n";
	print "Copying Table $table to File $filename (native mode)\n";
	output("Copying Table $table to File $filename (native mode)\n");
	output(" == $cmd\n");
	print $cmd;
	$cmd=~s/PXXX\n/P$password\n/;
	system($cmd);

	my($filename)=$opt_o.".".$table."_char.bcp";
	my($cmd)="$bcp $db..$table out $filename -c -t~ -S$server $loginpass\n";
	print "Copying Table $table to File $filename (char mode)\n";
	output("Copying Table $table to File $filename (char mode)\n");
	output(" == $cmd\n");
	print $cmd;
	$cmd=~s/PXXX\n/P$password\n/;
	system($cmd);
}

sub pr_query
{
	my($db,$query,$title)=@_;
	my(@results)= dbi_query(-db=>$db,-query=>$query,-print_hdr=>1,-add_batchid=>1,-debug=>$opt_d);
	output("$title: No results for query $query\n") if $#results<0;
	return if $#results<0;
	print "Query returned $#results Rows\n" if defined $opt_d;

	line();
	if( defined $title ) {
		$title=~s/\s+$//;
		output( $STHEAD,"$title",$ENDHEAD );
	} else {
		output( $STHEAD,"QUERY: $query IN DATABASE $db",$ENDHEAD );
	}
	line();

	# my(@rc);
	# foreach (@results) {
		# my(@dat)=dbi_decode_row($_);
		# $dat=~s/\s+\~\~/\~\~/g;
		# $dat=~s/\s+$//g;
		# push @rc,$dat;
	# }

	my(@widths)=(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
	#foreach my $dat ( @rc ) {
	foreach my $dat ( @results ) {
		# next unless $dat =~ /^\s*BATCH/;
		my @rcdat=dbi_decode_row($dat);
		shift @rcdat;
		my $count=0;
		foreach ( @rcdat ) {
			$widths[$count] = length($_) if $widths[$count] < length($_);
			$count++;
		}
	}

	my($fmt)="";
	foreach (@widths) {
		last if $_ == 0;
		$fmt.="%-".$_."s ";
	}

	$fmt.="\n";
	output( "<PRE><b>" ) if defined $opt_h;
	my($rowcnt)=0;
	foreach( @results ) {
		$rowcnt++;
		my @rcdat=dbi_decode_row($_);
		if( $rowcnt==1 ) {
			foreach(@rcdat){
				$_=uc;
			}
		}
		if( $#rcdat==0 ) {
			chomp $rcdat[0];
			print OUTFILE $rcdat[0],"\n" if defined $opt_o;
			info($rcdat[0],"\n") unless defined $opt_o;
			if( $rowcnt==1 ) {
				print "</b>" if defined $opt_h;
			}
			next;
		}
		shift @rcdat;
		printf OUTFILE ($fmt,@rcdat) if defined $opt_o;
		infof($fmt,@rcdat) unless defined $opt_o;
		if( $rowcnt==1 ) {
			print "</b>" if defined $opt_h;
		}

		#chomp;
		#if( /^\s*BATCH/ ) {
			#s/^\s*BATCH\s+\d+//;
			## next if /^\s*$/;
			#my(@rcdat)=dbi_decode_row($_);
			#shift @rcdat;
			#next if $rcdat[0] =~ /^txt/;
			#printf OUTFILE ($fmt,@rcdat) if defined $opt_o;
			#infof($fmt,@rcdat) unless defined $opt_o;
		#} else {
			## next if /^\s*$/;
			#next if /^txt/;
			#output( $_,"\n");
		#}
	}
	output( "</PRE>" ) if defined $opt_h;
	output("\n");
}

sub output
{
	if( defined $opt_o ) {
		print OUTFILE @_ ;
	} else {
		info(@_);
	}
}

sub archive_file {
	my($from, $versions)=@_;
	return unless -r $from;
	my($curdate)=do_time(-fmt=>'yyyymmdd_hhmiss');
	my($tofile)=$from.".".$curdate;
	print("Archived $from\n" );
	print("      to $tofile\n" ) if defined $opt_d;
	rename( $from , $tofile ) or die "Cant rename $from to $tofile : $!\n";

	my($dir)=dirname($from);
	my($file)=basename($from);
	opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
	my(@dirlist) = sort grep(/^$file\.\d\d\d\d\d\d\d\d/,readdir(DIR));
	closedir(DIR);

	if( $#dirlist>$versions ) {
		$#dirlist-=$versions;
		foreach (sort @dirlist) {
			print("Removing $dir/$_\n" );
			unlink $dir."/".$_;
		}
	}
	return $tofile;
}

__END__

=head1 NAME

config_report.pl - Server Configuration Report

=head2 SYNOPSIS

Creates a configuration report for a server that can be used to recreate that server from scratch.

=head2 AUTHOR

By Ed Barlow

=head2 USAGE

config_report.pl -USA_USER -SSERVER -PSA_PASS -oFILE -BBatchDesignator -Rsybase|sqlsvr
      -o FILE       (output file)
      -Q make the output file be in standard location
      -R run on all available servers sybase|sqlsvr|all
                will skip if a current audit file exists
      -n no optdiag output
      -h            (output in html format)
      -b bcp out system tables too (into outfile.tablename.bcp)
      -e errorlog   (optional)
      -l sessionlog (optional)
      -v versions to keep (default=7)

   Creates a configuration report for a server that can be used
   to recreate that server from scratch.

=head2 DESCRIPTION

This program creates a configuration report for a server that can be used to recreate that server from scratch.
This is intended as documentation for use by the system administrator in case your server crashes or has a problem.
This program should produce virtually everything about your server that you might care about except user object DDL.

Requires: Requires the extended stored procedure library

=head2 WHAT IT PRINTS
	SERVER:  srvname
	@@version
	helpdb
	configure
	helpmirror
	vdevno
	helpdevice
	helplogin
	helpuser by db
	all the reverse engineering routines

=cut
