#!/apps/sybmon/perl/bin/perl

# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(logdie infof info warning alert);
use strict;
use Getopt::Std;
use File::Basename;
use DBIFunc;
use CommonFunc;
use Time::Local;
use Logger;

use vars qw($opt_J $opt_l $opt_e $opt_Y $opt_n $opt_v $opt_U $opt_S $opt_P $opt_D $opt_d $opt_i
$opt_h $opt_t $opt_r %CONFIG $opt_X $opt_M $opt_O $opt_z $opt_s $opt_k);
my($log_msgs_to_stdout);
my($VERSION)="v2.15";

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed
my(@month)=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

sub usage
{
	print @_;
   print "load_database.pl -nnum_stripes -USA_USER -SSERVER
                           -PSA_PASS -Ddb -ifile_root -tdhk -MType

   -n Num stripes (.SX appendended to dump name)
   -v Num         use devices: Num dumps per subdirectory (device_#)
   -D DB_NAME     (may NOT contain wildcards)
   -i DIR         (input file root directory)
   -k 				dont kill users
   -M servertype  Sybase or ODBC
   -t             (Transaction Log only)
   -d             (debug mode - for dbi debugging use -z)
   -h             (html output)
   -r extension   (rename file extension (dbdump part of name) when done)
   -e errorlog    (optional)
   -l sessionlog  (optional)
	-s silent mode (only print summary)
   -X             dumps are compressed using internal sybase compression
   -Y             force std output prints (wont print to stdout if cron job)
   -O             no online database needed

   Load a database from the specified file in with the name
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss[.S?]
   where .S? represents optional stripe numbers if the backups are striped.
   The Root of the file (specified by -i) is the full name (excluding the
   .S? stripe name).

If -t and no file exactly matches the file_root, ALL files that start with the
root are loaded in order.  If full load, and no file matches the -i root,
the LATEST file starting with the root will be loaded.
   \n";
	return "\n";
}

$| =1;
my($curdir)=cd_home();

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List\n") unless getopts('U:D:S:zP:i:dv:khtn:e:l:r:XYJ:OM:s');

print "load_database.pl called in debug mode\n" if defined $opt_d;
logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  $opt_J,
            -subsystem =>$opt_J,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );
use Sys::Hostname;
info( "load_database.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

print "logging initialized\n" if defined $opt_d;

logdie usage($CONFIG{COMMAND_RUN}."\n"."Must pass sa password\n" )	unless defined $opt_P;
logdie usage($CONFIG{COMMAND_RUN}."\n"."Must pass server\n" )		unless defined $opt_S;
logdie usage($CONFIG{COMMAND_RUN}."\n"."Must pass sa username\n" )   	unless defined $opt_U;
logdie usage($CONFIG{COMMAND_RUN}."\n"."Must pass input root file with -ifname\n" ) 	unless defined $opt_i;

if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}
logdie usage("Must pass server type with -M\n") unless defined $opt_M;
logdie "Must Pass Database With -D"
	unless defined $opt_D;
logdie "Type must be Sybase or ODBC"
	unless $opt_M eq "Sybase" or $opt_M eq "ODBC";

$log_msgs_to_stdout = 1 if defined $opt_Y;

$opt_i=~s/\s+$//;

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

#
# CONNECT
#
info "load_database.pl $VERSION\n";
info "In File Spec: $opt_i\n" if defined $opt_d;
info "Connecting to $opt_M Server $opt_S",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -type=>$opt_M, -debug=>$opt_z) or logdie "Cant connect to $opt_M $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($server_type)="SQLSERVER";
foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
	($server_type)=dbi_decode_row($_);
}
info("Connected to Server type=$server_type\n") if defined $opt_d;

my($db)=$opt_D;
if( defined $opt_t ) {
	info "Checking that a FULL DB or TRAN DB load is not happening\n" unless defined $opt_s;
	my($count)=0;
	while( $count<10) {
		my($st)="OK";
		info "Checking for a running load",$NL if defined $opt_d;
		foreach( dbi_query(-db=>"master",-query=>"select * from master..sysprocesses where cmd in ( 'LOAD DATABASE', 'LOAD TRANSACTION' ) and dbid=db_id('".$db."')") ) {
			$st="LOADING";
			info "Database is currently undergoing load - sleeping 10 seconds\n";
			sleep(10);
		}
		$count++;
		last if $st eq "OK";
	}
	logdie "ERROR: Can not load tran logs... Database Undergoing Full Load\n" if $count>=10;
}

info "Killing Possible Users in $db (server $opt_S)\n" if defined $opt_d;
my($num_killed)=kill_users_in_db($opt_D);
my($count)=0;
while( $count<5 and $num_killed>0 and ! defined $opt_k ) {
	sleep 2;
	$count++;
	$num_killed=kill_users_in_db($opt_D)
}
info "Number of killed processes=$num_killed",$NL if defined $opt_d;

if( defined $opt_k and $num_killed>0 ) {
	info "Aborting Load - There is a user in the database (use -k flag to override)\n";
	info "This indicates an active user connection or that multiple loads are running\n";
	exit(1);
}

logdie("ERROR: Cant Kill All Processes in $opt_D\n")
	if $count>=5 and $num_killed>0;

info "<h2>" if defined $opt_h;
if( defined $opt_n and ! defined $opt_t ) {
	info "Loading Database $db ($opt_n stripes)",$NL unless defined $opt_s;
} else {
	info "Loading Database $db",$NL unless defined $opt_s;
}

my($start_time)=time;
info "</h2>" if defined $opt_h;
my($compress_ext)="";
$compress_ext="compress\:\:" if defined $opt_X;
my($command, $formatcmd);
if( defined $opt_t ) {
	if( $server_type eq "SQLSERVER" ) {
		$command="restore transaction $db from DISK=\'".$opt_i."\' with norecovery";
	} else {
		$command="load transaction $db from \'".$compress_ext.$opt_i."\'";
	}
} else {
	if( $server_type eq "SQLSERVER" ) {
		$command="restore database $db from disk=\'".$opt_i."\' with norecovery";
		# next command just lists files in the dump
		$formatcmd="restore filelistonly from disk=\'".$opt_i."\'";
	} elsif( defined $opt_n ) {

		if( $opt_v ) {
				my($b)=basename($opt_i);
				my($d)=dirname($opt_i);
				my($devicenum)=1;
				$command="load database $db\n from \"".$compress_ext.$d."/device_".$devicenum."/".$b.".S1\"\n";
				my($cnt)=2;
				while( $cnt<($opt_n+1) ) {
					$devicenum=int( ($cnt+1) / $opt_v);
					$command.=" stripe on \"".$compress_ext.$d."/device_".$devicenum."/".$b.".S".$cnt."\"\n";
					$cnt++;
				}
		} else {
			$command="load database $db from \"".$compress_ext.$opt_i.".S1\"\n";
			my($cnt)=2;
			while( $cnt<($opt_n+1) ) {
				$command.=" stripe on \"".$compress_ext.$opt_i.".S".$cnt."\"\n";
				$cnt++;
			}
		}
	} else {
		$command="load database $db from \"".$compress_ext.$opt_i."\"";
	}
}

# ok this is lame-ass but if you are restoring to a SQL Server then
# you might be hosed unless you remap.  Check log device matches (ignore
# case), then check if 1 data segment on each, then 1 log each
#
# but... oops... cant get this info if database isnt recovered... so if it
# isnt recovered just skip it...

if( defined $formatcmd ) {
	my(%dfilenm_by_nm, %dfiletyp_by_nm, %dmatched);
	my($num_sfiles, $num_dfiles)=(0,0);

	my($isloading);
	info "Command=>select status&(32+64+128+256+512+1024+4096+32768) from master..sysdatabases where name='$db'",$NL if defined $opt_d;
	foreach( dbi_query(-db=>"master",-query=>"select status&(32+64+128+256+512+1024+4096+32768) from master..sysdatabases where name = '".$db."'") ) {
		($isloading)=dbi_decode_row($_);
		info("The Database Is LOADING!\n") if defined $opt_d;
	}
	die "Woah - no database $db found\n" if ! defined $isloading;

	if( $isloading == 0 ) {

	# use prior defined command to retrieve files in the dump
	info "Command=>$formatcmd",$NL if defined $opt_d;
	foreach( dbi_query(-db=>"master",-query=>$formatcmd) ) {
		my($lnm,$pnm,$ty,$fgnm,$sz,$maxsz)=dbi_decode_row($_);
		info( "Dump: $lnm, $pnm, $ty, $fgnm, $sz, $maxsz \n");
		$dfilenm_by_nm{$lnm}  =  $pnm;
		$dfiletyp_by_nm{$lnm} =  $ty;
		$num_dfiles++;
	}

	my(%sfilenm_by_nm, %sfiletyp_by_nm, %smatched );
	# get the files in the server
	info "Command=>select name,filename,status from sysfiles" if defined $opt_d;
	foreach( dbi_query(-db=>$db,-query=>"select name,filename,status from sysfiles") ) {
		my($nm,$fnm,$stat)=dbi_decode_row($_);
		$nm=~s/\s+$//;
		$fnm=~s/\s+$//;
		$stat=~s/\s+$//;
		info( "Server: $nm, $fnm, $stat,",($stat&0x2),", ",($stat&0x40),"\n" );
		$sfilenm_by_nm{$nm}  =  $fnm;
		$sfiletyp_by_nm{$nm} =  ($stat&0x40);
		$num_sfiles++;
	}

	die "Unable to Map: Server has $num_sfiles files and Dump has $num_dfiles" if $num_sfiles != $num_dfiles;

	my($movecmd)="";
	my(%done);
	foreach my $sfile ( keys %sfilenm_by_nm ) {
		my($match)="";

		foreach ( keys %dfilenm_by_nm ) {
			if( $sfile =~ /^$_$/i ) {
				# they have the same name! check types
				next if $dfiletyp_by_nm{$_} =~ /D/ and $sfilenm_by_nm{$sfile} =~ /.ldf$/;
				next if $dfiletyp_by_nm{$_} =~ /L/ and $sfilenm_by_nm{$sfile} =~ /.mdf$/;

				$match=$_;
				last;
			};
		}

		if( $match ne "" ) {
			# well $sfile and $match match
			$movecmd .= "move '".$match."' to '".$sfilenm_by_nm{$sfile}."',";
			$smatched{$sfile}=1;
			$dmatched{$match}=1;
			$num_dfiles--;
			$num_sfiles--;
			next;
		}
	}

	# no match... see if there is one data/data match or one log/log match
	my($server_data, $server_log) = ("","");
	foreach ( keys %sfilenm_by_nm ) {
		next if defined $smatched{$_};
		my($f)= $sfilenm_by_nm{$_};
		if( $f=~/.mdf$/i ) {
			if( $server_data eq "" ) {
				$server_data=$_;
			} else {
				$server_data="x";
			}
		}
		if( $f=~/.ldf$/i ) {
			if( $server_log eq "" ) {
				$server_log=$_;
			} else {
				$server_log="x";
			}
		}
	}

	my($dump_data, $dump_log) = ("","");
	foreach ( keys %dfilenm_by_nm ) {
		next if defined $dmatched{$_};
		my($f)= $dfilenm_by_nm{$_};
		if( $f=~/.mdf$/i or $dfiletyp_by_nm{$_}=~/D/ ) {
			if( $dump_data eq "" ) {
				$dump_data=$_;
			} else {
				$dump_data="x";
			}
		}
		if( $f=~/.ldf$/i  or $dfiletyp_by_nm{$_}=~/L/ ) {
			if( $dump_log eq "" ) {
				$dump_log=$_;
			} else {
				$dump_log="x";
			}
		}
	}

	if( $dump_data ne "x"
	and $dump_data ne ""
	and $server_data ne "x"
	and $server_data ne "" ) { 				# we can match a dump
			$movecmd .= "move '".$dump_data."' to '".$sfilenm_by_nm{$server_data}."',";
			$smatched{$server_data}=1;
			$dmatched{$dump_data}=1;
			$num_dfiles--;
			$num_sfiles--;
	}

	if( $dump_log ne "x"
	and $dump_log ne ""
	and $server_log ne "x"
	and $server_log ne "" ) { 				# we can match a dump
			$movecmd .= "move '".$dump_log."' to '".$sfilenm_by_nm{$server_log}."',";
			$smatched{$server_log}=1;
			$dmatched{$dump_log}=1;
			$num_dfiles--;
			$num_sfiles--;
	}

	$movecmd =~ s/,$//;
	$movecmd = ", ".$movecmd;
	info( "Movecmd: $movecmd\n" );

	die "Hmmm You Have Not Matched All The Files... "
		unless $num_dfiles==0 and $num_sfiles==0;

	$command.=$movecmd;
	} else {
		info "DB Status $isloading : $db is recovering. Not mapping files\n";
	}
}

info "Command=>$command $NL" 	if defined $opt_d;
info  basename($opt_i) 			if defined $opt_s;
my($load_state) = "UNKNOWN";
my($load_msg)   = "";
dbi_msg_ok(1000);
dbi_msg_ok(911);
foreach( dbi_query(-db=>"master",-query=>$command, -debug=>$opt_d) ) {
	my($msg)=dbi_decode_row($_);
	chomp $msg;
	info( "Dbg $msg \n") if defined $opt_d;
	next if $msg =~ /^\s*$/;
	process_a_logmsg($msg);
	info( "Dbg $msg (Msg=$load_msg State=$load_state)\n") if defined $opt_d;
}
info( "Dbg: Load Command Completed\n") if defined $opt_d;

if( $server_type eq "SYBASE" and ! defined $opt_O ) {
	info("Online Database $opt_D\n");
	foreach( dbi_query(-db=>"master",-query=>"online database $opt_D", -no_results=>1) ) {
		my($msg)=dbi_decode_row($_);
		info "  > ".$msg,"\n";
	}
}

dbi_disconnect();

if( $load_state eq "GOOD" or $load_state eq "SKIP" ) {
   $start_time=time-$start_time;
   my $minutes=int($start_time/60);
   $start_time-=60*$minutes;
   info "Load of $db completed in $minutes minutes and $start_time secs.",$NL unless defined $opt_s;
	if( defined $opt_s ) {
   	info " ".$load_state." : $load_msg",$NL,$NL;
	} else {
   	info $load_state." ".basename($opt_i)." : $load_msg",$NL,$NL;
	}
	exit(0);
} else {
	if( defined $opt_s ) {
   	info " ".$load_state." : $load_msg",$NL,$NL;
	} else {
   	info $load_state." ".basename($opt_i)." : $load_msg",$NL,$NL;
	}
   exit(1);
}

#  $load_state = UNKNOWN | GOOD | BAD | SKIP
#  $load_msg	= text message about load
sub process_a_logmsg
{
	($_) = @_;
  	if( $server_type eq "SQLSERVER" ) {
      if( /RESTORE LOG successfully processed (\d+)/ ) {
            $load_msg   ="Loaded $1 Pages";
            $load_state = "GOOD" if $load_state eq "UNKNOWN";
            return 0;
      } elsif(/which is too early to apply to the database. A more recent/){
          /LSN (\d+),/;
          my($lsn)=$1;
          /LSN (\d+) can be restored/;
          my($wantlsn)=$1;
          #$load_msg="LSN $lsn Skipped. (ok)";
          $load_msg="LSN Skipped. (ok)";
          $load_state = "GOOD" if $load_state eq "UNKNOWN";
          return 0;
      } elsif( /Processed (\d+) pages for database '/ ) {
          $load_msg="loaded $1 pages";
          $load_state = "GOOD" if $load_state eq "UNKNOWN";
          return 0;
      } elsif( /RESTORE LOG is terminating abnormally/ ) {
          return 0;    # dont know if its good or bad jsut from this
      } elsif( /The log in this backup set terminates at LSN/ and
          /which is too late to apply to the database./ ) {
          $load_msg   ="Skipped Log ";
          $load_state = "GOOD" if $load_state eq "UNKNOWN";
          return 0;
      } elsif( /was only partially restored by a database or file restore./ ) {
          $load_state= "BAD";
          $load_msg="DB $db (Prior Load Failed)\n\n";
          return 0;
      } elsif( /Exclusive access could not be obtained because/ ) {
          $load_state= "BAD";
          $load_msg="DB $db In Use (Load Aborted)\n\n";
          return 0;
      } elsif( /The log in this backup set begins at LSN/ ) {
          /LSN (\d+),/;
          my($lsn)=$1;
          /LSN (\d+) can be restored/;
          my($wantlsn)=$1;
          # $load_msg="LSN $lsn Too Late. (abrt)";
          $load_state= "BAD";
          $load_msg="DB $db Needs LSN $wantlsn\n\n";
          return 0;
       } elsif( /Cannot open backup device/ and /Device error or device off-line/) {
       	s/Cannot open backup device//;
		 	s/Device error or device off-line\. See the SQL Server error log for more details\.//;
			info($_." : file not found");
         $load_state= "GOOD";
			return 0;
       } else {
          info("=>".$_."\n");
          $load_msg="Error: $_";
          $load_state= "BAD";
       }
       info "  > ".$_,"\n";
       die "Query: $command\nFatal Error: This shouldnt happen unless the above was a unhandled message." unless /The preceding restore operation did not spec
ify WITH NORECOVERY/;

   } else {
		return 0 if /Use this value when executing the \'sp_volchanged\' system stored procedure after ful/;
      return 0 if /WARNING: In order to LOAD the master database, the /;
      s/Backup Server:\s//;
		if( /LOAD is complete/ ) {
      	$load_msg="LOAD is Complete";
         $load_state = "GOOD" if $load_state eq "UNKNOWN";
		}
		return 0 if defined $opt_s and /mounted on disk file/;
		if( /^930:/ ) {			# last database load incomplete
      	$load_msg="Prior LOAD DATABASE incomplete";
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^911:/ ) {			# cant use datbase
      	$load_msg=$_;
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^9\d\d:/ ) {			# other 900 series messages
      	$load_msg=$_;
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^3137:/ ) {			# use online db message
			return;
		} elsif( /^31\d\d:/ ) {			# other 3100 series messages
      	$load_msg=$_;
         $load_state = "BAD" if $load_state eq "UNKNOWN";
			return;
		} elsif( /^4305/ ) {	# out of sequence
			#$opt_d=1;# DBG DBG
			# must find the timestamps
			info( "Msg $_ \n");# if defined $opt_d;
			/Current time stamp is (\w\w\w[\d\s:]+\w\w)/;
			my($dbtime)=$1;
			info( "db Time=$dbtime \n") if defined $opt_d;
			$dbtime =~ /(\w\w\w)\s+(\d+)\s+(\d+)\s+(\d+):(\d+)[\d\:]*(\w+)/;
			my($mon,$day,$yr,$hr,$min,$AM)=($1,$2,$3,$4,$5,$6);
			#print "DBG DBG dbtime m $mon d $day hr $hr min $min am $AM\n";
			$hr+=12 if $AM eq "PM" and $hr != 12;
			$hr-=12 if $AM eq "PM" and $hr == 12;
			$hr-=12 if $AM eq "AM" and $hr == 0;
			my($moncnt)=0;
			while($moncnt<=12) {
				last if $month[$moncnt] eq $mon;
				$moncnt++;
			}
			my($dbtm)=timelocal(0,$min,$hr,$day,$moncnt,$yr);
			info( "Converts To $dbtm which is ",scalar(localtime $dbtm),"\n") if defined $opt_d;

			/dump was from (\w\w\w[\d\s:]+\w\w).$/;
			my($dumptime)=$1;
			info( "dump Time=$dumptime \n") if defined $opt_d;
			$dumptime =~ /(\w\w\w)\s+(\d+)\s+(\d+)\s+(\d+):(\d+)[\d\:]*(\w+)/;
			($mon,$day,$yr,$hr,$min,$AM)=($1,$2,$3,$4,$5,$6);
			#print "DBG DBG ftime m $mon d $day hr $hr min $min am $AM\n";
			$hr+=12 if $AM eq "PM" and $hr != 12;
			$hr-=12 if $AM eq "PM" and $hr == 12;
			$hr-=12 if $AM eq "AM" and $hr == 0;
			($moncnt)=0;
			while($moncnt<=12) {
				last if $month[$moncnt] eq $mon;
				$moncnt++;
			}
			my($dumptm)=timelocal(0,$min,$hr,$day,$moncnt,$yr);
			info( "Converts To $dumptm which is ",scalar(localtime $dumptm),"\n") if defined $opt_d;

			if( $dumptm > $dbtm ) {
      		$load_msg="Cant Load - Tranloads Have Gap (".
      				$dumptm."=".
      				scalar(localtime($dumptm)).
      				" > $dbtm=".
      				scalar(localtime($dbtm)).
      				") \n";
         	$load_state = "BAD";
			} else {
      		$load_msg="Log Load Allready applied (db=$dbtime)";
         	$load_state = "GOOD" if $load_state eq "UNKNOWN";
			}
			#$opt_d=undef; # DBG DBG
			return 0;
		}
      info "  > ".$_,"\n";
   }
}

sub kill_users_in_db
{
	my($db)= @_;
	my(@processes)=dbi_query(-db=>"master",-query=>"select spid
			from master..sysprocesses
			where dbid=db_id(\'$opt_D\')
			and \'$opt_D\' != \'master\'");

	return 0 if $#processes<0;
	return ($#processes+1) if defined $opt_k;

	info "Killing ",($#processes+1)," Users in $db\n";
	foreach( @processes ) {
		my($pid)=dbi_decode_row($_);
		info " -- killing user suid=$pid\n";
		foreach (dbi_query(-db=>"master",-query=>"kill $pid", -no_results=>1) ) {
			my($msg)=dbi_decode_row($_);
			alert "ERROR: $msg\n";
		}
	}
	return $#processes;
}

__END__

=head1 NAME

load_database.pl - utility to load databases

=head2 USAGE

load_database.pl -nnum_stripes -USA_USER -SSERVER
                           -PSA_PASS -Ddb -ifile_root -tdhk -MType

   -n Num stripes (.SX appendended to dump name)
   -v Num         use devices: Num dumps per subdirectory (device_#)
   -D DB_NAME     (may NOT contain wildcards)
   -i DIR         (input file root directory)
   -k 				dont kill users
   -M servertype  Sybase or ODBC
   -t             (Transaction Log only)
   -d             (debug mode - for dbi debugging use -z)
   -h             (html output)
   -r extension   (rename file extension (dbdump part of name) when done)
   -e errorlog    (optional)
   -l sessionlog  (optional)
	-s silent mode (only print summary)
   -X             dumps are compressed using internal sybase compression
   -Y             force std output prints (wont print to stdout if cron job)
   -O             no online database needed

   Load a database from the specified file in with the name
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss[.S?]
   where .S? represents optional stripe numbers if the backups are striped.
   The Root of the file (specified by -i) is the full name (excluding the
   .S? stripe name).

If -t and no file exactly matches the file_root, ALL files that start with the
root are loaded in order.  If full load, and no file matches the -i root,
the LATEST file starting with the root will be loaded.

=head2 DESCRIPTION

Loads Databases.  Can be set for tran log dumps with -t or will, by default, back up full database dumps.

Loads a database from either a full backup or a transaction log dump.  The file root of the command is the path name to the dump file.
It should exclude the .S[num] extension for striped dumps. The program attempts to be intelligent about what file to load.
If a tran log is implied and no file exactly matches the file_root, the -i option identifies a file root and ALL files that match
the root are loaded in order of timestamp (from the file name).  If there is no file with exactly the correct root on a full load,
the LATEST file matching the root of the command will be loaded.  The -r prefix can be used to rename files when they are done loading.
For example, you may use -rloaded to rename SRVR.MYDB.dbdump.20001109.022233 to SRVR.MYDB.loaded.20001109.022233. The database name
may NOT contain wildcards.

Standard naming for dump files should be
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss
   or
      {SERVER}.{DATABASE}.dbdump.yyyymmdd.hhmmss.S1
     through
      {SERVER}.{DATABASE}.dbdump.yyyymmdd.hhmmss.SN
if the backups are striped.

=cut

