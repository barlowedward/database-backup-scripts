: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

#############################################################################
#
use lib 'mimi.pl';
#
# Copyright (c) 2002 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed
#
# See Perldoc (type perldoc rebuild.pl)
#
#############################################################################

use   strict;
use   Getopt::Long;
use	File::Copy;
use	File::Basename;
use	Cwd;

$|=1;

sub usage {
	die	"Usage: $0 [--UNSET] --OVERRIDE --LIBDIR=DIRECTORY,DIRECTORY --FILENAME=FILENAME,FILENAME --DIRECTORY=DIRECTORY,DIRECTORY [--DEBUG] --PERL=PERL\nUse --UNSET to reset to default perl";
}

use	vars qw( $SILENT $UNSET $DIRECTORY $DEBUG $LIBDIR $FILENAME $PERL $OVERRIDE);
usage() unless GetOptions(
	"DIRECTORY=s"	=>	\$DIRECTORY,
	"LIBDIR=s"			=>  \$LIBDIR,
	"FILENAME=s"		=>  \$FILENAME,
	"DEBUG"			=>  \$DEBUG,
	"SILENT"			=>  \$SILENT,
	"OVERRIDE"			=>  \$OVERRIDE,
	"PERL=s"			=>  \$PERL,
	"UNSET"			=>  \$UNSET );

#
# Get Perl
#
my($perl);
if( defined $PERL ) {
	$perl="#!".$PERL."\n" if defined $PERL;
} else {
	$perl="#!".$^X."\n";
	if( $^X eq "perl") {
		my($path)=$ENV{"PATH"};
		if( $path =~ /[C-Z]:/ )  { 		# NT/Windoze
			foreach( split /;/,$path ) {
				next unless -x $_."/perl.exe";
				s.\\./.g;
				$perl="#!".$_."/perl.exe\n";
				last;
			}
		} else { 				# UNIX
			foreach( split /:/,$path ) {
				next unless -x $_."/perl";
				$perl="#!".$_."/perl\n";
				last;
			}
		}
		die "perl program not found in your path\n" unless $perl ne "";
	}
}

if ( defined $UNSET ) {
	print "Resetting Hash Bangs To Long Format\n";
	$perl = "\: # use perl\n\teval \'exec perl -S \$0 \"\$@\"\'\n\tif \$running_under_some_shell;\n\n";
}
print "SETTING PERL LINES TO=$perl" if defined $DEBUG;

my $startdir=cwd();
print "Startdir is  $startdir\n" if defined $DEBUG;

my $codedir=dirname($0);
chdir $codedir or die "Cant cd to $codedir: $!\n";
$codedir=cwd();
print "Codedir is $codedir\n" if defined $DEBUG;

my $libdir=$LIBDIR;
$libdir=$codedir."/lib" if ! defined $libdir and -d "lib";
if( ! defined $libdir and -d "../lib" ) { chdir ".."; $libdir=cwd()."/lib"; }

if( $libdir=~/,/ ) {
	$libdir="use lib qw(".join(" ",split(/,/,$libdir)).");\n";
} else {
	die "Cant find lib subdirectory in . .. $codedir and $codedir/.."
		unless defined $libdir;
	$libdir="use lib \'$libdir\';\n";
}
print "Libdir is  $libdir\n" if defined $DEBUG;


chdir $startdir or die "Cant cd back to starting directory ($startdir): $!\n (try using full path to rebuild.pl)";

print "SETTING LIB LINES TO $libdir\n" if defined $DEBUG;

my( @files );
$DIRECTORY=$startdir	unless defined $DIRECTORY or defined $FILENAME;
push @files, split(/,/,$DIRECTORY) if defined $DIRECTORY;
push @files, split(/,/,$FILENAME)  if defined $FILENAME;
push @files,@ARGV;
foreach ( @files ) {
	die "ERROR: $_ does not exist.\n" unless -e $_;
	my($t)="File";
	$t="Directory" if -d $_;
	print "\Reformatting $t => $_    ";
	reformat_file($_)
};
print "\n";
exit(0);

# reformat_file a file => passed in with relative or full path name
sub reformat_file
{
	my($file)=@_;
	print "FILENAME=$file\n" if defined $DEBUG;
	return if $file=~/data$/ or $file=~/200\d\d\d/ or $file=~/\.out$/ or $file=~/\.lck$/ or $file=~/\.dat/;

	if( -d $file ) {		# directory so recurse down it
		if( $file =~ /data$/ or $file=~/^save$/) {
			print "Skipping data/save directory $file\n" if defined $DEBUG;
			return;
		}
		if( ! -r $file ) {
			print "Skipping directory $file - Can Not Read\n" if defined $DEBUG;
			return;
		}
		print "\n\t(",basename($file),")" unless $SILENT;
		opendir(DIR,$file) || die("Can't open directory $file for reading\n");
		my(@dirlist) = grep(!/^\./,readdir(DIR));
		closedir(DIR);
		foreach (@dirlist) { reformat_file($file."/".$_); }
	} elsif( -T $file ) { 	# text file => also remove ctrl m

		next if /data$/ or /200\d\d\d/ or /\.out$/ or /\.lck$/;
		if( ! -w $file ) {
			print "\n\t(warning: cant write $file - skipping)";
			next;
		}
		print "." unless $SILENT;

		my($is_perlfile)=0;
		$is_perlfile=1 if $file =~ /.pl$/ or $file=~/.dist$/;

		my($linenum)=0;
		my($is_use_perl)=0;
		my($in_file_text)=0;

		# just clean out the hashbangs
		$in_file_text=1 unless $is_perlfile;
		if( ! defined $OVERRIDE ) {
			$in_file_text=1 if $file =~ /rebuild.pl/;
			$in_file_text=1 if $file =~ /rebuild.pl/;
			$in_file_text=1 if $file =~ /configure.pl/;
			$in_file_text=1 if $file =~ /mimi.pl/;
		}

		open(INP,"$file") 		or die "Cant open $file: $!\n";
		unlink "$file.cpy" if -r "$file.cpy";
		open(OUT,"> $file.cpy") or die "Cant open temp file $file.cpy: $!\n";
		# STRIP OUT CTRL-M's
		# IF LINE1 and legal perl - change it appropriately
		# IF USE LIB then change it
		while( <INP> ) {
			chop;
			chomp;
			chomp;
			#s/\r$// unless defined $ENV{PROCESSOR_LEVEL};
			s/\r$//;
         s/$//;
			s/\s+$//;

			if( $in_file_text ) {
				print OUT $_,"\n";
				next;
			}

			$linenum++;
			if( $is_use_perl and $linenum<=3 ){	# skip first 3 lines
				print OUT $_,"\n";
				next;
			}
			if( $is_perlfile ) {
				if( $linenum==1  ) {
					# case 1) first line has #! and ends in perl
					if( /^\#\!/ and /perl/i ) {
						print OUT $perl;
					# case 2) first line has #! and ends in perl.exe
					} elsif( /^\#\!/ and /perl.exe\s*$/i ) {
						print OUT $perl;
					} elsif( /use\s*perl/i ) {
						if( defined $UNSET ) {
							print OUT $_;
							next;
						}
						print OUT $_,"\n";
						$is_use_perl=1;
					} else {
						print OUT $_,"\n";
						$is_perlfile=0;
					}
				} elsif( $linenum<8 and /use lib/ ) {
					print OUT $libdir;
					$in_file_text=1;
				} else {
					print OUT $_,"\n";
				}
			} else {
				print OUT $_,"\n";
			}
		}
		close INP;
		close OUT;

		# move the file back
		copy("$file.cpy",$file)
				or die "Cant Rename Temporary File $file.cpy to $file: $!";
		unlink("$file.cpy") or die("cant remove file $file.cpy");

		# Chmod the file
		chmod 0775,$file if $is_perlfile;

	} else {
		print "+"  unless $SILENT;
	}
}

__END__

=head1 NAME

rebuild.pl

=head2 SYNOPSIS

rebuild.pl - Rebuild Hashbang And use lib statements

=head2 COPYRIGHT

 Copyright (c) 2002-3 By Edward Barlow
 All Rights Reserved
 Explicit right to use can be found at www.edbarlow.com
 This software is released as free software and should be shared and enjoyed

=head2 USAGE

Usage: G:\dev\AHMR52~F\bin\rebuild.pl [--UNSET] --OVERRIDE --LIBDIR=DIRECTORY,DI
RECTORY --FILENAME=FILENAME,FILENAME --DIRECTORY=DIRECTORY,DIRECTORY [--DEBUG] -
-PERL=PERL
	USE --UNSET to reset your perl to default hashbangs

=head2 DESCRIPTION

Resets the perl hashbang lines at the top of the file.
If the first line starts with a hashbang #! then the next part is a
command to reformat_file.  This can either be of the format

    #!/usr/local/bin/perl

or if you pass --UNSET

   : # use perl
       eval 'exec perl -S $0 "$@"'
       if $running_under_some_shell;

These lines are reformated correctly based on the perl in your path or based on the perl you pass

=head2 USAGE

	rebuild.pl file file file...
or
	rebuild.pl *
or
	rebuild.pl -D.
or
	rebuild.pl -u

		to undo changes to generic format


 --OVERRIDE will work on restricted file types

=head2 NOTES

Reset perl on first or first 3 lines as necessary.  If the first line starts with a hashbang #!
then that word is reformatted as necessary.  If the first 3 lines looks like:

   : # use perl
	eval 'exec perl -S $0 "$@"'
	if $running_under_some_shell;

skips files named configure.pl or rebuild.pl.  Those should always be full path name to the perl executable using the above syntax.

=cut


