#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(logdie infof info);
use strict;
use Getopt::Std;
use Logger;
use vars qw($opt_U $opt_S $opt_P $opt_o $opt_h %CONFIG $opt_l $opt_e $opt_D $opt_J $opt_d);
use DBIFunc;
use Logger;


sub usage
{
   print @_;
   print "stop_sybase.pl -USA_USER -Sopt_S -PSA_PASS
   \n";
   return "\n";
}

$| =1;

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List") unless getopts('U:S:P:o:h');

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  $opt_J,
            -subsystem =>$opt_J,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

logdie usage("Must pass sa password\n" )  unless defined $opt_P;
logdie usage("Must pass server\n")         unless defined $opt_S;
logdie usage("Must pass sa username\n" )  unless defined $opt_U;

pingit("Before Shutdown Attempt");

dbi_set_mode("INLINE");
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
        or logdie "Cant connect to $opt_S as $opt_U\n";
foreach ( dbi_query(-db=>"master",-query=>"shutdown") ) {
        info join(" ",dbi_decode_row($_)),"\n";
}
dbi_disconnect();

dbi_syb_ping($opt_S,$opt_U,$opt_P) or cleanexit("The Server Has Been Shut Down");
test_login_to_server    qlogdie

sub pingit
{
        my($txt)=@_;
        my $msg=dbi_syb_ping($opt_S,$opt_U,$opt_P);
        cleanexit($msg) if $msg ne "0";
}

info "Issuing normal shutdown to Sybase Server $opt_S\n"
ServerShutdown();
sleep(2);
IsServerUp() and CleanExit("Successful Shutdown");
sleep(2);
IsServerUp() and CleanExit("Successful Shutdown");
info "Issuing shutdown with nowait to Sybase Server $opt_S\n"
ServerShutdown with nowait
sleep(2);
IsServerUp() and CleanExit("Successful Shutdown");
sleep(2);
IsServerUp() and CleanExit("Successful Shutdown");

Status "Unix kill Being Used On Server $opt_S"
numservers=`${MYPS} | grep "dataserver" | wc -l`
[ $numservers -eq 0 ] &&  { Status "$SHUTMSG"; break; }   # all done
if [ $numservers -ge 2 ]
then
        Status "Can Not Shutdown - Multiple Servers UP"
        break
fi

PID=`${MYPS} | grep "dataserver" | grep "$opt_S" | grep -v "grep" | awk "{info \\$$PID_COL}"`
KillProcess $PID

__END__

=head1 NAME

Stop_Sybase.pl

=head2 DESCRIPTION

Stops your sybase server cleanly

=head2 USAGE

Stop_Sybase.pl $opt_S $SAPASSWORD [$SYBASEDIR]

note server must be running on the unix system in question and
be using an errorlog named errorlog_$opt_S in $SYBASE/install
and a runserver file of RUN_$opt_S in the same directory
unless it is called SYBASE (will use defaults of RUN_opt_S and
errorlog

=head2 COPYRIGHT

Copyright (c) 1996 by Edward Barlow
All Rights Reserved

You are welcome to use and redistribute this software, without
charge, provided you make no money from it. To the best of my
knowledge this program works as specified, but I make no waranty
about it (ie.  you get what you pay for).  I recommend you browse
the scripts to see what i have done prior to using them in any
production environments.

=cut

sub pingit
"
Can't use global @_ in "my" at stop_sybase_beta.pl line 57, near "=@_"
syntax error at stop_sybase_beta.pl line 60, near "}"
Execution of stop_sybase_beta.pl aborted due to compilation errors.
