#!/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use Getopt::Std;
use File::Basename;
use CommonFunc;

use vars qw( %CONFIG $opt_J);

#Copyright (c) 2005-2006 by SQL Technologies.
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
   	print $0.' -Jjob ';
	return "\n";
}

$| =1;


die usage("Bad Parameter List\n") unless getopts('J:');

if( defined $opt_J ) {
	%CONFIG=read_configfile( -job=>$opt_J );
	foreach (sort keys %CONFIG) { print "debug: CONFIG{".$_."}=".$CONFIG{$_}."\n"; }
} else {
	%CONFIG=read_configfile();
	my(@keys)=get_job_list(%CONFIG);
	print "CURRENTLY DEFINED PLANS ARE : \n";
	print join(" ",@keys),"\n";
}

__END__

=head1 NAME

show_configvars.pl - print out the job configuration variables as they would be used

=head2 USAGE

show_configvars.pl

=head2 SYNOPSIS

This is a diagnostic utility

show_configvars.pl

CURRENTLY DEFINED PLANS ARE :

SYB1 PLATINUM MMMDBDB_COPY SYBDEV1 SYBDEV2 SYBDEV2_UPDSTATS MLPSYBDBCC CALYDB SYB2_UPDSTATS IMAGREP2SYB ADS060 ADS088 SYB1_TO_SYB2

or

show_configvars.pl -JJOB

show variables for the plan

 debug: CONFIG{AUDIT_PURGE_DAYS}=30
 debug: CONFIG{BASE_BACKUP_DIR}=/apps/sybmon/dev/data/BACKUP_LOGS
 debug: CONFIG{BCP_COMMAND}=
 debug: CONFIG{BCP_TABLES}=
 debug: CONFIG{CODE_LOCATION_ALT1}=
 debug: CONFIG{CODE_LOCATION_ALT2}=
 debug: CONFIG{COMPRESS}=/usr/local/bin/gzip
 debug: CONFIG{COMPRESSION_LEVEL}=0
 debug: CONFIG{COMPRESS_DEST}=
 debug: CONFIG{COMPRESS_LATEST}=n
 debug: CONFIG{DBCC_IGNORE_DB}=
 debug: CONFIG{DO_AUDIT}=y
 debug: CONFIG{DO_BCP}=n
 debug: CONFIG{DO_CLEARLOGSBEFOREDUMP}=y
 debug: CONFIG{DO_COMPRESS}=y
 debug: CONFIG{DO_DBCC}=y
 debug: CONFIG{DO_DUMP}=y
 debug: CONFIG{DO_INDEXES}=n
 debug: CONFIG{DO_LOAD}=n
 debug: CONFIG{DO_ONLINEDB}=n
 debug: CONFIG{DO_PURGE}=y
 debug: CONFIG{DO_REORG}=y
 debug: CONFIG{DO_UPDSTATS}=y
 debug: CONFIG{DSQUERY}=DACSSERVER
 debug: CONFIG{DUMP_DIR_BY_BKSVR}=/export/home/sybase-dump/DACSSERVER/dbdumps
 debug: CONFIG{DUMP_FILES_PER_SUBDIR}=
 debug: CONFIG{IGNORE_LIST}=model|tempdb|sybsyntax
 debug: CONFIG{IGNORE_SERVER}=
 debug: CONFIG{IS_REMOTE}=y
 debug: CONFIG{JOBNAME}=DACSSERVER
 debug: CONFIG{LOCAL_DIR}=//samba/sybmon/dev/data/BACKUP_LOGS
 debug: CONFIG{LOCAL_DUMP_DIR}=
 debug: CONFIG{LOCAL_LOG_DIR}=
 debug: CONFIG{MAIL_HOST}=mail1.mlp.com
 debug: CONFIG{MAIL_TO}=ebarlow@mlp.com,ryi@mlp.com
 debug: CONFIG{NT_CODE_LOCATION}=//samba/sybmon/dev
 debug: CONFIG{NT_PERL_LOCATION}=C:/perl/bin/perl.exe
 debug: CONFIG{NUMBER_OF_STRIPES}=4
 debug: CONFIG{NUM_BACKUPS_TO_KEEP}=2
 debug: CONFIG{NUM_DAYS_TO_KEEP}=4
 debug: CONFIG{REMOTE_ACCOUNT}=sybase
 debug: CONFIG{REMOTE_DIR}=/export/home/sybase-dump
 debug: CONFIG{REMOTE_DUMP_DIR}=
 debug: CONFIG{REMOTE_HOST}=dacs2
 debug: CONFIG{REMOTE_LOG_DIR}=
 debug: CONFIG{REMOTE_PASSWORD}=
 debug: CONFIG{REORG_ARGS}=--maxtimemins=60 --reorg_compact
 debug: CONFIG{SRVR_DIR_BY_BKSVR}=/export/home/sybase-dump/DACSSERVER
 debug: CONFIG{SUCCESS_MAIL_TO}=ebarlow@mlp.com,ryi@mlp.com
 debug: CONFIG{SYBASE}=/apps/sybase
 debug: CONFIG{TRAN_DIR_BY_BKSVR}=/export/home/sybase-dump/DACSSERVER/logdumps
 debug: CONFIG{UNCOMPRESS}=/usr/bin/gzip -d
 debug: CONFIG{UNIX_CODE_LOCATION}=/apps/sybmon/dev
 debug: CONFIG{UNIX_PERL_LOCATION}=/apps/sybmon/perl/bin/perl
 debug: CONFIG{UPD_STATS_FLAGS}=-i
 debug: CONFIG{XFER_BY_FTP}=
 debug: CONFIG{XFER_FROM_DB}=
 debug: CONFIG{XFER_SCRATCH_DIR}=
 debug: CONFIG{XFER_TO_DB}=
 debug: CONFIG{XFER_TO_DIR}=
 debug: CONFIG{XFER_TO_DIR_BY_TARGET}=
 debug: CONFIG{XFER_TO_HOST}=
 debug: CONFIG{XFER_TO_SERVER}=

=cut
