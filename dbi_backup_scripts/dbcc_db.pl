#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(logdie infof info warning alert);
use strict;
use Getopt::Std;
use Logger;
use DBIFunc;
use File::Basename;
use CommonFunc;
use vars qw($opt_U $opt_p $opt_S $opt_P $opt_D $opt_d $opt_h $opt_e $opt_l  $opt_M $opt_o %CONFIG);


sub usage
{
	print @_;
	print "dbcc_db.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST

	-p 			print all output from dbcc
	-T Type       Sybase or Odbc
   -D DB_LIST    pipe separated list with wildcards of databases
   -d	    debug mode
   -h	    html output
	-o outputdir  for raw output (file is server.db.dbcc.datestamp)
   -e errorlog   optional
   -l sessionlog optional

   Runs standard dbcc on the list of databases
\n";
	return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
die(usage("Bad Parameter List")) unless getopts('U:D:S:P:o:dhl:e:M:T:p');
%CONFIG=read_configfile( -srv=>$opt_S );
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  undef,
            -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

use Sys::Hostname;
info( "dbcc_db.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

my($start_time)=time;

logdie usage("Must pass sa password\n" )  unless defined $opt_P;
logdie usage("Must pass server\n")		      unless defined $opt_S;
logdie usage("Must pass sa username\n" )  unless defined $opt_U;
logdie usage("Output directory does not exist\n")
		if defined $opt_o and ! -d $opt_o;

if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}

logdie usage("Type must be Sybase|ODBC\n")
	unless $opt_M eq "Sybase" or $opt_M eq "ODBC";

#
# CONNECT TO DB
#
if( $opt_M eq "ODBC" ) {
	dbi_msg_ok(1000);
	# dbi_msg_exclude("
}

info "Connecting to sybase",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P,-type=>$opt_M, -debug=>$opt_d) or logdie "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($server_type)="SQLSERVER";
foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
	($server_type)=dbi_decode_row($_);
}
info("Server Type=$server_type\n");

# GET DB INFO
my($whereclause)="";
if( defined $opt_D ) {
	foreach ( split(/\|/,$opt_D)) {
		if( $whereclause eq "" ) {
			$whereclause="and ( name like \'$_\'";
		} else {
			$whereclause .= " or name like \'$_\' ";
		}
	}
	$whereclause.=")" if $whereclause ne "";
}

my( $status1bits, $status2bits );
if( $server_type eq "SQLSERVER" ) {
	$status1bits = 32+64+128+256+512+1024+4096+32768;
	$status2bits = 0x0000;
} else {
# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
#   30= 20 (offline until recovery) + 10 (offline)
	$status1bits = 0x1120;
	$status2bits = 0x0030;
}

my(@databaselist)=dbi_query(-db=>"master",-query=>"select name from sysdatabases where status & $status1bits = 0 and status2&$status2bits=0 $whereclause");

if( $#databaselist<0 ) {
	warning "No Databases Found On Server\n";
	warning "Query Was select name from sysdatabases where status & $status1bits = 0 and status2&$status2bits=0 $whereclause";
	exit(0);
}

my(@errormsg);

foreach (@databaselist) {
	my($db)=dbi_decode_row($_);
	info "Working on db $db",$NL if defined $opt_d;
	my($st)=time;
	run_a_dbcc($db);

	my($s)=time-$st;
	my $minutes=int($s/60);
	$s-=60*$minutes;
	info "Dbcc of $db completed in $minutes minutes and $s secs.\n";
	info $NL,$NL if $#databaselist>=0;
}

dbi_disconnect();

if( $#errormsg>=0 ) {
	bad_email("error:".join("\nerror:",@errormsg)."\n");
}

my($s)=time-$start_time;
my $minutes=int($s/60);
$s-=60*$minutes;
info "Dbcc program completed in $minutes minutes and $s secs.\n";

exit(0);

sub run_a_dbcc
{
	my($db)=@_;
	my($tot_numerr)=0;

	info("<h2>") if defined $opt_h;
	info("Checking Database $db in Server $opt_S\n");
	info("</h2>") if defined $opt_h;

	logdie("Cant Use Database $db\n") unless dbi_use_database($db);

	info("<h3>") if defined $opt_h;
	info("Checkalloc $db\n");
	info("</h3>")if defined $opt_h;

	my($logfile)=$opt_o;
	$logfile  =~ s|\/$|| if defined $logfile;
	$logfile  .=  "/".$opt_S.".".$db.".dbcc.".today() if defined $logfile;

	my($errfile)=$logfile.".errors" if defined $logfile;

	info "Raw Output Saved To $logfile\n" if defined $logfile and $opt_d;
	if( defined $logfile ) {
		open(DBCCOUT,">".$logfile) or die "Cant open $logfile for writing : $!\n";
		open(DBCCERR,">".$errfile) or die "Cant open $errfile for writing : $!\n";
	}

	print DBCCOUT "running dbcc checkalloc($db): \n" if defined $logfile;
	print DBCCERR "running dbcc checkalloc($db): \n" if defined $logfile;
	my @rc= dbi_query(-db=>"master",-query=>"dbcc checkalloc($db)", -no_results=>1, -debug=>$opt_d);
	info( "checkalloc($db) returned ".($#rc+1)." rows\n" ) if defined $opt_d or defined $opt_p;

	my($count,$numerr)=(0,0);
	my($in_error)=0;
	foreach ( @rc ) {
		my($msg)=dbi_decode_row($_);
		if( $server_type eq "SQLSERVER"
		and $msg=~/CHECKALLOC found \d+ allocation errors and \d+ consistency errors/) {
			# info($msg."\n");
			$msg=~/CHECKALLOC found (\d+) allocation errors and (\d+) consistency errors/;
			$numerr+=$1+$2;
		}

		info "checkalloc: ".$msg."\n" if defined $opt_d or defined $opt_p;
		print DBCCOUT "checkalloc: ".$msg."\n" if defined $logfile;
		if( $count ) {
			info $msg,"\n";
			$count--;
			print DBCCERR "checkalloc: ".$msg."\n" if defined $logfile;
		}

		if( $in_error or $msg=~/Serious or Fatal/ ) {
			$numerr++;
			push @errormsg,$msg unless $msg=~/^\s*$/;
			print DBCCERR "checkalloc: ".$msg."\n" if defined $logfile;
			$in_error=1;
			$in_error=0 if $msg=~/-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-/;
		}

		if( $msg=~/error:/ and $msg!~/[\w\_]error:/) {
			$count++;
			$numerr++;
			push @errormsg,$msg unless /^\s*$/;
		} elsif( $msg=~/corrupt/ and $msg!~/[\w\_]corrupt/ ) {
			$count+=2;
			$numerr++;
			push @errormsg,$msg unless /^\s*$/;
		} elsif( $msg=~/Msg.*Level.*State/ ) {
			$count+=3;
			$numerr++;
			print DBCCERR "checkalloc: ".$msg."\n" if defined $logfile;
			push @errormsg,$msg unless $msg=~/^\s*$/;
		}
	}
	info("Checkalloc completed successfully with no errors","\n")
		 if $numerr == 0;
	$tot_numerr+=$numerr;

	info("<h3>") if defined $opt_h;
	info("\nCheckdb $db\n");
	info("</h3>") if defined $opt_h;
	print DBCCOUT "\n\nrunning dbcc checkdb($db): \n" if defined $logfile;
	print DBCCERR "\n\nrunning dbcc checkdb($db): \n" if defined $logfile;
	@rc= dbi_query(-db=>"master",-query=>"dbcc checkdb($db)", -no_results=>1, -debug=>$opt_d);
	info( "checkdb($db) returned ".($#rc+1)." rows\n" ) if defined $opt_d or defined $opt_p;

	my($count,$numerr)=(0,0);
	$in_error=0;
	foreach ( @rc ) {
		my($msg)=dbi_decode_row($_);

		if( $server_type eq "SQLSERVER"
		and $msg=~/CHECKDB found \d+ allocation errors and \d+ consistency errors/) {
			# info($msg."\n");
			$msg=~/CHECKALLOC found (\d+) allocation errors and (\d+) consistency errors/;
			$numerr+=$1+$2;
		}
		info "checkdb: ".$msg."\n" if defined $opt_d or defined $opt_p;

		print DBCCOUT "checkdb: ".$msg."\n" if defined $logfile;
		if( $count ) {
			info $msg,"\n";
			$count--;
			print DBCCERR "checkdb: ".$msg."\n" if defined $logfile;
		}

		if( $in_error or $msg=~/Serious or Fatal/ ) {
			$numerr++;
			push @errormsg,$msg unless $msg=~/^\s*$/;
			print DBCCERR "checkalloc: ".$msg."\n" if defined $logfile;
			$in_error=1;
			$in_error=0 if $msg=~/-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-/;
		}

		if( $msg=~/error:/ and $msg!~/[\w\_]error:/) {
			$count++;
			$numerr++;
			push @errormsg,$msg unless /^\s*$/;
		} elsif( $msg=~/corrupt/ and $msg=~/[\w\_]corrupt/ ) {
			$count+=2;
			$numerr++;
			push @errormsg,$msg unless $msg=~/^\s*$/;
		} elsif( $msg=~/Msg.*Level.*State/ ) {
			$count+=3;
			$numerr++;
			print DBCCERR "checkdb: ".$msg."\n" if defined $logfile;
			push @errormsg,$msg unless $msg=~/^\s*$/;
		}
	}
	info("Checkdb completed successfully with no errors\n") if $numerr == 0;
	$tot_numerr+=$numerr;

	info("<h3>") if defined $opt_h;
	info("\nCheckcatalog $db\n");
	info("</h3>") if defined $opt_h;
	print DBCCOUT "\n\nrunning dbcc checkcatalog($db): \n" if defined $logfile;
	print DBCCERR "\n\nrunning dbcc checkcatalog($db): \n" if defined $logfile;
	@rc= dbi_query(-db=>"master",-query=>"dbcc checkcatalog($db)", -no_results=>1, -debug=>$opt_d);
	info( "checkcatalog($db) returned ".($#rc+1)." rows\n" ) if defined $opt_d or defined $opt_p;

	my($count,$numerr)=(0,0);
	$in_error=0;
	foreach ( @rc ) {
		($_)=dbi_decode_row($_);
		info "checkcatalog: ".$_."\n" if defined $opt_d or defined $opt_p;
		print DBCCOUT "checkcatalog: ".$_."\n" if defined $logfile;
		if( $count ) {
			info $_,"\n";
			$count--;
			print DBCCERR "checkcatalog: ".$_."\n" if defined $logfile;
		}

		if( $in_error or /Serious or Fatal/ ) {
			$numerr++;
			push @errormsg,$_ unless /^\s*$/;
			print DBCCERR "checkalloc: ".$_."\n" if defined $logfile;
			$in_error=1;
			$in_error=0 if /-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-\*-/;
		}

		if( /error:/ ) {
			$count++;
			$numerr++;
			push @errormsg,$_ unless /^\s*$/;
		} elsif( /corrupt/ ) {
			$count+=2;
			$numerr++;
			push @errormsg,$_ unless /^\s*$/;
		} elsif( /Msg.*Level.*State/ ) {
			$count+=3;
			$numerr++;
			print DBCCERR "checkcatalog: ".$_."\n" if defined $logfile;
			push @errormsg,$_ unless /^\s*$/;
		}
	}
	info "Checkcatalog completed successfully with no errors","\n" if $numerr == 0;
	$tot_numerr+=$numerr;

	if( defined $logfile ) {
      close(DBCCOUT);
      close(DBCCERR);
      chmod 0666,$logfile;
      chmod 0666,$errfile;
      unlink $errfile if -e $errfile and $tot_numerr==0;
   }
}

__END__

=head1 NAME

dbcc_db.pl - utility to dbcc databases

=head2 AUTHOR

By Ed Barlow

=head2 USAGE

dbcc_db.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST

	-p 			print all output from dbcc
	-T Type       Sybase or Odbc
   -D DB_LIST    pipe separated list with wildcards of databases
   -d	    debug mode
   -h	    html output
	-o outputdir  for raw output (file is server.db.dbcc.datestamp)
   -e errorlog   optional
   -l sessionlog optional

   Runs standard dbcc on the list of databases

=head2 DESCRIPTION

Runs dbcc checkdb, checkalloc, and checkcatalog on your databases. Only
prints output if there is a problem.

=cut

