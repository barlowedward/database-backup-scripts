#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(logdie infof info warning);
use strict;
use Getopt::Std;
use DBIFunc;
use File::Basename;
use CommonFunc;
use Logger;

use vars qw($opt_U $opt_M $opt_S $opt_P $opt_D $opt_d $opt_h $opt_e $opt_l $opt_O $opt_V $opt_k %CONFIG);


sub usage
{
	print @_;
	print "set_dboption.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -Ooption -Vvalue -k

  multi database option setting with checkpoint and kills if those r needed
  (ie... issue kills if set single user and user in the db)

   -O OptionName : option to set
   -D DB_LIST: may include wildcards or pipe separated list of databases
   -d debug mode
   -h html output
   -l logfile
   -e errorfile
   -k do kills (otherwise just prints it)
   -V value true or false\n";
	return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=basename($0)." ".join(" ",@ARGV);
die(usage("Bad Parameter List\n")) unless getopts('U:D:S:P:o:dhl:e:V:O:M:k');

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  undef,
            -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

logdie usage("Must pass sa password\n" )  unless defined $opt_P;
logdie usage("Must pass server\n")			unless defined $opt_S;
logdie usage("Must pass sa username\n" )  unless defined $opt_U;
logdie usage("Must pass option\n" )  		unless defined $opt_O;
logdie usage("Must pass value\n" )  		unless defined $opt_V;

if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}

#
# CONNECT TO DB
#
info "Connecting to database",$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P,-type=>$opt_M)
	or logdie "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($server_type,@databaselist)=dbi_parse_opt_D($opt_D,0,1);
info( "SERVER TYPE $server_type\n" );

if( $#databaselist<0 ) {
	warning "No Databases Matching $opt_D Found On Server\n";
	my($server_type,@databaselist)=dbi_parse_opt_D("%",0,1);
	print "Allowed Databases are : ",join(" ",@databaselist),"\n";
	exit(0);
}

info( "DB: ",join(" ",@databaselist),"\n" );
foreach my $db (@databaselist) {
	info("Working on db $db\n");

	#
	# if option requires nobody else in database then kill users
	#
	if( $opt_O =~ /read only/i or $opt_O=~ /single user/ or $opt_O=~/dbo use only/i) {
		my($count)=0;
		my($num_killed)=kill_users_in_db($db);
		info "$num_killed Users in $db Killed\n" unless $num_killed==0;
		while( $count<5 and $num_killed>0 ) {
			sleep 2;
			$count++;
			$num_killed=kill_users_in_db($db);
			info "$num_killed Users in $db Killed\n" unless $num_killed==0;
		}
	}

	info("sp_dboption \"$db\",\"$opt_O\",\"$opt_V\"\n");
	dbi_query(-db=>"master",-query=>"sp_dboption \"$db\",\"$opt_O\",\"$opt_V\"");
	info("database $db checkpoint\n");
	dbi_query(-db=>$db,	-query=>"checkpoint", -no_results=>1);
}

info("Completed...\n");
dbi_disconnect();
exit(0);

sub kill_users_in_db
{
	my($db)= @_;

	my($q)="select spid from master..sysprocesses where dbid=db_id(\'$db\')	and \'$db\' != \'master\'";
	my(@processes)=dbi_query(-db=>"master",-query=>$q);
	foreach (@processes) { print "rc=",dbi_decode_row($_),"\n"; }

	return 0 if $#processes<0;
	info "(not) " unless $opt_k;
	info "Killing ",($#processes+1)," Users in $db\n";

	foreach( @processes ) {
		my($spid)=dbi_decode_row($_);
		if( int($spid) ne $spid ) {
			foreach (@processes) { print "rc=",dbi_decode_row($_),"\n"; }
			die "ERROR: ABORTING\n";
		}

		info '(not)' unless $opt_k;
		info " -- killing user suid=$spid\n";
		if( $opt_k ) {
			foreach (dbi_query(-db=>"master",-query=>"kill $spid") ) { logdie("ERROR: $_\n"); }
		} else {
			die "Unable to continue.  User Found In Databsae $db\n";
		}

	}
	return ($#processes+1);
}

__END__

=head1 NAME

set_dboption.pl - utility to set database options in multiple databases

=head2 USAGE

set_dboption.pl -USA_USER -SSERVER -PSA_PASS -DDB_LIST -Ooption -Vvalue -k

  multi database option setting with checkpoint and kills if those r needed
  (ie... issue kills if set single user and user in the db)

   -O OptionName : option to set
   -D DB_LIST: may include wildcards or pipe separated list of databases
   -d debug mode
   -h html output
   -l logfile
   -e errorfile
   -k do kills (otherwise just prints it)
   -V value true or false

=head2 DESCRIPTION

Sets Db Options As Appropriate.  DB_LIST can contain wildcards. Kills users
in database if  read only,  single user or dbo use only. Runs chekcpoint
to close out the functionality.

=cut
