#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(debug logdie infof info warning alert);

use strict;
use Getopt::Std;
use DBIFunc;
use CommonFunc;
use File::Basename;
use Logger;

use vars qw($opt_M $opt_c $opt_Y $opt_J $opt_v $opt_f $opt_n $opt_z $opt_U $opt_S $opt_P $opt_D $opt_d $opt_o $opt_h $opt_t $opt_e $opt_l $opt_X $opt_s %CONFIG);
my($log_msgs_to_stdout);

# Copyright (c) 1997-2006 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

my($VERSION,$SHTVER)=get_version();

sub usage
{
	print @_;
	print "dump_database.pl -nnum_stripes -USA_USER -SSERVER -PSA_PASS -DDB_LIST -oDIRECTORY -tdhfc -X0-10 -MType

   -n Num stripes (.SX appendended to dump name)
   -v Num			use devices: Num dumps per subdirectory (device_#)
   -D DB_LIST     (pipe separated list with wildcards of databases)
   -o DIR         (output directory)
   -t             (Dump Transaction Log only)
   -f             (Full Dump - default)
   -M             Server Type for DBI connection String (ODBC, Sybase...)
   -d             (debug mode - for odbc errors use -z)
   -h             (html output)
   -e errorlog    (optional)
   -l sessionlog  (optional)
   -s time        (set time on dump files)
   -c             (clear tran log prior to dumping)
   -X             Compression Level On The Dumps (Sybase only)
   -Y             force std output prints (wont print to stdout if cron job)

   Dump a database to the specified directory in with the name
      {SERVER}.{DATABASE}.dbdump.yyyymmdd.hhmmss
   or (if striped)
      {SERVER}.{DATABASE}.dbdump.yyyymmdd.hhmmss.S?\n";
   return "\n";
}

$| =1;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
my $c=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List\n")
	unless getopts('J:cfU:D:S:P:o:dhtn:e:v:l:s:X:YM:z');

use CommonFunc;

%CONFIG=read_configfile( -job=>$opt_J , -srv=>$opt_S );
$CONFIG{COMMAND_RUN}=$c;

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  $opt_J,
            -subsystem =>$opt_J,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

use Sys::Hostname;
info( "dump_database.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

logdie usage("Must pass sa password\n" )	  		unless defined $opt_P;
logdie usage("Must pass server\n" )					unless defined $opt_S;
logdie usage("Must pass sa username\n" )	  		unless defined $opt_U;
logdie usage("Must pass output directory\n" )   unless defined $opt_o;
logdie usage("Must pass server type with -M\n") unless defined $opt_M;
if( is_nt() ) {
	$opt_M="ODBC" unless defined $opt_M;
} else {
	$opt_M="Sybase" unless defined $opt_M;
}

logdie usage("Cant Pass -f and -t\n" )    if defined $opt_f and defined $opt_t;
$log_msgs_to_stdout = 1 if defined $opt_Y;

$opt_o=~s/\s+$//;
$opt_o.="/" unless $opt_o =~ /\/$/;

$opt_s=today() unless defined $opt_s;
debug( "DateStamp For Dumps will be $opt_s\n" );

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

info("dump_database.pl $SHTVER $NL");

#
# CONNECT
#
debug( 'Connecting to '.$opt_M." Database\n");
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -type=>$opt_M, -debug=>$opt_z) or logdie "Cant connect to $opt_M $opt_S as $opt_U\n";

dbi_set_mode("INLINE");
dbi_set_web_page($opt_h);

# GET DB INFO
my($whereclause)="";
if( defined $opt_D ) {
   foreach ( split(/\|/,$opt_D)) {
      if( $whereclause eq "" ) {
	 		$whereclause="and ( name like \'$_\'";
      } else {
	 		$whereclause .= " or name like \'$_\' ";
		}
   }
   $whereclause.=")" if $whereclause ne "";
}

# find out if its a sql server or not
my($server_type)="SQLSERVER";
foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
	($server_type)=dbi_decode_row($_);
}

info("Server Type=$server_type\n");

#
# GET DATABASE INFO - AND SETUP %badreason TO HASH OF DB's NOT TO DUMP
#
my(@databaselist,%badreason,%si,%tl);

my( $status1bits, $status2bits );
if( $server_type eq "SQLSERVER" ) {
	$status1bits = 32+64+128+256+512+1024+4096+32768;
	$status2bits = 0x0000;
} else {
# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
#   30= 20 (offline until recovery) + 10 (offline)
	$status1bits = 0x1120;
	$status2bits = 0x0030;
}

my($query)= "select name,si=status&4, tl=status&8,ol=status2&16
					from master..sysdatabases
					where status&$status1bits = 0
					and status2&$status2bits =0 $whereclause";

debug("Retrieving Databases:$query\n");

foreach( dbi_query(-db=>"master", -query=>$query )) {
	my @dat=dbi_decode_row($_);
	push @databaselist,$dat[0];

	$si{$dat[0]}=1 if $dat[1] == 4;
	$tl{$dat[0]}=1 if $dat[2] == 8;

	# if tran dump not if trunc. log on checkpoint
	$badreason{$dat[0]}     = "trunc. log on chkpt set"
		if defined $opt_t and $dat[2]==8;

	$badreason{$dat[0]} = "offline" if $dat[3]==16;

	info "Warning: Select Into Erroneously Set In Database ",$dat[0],$NL,
"\tThis will not impact your backup but you should not set select into if",$NL,
"\ttruncate log on checkpoint isn't set or your transaction log dumps may",$NL,
"\tfail to load.",$NL,$NL
		if $dat[1]==4 and $dat[2]!=8
		and $dat[0] ne "master"
		and $dat[0] ne "model"
		and $dat[0] ne "tempdb"
		and $dat[0] ne "sybsystemprocs"
		and $dat[0] ne "sybsystemdb"
		and $dat[0] ne "sybsecurity"
		and $dat[0] ne "sybsyntax";

}
info "No Databases Returned From : select name,si=status&4,tl=status&8 from master..sysdatabases where status & 0x1120 = 0 and status2&0x0030=0 $whereclause" if $#databaselist < 0;

$badreason{'model'}	     = "model is never normally dumped"
	unless $opt_D eq "model";
$badreason{'tempdb'}	    = "database is never ever dumped";
$badreason{'sybsyntax'} = "database is never normally dumped"
	unless $opt_D eq "sybsyntax";

#
# MARK DATABASES WITH DATA & LOG ON SAME DEVICE
#
if( $server_type eq "SYBASE" and defined $opt_t ) {
	foreach( dbi_query(-db=>"master",-query=>"select distinct db_name(dbid) from master..sysusages where segmap&7=7")) {
		$_ = dbi_decode_row($_);
		$badreason{$_} = "Log and Data on the same device."
			unless defined $badreason{$_};
	}
}

#if( defined $opt_d ) {
	#info( "Printing Configuration Variables \n" );
	#info( "DEBUG: Job $opt_J\n") ;
	#foreach ( sort keys %CONFIG ) {
		#info( "DEBUG: KEY=$_ VAL={".$CONFIG{$_}."} $NL") ;
	#}
#}

#
# MARK DATABASE THAT ARE TRANFERED TO
#
#if( defined $CONFIG{XFER_TO_SERVER}
#and defined $CONFIG{XFER_TO_DB}
#and defined $CONFIG{XFER_FROM_DB}
#and defined $CONFIG{XFER_TO_DIR} ) {
	#foreach ( split(/\|/,$CONFIG{XFER_TO_DB})) {
		#$badreason{$_} = "Not Dumped As It Will Be Loaded."
			#unless defined $badreason{$_};
	#}
#}

#
# PRINT REASONS IF IN DEBUG MODE
#
if( defined $opt_d ) {
	foreach (@databaselist) {
		infof("Database:%30.30s Status=%s %s",$_,$badreason{$_},$NL)
			if defined $badreason{$_};
	}
}

my($dumps_that_have_failed)="";
my($num_dumps_done)=0;
info($NL."Starting Dumps of Databases (".join(",",@databaselist).")".$NL.$NL);
foreach my $db (@databaselist) {

	my($num_stripes)=$opt_n;

	$num_stripes=undef if $db eq "master" or $db eq "model"
								or $db eq "tempdb"
								or $db eq "sybsystemprocs"
								or $db eq "sybsystemdb"
								or $db eq "sybsecurity"
								or $db eq "sybsyntax";

	if( defined $badreason{$db} ){
		infof("Database %-30.30s : %s %s %s",$db,$badreason{$db},$NL,$NL);
		next;
	}

	logdie "SEVERE ERROR: Cant Use Database $db\n" unless dbi_use_database($db);

	my($outfile);
	if( defined $opt_t ) {
		$outfile = $opt_o.$opt_S.".".$db.".logdump.".$opt_s;
	} else {
		$outfile = $opt_o.$opt_S.".".$db.".dbdump.".$opt_s;
	}

	info "<h2>" if defined $opt_h;
	if( defined $num_stripes and ! defined $opt_t ) {
		info "Database $db (Backing Up To $num_stripes Stripes)",$NL;
		if( defined $opt_X ) {
			info "Sybase Internal Compression Level $opt_X",$NL;
		} else {
			info "Not Using Sybase Internal Compression Level", $NL
				if $server_type =~ /SYBASE/i;
		}
		info "Output File $outfile ",$NL;
	} else {
		if( defined $opt_t ) {
			info "Database $db (Dumping Transaction Log)",$NL;
		} else {
			info "Database $db",$NL;
		}
		if( defined $opt_X ) {
			info "Sybase Internal Compression Level $opt_X",$NL;
		} else {
			info "Not Using Sybase Internal Compression Level",$NL
				if $opt_M =~ /SYBASE/i;
		}
	}
	info "</h2>" if defined $opt_h;
	my($command);
	if( defined $opt_c ) {
		if( $server_type eq "SQLSERVER" ) {
			$command="backup transaction $db with no_log\n";
		} else {
			$command="dump transaction $db with no_log\n";
		}
		info "Clearing Transaction Log\n";
		info "... $command\n";
		foreach( dbi_query(-db=>$db,-query=>$command, -no_results=>1) ) {
			$_ = dbi_decode_row($_);
			chomp;
			info "  > ".$_,"\n";
		}
	}
	my($compress_ext)="";
	$compress_ext="compress\:\:$opt_X\:\:" if defined $opt_X;
	if( defined $opt_t ) {
		if( $server_type eq "SQLSERVER" ) {
			$command="backup log $db\n to DISK\:=\'".$compress_ext.$outfile."\'";
		} else {
			$command="dump transaction $db\n to \"".$compress_ext.$outfile."\"";
		}
	} else {
		if( defined $num_stripes and $server_type eq "SYBASE" ) {
			if( $opt_v ) {
				# stripe it by devices
				#$opt_o."device_N/".$opt_S.".".$db.".dbdump.".$opt_s;
				my($devicenum)=1;
				$command="dump database $db\n to \"".$compress_ext.$opt_o."device_".$devicenum."/".$opt_S.".".$db.".dbdump.".$opt_s.".S1\"\n";
				my($cnt)=2;
				while( $cnt<($num_stripes+1) ) {
					$devicenum=int( ($cnt+1) / $opt_v);
					$command.=" stripe on \"".$compress_ext.$opt_o."device_".$devicenum."/".$opt_S.".".$db.".dbdump.".$opt_s.".S".$cnt."\"\n";
					$cnt++;
				}
			} else {
				$command="dump database $db\n to \"".$compress_ext.$outfile.".S1\"\n";
				my($cnt)=2;
				while( $cnt<($num_stripes+1) ) {
					$command.=" stripe on \"".$compress_ext.$outfile.".S".$cnt."\"\n";
					$cnt++;
				}
			}
		} elsif( $server_type eq "SQLSERVER" ) {
			$outfile=~s/\//\\/g;
			$command="backup database $db to disk=\'$outfile\' with format";
		} else {
			$command="dump database $db\n to \"".$compress_ext.$outfile."\"\n";
		}
	}
	$num_dumps_done++;
	info( "Command=>$command".$NL."=================".$NL);
	my($is_ok)="";
	my($start_time)=time;
	dbi_msg_ok(1000);
	foreach( dbi_query(-db=>$db,-query=>$command,-debug=>$opt_d) ) {
		my($d)=join(" ",dbi_decode_row($_));

		next if $d=~/^dbg: MESSAGE/;
		next if $d=~/^WARNING: In order to LOAD the master database, the SQL Server must run in single-user mode.  If the master database dump uses multiple volumes, you must execute sp_volchanged on another SQL Server at LOAD time in order to signal volume changes.$/;

		next if $d=~/Backup Server session id is:  \d+.  Use this value when executing the 'sp_volchanged' system stored procedure after fulfilling any volume change request from the Backup Server.$/;
		next if $d=~/section number 0001 mounted on disk file/;

		$d=~s/^Backup Server:\s//;
		chomp $d;
		$is_ok="(Complete)" if $d=~/DUMP is complete/;
		if( $server_type eq "SQLSERVER" ) {
			if( $d=~/BACKUP LOG successfully processed/
				or $d=~/Processed \d+ pages for database/  ) {
				$is_ok="(Complete)"
			} else {
				$is_ok="(Unknown)";
			};
		} else {
			if( $d=~/kilobytes DUMPed./ or $d=~/Dump phase number \d+ completed./ ) {
				info " > ".$d,"\n";
				next;
			}
		}

		info "  $is_ok> ".$d,"\n";
	}

	if( $is_ok eq "(Complete)" ) {
		$start_time=time-$start_time;
		my $minutes=int($start_time/60);
		$start_time-=60*$minutes;
		info "Backup of $db completed in $minutes minutes and $start_time secs.",$NL,$NL;
	} else {
		alert "Backup of $db has failed $is_ok",$NL,$NL;
		$dumps_that_have_failed.=" $db $is_ok";
	}
}

if( $num_dumps_done == 0 ) {
	info "No Databases Found To Dump\n";
}

dbi_disconnect();

if( $dumps_that_have_failed eq "" ) {
	info "Backup Procedure Completed Successfully".$NL;
	exit(0);
} else {
	if( defined $opt_J ) {
		logdie "Backup Procedure Failed For Job=$opt_J".$NL.$NL;
	} else {
		logdie "Backup Procedure Failed For Srv=$opt_S DB=$opt_D".$NL.$NL;
	}
}

__END__

=head1 NAME

dump_database.pl - utility to backup databases

=head2 DESCRIPTION

Backs up database or does a transaction log dump. Can be set for tran log dumps with -t or will, by default, perform
full database dumps. Places database dumps in DIRECTORY/$SERVER.$(database)_dbdump.yyyymmdd.hhmmss and tran log dumps into
DIRECTORY/$SERVER.$(database).logdump.yyyymmdd.hhmmss It will ignore databases as appropriate, following the following rules:

             1) It will not perfore a transaction log dump if "trunc. log on chkpt" set

             2) model,tempdb, and sybsyntax are not dumped unless specifically identified with -d option

             3) It will not dump transaction logs if data and log on the same device

It will also warn if user databases have select into set but do not have trunc. log on chkpt set.  The dump files will have a .tmp in them until the dump is completed, at which point they will be renamed with the .dbdump or .logdump extension if the -r option is specified.

=head2 USAGE

dump_database.pl -nnum_stripes -USA_USER -SSERVER -PSA_PASS -DDB_LIST -oDIRECTORY -tdhfc -X0-10 -MType

   -n Num stripes (.SX appendended to dump name)
   -v Num			use devices: Num dumps per subdirectory (device_#)
   -D DB_LIST     (pipe separated list with wildcards of databases)
   -o DIR         (output directory)
   -t             (Dump Transaction Log only)
   -f             (Full Dump - default)
   -M             Server Type for DBI connection String (ODBC, Sybase...)
   -d             (debug mode - for odbc errors use -z)
   -h             (html output)
   -e errorlog    (optional)
   -l sessionlog  (optional)
   -s time        (set time on dump files)
   -c             (clear tran log prior to dumping)
   -X             Compression Level On The Dumps (Sybase only)
   -Y             force std output prints (wont print to stdout if cron job)

   Dump a database to the specified directory in with the name
      {SERVER}.{DATABASE}.dbdump.yyyymmdd.hhmmss
   or (if striped)
      {SERVER}.{DATABASE}.dbdump.yyyymmdd.hhmmss.S?


=head2 MORE DESCRIPTION

Backs Up Databases.  Can be set for tran log dumps with -t or will, by default,
back up full database dumps.  Places databases in

	DIRECTORY/$SERVER.$(database)_dbdump.yyyymmdd.hhmmss

and tran log dumps into

	DIRECTORY/$SERVER.$(database).logdump.yyyymmdd.hhmmss

It will ignore databases as appropriate.  Specifically the ruls are as follows:

	Cant Tran Dump if "trunc. log on chkpt" set
	model,tempdb, and sybsyntax are not dumped unless specifically set
		with -d option
	Cant Tran Dump if data and log on the same device

It will also warn if user databases have select into set but do not have
trunc. log on chkpt set.

=cut

