#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

#use subs qw(logdie infof info warning alert);
use strict;
use Logger;
use Getopt::Std;
use File::Basename;
use DBIFunc;

use vars qw($opt_n $opt_U $opt_S $opt_P $opt_D $opt_d $opt_i $opt_d $opt_h $opt_t $opt_r %CONFIG);

# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed


sub usage
{
	print @_;
   print "kill_users_db.pl -USA_USER -SSERVER -PSA_PASS -Ddb -d ";
	return "\n";
}

$| =1;
my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
die usage("Bad Parameter List") unless getopts('U:D:S:P:d');

logger_init(-logfile=>  undef,
            -errfile=>  undef,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  undef,
            -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

logdie usage("Must pass sa password\n" )	   unless defined $opt_P;
logdie usage("Must pass server\n" )				unless defined $opt_S;
logdie usage("Must pass sa username\n" )	   unless defined $opt_U;

$opt_i=~s/\s+$//;

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

#
# CONNECT
#
info 'Connecting to sybase',$NL if defined $opt_d;
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
	or logdie "Cant connect to $opt_S as $opt_U\n";

dbi_set_mode("INLINE");

my($db)=$opt_D;

info "Getting Users in $db (server $opt_S)\n";
my($num_killed)=kill_users_in_db($opt_D);
my($total_killed)=$num_killed;
my($count)=0;
while( $count<5 and $num_killed>0 ) {
	sleep 2;
	$count++;
	$num_killed=kill_users_in_db($opt_D);
	$total_killed+=$num_killed;
}
logdie("ERROR: Cant Kill All Processes in $opt_D\n")
	if $count==5 and $num_killed>0;
info "$total_killed Users Killed\n";

dbi_disconnect();
exit(0);

sub kill_users_in_db
{
	my($db)= @_;
	my(@processes)=dbi_query(-db=>"master",-query=>"select spid
												from master..sysprocesses
												where dbid=db_id(\"$opt_D\")
												and \"$opt_D\" != \"master\"");

	return 0 if $#processes<0;

	info "Killing ",($#processes+1)," Users in $db\n";
	foreach( @processes ) {
		($_) = dbi_decode_row($_);
		info " -- killing user suid=$_\n";
		foreach (dbi_query(-db=>"master",-query=>"kill $_", -no_results=>1) ) {
			($_) = dbi_decode_row($_);
			alert "ERROR: $_\n";
		}
	}
	return ($#processes+1);
}

__END__

=head1 NAME

kill_users_db.pl - utility to load databases

=head2 USAGE

 kill_users_db.pl -USA_USER -SSERVER -PSA_PASS -Ddb -d

=head2 DESCRIPTION

Kills all users in a database.

=cut
