#!/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use Getopt::Long;
use File::Basename;
use MlpAlarm;
use DBIFunc;
use Net::myFTP;
use CommonFunc;
use Logger;
use Repository;
use Sys::Hostname;

use vars qw($opt_J $opt_Y $opt_n $opt_U $opt_S $opt_P $opt_D $opt_d $opt_i $BATCH_ID
$opt_h $opt_t $opt_r $DELETE %CONFIG $opt_X $opt_M $opt_O $opt_z $opt_R $TESTRUN $RENAME $SKIPPRIOR
$FILEPATERN $SKIPDONE $MICROSOFT $HOSTNAME $UNIXLOGIN $UNIXPASSWORD $UNIXMETHOD $FULL $SOURCESERVER $COMPRESSED);

# Copyright (c) 2003 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub usage
{
	print @_;
   print "load_all_tranlogs.pl
   -USER=SA_USER      -SERVER=SERVER      -PASSWORD=SA_PASS
   -DATABASE=db       -INDIR=file_root    -TYPE=Type
   -DESTDIR=dest_dir  -JOB=job            -HOSTNAME --SKIPPRIOR
   -UNIXLOGIN=l       -DELETE             -UNIXPASSWORD=l
   -UNIXMETHOD=SSH|RSH|FTP		-STRIPES=num
   -SOURCESERVER - (for reporting only - this is the source server)
   -FULL   - full database loads (only on microsoft for now)
   -DEBUG debug mode  --FILEPATERN=pat --COMPRESSED (sybase compression)
   -SKIPDONE skip files with .done extension
   -MICROSOFT microsoft sql svr format (see below)
   -RENAME rename files to .done extension
   -INDIR identifies a directory / files as seen from this program
   -DESTDIR identifies the same directory/files as seen by remote server
		(or pass HOSTNAME)

   Will Load, in order, tranlogs that have the following name format
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss
   or {DB}_tlog_yyyymmddhhmm.TRN (sql server format - if --MICROSOFTpassed)\n";
	return "\n";
}

$| =1;
my($curdir)=cd_home();
my($system_state)="COMPLETED";
my($system_msg)="(failed)";

die usage("") if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);

die usage("Bad Parameter List") unless GetOptions(
        "SERVER=s"  	=> \$opt_S,
        "SOURCESERVER=s"  	=> \$SOURCESERVER,
        "USER=s" 		=> \$opt_U ,
        "DATABASE=s"   => \$opt_D ,
        "PASSWORD=s"   => \$opt_P ,
        "FULL"   => \$FULL ,
        "JOB=s"   		=> \$opt_J ,
        "TESTRUN"			=> \$TESTRUN,
        "HOSTNAME=s"	=> \$HOSTNAME,
        "UNIXLOGIN=s"	=> \$UNIXLOGIN,
        "UNIXMETHOD=s"	=> \$UNIXMETHOD,
        "FILEPATERN=s"	=> \$FILEPATERN,
        "SKIPPRIOR"	=> \$SKIPPRIOR,
        "COMPRESSED"	=> \$COMPRESSED,
        "DELETE"			=> \$DELETE,
        "UNIXPASSWORD=s"	=> \$UNIXPASSWORD,
        "INDIR=s"    	=> \$opt_i ,
        "TYPE=s"    	=> \$opt_M ,
        "STRIPES=s"   	=> \$opt_n ,
        "MICROSOFT"   	=> \$MICROSOFT ,
        "BATCH_ID=s"	=> \$BATCH_ID,
        "SKIPDONE"    	=> \$SKIPDONE ,
        "RENAME"			=> \$RENAME,
        "DESTDIR=s"		=> \$opt_R,
			"DEBUG"        => \$opt_d );


$BATCH_ID="LoadAllTranlogs" unless defined $BATCH_ID;

#logger_init(-logfile=>  undef,
            #-errfile=>  undef,
            #-database=> $opt_D,
            #-debug =>   $opt_d,
            #-system =>  $opt_J,
            #-subsystem =>$opt_J,
            #-command_run => $CONFIG{COMMAND_RUN},
            #-mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            #-mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            #-success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

sub pgheader
{
	my($VERSION,$SHTVER)=get_version();
	info( "load_all_tranlogs.pl $VERSION : started at ".localtime(time)."\n" );
	info( "run on hostname=",hostname(),"\n" );
	info( "Reading Configfile For Job $opt_J\n" );
	return ($VERSION,$SHTVER);
}

if( defined $opt_J ) {
			%CONFIG=read_configfile( -job=>$opt_J );
			logger_init(-logfile=>  undef,
            -errfile=>  undef,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  $opt_J,
            -subsystem =>$opt_J,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

			#
			# GET LOG SHIPPING VARIABLES
			#
        $opt_S=$CONFIG{XFER_TO_SERVER} 	unless defined $opt_S;
        $opt_i=$CONFIG{XFER_TO_DIR} 	unless defined $opt_i;
        $opt_R=$CONFIG{XFER_TO_DIR_BY_TARGET}||$CONFIG{XFER_TO_DIR} unless defined $opt_R;
        $HOSTNAME=$CONFIG{XFER_TO_HOST} unless $HOSTNAME;
        $SOURCESERVER=$CONFIG{DSQUERY} 	unless defined $SOURCESERVER;
		  #		use Data::Dumper;
			#die "NO HOSTNAME - ie. NO XFER_TO_HOST ".Dumper(\%CONFIG) unless $HOSTNAME;
			if( $HOSTNAME ) {
				($UNIXLOGIN,$UNIXPASSWORD)=get_password(-type=>"unix",-name=>$HOSTNAME)
					unless $UNIXLOGIN or $UNIXPASSWORD;
				my(%args)=get_password_info(-type=>"unix",-name=>$HOSTNAME);
				if( is_nt() ) {
					$UNIXMETHOD=$args{WIN32_COMM_METHOD} if is_nt();
				} else {
					$UNIXMETHOD=$args{UNIX_COMM_METHOD};
				}
				die "COMM_METHOD NOT DEFINED for SERVER $HOSTNAME\n" unless $UNIXMETHOD;

				if( $UNIXMETHOD eq "NONE" ) {	# hmm cant talk
					print "host $HOSTNAME: METHOD=NONE - skipping\n";
					next;
				}
			}

			$COMPRESSED=1 if $CONFIG{COMPRESSION_LEVEL} =~ /^\d$/	and $CONFIG{COMPRESSION_LEVEL} ne "0";

        if( !  defined $CONFIG{XFER_TO_SERVER} ) {
        	foreach (sort keys %CONFIG) { print "$_ $CONFIG{$_} \n"; }
        	logdie( "XFER_TO_SERVER does not exist\n" );
        }

        my($server_type)="SYBASE";
        ($opt_U,$opt_P)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sybase") if ! defined $opt_U;
        if( defined $opt_U ) {
	        	if( is_nt() ) {
	        			$opt_M="ODBC" unless defined $opt_M;
	        	} else {
	        			$opt_M="Sybase" unless defined $opt_M;
	        	}
        } else {
	        	($opt_U,$opt_P)=get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sqlsvr");
	        	$opt_M="ODBC";
	        	$server_type="SQLSERVER";
	        	#$MICROSOFT=1;
        }
     #   $UNIXLOGIN=$opt_U unless $UNIXLOGIN;
       # $UNIXPASSWORD=$opt_P unless $UNIXPASSWORD;
        $RENAME=1;
        $SKIPDONE=1;
} else {
	logger_init(-logfile=>  undef,
            -errfile=>  undef,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  $opt_S,
            -subsystem =>$opt_D,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );
}

	my($VERSION,$SHTVER) = pgheader();
	print "Arguments For load_all_tranlogs.pl set to:
			-SERVER           => $opt_S,
			-SOURCESERVER     => $SOURCESERVER,
			-USER             => $opt_U ,
			-DATABASE         => $opt_D ,
			-PASSWORD         => XXX,
			-FULL             => $FULL ,
			-JOB              => $opt_J ,
			-HOSTNAME         => $HOSTNAME,
			-UNIXLOGIN        => $UNIXLOGIN,
			-UNIXMETHOD       => $UNIXMETHOD,
			-DELETE           => $DELETE,
			-UNIXPASSWORD     => XXX,
			-COMPRESSED       => $COMPRESSED,
			-INDIR            => $opt_i ,
			-TYPE             => $opt_M ,
			-STRIPES          => $opt_n ,
			-DEBUG            => $opt_d ,
			-MICROSOFT        => $MICROSOFT ,
			-SKIPDONE         => $SKIPDONE ,
			-RENAME           => $RENAME,
			-DESTDIR          => $opt_R\n"; # if defined $opt_d or $TESTRUN;

	logdie usage("Must pass sa password\n" )	   unless defined $opt_P;
	logdie usage("Must pass server\n" )				unless defined $opt_S;
	logdie usage("Must pass sa username\n" )	   unless defined $opt_U;
	logdie usage("Must pass input root file\n" ) unless defined $opt_i;
	logdie usage("Must pass server type with -M\n") unless defined $opt_M;
	logdie usage("Must pass directory as seen by target server --DESDIR or --HOSTNAME=\n")
		unless defined $opt_R or defined $HOSTNAME;
	logdie "Type must be Sybase or ODBC"
		unless $opt_M eq "Sybase" or $opt_M eq "ODBC";

	$opt_i=~s/\s+$//;

	print "Saving batch info to Alarm Database\n"
		unless defined $FULL;
	if( defined $SOURCESERVER ) {
		MlpBatchStart(-monitor_program=>$BATCH_ID,-system=>$SOURCESERVER, -subsystem=>"Tran Log Load",-batchjob=>1)
			unless defined $FULL;
	} else {
		MlpBatchStart(-monitor_program=>$BATCH_ID,-system=>$opt_S, -subsystem=>"Tran Log Load",-batchjob=>1)
			unless defined $FULL;
	}

	info "Connecting to $opt_M Server $opt_S\n" if defined $opt_d;
	dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P, -type=>$opt_M, -debug=>$opt_d)
		or logdie "Cant connect to $opt_M $opt_S as $opt_U\n";

	# dbi_msg_exclude(42000);
	dbi_msg_ok(1000);
	dbi_set_mode("INLINE");

#
# BUILD DATABASE LIST - Tran Dump only appropriate databases
#
my($whereclause)="";
if( defined $opt_D ) {
   foreach ( split(/[\,\|]/,$opt_D)) {
      if( $whereclause eq "" ) {
         $whereclause="and ( name like \'$_\'";
      } else {
         $whereclause .= " or name like \'$_\' ";
      }
   }
   $whereclause.=")" if $whereclause ne "";
}

debug("processing ignore_list\n");
my(%ignore);
$CONFIG{IGNORE_LIST} =~ s/\s//g;
foreach ( split(/\|/,$CONFIG{IGNORE_LIST}) ) { $ignore{$_} = 1; }

info "Selecting Databases\n";
my($q)="select name from sysdatabases where 1=1 $whereclause";
debug("$q\n");
my(@d);
my(%tran_db_hash_lc);
foreach ( dbi_query(-db=>"master",-query=>$q ) ) {
	my($db)= dbi_decode_row($_);
	next if $db eq "master" or $db eq "sybsystemprocs" or $db eq "tempdb" or $db eq "sybsystemdb" or $db eq "model" or defined $ignore{$db};
	push @d,$db;
	$tran_db_hash_lc{lc($db)}=1;
};
info "Database List (",join(" ",@d),")\n";
debug( "database list=(",join(" ",sort keys %tran_db_hash_lc),")\n");

# ok Find The Files
my($rootdir, $root);
my(@dirlist);
my(%baddb);
my(%lastgoodtime_bydb,%lasttime_bydb, %firstbadmsg_bydb, %firstbadtime_bydb, %firstbadfile_bydb);
my(%laststate_bydb,%lastmsg_bydb);
my($ftpcon);
my(%config);
$config{Debug} = $opt_d;
$config{Timeout}=90000;
if( defined $HOSTNAME ) {
	info "Grabbing The File List Via FTP from $HOSTNAME dir=$opt_i\n";
  	$ftpcon= Net::myFTP->new($HOSTNAME,METHOD=>$UNIXMETHOD,%config)
                 or logdie "Failed To Create FTP to $HOSTNAME Object $@";
   logdie( "Failed To Connect To $HOSTNAME" ) unless $ftpcon;
   my($r,$err)=$ftpcon->login($UNIXLOGIN,$UNIXPASSWORD);
   logdie( "Cant Login To Server As $UNIXLOGIN : $err \n") unless $r;
   $ftpcon->cwd($opt_i)
         or logdie( "Cant cd to $opt_i  : $! \n");
	info "Logged in via FTP to $HOSTNAME\n";
	$opt_R=$opt_i;
	@dirlist = $ftpcon->ls();
	info(($#dirlist+1)," Files Read Via FTP From Directory on $HOSTNAME\n");

	$rootdir=$opt_i;
	$root="";

	if( defined $SKIPDONE ) {
		@dirlist= grep((!/^\./ and ! /.done/ and ! /.gz/),@dirlist);
		info(($#dirlist+1)," Files after removing .done and .gz \n");
		@dirlist= grep( /logdump.\d+.\d+/,@dirlist);
		info(($#dirlist+1)," Files after logdump.\\d+.\\d+\n");
	} else {
		@dirlist= grep((!/^\./ and ! /.gz/ and /logdump.\d+.\d+/),@dirlist);
		info(($#dirlist+1)," Files after ! .gz and logdump.\\d+\\d+\n");
	}
} else {
	debug("Hostname not defined...\n");
	if( -d $opt_i ) {
		$rootdir=$opt_i;
		$root="";
		info "Grabbing The File List from dir=$rootdir on $HOSTNAME\n";
	} else {
		$rootdir=dirname($opt_i);
		$root=basename($opt_i);
		info "Grabbing The File List from dir=$rootdir fileroot=$root\n";
	}
	$config{IS_REMOTE}="n";
  	$ftpcon= Net::myFTP->new($HOSTNAME,METHOD=>$UNIXMETHOD,%config)
       or logdie "Failed To Create FTP to $HOSTNAME Object $@";
	debug( "Dir=$rootdir FileRoot=$root\n" );

	opendir(DIR,$rootdir) or die("Can't open directory $rootdir : $!\n");
	@dirlist=readdir(DIR);
	info(($#dirlist+1)." Files Read In Directory $rootdir\n");
	if( defined $SKIPDONE ) {
		my(@files)= grep((!/^\./ and ! /.done/ and ! /.gz/ ),@dirlist);
		info(($#files+1)." Files Without .done and .gz Extensions\n");
		if( $#files<0 ) {
			info "All Files Have Been Loaded\n" ;
			MlpBatchDone() unless defined $FULL;
			exit(0);
		}

#		if( defined $MICROSOFT ) {
			if( defined $FULL ) {
#				debug("-MICROSOFT defined -SKIPDONE defined FULL defined\n");
				@dirlist= grep(( /$root/ and /_db_\d+.dump/i),@files);
				info(($#dirlist+1)." Files matching $root and _db_\\d+.dump\n")
					if defined $opt_d;
			} else {
#				debug("-MICROSOFT defined -SKIPDONE defined FULL not defined\n");
				@dirlist=      grep(( /$root/ and /tlog_\d+.trn/i),@files);
				push @dirlist, grep(( /$root/ and /logdump.\d+.\d+/),@files);
				info(($#dirlist+1)." Files matching $root and tlog_\\d+.trn or logdump.\\d+\\d+\n");
					#if defined $opt_d;
			}
#		} else {
#			debug("-MICROSOFT not defined -SKIPDONE defined\n");
#			@dirlist= grep((!/^\./ and /$root/ and /logdump.\d+.\d+/),@dirlist);
#			info(($#dirlist+1)." Files matching $root and !.done and !.gz and logdump.\\d+.\\d+\n")
#					if defined $opt_d;
#		}
	} else {
		info( "SKIPDONE not specified... processing .done files as well\n");
		my(@files)= grep((!/^\./ and ! /.gz/ ),@dirlist);
		info(($#dirlist+1)." Files Without .gz Extensions\n");
		#if( defined $MICROSOFT ) {
			debug("-MICROSOFT defined\n");
			@dirlist= grep((/$root/ and /tlog_\d+.trn/i),@files) unless defined $FULL;
			@dirlist= grep(( /$root/ and /_db_\d+.dump/i),@files) if defined $FULL;
		#} else {
			push @dirlist, grep((/$root/ and /logdump.\d+.\d+/),@files) unless defined $FULL;
		#}
	}
	closedir(DIR);
}
@dirlist= grep((/$FILEPATERN/),@dirlist) if $FILEPATERN;
if( $#dirlist<0 ) {
	print "No Files Found In Directory $rootdir Matching Spec\n";
	MlpBatchDone() unless defined $FULL;
	exit(0);
}
#if( $TESTRUN ) {
#	#foreach (sort keys %CONFIG) { print "$_ $CONFIG{$_} \n"; }
#	die "dONE";
#}

info( "Loading Dataserver - ",($#dirlist+1)," Unapplied Log Files Found\n" );

my($curdbmsg,$curdb)=("\n","");
my($lastping)=time;
my($next_db)="";
my($prior_count)=0;
foreach my $file ( sort @dirlist ) {
	my($srv,$db,$day,$tm);
		if( defined $FULL ) {
			next unless $file =~ /(\w+)_db_(\d\d\d\d\d\d\d\d)(\d+)/;
			($db,$day,$tm)=($1,$2,$3);
		} else {
			if( $file =~ /(\w+)_tlog_(\d\d\d\d\d\d\d\d)(\d+)/ ) {
				($db,$day,$tm)=($1,$2,$3);
				$srv="";
			} elsif( $file =~ /(\w+).(\w+).logdump.(\d+).(\d+)/ ) {
				($srv,$db,$day,$tm)=($1,$2,$3,$4);
			} else {
				next;
			}
		}

	if( $db eq $next_db and ! $SKIPPRIOR ) {
		info( "   (prior error) Skipping $file\n" );
		next;
	}

	if( time - $lastping > 60 ) {
		MlpBatchRunning() unless defined $FULL;
		$lastping=time;
	}

	if( ! defined $tran_db_hash_lc{lc($db)} ) {
		# debug( "DB - $db for $file - ignored\n" );
		next;
	};

	my($server_file)=$opt_R."/".basename($file);

	if( $curdb ne $db ) {
		info($curdbmsg) unless $curdb eq "";
		$curdb=$db;
		$curdbmsg="\n";
	}

	if( defined $baddb{$db} ) {
		info "$db $day $tm (auto skipped - $baddb{$db})\n";
		next;
	}

	# ok... here it is
	info "$db $day $tm \n" if defined $opt_d;
	my($command)="$curdir/load_database.pl -Usa -S$opt_S -P$opt_P -i$server_file -M$opt_M -D$db -sk -O";

	$command.=" -X" if $COMPRESSED;
	$command.=" -t" unless defined $FULL;
	# info( $command,"\n" ) if defined $opt_d;
	# info( "(dbg)",$command,"\n" );

	# double check here file existence
	# if( ! -r "$rootdir/$file" ) {
		# info( "> ".$rootdir."/".$file." : Does Not Exist... Skipping\n");
	# }

	my(@rc)=run_cmd(-cmd=>$command, -print=>$opt_d);

	if( defined $opt_d ) {
		info( "(dbg) cmd completed\n" );
		foreach (@rc) {
			info "returned $_\n";
		}
	}

	my($load_ok)=0;
	my($file_not_found)=0;
	foreach (@rc) {
		next if /^load_database.pl v/;
		next if /^load_database.pl :/;
		next if /^run on hostname=/;
		next if /^NO MAIL/i or /^\s*$/;
		next if /3137:/ or /3479:/;
		chomp;
		chomp;
		info(">".$_."\n");
		if( /: file not found/ ) {
			$file_not_found=2 if $file_not_found==0;
			next;
		}
		$next_db=$db if /\sNeeds LSN /;
		$file_not_found=1;
		$load_ok=1 if /\sGOOD\s/;
		$lastgoodtime_bydb{$db}=$day.".".$tm if /\sGOOD\s/;
		if( ! /\sGOOD\s/ and ! defined $firstbadtime_bydb{$db} ) {
			$firstbadtime_bydb{$db}=$day.".".$tm ;
			$firstbadfile_bydb{$db}=$file;
			$firstbadmsg_bydb{$db}=$_;
		}
		$lasttime_bydb{$db}   =$day.".".$tm;
		$laststate_bydb{$db} = $load_ok;
		$lastmsg_bydb{$db}   = $_;
	}

	if( $load_ok == 1 and defined $DELETE and $file!~/.done$/ ) {
		info("delete $rootdir/$file")  if defined $opt_d;
		my($rc)=$ftpcon->delete($rootdir."/".$file) ;
	} elsif( $load_ok == 1 and defined $RENAME and $file!~/.done$/ ) {
		info("rename $rootdir/$file, $rootdir/$file.done\n")  if defined $opt_d;
		my($rc)=$ftpcon->rename($rootdir."/".$file, $rootdir."/".$file.".done") ;
	} else {
		info("LoadOk=$load_ok File=$file\n") if defined $opt_d;
	}
	save_db_state( $db ) unless $file_not_found==2;
}
info($curdbmsg) unless $curdb eq "";
info "Transaction Log Recovery Complete\n\n";

info "Printing Completion Messages\n";
#foreach (sort keys %baddb) { info( "Bad db:".$baddb{$_}."\n" ); }
foreach (sort keys %firstbadtime_bydb) {
	info( $firstbadmsg_bydb{$_}."\n" );
}

# foreach (keys %lasttime_bydb) {
	# save_db_state( $_ );
# }

info "Saving Completion Heartbeat\n";
if( $system_state eq "COMPLETED" ) {
	MlpBatchDone() unless defined $FULL;
} else {
	MlpBatchErr(-message=>$system_msg) unless defined $FULL;
}

dbi_disconnect();
print "load_all_tranlogs.pl: Program Completed at ".localtime(time)."\n";
exit(0);

sub	save_db_state {
	my($db)=@_;
	my($state,$msg);
	if( $laststate_bydb{$db} != 1 ) {
		$state="ERROR" ;
		$system_state="ERROR";
		if( ! defined $lastgoodtime_bydb{$db} ) {
			if( ! $firstbadtime_bydb{$db} ) {
				$msg= "Couldnt Load - Msg=".$lastmsg_bydb{$db};
			} else {
				$msg= "Bad File Time=$firstbadtime_bydb{$db} Msg=".$lastmsg_bydb{$db};
			}
		} else {
			$msg= "LastGood=$lastgoodtime_bydb{$db} LastFile=$lasttime_bydb{$db} Msg=$lastmsg_bydb{$db}";
		}
		$system_msg.="{ db=$db bad file\@$firstbadtime_bydb{$db} }\n"
			unless $system_msg=~/$db bad file/;
	} else {
		$state="COMPLETED";
      # $msg = "Logs Restored Until " .substr($lastgoodtime_bydb{$db} ,0,2).":".substr($lastgoodtime_bydb{$db} ,2,2)." at ".scalar(localtime(time));
		# my($tm)=$lastgoodtime_bydb{$db};	# yyyymmdd.hhmmss
		my($tm)=substr($lastgoodtime_bydb{$db},4,2);
		$tm.="/".substr($lastgoodtime_bydb{$db},6,2);
		$tm.=" ".substr($lastgoodtime_bydb{$db},9,2);
		$tm.=":".substr($lastgoodtime_bydb{$db},11,2);

#MlpSetBackupState();
#-system
#-dbname => $db,
#    	-last_fulldump_time}).",".
#        -last_fulldump_file}).",".
#        -last_fulldump_file}).",".
#        -last_fulldump_lsn}).",".
#        -last_trandump_time}).",".
#        -last_trandump_file}).",".
#        -last_trandump_lsn}).",".
#        -last_truncation_time}).",".
#        -last_fullload_time}).",".
#        $connection->quote($args{-last_fullload_file}).",".
#        $connection->quote($args{-last_fullload_lsn}).",".
#        $connection->quote($args{-last_tranload_time}).",".
#        $connection->quote($args{-last_tranload_file}).",".
#        $connection->quote($args{-last_tranload_lsn});

      $msg = "Logs Restored Until " .$tm." at ".scalar(localtime(time));
	};
	if( ! defined $FULL ) {
		if( defined $opt_d ) {
			info( "DB=$db State=$state (".$laststate_bydb{$db}.") Msg=$msg\n") unless $state eq "COMPLETED";
		}

		MlpHeartbeat(-monitor_program   =>  $BATCH_ID,
      	-system            =>      $opt_S,
      	-subsystem         =>      "$db Log Restore",
      	-state             =>      $state,
      	-message_text      =>      $msg,
			# -batchjob=>1
		);
	}
	info( "Save State Completed\n" ) if defined $opt_d;
}

sub run_cmd
{
   my(%OPT)=@_;
   my(@rc);
   chomp $OPT{-cmd};

   $OPT{-cmd} .= " 2>&1 |" unless defined $ENV{WINDIR};
   $OPT{-cmd} .= " |" 		    if defined $ENV{WINDIR};
   $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;

   my $cmd_no_pass=$OPT{-cmd};
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   info("(".localtime(time).") ".$cmd_no_pass."\n") if  defined $OPT{-print};
   open( SYS2STD,$OPT{-cmd} ) || logdie("Unable To Run $OPT{-cmd} : $!\n");
   while ( <SYS2STD> ) {
      chop;
      push @rc, $_;
   }
   close(SYS2STD);
	return @rc;
}


__END__

=head1 NAME

load_all_tranlogs.pl - Load Transaction Logs In Mass

=head2 SYNOPSIS

This is the primary driver to load more than one transaction log.  It applies file
filters and runs load_database.pl on each file that survives the filtering in order.
It renames the files to append .done to the ones applied.

=head2 USAGE

load_all_tranlogs.pl -JJOBNAME

or

load_all_tranlogs.pl
   -USER=SA_USER      -SERVER=SERVER      -PASSWORD=SA_PASS
   -DATABASE=db       -INDIR=file_root    -TYPE=Type
   -DESTDIR=dest_dir  -JOB=job            -HOSTNAME --SKIPPRIOR
   -UNIXLOGIN         -DELETE             -UNIXPASSWORD -STRIPES
   -SOURCESERVER - (for reporting only - this is the source server)
   -FULL   - full database loads (only on microsoft for now)
   -DEBUG debug mode  --FILEPATERN=pat --COMPRESSED (sybase compression)
   -SKIPDONE skip files with .done extension
   -MICROSOFT microsoft sql svr format (see below)
   -RENAME rename files to .done extension
   -INDIR identifies a directory / files as seen from this program
   -DESTDIR identifies the same directory/files as seen by remote server
		(or pass HOSTNAME)

   Will Load, in order, tranlogs that have the following name format
      {SERVER}.{DATABASE}.dbdump.yyyymmdd-hhmmss
   or {DB}_tlog_yyyymmddhhmm.TRN (sql server format - if --MICROSOFTpassed)

=head2 DESCRIPTION

Works on standard SQL server and My standard file naming conventions.
If your files are not using standard naming (yyyymmdd.hhmmss) then that
needs to be looked at.

=head2 OTHER NOTES

If you get in a situation where your files are jumbled, just run with --SKIPPRIOR.  This
option will stop the check that skips logs once an error is found.  All logs will be applied
not in order!  This can be used to unjumble tran logs when there are ordering issues.

=cut
