#!/apps/sybmon/perl/bin/perl
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

# use subs qw(logdie infof info);
use strict;
use Getopt::Std;
#use Sybase::DBlib;
use Logger;
use DBIFunc;
use File::Basename;
use vars qw($opt_U $opt_d $opt_S $opt_P $opt_D $opt_o $opt_i $opt_c $opt_r %CONFIG);


# Copyright (c) 1997-2000 By Edward Barlow
# All Rights Reserved
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed

sub Usage {
	my($str)= @_;
	chomp $str;

   die "$str
Usage: rebuild_index.pl -UUSER -PPASS -SSERVER -DDB [-oOUTFILE|-iINFILE] -c

Index Comparator.  Extracts indexes for a DB and then allows you to compare
the saved indexes to the indexes on your servers.

 -o : if outfile specified then creates a file with the indexes from the
      server.  This file can be used as an infile for further comparisons.
 -c : correct duplicate keys in indexes.  Deletes duplicate data.  One random
      row in unique indexes will be kept.
 -r : report only - this will report missing indexes
      as well as duplicate data but will not rebuild any indexes.

note that this program requires sp__revindex (extended stored proc library)
		\n";
}
$|=1;


Usage( "" ) if $#ARGV<0;
$CONFIG{COMMAND_RUN}=$0." ".join(" ",@ARGV);
Usage("Bad Argument")
	unless getopts('U:D:S:P:o:i:crd');
Usage("Must Pass User/Password/Server/Database")
	unless  defined $opt_U and  defined $opt_P
      and  defined $opt_S and  defined $opt_D;
Usage("Must Define Output or Input File")
	unless defined $opt_o or defined $opt_i;

my($curdir)=dirname($0);
chdir $curdir or die "Cant cd to $curdir: $!\n";

logger_init(-logfile=>  undef,
            -errfile=>  undef,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -system =>  undef,
            -subsystem =>undef,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{SUCCESS_MAIL_TO},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

use Sys::Hostname;
info( "rebuild_index.pl : run at ".localtime(time)."\n" );
info( "run on hostname=",hostname(),"\n" );

my($query_results)="";
my($index_name)="";
my($table_name)="";
my($columns)="";
my(%found_indexes);
my(%found_c_indexes);
my(%found_tables);

info "Connecting to sybase\n";
dbi_connect(-srv=>$opt_S,-login=>$opt_U,-password=>$opt_P)
	or logdie "Cant connect to $opt_S as $opt_U\n";

if( defined $opt_o ) {

	#
	# Create an output file... write sp__revindex to it
	#
	info( "Running sp__revindex in database $opt_D\n" );
	unlink $opt_o if -e $opt_o;
	open(OUTFILE,">".$opt_o) or logdie "Cant write to file $opt_o : $!\n";

	my(@indout)=dbi_query(-db=>$opt_D,-query=>"exec sp__revindex");
	foreach (@indout) {
		#($_)=dbi_decode_row($_);
		#chomp;
		my($x)=join("",dbi_decode_row($_));
		chomp $x;
		print OUTFILE $x,"\n";
	}
	close OUTFILE;
	info( "Output Completed\n" );
} else {

	my($sv_type,@db)=dbi_parse_opt_D($opt_D,1);
	logdie ("NO DATABASES FOUND MATCHING $opt_D") if $#db < 0 ;

	info "DATABASES: ",join(" ",@db),"\n" if $#db>=1;

	# you Have an input file... make indexes like what they should be
	dberrhandle("error_handler_new");
	dbmsghandle("message_handler_new");

	foreach my $datab (@db) {
		info "\nWORKING ON DATABASE $datab\n\n" if $#db>=1;

		undef %found_indexes;
		undef %found_c_indexes;
		undef %found_tables;

		foreach (dbi_query(-db=>$datab,-query=>"select name from sysobjects where uid=1")) {
			($_)=dbi_decode_row($_);
			$found_tables{$_}=1;
		}

		foreach (dbi_query(-db=>$datab,-query=>"select o.name,i.name,indid from sysindexes i, sysobjects o where o.id=i.id and o.uid=1")) {
			my(@rc)=dbi_decode_row($_);
			$found_indexes{$rc[0].".".$rc[1]}=1;
			$found_c_indexes{$rc[0]}=1 if $rc[2] eq "1";
		}

		dbi_msg_exclude(1505);
		dbi_msg_exclude(1902);
		dbi_msg_exclude(1906);
		dbi_msg_exclude(1913);

		open(INFILE,$opt_i) or logdie "Cant read file $opt_i : $!\n";
		while( <INFILE> ) {
			if( /^use/i ) {
				# CHANGE DEFAULT DATABASE!
			} elsif( /^CREATE/i ) {
				rebuild_index($datab,$_);
			}
		}
		close INFILE;
	}
}

info "Program Completed Successfully\n";

sub rebuild_index
{
	my($database);
	info "." if defined $opt_r;

	($database,$_) = @_;

	$query_results="";
	$index_name="";
	$table_name="";
	$columns="";

	# Get The Table name And Index Name
	m/INDEX\s+([_\.\w]+)\s+ON\s+([_\.\w]+)\s+\(([\w,]+)\)/;
	$columns=$3;
	$table_name=$2;
	$index_name=$1;

	return if $table_name !~ /^dbo./;
	$table_name=~s/^dbo.//;

	if( ! defined $found_tables{$table_name} ) {
		info "\nCant find table $table_name in server $opt_S";
		return;
	}

	return if defined $found_indexes{$table_name.".".$index_name};
	if( defined $found_c_indexes{$table_name} and /CLUSTERED/ and ! /NONCLUSTERED/ ) {
		#info "DBG SKIPPING DUPLICATE CLUSTERED INDEX ON $table_name ($index_name)\n";
		return;
	}

	if(defined $opt_r) {
		info "($database) REBUILD $table_name.$index_name\n";
		next unless /\bunique\b/i;

		my($query) = "select count(*),$columns from $table_name group by $columns order by $columns";

		my(@dupcoldata);
		foreach my $dup ( dbi_query(-db=>$database,-query=>$query) ) {
			my(@rc)=dbi_decode_row($dup);
			next if $rc[0] == 1;
			push @dupcoldata,$dup;
		}

		if( $#dupcoldata >= 0 ) {
			info "Duplicates Found in $table_name\n";
			info "COLUMNS=$columns\n";
			my(@cols)=split(/,/,$columns);
			info join("\t",("Rows",@cols)),"\n";
			foreach (@dupcoldata) {
				my(@rc)=dbi_decode_row($_);
				info join("\t",@rc),"\n";
			}
		}

		return;
	}

	info "rebuilding $table_name.$index_name\n";
	my($query)=$_;

	foreach ( dbi_query(-db=>$database,-query=>$query) ) {
		($_)=dbi_decode_row($_);
		info "\tOUTPUT: $_\n";
	}

	return  if $query_results eq "Ok";

	if( $query_results =~ /^Duplicate/ ) {
		info "  Duplicates Found....\n";
		info "  INDEX  = $query";
		info "  COLUMNS=$columns\n";
		info "\n  ************************************\n" unless defined $opt_c;
		info "  THE INDEX WAS NOT BUILT SUCCESSFULLY\n"   unless defined $opt_c;
		info "  ************************************\n\n" unless defined $opt_c;

		return unless defined $opt_c;

		info "  RESULTS=$query_results\n";

		my(@colkey)=split /,/,$columns;
		$query = "select count(*),$columns from $table_name group by $columns order by $columns";

		my(@dupcoldata);
		foreach my $dup ( dbi_query(-db=>$database,-query=>$query) ) {
			my(@rc)=dbi_decode_row($dup);
			next if $rc[0] == 1;
			push @dupcoldata,$dup;
		}

		if( $#dupcoldata >= 0 ) {
			my(@cols)=split(/,/,$columns);
			info "  ",join("\t",("Rows",@cols)),"\n";
			foreach (@dupcoldata) {
				my(@rc)=dbi_decode_row($_);
				info "  ",join("\t",@rc),"\n";
			}
		}

		# Get Column Types
		my(%coltypes);		  # q for quoted or nothing
		foreach ( dbi_query(-db=>$database,-query=>"exec sp__helpcolumn ".$table_name) ) {
			my(@rc)=dbi_decode_row($_);
			$rc[0] =~ s/\s//g;
			$coltypes{$rc[0]} = '0';
			$coltypes{$rc[0]} = '1' if $rc[1] =~ /char/ or $rc[1] =~ /binary/;
			#info "Column |$rc[0]| Key Set to ".$coltypes{$rc[0]}."\n";
		}

		info "Deleting Duplicate key information\n";
		foreach ( @dupcoldata ) {
			my(@rc)=dbi_decode_row($_);
			$rc[0]--;
			my $count=0;

			$query= "set rowcount $rc[0]";
			foreach ( dbi_query(-db=>$database,-query=>$query) ) {
				($_)=dbi_decode_row($_);
				info "ERROR: $query generated $_\n";
			}

			$query= "DELETE $table_name where ";
			foreach (@colkey) {
				$query.= " and " if $count>0;
				#info "Column |$_| is ".$coltypes{$_}."\n";
				$query.= $colkey[$count]." = ";
				$query.= "\"" if $coltypes{$_} eq "1";
				$query.= $rc[$count+1];
				$query.= "\"" if $coltypes{$_} eq "1";
				$count++;
			}
			foreach ( dbi_query(-db=>$database,-query=>$query) ) {
				($_)=dbi_decode_row($_);
				info "ERROR: $query generated $_\n";
			}
		}
		rebuild_index($database,$_);
	} elsif($query_results eq "") {
		info "		  Ok\n";
	} else {
		infof( "%-55.55s %s\n",
			$table_name.".".$index_name,$query_results);
	}
}

sub message_handler_new
{
	my($db, $message, $state, $severity, $text, $server, $procedure, $line)=@_;

	my(@str)=();				    # FORMATTED MESSAGES - 1 LINE PER ITEM
	chomp $text;				    # MESSAGES HAVE NO NEW LINE

	#info "MSG: $message\n";

	if( $message==1902 ) {
		$query_results="Clustered Index Exists";
		return 0;
	}
	if( $message==1913 ) {
		$query_results="Index Exists";
		return 0;
	}

	if( $message==1906  ) {
		$query_results="Bad Table name";
		return 0;
	}

	if( $message==1505  ) {
		substr($text,0,46)="";
		$query_results="Duplicate Data: $text";
		return 0;
	}

	# IS MESSAGE TO BE EXCLUDED
	foreach (dbi_msg_ok()) {
		next unless $_  eq $message     # MESSAGE NUMBER EXCLUDED
				or $text =~ /$_/;		       # TEXT EXCLUDED
		return 0;
	}

	if ($severity > 10) {

		push @str, "Sybase message ".$message.", Severity ".$severity.", state ".$state;
		push @str, "Server '".$server."'"
			if defined ($server);
		push @str, "Procedure '".$procedure."'"
			if defined ($procedure);
		push @str, "Line ". $line
			if defined ($line);
		push @str,  $text;

		# &dbstrcpy returns the command buffer.
		if(defined($db)) {
			my ($lineno, $cmdbuff) = (1, undef);
			$cmdbuff = &Sybase::DBlib::dbstrcpy($db);
			foreach (split (/\n/, $cmdbuff)) {
				push @str , sinfof ("%5d", $lineno ++). "> ". $_;
			}
		}

		unshift @str,"Serious/Fatal Error";
		logdie join("\n",@str)."\n FATAL DATABASE ERROR\n";

	} elsif ($message == 0) {

		$query_results.= $text."\n";

	}

	return 0;
}

# =========================
# PROC:		 DEFAULT Error Handler
# DESCRIPTION:	  info Error sent from server
# NOTES:		Handles messages on exclude list
# =========================
sub error_handler_new {
	my ($db, $severity, $error, $os_error, $error_msg, $os_error_msg) = @_;
	#info "ERROR: $error\n";

	dbi_set_ignore(0);

	foreach (dbi_msg_ok()) {
		next unless $error == $_ and $_ ne $error_msg;
		dbi_set_ignore(1) if $error == 2007;	     # dependencies
		dbi_set_ignore(1) if $error == 20018;	    # general
		#return INT_CANCEL;
		return 0;
	}

	my($str) = "ERROR # $error: $error_msg \n";
	$str.= "OS ERROR $os_error: ".$os_error_msg."\n"
				if defined $os_error_msg;
	logdie $str."\n FATAL SYBASE ERROR\n";
	$query_results .= $str;

	#return INT_CANCEL;
	return 0;
}

__END__

=head1 NAME

rebuild_index.pl - Rebuild Indexes

=head2 DESCRIPTION

This procedure builds missing indexes in a server.  It relies on a file extract
from another server to understand what indexes to rebuild.  The magic of
this proc is in its error handler, which understands the errors that can be
generated by index generation and in its duplicate row processing.  You can
rely on the fact that when this program is done, your indexes will be correct.
Use it to maintain indexes between identical servers.

Note: this is not a script to recreate existing indexes.  This is a script
that corrects indexes in a server that might be incorrect by making them
like the indexes in a file (which can be extracted from another server).

The extract file
is is just index create scripts, 1 per line.  For syntactical clarity, it
is recommended that the extract file be based on the extended
stored procedure sp__revindex, which is found in
Ed Barlows extended procedure library.  The program can be used to extract
output into a file (using -o) which can later be run into another server
(using -i).  Duplicate rows are cleaned
up if the -c option is used

=head2 USAGE

Usage: rebuild_index.pl -UUSER -PPASS -SSERVER -DDB [-oOUTFILE|-iINFILE] -c

Index Comparator.  Extracts indexes for a DB and then allows you to compare
the saved indexes to the indexes on your servers.

 -o : if outfile specified then creates a file with the indexes from the
      server.  This file can be used as an infile for further comparisons.
 -c : correct duplicate keys in indexes.  Deletes duplicate data.  One random
      row in unique indexes will be kept.
 -r : report only - this will report missing indexes
      as well as duplicate data but will not rebuild any indexes.

note that this program requires sp__revindex (extended stored proc library)

=cut

