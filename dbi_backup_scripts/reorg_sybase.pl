#!/apps/sybmon/perl/bin/perl

use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

use strict;
use Getopt::Long;
use Do_Time;
use DBIFunc;
use CommonFunc;
use Sys::Hostname;
use Repository;
use File::Basename;

use vars qw( $USER $SERVER $RUNONALLSERVERS $PASSWORD $RUN_OPTDIAG $DEBUG
	$ASNEEDEDONLY $REORG_COMPACT $MAXTIMEMINS $CONFIG_FILE $DB $HTML $OPTDIAG_FILE
	$REORG_REBUILD $NOEXEC $OUTFILE);

sub usage
{
print @_;
print basename($0)." -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS
or \n".
basename($0)." -RUNONALLSERVERS

Additional Arguments
	--OPTDIAG_FILE=file
	--OUTFILE	: Output File (all output also goes here)
	--RUN_OPTDIAG
	--ASNEEDEDONLY
	--MAXTIMEMINS
	--CONFIG_FILE
	--HTML
	--OPTDIAG_FILE
	--DEBUG
	--REORG_COMPACT
	--REORG_REBUILD
	--NOEXEC\n";
return "\n";
}

$| =1; 				# unbuffered standard output

# change directory to the directory that contains THIS file
my($curdir)=cd_home();

die usage("") if $#ARGV<0;
die usage("Bad Parameter List\n") unless GetOptions(
		"SERVER=s"	=> \$SERVER,
		"USER=s"	=> \$USER ,
		"PASSWORD=s" 	=> \$PASSWORD ,
		"DATABASE=s" 	=> \$DB ,
		"RUNONALLSERVERS" 	=> \$RUNONALLSERVERS ,

		"OUTFILE=s"	=> \$OUTFILE,
		"HTML" 		=> \$HTML ,

		"REORG_COMPACT" 	=> \$REORG_COMPACT ,
		"REORG_REBUILD" 	=> \$REORG_REBUILD ,
		"ASNEEDEDONLY" 	=> \$ASNEEDEDONLY ,

		"MAXTIMEMINS=i" 	=> \$MAXTIMEMINS ,
		"CONFIG_FILE=s"		=> \$CONFIG_FILE,
		"RUN_OPTDIAG"		=> \$RUN_OPTDIAG,
		"OPTDIAG_FILE=s" 	=> \$OPTDIAG_FILE ,
		"NOEXEC" 	=> \$NOEXEC ,
		"DEBUG"      	=> \$DEBUG );

die usage("Must pass server\n" )	unless defined $SERVER   or $RUNONALLSERVERS;
die usage("Must pass username\n" )	unless defined $USER     or $RUNONALLSERVERS;
die usage("Must pass password\n" )	unless defined $PASSWORD or $RUNONALLSERVERS;

my($STARTTIME)=time;

die "Must pass --REORG_COMPACT or --REORG_REBUILD" unless $REORG_COMPACT or $REORG_REBUILD;

#$TYPE="Sybase" unless defined $TYPE;
open(OUTF,">$OUTFILE") if $OUTFILE;

sub myprint {
	print @_;
	print OUTF @_ if $OUTFILE;
}

my($NL)="\n";
$NL="<br>" if $HTML;
myprint( "Reorg Rebuild v1.0",$NL );
myprint( "Run at ".localtime(time)." on host ".hostname().$NL );
$DB="%" unless $DB;

if( $SERVER ) {
	myprint( "Working on Server $SERVER\n" );
	if( ! $USER or ! $PASSWORD ) {
		($USER,$PASSWORD)=get_password(-type=>"sybase",-name=>$SERVER);
	}
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
	dbi_set_debug(1) if $DEBUG;
	if( ! $rc ) {
		myprint( "Cant connect to $SERVER as $USER\n" );
	} else {
		#print "DBG DBG: getting databases\n";
		my($typ,@db)=dbi_parse_opt_D($DB,1,1);
		dbi_disconnect();
		foreach my $DATABASE (@db) {
			run_a_server_db($SERVER,$USER,$PASSWORD,$DATABASE);
		}
	}
} elsif( defined $RUNONALLSERVERS ) {
	debug("Running on all servers\n");
	my(@serverlist)=get_password(-type=>"sybase",-name=>undef);
	foreach my $server (@serverlist) {
		myprint( "Processing Server $server \n" );
	        ($USER,$PASSWORD)=get_password(-type=>"sybase",-name=>$server);
	        my($rc)=dbi_connect(-srv=>$server,-login=>$USER,-password=>$PASSWORD);
		if( ! $rc ) {
			print "Cant connect to $server as $USER\n";
			next;
		}
		my($typ,@db)=dbi_parse_opt_D($DB,1,1);
		dbi_disconnect();
		foreach my $DATABASE (@db) { run_a_server_db($server,$USER,$PASSWORD,$DATABASE); }
	        # run_a_server_db($server,$USER,$PASSWORD,$DATABASE);
	}
} else {
	die "Must pass server options\n";
}
debug( "program completed successfully.\n" );
close(OUTF) if $OUTFILE;

exit(0);

sub run_a_server_db {
	my($SERVER,$USER,$PASSWORD,$DATABASE)=@_;
	debug("Working on server $SERVER DB=$DATABASE\n");

#if( defined $OPTDIAG_FILE and ! defined $SERVER ) {
#	open(OPTD,$OPTDIAG_FILE) or die "Cant read file $OPTDIAG_FILE $!\n";
#} else {
#	if( defined $OPTDIAG_FILE ) {
#		open(OUT,">".$OPTDIAG_FILE) or die "Cant write file $OPTDIAG_FILE $!\n";
#	}
#	my($cmd)="optdiag statistics $DATABASE -U$USER -P$PASSWORD -S$SERVER ";
#	my($cmd_no_pass)="optdiag statistics $DATABASE -U$USER -Pxxx -S$SERVER ";
#	debug("Running $cmd_no_pass\n");
#	open(OPTD,"$cmd |")
#		or die "Cant run optdiag\n";
#}

#my($success);
#my($tblname,$tblowner,$index);
#my(%rows,%dpcr);
#my($key);
#my($optdiag_failed)="FALSE";
#while (<OPTD>) {
#	myprint( "dbg: $_") if $DEBUG;
#	if( /Optdiag failed./ ) {
#		warn "ERROR>>> OPTDIAG FAILED!\n";
#		warn $_;
#		$optdiag_failed="TRUE";
#		next;
#	}
#	if( $optdiag_failed eq "TRUE" ) {
#		warn $_;
#		next;
#	}
#	print OUT $_ if defined $OPTDIAG_FILE and defined $SERVER;
#	chomp;
#	s/^\s+//;
#	if(/^Table owner:/){
#		s/Table owner:\s+//;
#		s/"//g;
#		#push @lines,$_;
#		($tblname,$tblowner,$index)=("",$_,"");
#	} elsif( /^Table name:/){
#		s/"//g;
#		s/Table name:\s+//;
#		#push @lines,$_;
#		$tblname=$_;
#		$key=$tblowner."|".$tblname."|".$index;
#	} elsif( /^Data page cluster ratio:/){
#		s/"//g;
#		s/Data page cluster ratio:\s+//;
#		#push @lines,$_;
#		$dpcr{$key}=$_;
#	} elsif( /^Data row count:/){
#		s/"//g;
#		s/Data row count:\s+//;
#		#push @lines,$_;
#		$rows{$key}=int($_);
#	} elsif( /^Statistics for index:/){
#		s/"//g;
#		s/Statistics for index:\s+//;
#		#push @lines,$_;
#		$index=$_;
#		$key=$tblowner."|".$tblname."|".$index;
#	} elsif( /^Optdiag succeeded./ ) {
#		$success="TRUE";
#		#push @lines,$_;
#	}
#}
#close(OPTD);
#
#if( $optdiag_failed eq "TRUE" ) {
#	myprint( "<h2>"  ) if $HTML;
#	myprint( "\nOPTDIAG FAILED FOR SERVER $SERVER DATABASE $DATABASE\n" );
#	myprint( "</h2>" ) if $HTML;
#	return;
#}

	#
	# CONNECT TO THE DATABASE
	#
	debug( "Connecting to the database\n" );
	my($rc)=dbi_connect(-srv=>$SERVER,-login=>$USER,-password=>$PASSWORD);
	if( ! $rc ) {
		myprint ("Cant connect to $SERVER as $USER\n");
		return;
	}
	debug("Connected\n");

	myprint( "<h2>"  ) if $HTML;
	myprint( "\nSERVER $SERVER DATABASE $DATABASE\n" );
	myprint( "</h2>" ) if $HTML;

	my(%oktbls);
	my($sql)="select user_name(uid),o.name, forwrowcnt, delrowcnt, rowcnt, rslastoam, rslastpage, frlastoam, frlastpage
	from sysobjects o, systabstats ts
	where (sysstat2&16384 !=0 or sysstat2&32768 !=0) and type='U' and ts.id=o.id";
	foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
		my($usr,$tbl,@vals)= dbi_decode_row($_);
		if( $ASNEEDEDONLY ) {
			next unless $vals[0] + $vals[1] > $vals[2]/10;
		}
		$oktbls{$usr.".".$tbl}=1;
	}

	foreach ( keys %oktbls ) {
		if( $REORG_REBUILD ) {
			my $sql = "reorg rebuild $_";
			if( $MAXTIMEMINS ) {
				my($minremaining)= $MAXTIMEMINS - int((time-$STARTTIME)/60);
				last if $minremaining<=0;
			}
			if( defined $NOEXEC ) {
				myprint( "(noexec) $sql\n" );
			} else {
				debug( "query  is $sql\n" );
				myprint( "-- ".localtime(time)." ".$sql."\n" );
				foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
					my @dat=dbi_decode_row($_);
					myprint( "Query Returned : ",join(" ",@dat),"\n" );
				}
			}
		} elsif( $REORG_COMPACT ) {
			my $sql = "reorg compact $_";

			if( $MAXTIMEMINS ) {
				my($minremaining)= $MAXTIMEMINS - int((time-$STARTTIME)/60);
				last if $minremaining<=0;
				$sql .= " with resume,time=$minremaining";
			}

			if( defined $NOEXEC ) {
				myprint( "(noexec) $sql\n" );
			} else {
				debug( "query  is $sql\n" );
				myprint( "-- ".localtime(time)." ".$sql."\n" );
				foreach( dbi_query(-db=>$DATABASE,-query=>$sql)) {
					my @dat=dbi_decode_row($_);
					myprint( "Query Returned : ",join(" ",@dat),"\n" );
				}
			}
		}
	}
	debug( "server / database completed successfully.\n" );
}

sub debug { myprint( @_ ) if defined $DEBUG; }

__END__

=head1 NAME

reorg_sybase.pl - sybase reorg manager

=head2 USAGE

reorg_sybase.pl -USER=SA_USER -DATABASE=db  -SERVER=SERVER -PASSWORD=SA_PASS

or

reorg_sybase.pl -RUNONALLSERVERS

Additional Arguments
	--OPTDIAG_FILE=file
	--OUTFILE	: Output File (all output also goes here)
	--RUN_OPTDIAG
	--ASNEEDEDONLY
	--MAXTIMEMINS
	--CONFIG_FILE
	--HTML
	--OPTDIAG_FILE
	--DEBUG
	--REORG_COMPACT
	--REORG_REBUILD
	--NOEXEC

=head2 ARGUMENTS

You can run on all your servers with --RUNONALLSERVERS or you can specify
a server with --SERVER/--USER/--PASSWORD.  If no --DATABASE is specified, it
will run on all user databases.

The command run will be reorg REORG_COMPACT (--REORG_COMPACT) or reorg REORG_REBUILD (--REORG_REBUILD).

If you specify --ASNEEDEDONLY, it will only reorg tables that need to be reorged. The default behavior is to reorg all tables.  This command will run optdiag if --RUN_OPTDIAG or --OPTDIAG_FILE are passed.  It will look for tables with low cluster ratios.  If not, it will get the info from systabstats looking for tables
with forwarded/deleted rows.

You can also pass in --NOEXEC to print what will happen but not run the commands

Output can be directed to --OUTFILE and can be saved in HTML format if --HTML is passed.

The MAXTIMEMINS argument will specify the max time in minutes that the entire operation
can run (all servers all databases).  You can specify --CONFIG_FILE to store the order
and time of this run for the future...

=cut
