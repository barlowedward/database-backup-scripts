#!/apps/sybmon/perl/bin/perl

# this should be automodified by configuration process
use lib qw(/apps/sybmon/dev/lib //samba/sybmon/dev/lib //samba/sybmon/dev/Win32_perl_lib_5_8);

#
# Copyright (c) 1997-2006 By Edward Barlow
# All Rights Reserved
#
# Explicit right to use can be found at www.edbarlow.com
# This software is released as free software and should be shared and enjoyed
# You may modify this code provided this comment block remains intact
#

use strict;
use Getopt::Std;
use File::Basename;
use Sys::Hostname;
use Net::myFTP;
use Net::myRSH;
use MlpAlarm;
use Repository;
use Logger;
use CommonFunc;
use Cwd;

BEGIN {
	$SIG{ALRM} = sub { die "ERROR: PROGRAM TIMED OUT\n"; }
}

use vars qw($opt_R $opt_J $opt_l $opt_f $opt_d $opt_b $opt_D $opt_x
	$opt_e $opt_h $opt_s $opt_t %CONFIG $opt_n $opt_m);
	
use DBIFunc;
my($full_cmd_line);
my(@required_dirs)=qw(dbdumps dbcc audits bcp logdumps errors sessionlog);
my(%RUN);

# remove error logs
END { unlink $opt_e if defined $opt_e and -z $opt_e; }

sub usage
{
   my($data)=@_;

   die $data."\nUsage: backup_pl -JJOB -t|-f [-dh] -mmode

This is THE Master backup script.
This script performs appropriate tasks as defined by maintenance plans (specified with -JJOB)
which are defined in configure.cfg (located in backup scripts directory or one level above it).

 -x noexec mode 			(purges will exec but other stuff wont)
 -n copy but dont load for logshipping
 -t for transaction log dumps only
 -f for full backups 	(default).
 -m mode = ssh|ftp|rsh	(override communication mode)
 -d is debug mode
 -b batchid (for alarming)
 -h is html output.
 -s skip transfer and load step
 -DDB_LIST (pipe separated) restriction on databases
 -RlpdDuabCir will run only the (l)oad, (p)urge, (d)ump, (D)bcc, (r)eorg,
      (u)pdate stats, (a)udit, (b)cp, (C)ompress, and (i)ndex steps.\n";
}

$| =1;

my($curdir)=cd_home();
my($VERSION,$SHTVER)=get_version();

my(%start_time,%end_time);
$start_time{BEGIN}=time;

# PARSE COMMAND LINE ARGUMENTS
die usage("") if $#ARGV<0;
$full_cmd_line=$0." ".join(" ",@ARGV);
usage("Bad Parameter List")  unless getopts('D:J:fdhtR:snb:m:x');
usage("Must Pass Job (Server) Name with -J")       unless defined $opt_J;
usage("Cant Pass Both -t and -f") if defined $opt_t and defined $opt_f;
$opt_f = 1 unless defined $opt_t;

# READ THE CONFIGURATION FILE
%CONFIG=read_configfile( -job=>$opt_J );
$CONFIG{COMMAND_RUN}=$full_cmd_line;

if( ! defined $opt_b ) {
	$opt_b="Backup";
	$opt_b.="Tran" if defined $opt_t;
	$opt_b.="Full" unless defined $opt_t;
}

# set up some stuff for the alarm routines
my($tran_log_text)="(tran log)" if defined $opt_t;
$tran_log_text="(full)"     unless defined $opt_t;

my($subsystem)="Job: ".$opt_J." ".$tran_log_text;
$subsystem.=" ".$opt_D if defined $opt_D and $opt_D ne "%";

# SET DEFAULT OPTIONS FOR FTP CONNECTIONS
my(%FTPCONFIG);
$FTPCONFIG{Debug}=$opt_d;
$FTPCONFIG{Timeout}=90000;
$FTPCONFIG{IS_REMOTE}=$CONFIG{IS_REMOTE};
$FTPCONFIG{IS_REMOTE}="n" if $CONFIG{REMOTE_HOST} =~ /^\s*$/;
$FTPCONFIG{PRINTFUNC}=\&info;
$FTPCONFIG{DEBUGFUNC}=\&debug;

# REWRITE DO_* OPTIONS BASED ON -R COMMAND LINE ARGUMENTS
if( defined $opt_R ) {
	foreach ( keys %CONFIG ) {
		if( /^DO_/ and $CONFIG{$_} eq "y" ) {
			next if /^DO_ONLINE/ or /^DO_CLEAR/;
			print "Setting $_ To n\n";
			$CONFIG{$_}='n';
		}
	}
	foreach ( split (//,$opt_R )) {
		my($k)="";
		$k="DO_LOAD"     if $_ eq 'l';
		$k="DO_PURGE"    if $_ eq 'p';
		$k="DO_DUMP"     if $_ eq 'd';
		$k="DO_DBCC"     if $_ eq 'D';
		$k="DO_UPDSTATS" if $_ eq 'u';
		$k="DO_AUDIT"    if $_ eq 'a';
		$k="DO_BCP"      if $_ eq 'b';
		$k="DO_COMPRESS" if $_ eq 'C';
		$k="DO_INDEXES"  if $_ eq 'i';
		$k="DO_REORG"    if $_ eq 'r';
		$CONFIG{$k}="y";
		print "Re-Setting $k To y\n";
	}
}

my $progname=basename($0);

# SET UP SOME PATHS
my($srvr_dir_by_bksvr) 	= $CONFIG{REMOTE_DIR}."/".$CONFIG{DSQUERY};
my($dump_dir_by_bksvr) 	= $CONFIG{REMOTE_DIR}."/".$CONFIG{DSQUERY}."/dbdumps";
$dump_dir_by_bksvr 		= $CONFIG{REMOTE_DUMP_DIR} if defined $CONFIG{REMOTE_DUMP_DIR};
my($log_dir_by_bksvr) 	= $CONFIG{REMOTE_DIR}."/".$CONFIG{DSQUERY}."/logdumps";
$log_dir_by_bksvr 		= $CONFIG{REMOTE_LOG_DIR} if defined $CONFIG{REMOTE_LOG_DIR};
my($base_bk_dir)       	= $CONFIG{BASE_BACKUP_DIR}."/".$CONFIG{DSQUERY};

# THese vars are for use by the myftp module and should represent a full
#   path to a local file if IS_REMOTE=n or the remote path if IS_REMOTE=y
#   fundamentally, we will use this path to get to the file
my($srvr_dir_for_myftp, $dump_dir_for_myftp, $log_dir_for_myftp );
if($CONFIG{IS_REMOTE} eq "n") {
	$srvr_dir_for_myftp    = $CONFIG{LOCAL_DIR}."/".$CONFIG{DSQUERY};
	$dump_dir_for_myftp    = $srvr_dir_for_myftp."/dbdumps";
	$log_dir_for_myftp     = $srvr_dir_for_myftp."/logdumps";
} else {
	$srvr_dir_for_myftp    = $srvr_dir_by_bksvr;
	$dump_dir_for_myftp    = $dump_dir_by_bksvr;
	$log_dir_for_myftp     = $log_dir_by_bksvr;
}

my($ftp);
my($ftplogin,$ftphost,$ftppass,$ftpmethod);   # needed in case you timeout during dbcc's

#
# Define Log Files For The Session
#
my($today_val)=today();
$opt_l  =$base_bk_dir."/sessionlog/$opt_J.";
$opt_l .=$opt_D."." if defined $opt_D and $opt_D ne "%" and $opt_D!~/\|/;
$opt_l .= "tranlog." if defined $opt_t;
$opt_l .= "full." unless defined $opt_t;
$opt_l .=$today_val.".log";

$opt_e  =$base_bk_dir."/errors/$opt_J.";
$opt_e .=$opt_D."." if defined $opt_D and $opt_D ne "%" and $opt_D!~/\|/;
$opt_e .= "tranlog." if defined $opt_t;
$opt_e .= "full." unless defined $opt_t;
$opt_e .=$today_val.".log";;

MlpBatchStart( -monitor_program =>	 $opt_b, -system          =>   $CONFIG{DSQUERY}, -subsystem       =>   "Job: ".$opt_J." ".$tran_log_text, -batchjob	    =>	 1, -message         =>   "$tran_log_text Backup Job Started at ".scalar(localtime(time)));

check_directory_structure();

logger_init(-logfile=>  $opt_l,
            -errfile=>  $opt_e,
            -database=> $opt_D,
            -debug =>   $opt_d,
            -jobname=>  $opt_J,
            -system =>  $opt_J,
            -subsystem =>$opt_J,
            -command_run => $CONFIG{COMMAND_RUN},
            -mail_host   => $CONFIG{MAIL_HOST},
            -mail_to     => $CONFIG{SUCCESS_MAIL_TO},
            -success_mail_to => $CONFIG{SUCCESS_MAIL_TO} );

debug "****** DEBUG FLAG SET (debug info to screen only) ******\n";

my($NL)="\n";
$NL="<br>\n" if defined $opt_h;

# GET PASSWORD INFO - TRY SYBASE AND IF NO PASSWORD FOUND TRY SQL SERVER PASSWORD FILES
debug("Getting Password Information for $CONFIG{DSQUERY}\n");
my($login,$password,$type)   = get_password(-name=>$CONFIG{DSQUERY},-type=>"sybase");
my($server_type)="SYBASE";
if( ! defined $login or ! defined $password or $login =~ /^\s*$/ or $password =~ /^\s*$/ ) {
	$server_type="SQLSERVER";
	($login,$password,$type)   = get_password(-name=>$CONFIG{DSQUERY},-type=>"sqlsvr")
}
logdie "No Password Found For Server $CONFIG{DSQUERY}.  Please edit your configuration file to ensure that an oper_role password is available.\n" unless defined $login and defined $password and $login !~ /^\s*$/ and $password !~ /^\s*$/;

# ERROR MESSAGES SHOULD COME BACK WITH THE BATCH RESULTS - INLINE
dbi_set_mode("INLINE");

my($header_str)=create_page_header();
info($header_str);

# DEBUG MODE - DO SOME PRINTS
if( defined $opt_d ) {
	info("VARIABLES\n");
	foreach (sort keys %CONFIG ) {
		infof( "%-23.23s %-54.54s\n",$_,$CONFIG{$_});
	}
}

#
# Check if it is a remote server - if so build $REMOTE_DIR if needed
#
if( $CONFIG{IS_REMOTE} eq "y" and defined $CONFIG{REMOTE_HOST}) {
	debug("Creating Ftp Connection to $CONFIG{REMOTE_HOST} \n");
	my($unix_login,$unix_password,$unix_type);
	if( ! $CONFIG{REMOTE_ACCOUNT}
	or  ! $CONFIG{REMOTE_PASSWORD} ) {
			($unix_login,$unix_password,$unix_type) = get_password(-name=>$CONFIG{REMOTE_HOST},-type=>"unix");
	} else {
	      $unix_login=$CONFIG{REMOTE_ACCOUNT};
	   	$unix_password=$CONFIG{REMOTE_PASSWORD};
			print "Remote Account Defined from REMOTE_ACCOUNT/REMOTE_PASSWORD as $unix_login\n";
	}

	my(%svrargs)=get_password_info(-type=>"unix",-name=>$CONFIG{REMOTE_HOST});
	my($method)=$svrargs{UNIX_COMM_METHOD};
	$method=$svrargs{WIN32_COMM_METHOD} if is_nt();
	$method="FTP" if ! defined $method or $method eq "";
	die "Can not run a backup on host $CONFIG{REMOTE_HOST} as COMM_METHOD=NONE"	if $method eq "NONE";

	debug("Logging In Using $unix_login \n");
   die "Unix Login Not Defined for REMOTE_HOST ",$CONFIG{REMOTE_HOST}
		unless defined $unix_login and $unix_login !~ /^\s*$/;
   die "Unix Password Not Defined for REMOTE_HOST ",$CONFIG{REMOTE_HOST}
		unless defined $unix_password and $unix_password !~ /^\s*$/;

	$ftp=ftp_connect($CONFIG{REMOTE_HOST},\%FTPCONFIG,$unix_login,$unix_password,$method);
	($ftphost,$ftplogin,$ftppass,$ftpmethod)=
		($CONFIG{REMOTE_HOST},$unix_login,$unix_password,$method);
	$ftp->ascii();

	# check if remote directories exist
	my($ok)=0;
	info("Listing Files in $CONFIG{REMOTE_DIR} \n" );
	my(@rc)=$ftp->ls( $CONFIG{REMOTE_DIR} );
	foreach (@rc) {
		info("ls returned: $_\n");
		chomp;
		next unless basename($_) eq $CONFIG{DSQUERY};
		$ok=1;
		last;
	}
	if( $ok == 0 ) {
		info("Creating Remote Directories in $srvr_dir_for_myftp\n");
		$ftp->mkdir($srvr_dir_for_myftp);
		foreach my $rd (@required_dirs) {
			$ftp->mkdir($srvr_dir_for_myftp."/$rd");
		}
	} else {
		debug("File Top Level Is Ok\n");
		debug("Listing Files in $CONFIG{REMOTE_DIR} \n" );
		my(@rc)=$ftp->ls($srvr_dir_for_myftp);
		my(%dirs_found);
		foreach (@rc) {
			debug("Found $_\n");
			$dirs_found{$_}=1;
		}
		foreach (@required_dirs) {
			next if $dirs_found{$_};
			debug( "Creating Remote Directory $srvr_dir_for_myftp/$_");
			$ftp->mkdir($srvr_dir_for_myftp."/$_");
		}
	}
} elsif( $CONFIG{IS_REMOTE} eq "y" and ! defined $CONFIG{REMOTE_HOST}) {
	die "Cant have IS_REMOTE defined but have no remote host defined\n";
} else {
	$ftp=ftp_connect("localhost",\%FTPCONFIG, "null", "null",undef);
	($ftphost,$ftplogin,$ftppass,$ftpmethod)= ("localhost","null","null","FTP");
}

# DEFAULT CONNECTION TYPES
if( is_nt() ) {
	$type="ODBC" unless defined $type;
} else {
	$type="Sybase" unless defined $type;
}

#
# CONNECT TO DB
#
info( "Connecting to server=$CONFIG{DSQUERY} as $login\n" );
dbi_connect( -srv=>$CONFIG{DSQUERY}, -login=>$login, -password=>$password, -type=>$type) or logdie "Cant connect to $CONFIG{DSQUERY} as $login\n";
dbi_set_mode("INLINE");

#
# BUILD DATABASE LIST - Tran Dump only appropriate databases
#
my($whereclause)="";
if( defined $opt_D ) {
   foreach ( split(/\|/,$opt_D)) {
      if( $whereclause eq "" ) {
         $whereclause="and ( name like \'$_\'";
      } else {
         $whereclause .= " or name like \'$_\' ";
      }
   }
   $whereclause.=")" if $whereclause ne "";
}
my( $status1bits, $status2bits );
if( $server_type eq "SQLSERVER" ) {
	$status1bits = 32+64+128+256+512+1024+4096+32768;
	$status2bits = 0x0000;
} else {
	# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
	#   30= 20 (offline until recovery) + 10 (offline)
	$status1bits = 0x1120;
	$status2bits = 0x0030;
}
info "Selecting Databases\n";
my($q)="select name,status,status2 from sysdatabases where status & $status1bits  = 0 and status2&$status2bits=0 $whereclause";
debug("$q\n");
my(@d)=dbi_query(-db=>"master",-query=>$q );

my(%ignore,%ignore_for_dbcc);
$CONFIG{IGNORE_LIST} =~ s/\s//g;
foreach ( split(/\|/,$CONFIG{IGNORE_LIST}) ) { $ignore{$_} = 1; $ignore_for_dbcc{$_}=1; }
if( defined $CONFIG{DBCC_IGNORE_DB} ) {
	foreach ( split(/\|/,$CONFIG{DBCC_IGNORE_DB}) ) { $ignore_for_dbcc{$_} = 1; }
}

if( defined $opt_D ) {	# if you pass a db ... then it should be the override!
	undef $ignore{$opt_D};
	undef $ignore_for_dbcc{$opt_D};
}

my(@full_db_list,@tran_db_list,@dbcc_db_list);
my(%full_db_list_hash,%tran_db_list_hash);
foreach( @d ) {
   	s/\s//g;
   	my($nm,$s1,$s2)=dbi_decode_row($_);
   	info "-- ignoring db $nm as per config file\n" if defined $ignore{$nm};
   	next if defined $ignore{$nm};
   	push @full_db_list,$nm;
   	push @dbcc_db_list,$nm unless defined $ignore_for_dbcc{$nm}
				or  $nm eq "tempdb" or $nm eq "model";
   	$full_db_list_hash{$nm}=1;
	$s2+=32768 if $s2 < -17000 and $server_type eq "SYBASE";
	if( $nm eq "master" or $nm eq "tempdb" or $nm eq "model" or $nm eq "sybsystemprocs"
		or $nm =~ /^sysaudit/ or $nm eq "msdb" or $nm eq "sybsystemdb" ) {
		info "-- not tran dumping $nm (illegal tran db)\n"
	} else {
		#info "FOUND $nm $s1/$s2\n";
		if( ($s2<0) or ($s1 & 4) or ($s1 & 8) ) {
			if($s1 & 4 and $s1 & 8 ) {
				info "-- not tran dumping $nm (select/into, trunc log on chkpt)\n";
			} elsif( $s1 & 4 ) {
				info "-- not tran dumping $nm (select/into)\n";
			} elsif( $s1 & 8 ) {
				info "-- not tran dumping $nm (trunc. log on chkpt)\n";
			} else {
				info "-- not tran dumping $nm (data/log on same device)\n";
			}
		} else {
   			push @tran_db_list,$nm;
   			$tran_db_list_hash{$nm}=1;
		}
	};
}

info "DBCC OK FOR DB=(",join(" ",@dbcc_db_list),")\n" if $CONFIG{DO_DBCC} eq "y";
info "TRAN OK FOR DB=(",join(" ",@tran_db_list),")\n" if $opt_t;

if( defined $opt_t ) {
   	if( $#tran_db_list<0 ) {
      		exitout("Run on host ".hostname()."\nJob Failed: No Databases Found To Tran Dump\n",255,"ERROR");
   	}
	info "TRAN LOG DUMP OK FOR DB=(",join(" ",@tran_db_list),")\n" ;
} else {
	if( $#full_db_list<0 ) {
   		exitout( "Run on host ".hostname()."\nJob Failed: No Databases Found For Full Dump\nQuery Was $q",255,"ERROR" );
	}
	info "FULL DUMP OK FOR DB=(",join(",",@full_db_list),")" ,$NL;
}

# my($COMMAND_OPTIONS)="-U$login -S$CONFIG{DSQUERY} -P$password -l$opt_l -e$opt_e";
my($COMMAND_OPTIONS)="-U$login -S$CONFIG{DSQUERY} -P$password ";
$COMMAND_OPTIONS .= " -d" if defined $opt_d;

my($COMMAND_OPTIONS_LONG)="-USER=$login -SERVER=$CONFIG{DSQUERY} -PASSWORD=$password ";
$COMMAND_OPTIONS_LONG .= " -DEBUG" if defined $opt_d;

my($COMPRESS_LOAD,$COMPRESS_DUMP)=("","");
if( $CONFIG{COMPRESSION_LEVEL} =~ /^\d$/
	and $CONFIG{COMPRESSION_LEVEL} ne "0") {
	$COMPRESS_DUMP=" -X".$CONFIG{COMPRESSION_LEVEL};
	$COMPRESS_LOAD=" -X";
}

#
# DUMP TRANSACTION LOGS AND EXIT IF -t PASSED
#
if( defined $opt_t ) {
   $start_time{TRAN_DUMPS}=time;

   info("Dumping Transaction Log\n");
   my $cmd = "dump_database.pl -Y -J$opt_J -s$today_val $COMMAND_OPTIONS -o$log_dir_by_bksvr -M$type -t $COMPRESS_DUMP";
   $cmd.=" -D\"".join("\|",@tran_db_list)."\"";
   $cmd.="\n";

   run_cmd(-cmd=>$cmd,-print=>1, -err=>"dump transaction log command failed");
	info("Dump Transaction Succeeded\n");

   $end_time{TRAN_DUMPS}=time;

	# ok transfer and load the logs if needed...
	do_copy_load("FALSE",$log_dir_for_myftp)
		if $CONFIG{DO_LOAD} eq "y" and ! defined $opt_s;

	# info("THE FILES ARE: \n");
	# foreach( @tran_db_list ) {
		# info( $log_dir_for_myftp."/".$CONFIG{DSQUERY}.".".$_.".logdump.".$today_val."\n");
	# }
	info("\n");

	if( $CONFIG{DO_COMPRESS} eq "y"
   	and   $CONFIG{COMPRESS} =~ /^\s*$/ ) {
   	$start_time{COMPRESS}=time;
   	info("Compressing LogDumps Files at ".scalar(localtime(time))."\n");
   	run_compression( $ftp, $log_dir_for_myftp, $log_dir_by_bksvr);
   	$end_time{COMPRESS}=time;
	}

	info(backup_timing());
	info("Transaction Log Backup Completed\n");

	# remove empty error logs if they exist
	if( defined $opt_e ) {
		if( -z $opt_e ) { unlink $opt_e; }
		if( (-s $opt_e) == 0 ) { unlink $opt_e; }
	}
	info( "\n****************************************************************\n");
	info( "*            Transaction Log Dump Completed                     \n");
	info( "****************************************************************\n");
	info(backup_timing());

   exitout("Succeeded: Transaction Log Backup",0,"COMPLETED");
}

#
# WARN IF NO DATABASES FOUND TO WORK ON
#
if( $#full_db_list<0 ) {
   exitout( "Failed: No Databases Found For Full Dump\nQuery Was select name from sysdatabases where status & 0x1120 = 0 and status2&0x0030=0 $whereclause",0,"ERROR" );
}
debug "Found Databases: ",join(" ",@full_db_list),$NL;

# MARK BATCH AS RUNNING IN THE ALARM DB
MlpBatchRunning(-message=>"Databases Fetched...");

#
# PURGE OLD BACKUPS
#
info("\n");
if( $CONFIG{DO_PURGE} eq "y" ) {
	$start_time{PURGE}=time;

	# if u are going to dump one more, lower number u want to keep
	logdie "NUM BACKUPS TO KEEP MUST BE >= 1" if $CONFIG{NUM_BACKUPS_TO_KEEP}<1;
	$CONFIG{NUM_BACKUPS_TO_KEEP}-=1 if $CONFIG{DO_DUMP} eq "y";

	infobox("Purging Old Backups ($CONFIG{NUM_BACKUPS_TO_KEEP} versions kept)\n");
	info("-- Purge Directory is $dump_dir_for_myftp\n" );

	my($ft,$fs)=get_files_in_dir_for_server($ftp,$dump_dir_for_myftp);
	my(%FILE_TIMES)=%$ft;
	my(%FILE_SIZES)=%$fs;

	# Get Day/Times By Server/DB
	my(%ok_roots);      # key = server.db  value=daytime|daytime
	my(%first_dump);   # key = server.db  value=daytime
   if( $CONFIG{NUM_BACKUPS_TO_KEEP} >= 1 ) {
		debug("Calculating File Roots To Maintain A Given # of Backups\n");
		foreach(keys %FILE_TIMES) {
		      	my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
		      	next unless $srv eq $CONFIG{DSQUERY} and defined $time;
		      	next if $day=~/\D/ or $time=~/\D/;
		      	next unless defined $full_db_list_hash{$db};
		      	next if $opt_x; # noexec mode

					debug("File $_ \n");

		      	if( defined $ok_roots{$srv.$db} ) {
		         	my($dup)=0;
		         	my(@order)=();      # The Kept DayTimes for This SRV/DB

		         	foreach ( split(/\|/,$ok_roots{$srv.$db} )) {
			            	$dup=1 if $day.$time eq $_;
			            	push @order,$_;
		         	}
		         	if( $dup == 1 ) {
							debug("  (Duplicate Root)\n");
			         	next;          # ignore duplicate roots
		         	}

		         	if( $#order > $CONFIG{NUM_BACKUPS_TO_KEEP} -1 ) {
					# this really shouldnt happen... sooo...

					my($msg)="\nA Internal Error Has Occurred During The Purge\nError: Server $srv Database $db\n";
					foreach (@order) {
						$msg.="Previously Found Root of $_\n";
					}
					$msg.="Current Root is $day$time\n";
					$msg.="This is kind of an error\n";
					$msg.="  ive found ".($#order+1)." roots but can only keep $CONFIG{NUM_BACKUPS_TO_KEEP} files\n";
					$msg.="  how did i not purge previously\n";

					logdie($msg);
				} elsif( $#order == $CONFIG{NUM_BACKUPS_TO_KEEP} -1 ) {
			            	if( $first_dump{$srv.$db} < $day.$time ) {
			               	# replace first dump with $day.$time
			               	my($count) = -1;
			               	foreach (@order) {
			                  	$count++;
			                  	next unless $first_dump{$srv.$db}==$_;
			                  	$order[$count]=$day.$time;
			                  	last;
			               	}
					debug("  Replacing first dump with this dump\n");
		               		$first_dump{$srv.$db} = $day.$time;
		            	} else {
					debug("  This dump is before first kept dump and will be removed.\n");
							}
		         	} else {
			            	debug(" -- ".($#order+1)." other roots exist, keeping $CONFIG{NUM_BACKUPS_TO_KEEP} versions, so keep all\n");
			            	push @order,$day.$time;
		         	}
		         	$ok_roots{$srv.$db}=join("|",@order);
		      	} else {
		         	debug("root(srv=>$srv db=>$db day=>$day time=>$time)\n");
		         	$ok_roots{$srv.$db}=$day.$time;
		         	$first_dump{$srv.$db}=$day.$time;
		      	}
	   	}
   	}

	if( $CONFIG{IS_REMOTE} eq "y" ) {
   		info("-- Ready To Purge $dump_dir_for_myftp On Host $CONFIG{REMOTE_HOST}\n")
	} else {
   		info("-- Ready To Purge $dump_dir_for_myftp on local system\n");
	}

	my($num_files_deleted)=0;
	foreach(sort keys %FILE_TIMES) {
	      # info the files you want to remove
	      my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
	      debug("checking: $_\n");
	      # debug(" This File Is Not On The Current Server (not purging)\n")
	 			# unless $srv eq $CONFIG{DSQUERY} and defined $time;
	      next unless $srv eq $CONFIG{DSQUERY} and defined $time;

	      debug(" This File Did Not Parse Correctly (not purging)\n")
	      	if $day=~/\D/ or $time=~/\D/;
	      next if $day=~/\D/ or $time=~/\D/;

	      # debug(" This File Is Not From A Database That Is Being Dumped ($db)\n")
	      	# unless defined $full_db_list_hash{$db};
	      next unless defined $full_db_list_hash{$db};

	      my $ok;
	      foreach ( split(/\|/,$ok_roots{$srv.$db} )) {
	         if( $day.$time eq $_ ) { $ok=1; last; }
	      }

	      debug(" Explicitly Keeping This File Ok\n") if defined $ok;
	      next if defined $ok;

		$num_files_deleted++;
	      info("-- Purging $_\n");
	      $ftp->delete("$dump_dir_for_myftp/$_")
	      	or info("Cant Delete $_ $!\n");
	}
	info("-- $num_files_deleted Files Deleted\n");

   	# remove tran logs associated with those dumps
	if( $CONFIG{IS_REMOTE} eq "y" ) {
   		info("-- Purge Associated Tran Logs On Remote Host $CONFIG{REMOTE_HOST}\n");
	} else {
   		info("-- Purge Associated Transaction logs on local system\n");
	}
	info("-- Purge Directory is $log_dir_for_myftp\n" );
	info("-- source\-\>cwd to $log_dir_for_myftp\n" );
	$ftp->cwd($log_dir_for_myftp);
   ($ft,$fs)=get_files_in_dir_for_server($ftp,".");

	undef %FILE_TIMES;
	undef %FILE_SIZES;
	%FILE_TIMES=%$ft;
	%FILE_SIZES=%$fs;
	foreach(keys %FILE_TIMES) {
		my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
		debug( "DBG DBG: skipping $srv != $CONFIG{DSQUERY} \n" )
			  unless $srv eq $CONFIG{DSQUERY} and defined $time;
		next unless $srv eq $CONFIG{DSQUERY} and defined $time;
		debug( "DBG DBG: skipping day/time $day / $time\n" ) if $day=~/\D/ or $time=~/\D/;
		next if $day=~/\D/ or $time=~/\D/;
		debug( "DBG DBG: skipping FIRST DUMP $day.$time >= $first_dump{$srv.$db} \n" )
				if defined $first_dump{$srv.$db} and $day.$time >= $first_dump{$srv.$db};
		next  if defined $first_dump{$srv.$db} and $day.$time >= $first_dump{$srv.$db};
		#debug( "DBG DBG: skipping fulldbhash \n" )
		#	unless defined $full_db_list_hash{$db};
		next unless defined $full_db_list_hash{$db};
		next if $opt_x; # noexec mode
		chomp;
		s/\s//g;
		info("-- Purging ($_)\n");
		$ftp->delete("$log_dir_for_myftp/$_")
			or info("Cant Delete $_ $!\nIn Remote Directory ".$ftp->pwd());
	}

   	info("-- Purging Misc Local Files ($CONFIG{NUM_DAYS_TO_KEEP} days kept)\n");
   	foreach (@required_dirs) {
      		next if $_ eq "dbdumps" or $_ eq "logdumps" or $_ eq "audits";
      		next if $opt_x; # noexec mode
      		purge_local($base_bk_dir."/$_",$CONFIG{NUM_DAYS_TO_KEEP});
   	}
   	purge_local($base_bk_dir."/audits",$CONFIG{AUDIT_PURGE_DAYS});
   	debug("Purging Done\n");
   	$end_time{PURGE}=time;
} else {
   	info("Skipping Purge Step As Per Configuration File\n");
}

#
# DBCC
#
info("\n");
MlpBatchRunning(-message=>"Finished Purge");
if( $CONFIG{DO_DBCC} eq "y" ) {
   $start_time{DBCC}=time;
   infobox("Running Normal DBCC's\n");
   my $cmd=   "dbcc_db.pl $COMMAND_OPTIONS";
   $cmd.=" -D\"".join("\|",@dbcc_db_list)."\"";
   $cmd.=" -o\"".$base_bk_dir."/dbcc\"";
   $cmd.="\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   debug("(executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-print=>1,-err=>"Dbcc Command Failed\n");
   debug("(command completed executing)");
   $end_time{DBCC}=time;
} else {
   info("Skipping Dbcc Step As Per Configuration File\n");
}

#
# TRAN DUMP WITH NO LOG ONLY IF WE ARE NOT LOG SHIPPING
# DO_CLEARLOGSBEFOREDUMPS defaults to y but is n if logs will be shipped
#
info("\n");
if( defined $opt_f and $CONFIG{DO_DUMP} eq "y" and $CONFIG{DO_CLEARLOGSBEFOREDUMPS} eq "y" and $CONFIG{DO_LOAD} ne "y" ) {

   $start_time{CLEAR_TRAN_LOGS}=time;
   infobox("Clearing Transaction Logs\n");
   foreach (@full_db_list) {
      info("-- Clearing Transaction Log For $_\n");
      foreach(dbi_query(-db=>"master",-query=>"dump tran $_ with no_log", -no_results=>1)) {
			my($err)=dbi_decode_row($_);
         error("WARNING: $err\n");
      };
   }
   $end_time{CLEAR_TRAN_LOGS}=time;
} else {
   info("Skipping Clearing Transaction Log Step As Per Configuration File\n");
}

#
# FULL DUMP
#
info("\n");
if( $CONFIG{DO_DUMP} eq "y" ) {
   $start_time{FULL_DB_DUMP}=time;
   infobox("Starting Full Database Dump at ".scalar(localtime(time))."\n");
   my $cmd = "dump_database.pl -Y -J$opt_J -M$type -s$today_val $COMMAND_OPTIONS -o$dump_dir_by_bksvr -f $COMPRESS_DUMP";
   $cmd.=" -D\"".join("\|",@full_db_list)."\"";
   $cmd.=" -n$CONFIG{NUMBER_OF_STRIPES}"
      if defined $CONFIG{NUMBER_OF_STRIPES}
      and $CONFIG{NUMBER_OF_STRIPES}>1;
   $cmd.=" -v$CONFIG{DUMP_FILES_PER_SUBDIR}"
      if defined $CONFIG{DUMP_FILES_PER_SUBDIR}
      and $CONFIG{DUMP_FILES_PER_SUBDIR}>1
      and defined $CONFIG{NUMBER_OF_STRIPES}
      and $CONFIG{NUMBER_OF_STRIPES}>1;
   $cmd.="\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   debug("-- (executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-err=> "Dump Database Failed\n",-print=>1);
   $end_time{FULL_DB_DUMP}=time;

} else {
   info("Skipping Dump Step As Per Configuration File\n");
}

MlpBatchRunning(-message=>"Finished Full Backup");

#
# REORG REBUILD/COMPACT
#
info("\n");
if( $CONFIG{DO_REORG} eq "y") {
	if( $server_type eq "SQLSERVER" ) {
		info("SKIPPING REORG AS THIS IS A SQL SERVER\n");
	} else {
	   info("Starting Reorg\n");
	   MlpBatchRunning(-message=>"Starting Reorg");
	   $start_time{REBUILD_REORG}=time;
	   logdie("NO REORG ARGS") unless $CONFIG{"REORG_ARGS"};
	   my $cmd=   "reorg_sybase.pl $COMMAND_OPTIONS_LONG ".$CONFIG{"REORG_ARGS"}."\n";
	   my $cmd_no_pass=$cmd;
	   $cmd_no_pass=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
	   debug("-- (executing) $cmd_no_pass");
	   run_cmd(-cmd=>$cmd,-err=>"Sybase Reorg Failed\n",-print=>1);
	   $end_time{REBUILD_REORG}=time;
	}
	MlpBatchRunning(-message=>"Finished Reorg Step");
} else {
   info("Skipping Reorg Step As Per Configuration File\n");
}

#
# UPDATE STATISTICS
#
info("\n");
if( $CONFIG{DO_UPDSTATS} eq "y" ) {
   $start_time{UPDATE_STATISTICS}=time;
   infobox("Starting Update Statistics\n");
   my $cmd=   "update_stats.pl $COMMAND_OPTIONS -s ";
   $cmd.=" -D\"".join("\|",@full_db_list)."\"";
	$cmd.=" ".$CONFIG{UPD_STATS_FLAGS};
   $cmd.="\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   debug("-- (executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-err=>"Update Stat Failed\n",-print=>1);
   $end_time{UPDATE_STATISTICS}=time;
} else {
   info("Skipping Update Statistics Step As Per Configuration File\n");
}

#
# LOAD DATABASE
#  read in server to copy to and db list to copy from/to
#
info("\n");
if( $CONFIG{DO_LOAD} eq "y" ) {
	MlpBatchRunning(-message=>"Finished Update Stats - Starting Load");
   $start_time{LOAD_DATABASES}=time;
	if( defined $opt_s ) {
   	info("Skipping Copy/Load As -s Option Passed\n");
	} else {
		info("Starting Copy/Load\n");
   	do_copy_load("TRUE",$dump_dir_for_myftp);
   	info("Completed Copy/Load\n");
	}
   $end_time{LOAD_DATABASES}=time;
} else {
	MlpBatchRunning(-message=>"Finished Update Stats");
   info("Skipping Load DB Step As Per Configuration File\n");
}

#
# DO AUDITING OF CONFIGURATION
#
info("\n");
if( $CONFIG{DO_AUDIT} eq "y" ) {
   MlpBatchRunning(-message=>"Starting Audit");
   $start_time{AUDIT}=time;
   infobox("Running Audit Of The Server  at ".scalar(localtime(time))."\n");
   my($audfile)= $base_bk_dir."/audits/$CONFIG{DSQUERY}.".$today_val;
   my $cmd=   "config_report.pl $COMMAND_OPTIONS -b -o$audfile\n";
   my $cmd_no_pass=$cmd;
   $cmd_no_pass=~s/-P\w+/-PXXX/;
   info("-- (executing) $cmd_no_pass");
   run_cmd(-cmd=>$cmd,-err=>"Audit Failed\n",-print=>1);
   $end_time{AUDIT}=time;
} else {
   info("Skipping Creation of Audit Report Step As Per Configuration File\n");
}

info( "Reconnecting To $CONFIG{REMOTE_HOST}\n");
$start_time{RECONNECT}=time;
#if( $FTPCONFIG{IS_REMOTE} eq "y" ) {
#	debug( "" );
#	info( "Creating Connection To $CONFIG{REMOTE_HOST} \n" );
#}
my($ftp4)= ftp_connect($CONFIG{REMOTE_HOST},\%FTPCONFIG, $ftplogin, $ftppass, $ftpmethod);
$end_time{RECONNECT}=time;

MlpBatchRunning();

#
# COMPRESSION
#
info("\n");
if( $CONFIG{COMPRESSION_LEVEL} =~ /\d$/
and $CONFIG{COMPRESSION_LEVEL}  ne "0" ) {
	MlpBatchRunning(-message=>"Skipping Compression - Internal Sybase Compression Used");
	# Do nothing
	info( "Skipping Compression - Using Sybase Internal Compression level ".$CONFIG{COMPRESSION_LEVEL}."\n" );
} elsif( $CONFIG{DO_COMPRESS} eq "y"
   and   $CONFIG{COMPRESS} =~ /^\s*$/ ) {
	MlpBatchRunning(-message=>"Running Compression But Compress Directive Empty");
	# no compression command
   info("Skipping Compress: DO_COMPRESS=y but COMPRESS directive empty\n");
} elsif( $CONFIG{DO_COMPRESS} eq "y" ) {
	MlpBatchRunning(-message=>"Starting External Compression");
   $start_time{COMPRESS}=time;
   info("Compressing Backup Files at ".scalar(localtime(time))."\n");
   run_compression( $ftp4, $dump_dir_for_myftp, $dump_dir_by_bksvr);
   $end_time{COMPRESS}=time;
} else {
	MlpBatchRunning(-message=>"Skipping Compression Step");
   info("Skipping Compress Step As Per Configuration File\n");
}

info("\n");
if( $CONFIG{DO_INDEXES} eq "y") {
   info("Rebuilding Indexes\n");
	MlpBatchRunning(-message=>"Rebuilding Indexes");
   $start_time{REBUILD_INDEXES}=time;
   my($count)=0;
   my(@f) = split(/\|/,$CONFIG{REBUILD_INDEX_FILE});
   foreach my $db ( split(/\|/,$CONFIG{REBUILD_INDEX_DB})) {
      my $file=$f[$count];
      my $cmd=   "rebuild_index.pl $COMMAND_OPTIONS -r -i$file\n";
      info("Rebuilding Indexes Based On File $file\n");
      $count++;
      my $cmd_no_pass=$cmd;
      $cmd_no_pass=~s/-P\w+/-PXXX/;
      debug("-- (executing) $cmd_no_pass");
      run_cmd(-cmd=>$cmd,-err=>"Rebuild Index Failed\n",-print=>1);
   }
   $end_time{REBUILD_INDEXES}=time;
} else {
   info("Skipping Index Step As Per Configuration File\n");
}

# almost Successful Completion
#  - check directory for sizes of the dumps
info("Cross Check For Dump File Size ($dump_dir_for_myftp)\n");
MlpBatchRunning(-message=>"Cross Check Dump File Sizes");
$start_time{CROSS_CHECK}=time;
my(%FILE_TIMES);
my(%FILE_SIZES);

if( defined $CONFIG{DUMP_FILES_PER_SUBDIR}
      and $CONFIG{DUMP_FILES_PER_SUBDIR}	>1
      and defined $CONFIG{NUMBER_OF_STRIPES}
      and $CONFIG{NUMBER_OF_STRIPES}		>1 ) {
      	my($val)=int(($CONFIG{NUMBER_OF_STRIPES}/$CONFIG{DUMP_FILES_PER_SUBDIR})+.99);
      	for my $cnt (1..$val) {
      		my($ft,$fs)=get_files_in_dir_for_server($ftp4,$dump_dir_for_myftp."/device_".$cnt);
      		foreach (keys %$ft) { $FILE_TIMES{$_}=$$ft{$_}; }
      		foreach (keys %$fs) { $FILE_SIZES{$_}=$$fs{$_}; }
      	}
} else {
	my($ft,$fs)=get_files_in_dir_for_server($ftp4,$dump_dir_for_myftp);
	%FILE_TIMES=%$ft;
	%FILE_SIZES=%$fs;
}
my(%num_stripes_at_time,%num_zero_sized_files,%file_sizes);
foreach (sort keys %FILE_TIMES) {
	debug("Found : File $_ Size=",$FILE_SIZES{$_},"\n");
   my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
   next unless $srv eq $CONFIG{DSQUERY}
            and defined $time
            and $day.".".$time eq $today_val;
   $num_stripes_at_time{$db}++;
   $num_zero_sized_files{$db}++ if $FILE_SIZES{$_}==0;
   $file_sizes{$db}+=$FILE_SIZES{$_};
}

my($file_size_str)="";

if( $CONFIG{DO_DUMP} eq "y" ) {
	$file_size_str= "Dump Files Created By This Run:\n".
		sprintf("%30.30s %14.14s %6.6s %s\n","DataBase Name","Stripes-Made","Errors","Dump Size (bytes)");
	foreach (sort @full_db_list) {
   	$file_size_str.=sprintf("%30.30s %14s %6s %s\n",$_,$num_stripes_at_time{$_},$num_zero_sized_files{$_},$file_sizes{$_});
	}
}
$end_time{CROSS_CHECK}=time;

#
# BCP OUT STUFF AS NEEDED
#
MlpBatchRunning(-message=>"Bcp's");
info("\n");
if( $CONFIG{DO_BCP} eq "y") {
	if( ! defined $CONFIG{BCP_TABLES} or $CONFIG{BCP_TABLES}=~/^\s*$/ ) {
		   info("BCP Flag=Y but BCP_TABLES='' - skipping step as nothing to do\n");	
	} else {
	   MlpBatchRunning(-message=>"Starting Bcp");
	   
	   $start_time{BCP}=time;
	   info("Starting Table Level BCP's\n");
	   info("> Table Spec=$CONFIG{BCP_TABLES}\n");
	   info("> BCP Command=$CONFIG{BCP_COMMAND}\n") if $CONFIG{BCP_COMMAND};
	   my(@bcp_tables)=split(/[\|\,]/,$CONFIG{BCP_TABLES});
	   foreach my $tbl (@bcp_tables) {
	   	logdie "ERROR: bcp table ($tbl) is not fullpath spec (ie. db..tblname)\n"
	   		unless $tbl=~/\./;
	   	info("Copying out table $tbl\n");
			$tbl =~ s/^\s+//;
			$tbl =~ s/\s+$//;
	   	my($cmd)="bcp";
	   	$cmd=$CONFIG{BCP_COMMAND} if $CONFIG{BCP_COMMAND};
	
	   	$cmd.=" $tbl out $base_bk_dir/bcp/$tbl.bcp -U$login -P$password -S$CONFIG{DSQUERY} -n\n";
	   	run_cmd(-cmd=>$cmd, -print=>1, -err=>"bcp failed");
	   }
	   info("Table Level BCP's Completed\n");
	   $end_time{BCP}=time;
	}
} else {
   info("Skipping Bcp Step As Per Configuration File\n");
}

MlpBatchRunning(-message=>"Almost Complete");

# remove empty error logs if they exist
my($errors_exist)="TRUE";
if( defined $opt_e ) {
	if( -z $opt_e ) {
		$errors_exist="FALSE";
		unlink $opt_e;
	}
	if( (-s $opt_e) == 0 ) {
		$errors_exist="FALSE";
		unlink $opt_e;
	}
}

my($backup_timing_str)= backup_timing();

# send email alert if there was an error
my($str)="";
if( defined $opt_e and -e $opt_e and $errors_exist eq "TRUE" ) {

   $str="The Following Error File Was Found:

===========================================================================

   command: $CONFIG{COMMAND_RUN}
   errorlog: $opt_e
   sessionlog: $opt_l
	errorlog size: ".(-s $opt_e)."

===========================================================================

   $file_size_str

===========================================================================

   $backup_timing_str

===========================================================================
ERROR FILE CONTENTS:
";

   open(ELOG,$opt_e) or logdie "Cant open error file $opt_e : $!\n";
   foreach (<ELOG>) { $str.="ERROR: ".$_; }
   close ELOG;
   $str.="\n\n".$header_str;
   bad_email($str);
   print $str;
	exitout("Failed: Backup Job Failed - Check Logs",255,"ERROR");
} else {
	infobox( "Backup Process Completed Successsfully\n");

   info("Job $opt_J Completed With No Errors at ".localtime(time)."\n\n".
         "$file_size_str\n\n".
         $backup_timing_str);
   ok_email("Job $opt_J Completed With No Errors at ".localtime(time)."\n\n".
         "$file_size_str\n\n".
         $backup_timing_str);
}

#
# WE ARE DONE
#
MlpBatchRunning(-message=>"Complete");
dbi_disconnect();
exitout( "Succeeded: Full Backup Batch\n",0,"COMPLETED");

# BELOW ARE SUBROUTINES USED ABOVE
# EVERYTHING BELOW THE I !!

#                                                                          #
#                                                                          #
############################################################################
#                                                                          #
#                                                                          #
sub ftp_connect {
	my($host,$config,$ftplogin,$ftppass,$method)=@_;
	#my(%args);
	if( $opt_m ) {
		$opt_m=uc($opt_m);
		die "-m must be SSH or RSH or FTP" unless $opt_m eq "SSH" or $opt_m eq "RSH" or $opt_m eq "FTP";
	#	foreach (keys %$config) { 	$args{$_}=$$config{$_}; }
		$method="SSH" if $opt_m eq "SSH";
		$method="FTP" if $opt_m eq "FTP";
		$method="RSH" if $opt_m eq "RSH";
	}

	info("connecting to host=$host with method=$method\n");
	my($ftptmporary);
	eval {
		alarm( 10 );
		$ftptmporary= Net::myFTP->new($host,METHOD=>$method,%$config)
              or logdie "Failed To Create FTP to $host Object $@";
      logdie( "Failed To Connect To $host" ) unless $ftptmporary;
		info("ftp_connect() Net::myFTP->new completed to host=$host\n");
		if( $ftplogin ne "null" and $ftppass ne "null" ) {
			debug("ftp_connect() login(login=$login)\n");
			my($erc,$err)=$ftptmporary->login($ftplogin,$ftppass);
			logdie( "Cant $opt_m Connect To host ($host) As $ftplogin : $! // $err\n") if ! $erc;
		}
		info("ftp_connect() connected to host=$host\n");
		alarm( 0 );
	};
	if( $@ ) {
		if( $@ =~ /ERROR: PROGRAM TIMED OUT/ ) {
			logdie( "host $host connection timed out\n" );
		} else {
			logdie( "host $host method $method ERROR ERROR $@\n" );
		}
		alarm(0);
	}
	#my($ftptmporary)= Net::myFTP->new($host,METHOD=>$method,%$config)
   #           or logdie "Failed To Create FTP to $host Object $@";

	debug("completed ftp_connect($ftptmporary)\n");
	return $ftptmporary;
}

sub run_compression {
	my($conn,$dir,$remote_dir)=@_;
   my($ft,$fs)=get_files_in_dir_for_server($conn,$dir);
   my(%FILE_TIMES)=%$ft;
   my(%FILE_SIZES)=%$fs;

	# Find the "latest" file if needed (so we dont compress it)
   my(%LATEST_FILE);
   if( $CONFIG{COMPRESS_LATEST} eq "n" ) {
      foreach (sort keys %FILE_TIMES) {
         next if /\.gz$/ or /\.Z/;
         my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);
         next unless defined $full_db_list_hash{$db};
         next unless $srv eq $CONFIG{DSQUERY} and defined $time;
         if( ! defined $LATEST_FILE{$srv." ".$db}  or
               $LATEST_FILE{$srv." ".$db} < $day.$time ) {
            $LATEST_FILE{$srv." ".$db}=$day.$time;
         }
      }
   }

   foreach (sort keys %FILE_TIMES) {
      if( /\.gz$/ or /\.Z$/ ){
			debug( "(allready compressed) $_\n" );
			next;
		};
		debug("Compress: File $_ Size=",$FILE_SIZES{$_},"\n");
      my($srv,$db,$key,$day,$time,$stripe,$compress)=split(/\./,$_);

      if( ! defined $full_db_list_hash{$db} ) {
			debug("Ignoring $_ (not dumped) \n");
			next;
		};

		debug("Ignoring $_ As Not Server or No Time Defined")
      	unless $srv eq $CONFIG{DSQUERY} and defined $time;
      next unless $srv eq $CONFIG{DSQUERY} and defined $time;

      if( $CONFIG{COMPRESS_LATEST} eq "n"
            and $day.$time eq $LATEST_FILE{$srv." ".$db} ) {
			debug( "Skipping $_ (COMPRESS_LATEST=n)\n" );
			next;
		};

		debug( "Will attempt to compress $_ \n" );
      my $cmd;
      if( $CONFIG{IS_REMOTE} eq "n" ) {
			unlink "$dir/$_.gz" if -r "$dir/$_.gz";
			unlink "$dir/$_.Z" if -r "$dir/$_.Z";
         $cmd="$CONFIG{COMPRESS} $dir/$_\n";
         info("-- Compressing $_ using $CONFIG{COMPRESS}\n");
         debug("-- (executing) $cmd");
			if( $CONFIG{COMPRESS}=~/gzip/ ) {
        		info("-- Gzip Found.  Running Command $cmd\n");
         		run_cmd(-cmd=>$cmd,-print=>1);
				sleep(10);
        		info("-- Warning: Compress Failed To Remove File\n")
						if -e "$dir/$_";
				if( -e "$dir/$_" and -e "$dir/$_.gz" ) {
         		info("-- Attempting to Remove File $_\n") ;
						# if  -e "$dir/$_.gz" and $CONFIG{COMPRESS}=~/gzip/;
					unlink "$dir/$_";
						# if -e "$dir/$_.gz" and $CONFIG{COMPRESS}=~/gzip/;
         		info("-- Error $!\n") if  -e "$dir/$_";
							# and $CONFIG{COMPRESS}=~/gzip/;
				} elsif( -e "$dir/$_" ) {
         		info("-- File Exists W/No Compressed File - Recompressing\n") ;
         		run_cmd(-cmd=>$cmd,-print=>1);
				}
				if( -e "$dir/$_" ) {
         		info("-- Backup File *STILL* Exists \n") ;
				}
			} else {
         	# run_cmd(-cmd=>$cmd,-err=>"Compress Failed\n",-print=>1);
         	run_cmd(-cmd=>$cmd,-print=>1);
			}
      } else {
         info("-- Remote Compressing $_\n");
			do_rsh(-login=>$CONFIG{REMOTE_ACCOUNT},
					-password=>$CONFIG{REMOTE_PASSWORD},
					-hostname=>$CONFIG{REMOTE_HOST},
					-debug=>$opt_d,
					-command=>"$CONFIG{COMPRESS} $remote_dir/$_");
		}
      # $cmd="rsh -l $CONFIG{REMOTE_ACCOUNT} $CONFIG{REMOTE_HOST} $CONFIG{COMPRESS} $remote_dir/$_\n";
      # debug("-- (executing) $cmd");
      # run_cmd(-cmd=>$cmd,-err=>"Compress Failed\n",-print=>1);
      # run_cmd(-cmd=>$cmd,-print=>1);
      #
   }
}

sub by_time {
   $start_time{$a} <=> $start_time{$b};
}

sub backup_timing {
	my $seconds=time-$start_time{BEGIN};
	my $minutes=int($seconds/60);
	$seconds-=60*$minutes;
	my($str)= "Job $opt_J completed successfully in $minutes minutes and $seconds secs.".$NL;
	foreach (sort by_time keys %start_time ) {
   	next if $_ eq "BEGIN";
   	if( ! defined $end_time{$_} ) {
      	warning "Hmmmm... No End Time Defined For $_\n";
      	next;
   	}
   	my $seconds = $end_time{$_} - $start_time{$_};
   	my($minutes)= int($seconds/60);
   	$seconds   -= (60*$minutes);
   	$str.=sprintf("%30.30s : %s%s",
                  	" -- $_",
                  	"completed in $minutes minutes $seconds secs.",
                  	$NL);
	}
	return $str;
}

#
# Returns FILE_TIME,FILE_SIZE hashes
#
sub get_files_in_dir_for_server
{
	my($ftp,$directory)=@_;
	debug( "Getting Files In $directory\n" );
	my(%FILE_TIME,%FILE_SIZE);
	my($file_count)=0;
	debug( "ftp\-\>cwd($directory) \n");
	$ftp->cwd($directory) or warn "get_files_in_dir_for_server() Unable to cwd to $directory: $!\n";
	foreach ( $ftp->fspec(".") ) {
		my($filenm,$filetime,$filesize)=@$_;
		next if $filenm =~ /^\./;
		$file_count++;
		$FILE_TIME{$filenm}=$filetime;
		$FILE_SIZE{$filenm}=$filesize;
		debug("$file_count) $filenm  $filetime $filesize\n");
	}
	debug("$file_count Files Found In Directory=$directory\n");
	return(\%FILE_TIME,\%FILE_SIZE);
}

#
# Remove Local Files
#
sub purge_local
{
  	my($dir,$days)=@_;
   	debug("Purging Directory $dir ($days Days)\n");

   	opendir(DIR,$dir) || croak("Purge Can't open directory $dir for reading\n");
   	my(@files) = grep(!/^\./,readdir(DIR));
   	closedir(DIR);

	debug( ($#files+1)." Files Found\n" );
   	foreach (@files) {
				my($filedays)= -M "$dir/$_";
      		next  if $filedays < $days or -z "$dir/$_";
      		$filedays = int( $filedays * 10 ) / 10;
      		info "-- Purging $_ : ".$filedays." days old\n";
      		unlink $dir."/".$_ or info( "Cant Unlink $dir/$_  $!\n" ) unless $opt_x;
   	}
   	debug("Done Purging Directory $dir ($days Days)\n");
}

sub run_cmd
{
   my(%OPT)=@_;
   my(@rc);
   chomp $OPT{-cmd};

   $OPT{-cmd} .= " 2>&1 |" unless defined $ENV{WINDIR};
   $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;

   my $cmd_no_pass=$OPT{-cmd};
   if( $cmd_no_pass=~ /-PASSWORD=/ ) {
   	$cmd_no_pass=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
   } else {
   	$cmd_no_pass=~s/-P\w+/-PXXX/;
   }
   info("(".localtime(time).") ".$cmd_no_pass."\n") if  defined $OPT{-print};

   if( ! $opt_x ) {
	   $OPT{-cmd} .= " |" 		    if defined $ENV{WINDIR};

	   open( SYS2STD,$OPT{-cmd} ) || logdie("Unable To Run $OPT{-cmd} : $!\nPath is $ENV{PATH}\n");
	   while ( <SYS2STD> ) {
	      chop;
	      push @rc, $_;
	   }
	   close(SYS2STD);
	} else {
		info("NOEXEC MODE - SKIPPING EXECUTION OF STEP\n");
		info("The Command Would Have Been: $cmd_no_pass\n");
	}

   my($cmd_rc)=$?;
   if( $cmd_rc != 0 ) {
	if( defined $OPT{-err} ) {
  		info("completed (failed) return_code=$cmd_rc\n");
			my($str)="Error: ".$OPT{-err}."\n";
			$str.="Return Code: $?\n";
      		$str.="Command: ".$cmd_no_pass."\n";
			$str.="\n\nResults:\n";
      		logdie( $str."\n-- ".join("\n-- ",@rc)."\n");
	} elsif( defined $OPT{-print} ) {
		info( "Return Code:$?\n");
      		info( "Results:\n-- ".join("\n-- ",@rc)."\n");
	}
	unshift @rc,"Error: Return Code $?";
		return @rc;
	} else {
		info("Command completed at ".localtime(time)."\n")
			if defined $OPT{-print};
		info( "-- ".join("\n-- ",@rc)."\n" )
			if defined $OPT{-print};
		return @rc;
	}
}

sub check_directory_structure
{
   print "debug: Checking Directory Structure\n" if defined $opt_d;

   #
   # Make sure directory tree is ok locally
   # Make local directories as needed
   #
   die "Error Cant Find Base Directory $CONFIG{BASE_BACKUP_DIR}.\nThis local directory is REQUIRED for the system to function and should exist on the local system prior to runnig this command.\n" unless -d $CONFIG{BASE_BACKUP_DIR};

   if( ! -d $base_bk_dir ) {
      print("Creating Directory $base_bk_dir\n");
      mkdir($base_bk_dir,0775)
         or logdie "Cant Make Directory $base_bk_dir : $!\n";
   };

   foreach (@required_dirs) {
      next if -d "$base_bk_dir/$_";
      mkdir($base_bk_dir."/$_",0775)
            or die("Cant Make Directory $base_bk_dir/$_ : $!\n");
   }

   die "Error Cant Find Directory $base_bk_dir after mkdir is done\n"
      unless -d $base_bk_dir;
}

#
# Print Header Information
#
sub create_page_header {

   my($header_str)="Backup Scripts: ".$VERSION. "\n";
   $header_str.="    Run At ".(scalar localtime(time))."\n";
   $header_str.="    Run on hostname=".hostname()."\n\n" ;


   $header_str.=sprintf( "%-13.13s %-24.24s\n","Job Name:",$opt_J);
   $header_str.=sprintf( "%-13.13s %-24.24s\n","Server:",$CONFIG{DSQUERY});
   $header_str.=sprintf( "%-13.13s %-s\n","Ftp Path:",$srvr_dir_for_myftp );
   $header_str.=sprintf( "%-13.13s %-s\n","Ignore Db:",$CONFIG{IGNORE_LIST} );
   $header_str.=sprintf( "%-13.13s %-s\n","SessionLog:",$opt_l);
   $header_str.=sprintf( "%-13.13s %-s\n","ErrorLog:",$opt_e);

   $header_str.=sprintf( "%-13.13s %-s\n","Output To:",$CONFIG{BASE_BACKUP_DIR});
   $header_str.=sprintf( "%-13.13s %-s\n","File Stamp:",$today_val);

   if( defined $opt_f ) {
      $header_str.=sprintf( "%-13.13s %-s\n","Dir (local):",$CONFIG{LOCAL_DIR} );
      if( $CONFIG{IS_REMOTE} eq "y" ) {
         $header_str.=sprintf( "%-13.13s %-s\n","Rsh Dir:",$CONFIG{REMOTE_DIR} );
         $header_str.=sprintf( "%-13.13s %-24.24s\n","Rsh Host:",$CONFIG{REMOTE_HOST} );
      }

      $header_str.=sprintf( "%-13.13s %-24.24s ","Purge Keeps:",$CONFIG{NUM_BACKUPS_TO_KEEP});
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Num Stripes:",$CONFIG{NUMBER_OF_STRIPES});

      $header_str.=sprintf( "%-13.13s %-24.24s","Is Remote:",$CONFIG{IS_REMOTE} );
      $header_str.=sprintf( "%-13.13s %-24.24s","Do Compress:",$CONFIG{DO_COMPRESS} )
      		unless $CONFIG{COMPRESSION_LEVEL} =~ /^\d$/;
		$header_str.="\n";
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Purge:",$CONFIG{DO_PURGE} );
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Audit:",$CONFIG{DO_AUDIT} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Dbcc:",$CONFIG{DO_DBCC} );
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Bcp:",$CONFIG{DO_BCP} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Upd Stats:",$CONFIG{DO_UPDSTATS});
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Indexes:",$CONFIG{DO_INDEXES} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Dump:",	$CONFIG{DO_DUMP});
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Load:",	$CONFIG{DO_LOAD} );
      $header_str.=sprintf( "%-13.13s %-24.24s ","Do Reorg:",	$CONFIG{DO_REORG} );
      $header_str.=sprintf( "%-13.13s %-24.24s\n","Do Tran Clear:",$CONFIG{DO_CLEARLOGSBEFOREDUMPS} );
	   $header_str.="UPDATE STATISTICS FLAG: ".$CONFIG{UPD_STATS_FLAGS}."\n";

      if( $CONFIG{COMPRESSION_LEVEL} =~ /^\d$/
      and $CONFIG{COMPRESSION_LEVEL} ne "0" ) {
         $header_str.=sprintf( "%-13.13s Sybase Internal Level %d\n","Compress:",$CONFIG{COMPRESSION_LEVEL} );
      } elsif( $CONFIG{DO_COMPRESS} eq "y" ) {
         $header_str.=sprintf( "%-13.13s %-24.24s ","Compress:",$CONFIG{COMPRESS} );
         $header_str.=sprintf( "%-13.13s %-24.24s\n","UnCompress:",$CONFIG{UNCOMPRESS} );
      }

		$header_str.="\n";


		foreach (keys %CONFIG ) {
			next unless /^XFER_/;
			next unless defined $CONFIG{$_};
			next unless $CONFIG{$_} !~ /^\s*$/;
      	$header_str.=sprintf( "%-23.23s %-24.24s\n",$_,$CONFIG{$_});
		}

      $header_str.="\n";

   } else {
      $header_str.="\nTransaction Log Dump\n";
   }

	# Program Flow
#	$header_str .= "Program Flow\n";
#	$header_str .= " -- Get Database List\n";
#	if( defined $opt_t ) {
#		$header_str .= " -- Dump transaction Logs to $log_dir_by_bksvr\n";
#		$header_str .= " -- Do_Copy_Load($log_dir_for_myftp)\n";
#	} else {
#		$header_str .= " -- Purge Files\n" if $CONFIG{DO_PURGE} eq "y";
#		$header_str .= " -- Do DBCC\n" if $CONFIG{DO_DBCC} eq "y";
#		$header_str .= " -- Dump Tran NoLog\n"
#			if defined $opt_f and $CONFIG{DO_DUMP} eq "y";
#		$header_str .= " -- Full Dump\n"
#			if $CONFIG{DO_DUMP} eq "y";
#		$header_str .= " -- Update Stats\n"
#			if $CONFIG{UPDATE_STATISTICS} eq "y";
#		$header_str .= " -- Do_Copy_Load($dump_dir_for_myftp)\n"
#			if $CONFIG{DO_LOAD} eq "y";
#		$header_str .= " -- Audit DB \n"
#			if $CONFIG{DO_AUDIT} eq "y";
#		$header_str .= " -- Bcp Tables \n"
#			if $CONFIG{DO_BCP} eq "y";
#		if( $CONFIG{COMPRESSION_LEVEL} =~ /\d$/
#		and $CONFIG{COMPRESSION_LEVEL}  ne "0" ) {
#		} elsif( $CONFIG{DO_COMPRESS} eq "y"
#   		and   $CONFIG{COMPRESS} =~ /^\s*$/ ) {
#		} elsif( $CONFIG{DO_COMPRESS} eq "y" ) {
#			$header_str .= "-- Compressing Files";
#		}
#		$header_str .= " -- Rebuild Indexes\n" 	if $CONFIG{DO_INDEXES} eq "y";
#	}
#	$header_str.="\n";

   return $header_str;
}

# do_copy_load($is_full_dump,$source_dir);
#		@cp_files= files to copy
#		if( $CONFIG{XFER_BY_FTP} eq "y" )
#			$ftp_from = new connection to ftphost/REMOTE_HOST
#			cd XFER_SCRATCH_DIR
#			$ftp_to   = new connection to XFER_TO_HOST
#			$ftp_to->cwd(XFER_TO_DIR)
#			foreach (@cp_files) { ftp_from->get(); ftp_to->put(); unlink(); }
#     else
#			foreach (@cp_files) { copy $_, XFER_TO_DIR/...
#
#     foreach pair of xfer databases
#			load_database.pl -SXFER_TO_SERVER -iXFER_TO_DIR/DSQUERY.db.type.today
#		foreach @cp_files { ftp_to->delete($_) }
#
#   - source_dir used to calculate the files that you want to fetch/put
#   - you will then fetch to scratch and put to target dir.
sub do_copy_load
{
	my($is_full_dump, $source_dir)=@_;
  	$start_time{REMOTE_SYSTEM_COPY}=time;
	info( "Copying data for load - Full Dump=$is_full_dump\n-- Source Directory=$source_dir\n");
	my($filetype)="dbdump";

	# if XFER_TO_DB and XFER_FROM_DB both equal * then fill them out
	if( $is_full_dump eq "FALSE" ) {
		$filetype="logdump";
		if( $CONFIG{XFER_FROM_DB} eq "*" and $CONFIG{XFER_TO_DB} eq "*" ) {
			$CONFIG{XFER_TO_DB}=join("|",@tran_db_list);
			$CONFIG{XFER_FROM_DB}=$CONFIG{XFER_TO_DB};
		}
	} else {
		if( $CONFIG{XFER_FROM_DB} eq "*" and $CONFIG{XFER_TO_DB} eq "*" ) {
			$CONFIG{XFER_TO_DB}=join("|",@full_db_list);
			$CONFIG{XFER_FROM_DB}=$CONFIG{XFER_TO_DB};
		}
	}

	# ok check required variables
   if( defined $CONFIG{XFER_TO_SERVER}
   and defined $CONFIG{XFER_TO_DB}
   and defined $CONFIG{XFER_FROM_DB}
   #and defined $CONFIG{XFER_TO_DIR}
   and $CONFIG{DO_DUMP} eq "y" ) {
      info("-- Variables Checked\n");
		my($ftp_to,$ftp_from);

		# create expected list of files
		my(@cp_files);
      if( defined $CONFIG{NUMBER_OF_STRIPES}
		and 			$CONFIG{NUMBER_OF_STRIPES}>1
		and 			! defined $opt_t ) {
			foreach my $rdb ( split(/\|/,$CONFIG{XFER_FROM_DB})) {
            next if ! defined $full_db_list_hash{$rdb}
                    and $is_full_dump eq "TRUE";
            next if ! defined $tran_db_list_hash{$rdb}
                    and $is_full_dump ne "TRUE";

				for (my $stripe=1; $stripe<=$CONFIG{NUMBER_OF_STRIPES}; $stripe++) {
					push @cp_files,$CONFIG{DSQUERY}.".".$rdb.".$filetype.".$today_val.".S".$stripe;
				}
			}
		} else {
			foreach my $rdb ( split(/\|/,$CONFIG{XFER_FROM_DB})) {
            next if ! defined $full_db_list_hash{$rdb}
                    and $is_full_dump eq "TRUE";
            next if ! defined $tran_db_list_hash{$rdb}
                    and $is_full_dump ne "TRUE";
				push @cp_files,$CONFIG{DSQUERY}.".".$rdb.".$filetype.".$today_val;
			}
		}

		foreach (@cp_files) { info("-- File = $_\n"); }
      my($lunix_login,$lunix_password,$unix_type,$lmethod);
		if( $CONFIG{XFER_BY_FTP} eq "y" ) {
			info("-- Copying These Files\n");

			# if REMOTE_HOST != LOCAL HOST
			# 		connect to system with backup server (REMOTE_HOST)
			# 		copy dump files locally XFER_SCRATCH_DIR

			info("Transfer To Remote System\n");
			info( "   - XFER_BY_FTP: ".$CONFIG{XFER_BY_FTP}."\n");
			info( "   - SOURCE_HOST: ".$CONFIG{REMOTE_HOST}."\n");
			info( "   - SOURCE_DIR: ".$source_dir."\n");
			info( "   - SOURCE_DB: ".$CONFIG{XFER_FROM_DB}."\n");
			info( "   - LOCAL HOST (from hostname()): ".hostname()."\n");
			info( "   - XFER_SCRATCH_DIR:  ".$CONFIG{XFER_SCRATCH_DIR}."\n");
			info( "   - TARGET HOST: ".$CONFIG{XFER_TO_HOST}."\n" );
			info( "   - TARGET DIRECTORY: (".$CONFIG{XFER_TO_DIR}.")\n" );
			info( "   - TARGET_DB: ".$CONFIG{XFER_TO_DB}."\n");
			info( "   - TARGET_SERVER: ".$CONFIG{XFER_TO_SERVER}."\n");

			# its silly but you need to reconnect here because of timeouts
			debug("Creating Another Ftp Connection to $CONFIG{REMOTE_HOST} \n");
			$ftp_from=ftp_connect($ftphost,\%FTPCONFIG, $ftplogin, $ftppass,$ftpmethod);

			debug("source->binary()");
   		$ftp_from->binary();
			debug("source->cwd($source_dir)\n");
         $ftp_from->cwd($source_dir)
				or logdie "FTP connection on $CONFIG{REMOTE_HOST} cant cwd to $source_dir";

			# cd on local system
			debug("(local) cd $CONFIG{XFER_SCRATCH_DIR} \n");
			chomp $CONFIG{XFER_SCRATCH_DIR};
			chdir $CONFIG{XFER_SCRATCH_DIR}
				or logdie "Cant cd to $CONFIG{XFER_SCRATCH_DIR}: $!\n";

			info( "" );
			info( "... Creating Second FTP Connection To $CONFIG{XFER_TO_HOST} \n" );
      	($lunix_login,$lunix_password,$unix_type)=get_password(-name=>$CONFIG{XFER_TO_HOST},-type=>"unix");
			logdie("No Login/Password Available for ($CONFIG{XFER_TO_HOST})\n")
				unless defined $lunix_login and defined $lunix_password;

			my(%args)=get_password_info(-type=>"unix",-name=>$CONFIG{XFER_TO_HOST});
			if( is_nt() ) {
				$lmethod=$args{WIN32_COMM_METHOD} if is_nt();
			} else {
				$lmethod=$args{UNIX_COMM_METHOD};
			}
			logdie( "COMM_METHOD NOT DEFINED for SERVER $CONFIG{XFER_TO_HOST}\n" ) unless $lmethod;

			if( $lmethod eq "NONE" ) {	# hmm cant talk
				die "host $CONFIG{XFER_TO_HOST}: METHOD=NONE - aborting\n";
			}

			debug("tgt->login($CONFIG{XFER_TO_HOST},$lunix_login,$lunix_password)\n");
			$ftp_to=ftp_connect($CONFIG{XFER_TO_HOST},\%FTPCONFIG, $lunix_login, $lunix_password,$lmethod);
			debug("tgt->binary()");
   		$ftp_to->binary();
			debug("tgt->cwd($CONFIG{XFER_TO_DIR})\n");
         $ftp_to->cwd($CONFIG{XFER_TO_DIR})
				or logdie "Cant Chdir to $CONFIG{XFER_TO_DIR}";
			info( "... Connection Ok \n" );

			# ok we have connections lets start copying
			foreach my $fl ( @cp_files ) {
				info( "get from $ftphost:$fl\n" );
				$ftp_from->get( $fl )
					or logdie "Cant Get File $fl from $ftphost : $!\nls on $ftphost returns ".join("\n",sort( $ftp_from->ls()))."\n";
				if( ! -e "$CONFIG{XFER_SCRATCH_DIR}/$fl" ) {
					#use Cwd;
					#my($curdir)=cwd();
					logdie "File $fl Not Received after ftp copy from $ftphost ????\nSCRATCH DIR = $CONFIG{XFER_SCRATCH_DIR}\n";
				}
				info( "     ... ",-s "$CONFIG{XFER_SCRATCH_DIR}/$fl" , " Bytes Fetched into $CONFIG{XFER_SCRATCH_DIR}\n" );
				info( "put to $CONFIG{XFER_TO_HOST} directory $CONFIG{XFER_TO_DIR}\n" );
				$ftp_to->put("$CONFIG{XFER_SCRATCH_DIR}/$fl",$fl)
					or logdie "Cant Put File $fl : $!\nls on $CONFIG{XFER_TO_HOST} returns ".join("\n",sort( $ftp_to->ls()))."\n";
				info( "     Put $fl To Target ".$CONFIG{XFER_TO_HOST}."\n" );
				info( "     Unlink $CONFIG{XFER_SCRATCH_DIR}/$fl\n" );
				unlink "$CONFIG{XFER_SCRATCH_DIR}/$fl" or print "Cant unlink $CONFIG{XFER_SCRATCH_DIR}/$fl : $!\n";
			}
			info( ($#cp_files+1)," Files Ftp'ed...\n" );
   			undef $ftp_from;
   		chomp $curdir;
   		debug("(local) cd $curdir \n");
			chdir $curdir or die "Cant cd to $curdir: $!\n";
   		info("\n");
		} else {
			if( defined  $CONFIG{XFER_TO_DIR} and $CONFIG{XFER_TO_DIR} !~ /^\s*$/){
				info("-- Using Regular Copy To $CONFIG{XFER_TO_DIR} \n");
				info "-- From Directory=$source_dir\n";
				info "-- To Directory=$CONFIG{XFER_TO_DIR}\n";
				die "Cant Write To XFER_TO_DIR" unless -w $CONFIG{XFER_TO_DIR};
				use File::Copy;
				# my($dir);
				# if( defined $opt_t )
				# { $dir=$log_dir_for_myftp; }
				# else 	{ $dir=$dump_dir_for_myftp; }
				foreach( @cp_files ) {
					if( ! $opt_x ){ # noexec mode
					# die "Hmmm File $_ not in $dir\n" unless -r "$dir/$_";
						die "Hmmm File $_ not in $source_dir\n" unless -r "$source_dir/$_";
	         		info("   -> copying $_\n");
	         		copy("$source_dir/$_",$CONFIG{XFER_TO_DIR}."/".$_)
							or die "File Copy Failed : $!";
					}
				}
			} else {
				info("-- Skipping Ftp (XFER_BY_FTP=".$CONFIG{XFER_BY_FTP}.")\n\n");
				info("-- Files Stay Local \n");
				if( $CONFIG{XFER_TO_DIR} ) {
					if( defined $opt_t ) {
						$CONFIG{XFER_TO_DIR} = $CONFIG{XFER_TO_DIR}."/logdumps";
					} else {
						$CONFIG{XFER_TO_DIR} = $CONFIG{XFER_TO_DIR}."/dbdumps";
					}
				} else {
					$CONFIG{XFER_TO_DIR}=$source_dir;
				}
			}
		}

  		$end_time{REMOTE_SYSTEM_COPY}=time;
      		$start_time{FULL_DB_LOAD}=time;

		if( defined $opt_n ) {
   			info("\nSkipping Load Database at ".scalar(localtime(time))."\n");
		} else {
   			infobox("Starting Load Database at ".scalar(localtime(time))."\n");
      			info "-- Directory: $CONFIG{XFER_TO_DIR}  \n\n";
		}

		my(@fr_dblist) = split( /\|/, $CONFIG{XFER_FROM_DB} );
		info "-- From DB List=".join(" ",@fr_dblist)."\n";
		my(@to_dblist) = split( /\|/, $CONFIG{XFER_TO_DB}   );
		info "--   To DB List=".join(" ",@to_dblist)."\n";

		my($count)=0;
		my($to_syb_login,$to_syb_password,$to_syb_type) = get_password(-name=>$CONFIG{XFER_TO_SERVER},-type=>"sybase");
		($to_syb_login,$to_syb_password,$to_syb_type)   = get_password(-name=>$CONFIG{DSQUERY},-type=>"sqlsvr")
			unless defined $to_syb_login and defined $to_syb_password and $to_syb_login !~ /^\s*$/ and $to_syb_password !~ /^\s*$/;
		logdie("No Login Found for server $CONFIG{XFER_TO_SERVER}") unless defined $to_syb_login and $to_syb_login!~/^\s*$/;
		logdie("No Password Found For $to_syb_login") unless defined $to_syb_password and $to_syb_password!~/^\s*$/;

		my(@load_msgs);
		while ( $count <= $#fr_dblist ) {
			my($fr_db)=$fr_dblist[$count];
			my($to_db)=$to_dblist[$count];
			$count++;

			if( $is_full_dump eq "TRUE" ) {
				if( ! defined $full_db_list_hash{$fr_db} ) {
					info "Skipping $fr_db As it is not in Full DB List (".join(" ",@full_db_list).")\n";
					next;
				}
			} else {
				if( ! defined $tran_db_list_hash{$fr_db} ) {
					info "Skipping $fr_db As it is not in Tran DB List (".join(" ",@tran_db_list).")\n";
					next;
				}
			}

      	info "-- Loading $fr_db from $CONFIG{DSQUERY} to $to_db on ",$CONFIG{XFER_TO_SERVER},"\n";

			my($ld_dir)= $CONFIG{XFER_TO_DIR};
			$ld_dir= $CONFIG{XFER_TO_DIR_BY_TARGET}
				if defined $CONFIG{XFER_TO_DIR_BY_TARGET};
			# set it to the dump directory if it is not set
			$ld_dir= $CONFIG{LOCAL_DIR} 				unless $ld_dir;
			# Final Check On Options
      	my $cmd = "$curdir/load_database.pl ".
         	" -Y -J$opt_J -U$to_syb_login -S$CONFIG{XFER_TO_SERVER} $COMPRESS_LOAD -P$to_syb_password -l$opt_l -e$opt_e -M$to_syb_type".
         	" -i".$ld_dir."/".
         	$CONFIG{DSQUERY}.".".$fr_db.".$filetype.".$today_val.
         	" -D\"".$to_db."\"";

			$cmd .= " -O" unless $CONFIG{DO_ONLINEDB} eq "y";

      	$cmd.=" -n$CONFIG{NUMBER_OF_STRIPES}"
         	if defined $CONFIG{NUMBER_OF_STRIPES}
         	and $CONFIG{NUMBER_OF_STRIPES}>1
				and $is_full_dump eq "TRUE";

			$cmd.=" -tk" if $is_full_dump eq "FALSE";
      	$cmd.="\n";

			die "Bad Parameter List\n$cmd\nThis is an internal error pls contact author."
				unless 	defined $opt_J
					and	defined $login
					and 	defined $CONFIG{XFER_TO_SERVER}
					and	defined $password
					and	defined $opt_l
					and	defined $opt_e
					and	defined $to_db
					and	$opt_J	!~ /^\s*$/
					and	$login	!~ /^\s*$/
					and 	$CONFIG{XFER_TO_SERVER}	!~ /^\s*$/
					and	$password	!~ /^\s*$/
					and	$opt_l	!~ /^\s*$/
					and	$opt_e	!~ /^\s*$/
					and	$to_db	!~ /^\s*$/;

      	my $cmd_no_pass=$cmd;
      	$cmd_no_pass =~ s/-P\w+/-PXXX/;
		if( defined $opt_n ) {
      			info("-- (skipping - -n defined) $cmd_no_pass");
		} else {
      			info("-- (executing) $cmd_no_pass");
   			push @load_msgs,run_cmd(-cmd=>$cmd,-print=>1);
		}
	}
      	$end_time{FULL_DB_LOAD}=time;
	my($load_ok)="NA";
	foreach ( @load_msgs ) {
		if( /Error: Return Code/ ) {
			$load_ok = "FAILED";
			last;
		}

		next unless /^GOOD/;
		info("LOAD SUCCEEDED: $_\n");
		$load_ok="GOOD" if $load_ok eq "NA";
		last;
	}
	$load_ok="GOOD" if $#load_msgs<0;

		# cleanup if needed
		if( defined $ftp_to and ! defined $opt_n ) {
			info("   (clean up temporary ftp files on $CONFIG{XFER_TO_HOST}) \n");
			undef $ftp_to;

			# reconnect because full loads may have timed you out
			debug("tgt->login($CONFIG{XFER_TO_HOST},$lunix_login,$lunix_password)\n");
			$ftp_to=ftp_connect($CONFIG{XFER_TO_HOST},\%FTPCONFIG, $lunix_login, $lunix_password,$lmethod);
         $ftp_to->cwd($CONFIG{XFER_TO_DIR}) or info "ERROR: Cant Chdir to $CONFIG{XFER_TO_DIR}";

			foreach( @cp_files ) {
				info("   (deleting) $_\n");
				$ftp_to->delete($_)
					or info("Cant delete $_ $!");
			}
			undef $ftp_to;
			info("   clean up temporary ftp files completed\n");			
		}

      logdie("LOAD COMMAND FAILED\n".join("\n--",@load_msgs)."\nLOAD COMMAND FAILED")
			if $load_ok ne "GOOD";

   } else {
      info("Configuration Error For Load Part of These Dumps\n");
      info("MUST DEFINE XFER_TO_SERVER\n") 	unless defined $CONFIG{XFER_TO_SERVER};
      info("MUST DEFINE XFER_TO_DB\n") 		unless defined $CONFIG{XFER_TO_DB};
      info("DO_DUMP!=y\n") 			unless $CONFIG{DO_DUMP} eq "y";
      info("MUST DEFINE XFER_FROM_DB\n") 	unless defined $CONFIG{XFER_FROM_DB};
    #  info("MUST DEFINE XFER_TO_DIR\n") 	unless defined $CONFIG{XFER_TO_DIR} ;
   }
}

sub exitout {
	my($msg,$rc,$state)=@_;
	if( $rc==0 ) {
		MlpBatchDone();
	} else {
   	warning($msg);
		MlpBatchErr( -message =>  $msg);
	}
#	MlpHeartbeat(-monitor_program   =>  "backup.pl",
#                -system            =>  $CONFIG{DSQUERY},
#                -subsystem         =>  $subsystem,
#                -state             =>  $state,
#                #-message_text      =>  $tran_log_text." ".$msg,
#                -message_text      =>  $msg,
#					 -batchjob			  =>	1
#                # -event_time      =>  $EVENT_TIME,
#                # -debug           =>  $DEBUG,
#                # -event_id        =>  $EVENT_ID,
#                # -message_value   =>  $MESSAGE_VALUE,
#                # -document_url    =>  $DOCUMENT_URL,
#	);
	exit($rc);
}

__END__

=head1 NAME

backup.pl - master driver for backup scripts

=head2 USAGE

backup.pl -S SERVER -l|-f

=head2 DESCRIPTION

backup.pl is the main driver script for your operations and will appropriately call the other scripts in this package
to get its work done.  It is probably the only program you will add to your scheduler. It reads the configuration
file and process servers based on the options you have specified. Operations are performed in the following order:

        Purge Old Dumps (if necessary use rsh)
        Dbcc Databases
        Dump Tran Log
        Full Dump
        Check for Required Database Loads
        Update Statistics
        Run Audit
        Optional Table Level Bcps
        Compress The Dumps (if necesary use rsh)
        Rebuild Indexes

=head2 USAGE

Basic Usage:

 Usage: backup_pl -JJOB -t|-f [-dh]

Advanced Usage:

Usage: backup_pl -JJOB -t|-f [-dh] -mmode

This is THE Master backup script.
This script performs appropriate tasks as defined by maintenance plans (specified with -JJOB)
which are defined in configure.cfg (located in backup scripts directory or one level above it).

 -x noexec mode                         (purges will exec but other stuff wont)
 -n copy but dont load for logshipping
 -t for transaction log dumps only
 -f for full backups    (default).
 -m mode = SSH|FTP|RSH  (override communication mode)
 -d is debug mode
 -b batchid (for alarming)
 -h is html output.
 -s skip transfer and load step
 -DDB_LIST (pipe separated) restriction on databases
 -RlpdDuabCir will run only the (l)oad, (p)urge, (d)ump, (D)bcc, (r)eorg,
      (u)pdate stats, (a)udit, (b)cp, (C)ompress, and (i)ndex steps.

=head2 SYNOPSIS

This is the master driver for the backup scripts.  It relies on the Job being defined in the configure.cfg (located in
backup scripts directory or one level above it).  It follows instructions and performs the appropriate tasks as needed.
-t for transaction log dumps only, -f for full backups. -d is debug mode and -h is html output.

You can also specify -DDB_LIST to restrict databases operations are performed on further.

=head2 Notes

A transaction log dump (with no_log) is always done prior to the full dump.

tempdb and model are NEVER dumped

Optionally Compress the dumps

Update Statistics and Recompile will never work on tempdb/model

Audit Reports require extended procs to be installed

On production servers, you will also wish to run backup.pl -l. This does a transaction log dump on SOME of your databases. The databases that are dumped are those that are "production", which means that "trunc. log on checkpoint" is off and "select into/bcp" is also off. Of course, master, model, and tempdb are never transaction log dumped.  Log dumps are placed in dated files in the .../logdumps sub directory.

=cut

