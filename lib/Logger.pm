#!/usr/local/bin/perl-5.6.1

package 		Logger;

require	  	5.000;
use	     	Exporter;
use	     	Carp;

my($VERSION) = 1.01;

@ISA	    	= qw(Exporter);
@EXPORT 	= qw( logger_init logdie infof infobox info debug alert warning bad_email ok_email );

use strict;
use Carp;
use File::Basename;
use Log::Dispatch;
use MlpAlarm;
use Log::Dispatch::Screen;
use Log::Dispatch::File;
use DBIFunc;					# for dbi_set_option so database errors get alarmed
use Sys::Hostname;

my $log;
my $errfile;
my $command_run;
my $mail_host;
my $userdnsdomain;
my $success_mail_to;
my %init_args;
my $mail_to;
my $jobname;
my $arg_debug;
my $log_msgs_to_stdout;
my($warning_printed)=0;			# print command messge only on the FIRST warning() call

# When the program ends, remove empty error logs if they exist
END {
	unlink $errfile if defined $errfile and -z $errfile;
}

# args
#  -debug=>			optional
#  -logfile			$opt_l
#  -errfile			$opt_e
#  -database			$opt_d
#  -command_run 		$CONFIG{COMMAND_RUN}
#  -mail_host 			$CONFIG{MAIL_HOST}
#  -mail_to 			$CONFIG{MAIL_TO}
#  -success_mail_to 		$CONFIG{SUCCESS_MAIL_TO}
#  -system, -subsystem, -message, -batchjob (optional) if defined a heartbeat will be sent with MlpHeartbeat
sub logger_init {
	my(%args)=@_;
	$log = Log::Dispatch->new();

	$errfile 			= $args{-errfile};
	$command_run 			= $args{-command_run};
	$mail_host			= $args{-mail_host};
	$success_mail_to		= $args{-success_mail_to};
	$mail_to			= $args{-mail_to};
	$jobname			= $args{-jobname};
	$arg_debug			= $args{-debug};
	$log_msgs_to_stdout	= $args{-log_msgs_to_stdout};

	foreach (keys %args) { $init_args{$_}=$args{$_}; }
   	print "logger(): running in debug mode\n" if defined $args{-debug};

	# set user definable handlers for sybase functions
	dbi_set_option(-print=>\&info,-croak=>\&logdie);

	# Debug Messages only show if $args{-debug} is defined
	$log->add(Log::Dispatch::Screen->new( name=>'screen2',
      		min_level=>'debug',
      		max_level=>'debug',
      		screen=>0 )) if defined $args{-debug};

	if( defined $args{-system} and defined $args{-message} ) {
		my($state)="OK";
		$state="STARTED" if $args{-batchjob};
		MlpHeartbeat(
			-quiet=>1,
			-state          => $state,
			-system		=> $args{-system},
   	    		-subsystem	=> $args{-subsystem},
   	    		-message_text   => "Started: $args{-message}",
			-batchjob	=> $args{-batchjob}
		);
	}

	# Add Dispatcher To The Screen For Info through critical Mesages
	$log->add(Log::Dispatch::Screen->new(   name=>'screen1',
      		min_level=>'info',
      		max_level=>'critical',
      		screen=>0 )
      	);

	# Add Dispatcher To Log File For Info through critical Messages
	if( defined $args{-logfile} ) {
   	my $dir = dirname($args{-logfile});

   	logdie( "Error: Directory $dir does not exist\n")  unless -d $dir;
   	logdie( "Error: Directory $dir is not writable\n") unless -w $dir;

   	$log->add(Log::Dispatch::File->new( name=>'file1',
  	     	min_level=>'info',
       		max_level=>'critical',
       		filename=>$args{-logfile},
       		mode => 'append' ));
	}

	if( defined $args{-errfile} ) {
   	my $dir = dirname($args{-errfile});

   	logdie("Error: Directory $dir does not exist\n")  unless -d $dir;
   	logdie("Error: Directory $dir is not writable\n") unless -w $dir;

   	# Add Dispatcher To Error File For error+ Messages
   	$log->add(Log::Dispatch::File->new( name=>'errfile1',
       	min_level=>'error',
       	max_level=>'critical',
       	mode=>'append',
       	filename=>$args{-errfile} ));
	}

	# Add Dispatcher For Email error+ Messages
	# emergency is the message level that im currently using for mail.
	# sent via the email method defined above
	if( defined $mail_to or defined $success_mail_to ) {
		print "Setting Up Mail Target $mail_to\n" if defined $args{-debug};
		$success_mail_to=$mail_to unless defined $success_mail_to;
		$mail_to=$success_mail_to unless defined $mail_to;

   	if( eval "require Log::Dispatch::Email::MIMELite" ) {
      	Log::Dispatch::Email::MIMELite->import();
         print "MAIL HOST = $mail_host\n" if defined $args{-debug};
         MIME::Lite->send("smtp",$mail_host,Timeout=>60) if defined $mail_host;

      	my($user)=$ENV{USERNAME} || $ENV{USER} || $ENV{LOGNAME};
      	$user.= "\@".hostname();
      	$user.= ".".$ENV{USERDNSDOMAIN} if defined $ENV{UISERDNSDOMAIN};
			print "User=$user\n" if defined $args{-debug};

      	if( defined $success_mail_to and $success_mail_to !~ /^\s*$/ ) {
         	my(@l_successmailto)= split( /,/,$success_mail_to );
         	my($subject);
				if( defined $args{-success_subject} ) {
					$subject=$args{-success_subject};
				} else {
					$subject= "Successful Run Of ".basename($0);
					$subject.=" on host=".hostname();
					$subject.=" job=".$jobname if defined $jobname;
				}
         	$log->add(Log::Dispatch::Email::MIMELite->new(
             	name=>'emailok',
             	min_level=>'alert',
             	max_level=>'alert',
             	to=>\@l_successmailto,
	     			from=>$user,
             	subject => $subject));
      	}

      	if( defined $mail_to and $mail_to !~ /^\s*$/ ) {
         	my(@l_mailto)= split( /,/,$mail_to );
         	my($subject);
				if( defined $args{-fail_subject} ) {
					$subject=$args{-fail_subject};
				} else {
					$subject="Error ".basename($0)." host=".hostname();
					$subject.=" job=".$jobname if defined $jobname;
         		$subject.= " DB=$args{-database}" if defined $args{-database};
				}
				print "Subject for emergency =$subject\n" if defined $args{-debug};
				print "Mail To for emergency =$mail_to\n" if defined $args{-debug};
         	$log->add(Log::Dispatch::Email::MIMELite->new(
             	name=>'emailbad',
             	min_level=>'emergency',
             	max_level=>'emergency',
             	to=>\@l_mailto,
	     			from=>$user,
             	subject =>$subject ));
      	} else {
         	print "No MAIL_TO Address Defined\n";
      	}
   	} else {
      	print "require Log::Dispatch::Email::MIMELite failed.\n";
   	}
	} else {
		print "Not Setting Up Mail \n" if defined $args{-debug};
	}
}

sub warning  {
   $log->warning("Command Run Was:\n\t".$command_run."\n")
      if defined $command_run and $warning_printed == 0;
   $warning_printed++;
   $log->warning(join("",@_));
   print(join("",@_)) if defined $log_msgs_to_stdout;
}

sub logdie{
   my(@lines)=@_;
   print "DEBUG: LOGDIE CALLED (@lines)\n" if defined $log_msgs_to_stdout;
   print "DEBUG: logdie called (".join("\n->",@lines),")\n";

   my $cmd_no_pass=$command_run;
   $cmd_no_pass=~s/-P\w+/-PXXX/;

	if( defined $init_args{-system}
	and defined $init_args{-message} ) {
		my($state)="CRITICAL";
		$state="FAILED" if $init_args{-batchjob};
		MlpHeartbeat(
			-state      => $state,
			-system		=> $init_args{-system},
			-quiet		=> 1,
	   	   	-subsystem	=> $init_args{-subsystem},
   	   		-message_text   => "Failed: ".$init_args{-jobname}." ".join(" ",@lines),
			-batchjob	=> $init_args{-batchjob}
		);
	}

   my($str)="\n=================================\nTime: ".localtime(time)."\n";
   $str.="Command: ".$cmd_no_pass."\nHost: ".hostname()."\n" if defined $command_run;
   $str.="Log File: $init_args{-logfile} \n" 	if defined $init_args{-logfile};
   $str.="Error File: $init_args{-errfile} \n" 	if defined $init_args{-errfile};
   $str.= "\n";

   foreach my $row (@lines) {
		my(@x)=split(/[\r\n]/,$row);
		foreach (@x) {
			next if /^\s*$/;
      			$str= " $_\r\n".$str;
		}
   }
   bad_email($str);

   MlpBatchErr(-quiet=>1,-message=>join("\n",@lines));	# this will only save results if you have set up previously

   exit 1;
}

sub infof { my($fmt)= shift @_; my($str)= sprintf($fmt,@_); $log->info($str); }

sub infobox  {
	my($in)=join("",@_);
	my(@out);
	push @out,"#################################\n";
	foreach(split(/\n/,$in)) { push @out,"# ".$_."\n"; }
	push    @out,"#################################\n";
	info(@out);
}

sub info  {
   $log->info(join("",@_));
   print(join("",@_)) if defined $log_msgs_to_stdout;
}

sub debug {
   $log->debug("debug: ".join("",@_));
   print("debug: ".join("",@_)) if defined $log_msgs_to_stdout;
}

sub alert {
	my($str);
	foreach (@_) {
		chomp;
		next if /^\s*$/;
		$str.="alert: ".$_."\n";
	}
   $log->critical($str);
   print($str) if defined $log_msgs_to_stdout;

   # $log->critical("alert: ".join("alert: ",@_));
   # print("alert: ".join("alert: ",@_)) if defined $log_msgs_to_stdout;
}

sub bad_email {
   print "DEBUG: BAD EMAIL CALLED (@_)\n"
		if defined $log_msgs_to_stdout or defined $arg_debug;

   if( defined $mail_to and $mail_to !~ /^\s*$/ ) {
   	my($str)=join("",@_);
   	$str =~ s/\!\!\!\s*\!\!\!/!!!/g;
   	$str =~ s/ +\!\!\!/!!!/g;

      print "LOG MSGS IS DEFINED\n"    if defined $log_msgs_to_stdout;
      print "DEBUG  IS DEFINED\n"     	if defined $arg_debug;
   	print("\nSending Email to ".$mail_to." Regarding:\n".$str)
            if defined $log_msgs_to_stdout or defined $arg_debug;

   	$log->emergency( $str );
   	$log->critical("\nSending Email to ".$mail_to." Regarding:\n".$str);

   } else {
      	$log->critical("\n!!! (Email List Is Empty)\n".join("",@_));
      	print("\n!!!! (Email List Is Empty)\n".join("",@_))
        		if defined $log_msgs_to_stdout or defined $log_msgs_to_stdout or defined $arg_debug;
   }
}

sub ok_email {
   	# $log->info(join("",@_));
   	print(join("",@_)) if defined $log_msgs_to_stdout;
   	if( defined $success_mail_to and $success_mail_to !~ /^\s*$/ ) {
       		$log->alert(join("",@_));
   	} elsif( ! defined $success_mail_to ) {
		print "Not sending success mail as variables \$success_mail_to not defined. please set with -success_mail_to in logger_init()\n"
				if defined $arg_debug;
	} else {
		print "Not sending success mail as variable \$success_mail_to = rsuccess_mail_to) not defined. please set with -success_mail_to in logger_init().\n"
				if defined $arg_debug;
	}
	print "LOG=$log_msgs_to_stdout SuccessMail=$success_mail_to\n" if $arg_debug;
}

1;

=head1 NAME

Logger.pm - master message handler

=head2 SYNOPSIS

Logger.pm allows you a variety of utility functions to log messages approrpiately according to arguments you have passed in to the logger_init() function.  These directives allow you to specify files to log to as well as potential email addresses to send stuff to.

=head2 LOGGER OPTIONS

The Arguments passed to logger_init() define the process.

  -debug					do you wish to see debug messages (1 or undef)
  -logfile				a log file
  -errfile				an error log file.  removed at process end if empty
  -database				a database name for error messages
  -command_run 		the current command being run with arguments
  -mail_host 			a mail server/host for mail messages
  -mail_to 				comma separated list of mail targets for errors
  -success_mail_to 	comma separated list of mail targets for success
  -success_subject   subject line for successes
  -fail_subject      subject line  for fails

=head2 logger_init() options for Ed Barlow Scripts

  -debug						optional
  -logfile					$opt_l
  -errfile					$opt_e
  -database				   $opt_d
  -command_run 			$CONFIG{COMMAND_RUN}
  -mail_host 				$CONFIG{MAIL_HOST}
  -mail_to 				   $CONFIG{MAIL_TO}
  -success_mail_to 		$CONFIG{SUCCESS_MAIL_TO}

=head2 FUNCITONS

	logger_init()
	logdie(@msgs)
	infof(fmt,msg)
	info(@msgs)
	debug(@msgs)
	alert(@msgs)
	warning(@msgs)

	bad_email(@msgs)
	ok_email(@msgs)

Priority order is

	debug() < info() < alert() < logdie()

=head2 ROUTING

The following maps functions to actions.

  HANDLER    MIN LVL     MAX LVL
  screen1    info        critical      print to screen normal msgs
  screen2    debug       debug         debug messages printed only if -d
  file1      info        critical      log file  - normal messages
  errfile1   error       critical      error file-  for warnings and up
  emailok    alert       alert         use alert level for ok emails
  emailbad   emergency   emergency     use emergency for failures

=cut

