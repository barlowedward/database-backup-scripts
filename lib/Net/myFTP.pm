# Copyright (c) 1999-2006 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package Net::myFTP;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use strict;
use Exporter;
use File::Copy;
use Carp;
use File::Basename;
use Sys::Hostname;
use Cwd;
use Net::FTP;
use Net::myRSH;
use Data::Dumper;

my $debug;
my $developer_debug;	# set if developing - will remove eventually
							# currently this is set if the -debug flag is passed to new()

$VERSION= 1.0;

@ISA     = qw(Exporter Net::FTP);
@EXPORT  = (@Net::FTP::EXPORT,'get_local','chmod','runcmd');

$|=1;
#my $connection_mode;
my %local_hash;		# contains connection type for all connections
							# need to do it this way due to $package vs $$package distinctions...

# outstanding issue : why do i need to do this $package vs $$package distinction?  When i return a
# Net::FTP object it requires two $$ 
sub _print { 	
	print @_; 
}
sub new {
	my($package,$tohost,%FLAGS)=@_;
	my $host = hostname();
	my $thost= $tohost;
	my $connection_mode;
	$host	=~ s/\.[\w\.]+//;
	$thost  =~ s/\.[\w\.]+//;
	$debug = $FLAGS{Debug} || $FLAGS{-debug} || $developer_debug;
	$FLAGS{Debug} = 1 if  $debug;
	$developer_debug=1 if $debug;		
	
	if( $developer_debug ) {	
		print "DEV DBG $host / $thost / $debug \n";
		foreach ( keys %FLAGS ) { print "DEV DBG $_ $FLAGS{$_} \n"; }
	}
	print "myftp->new() Debug mode set for connection to $tohost\n" if defined $debug;
	
	if( $host eq "localhost" or ( defined $FLAGS{IS_REMOTE} and $FLAGS{IS_REMOTE} eq "n")
		or $host eq $thost ) {
		$connection_mode="LOCAL";
		my($self)={};
		bless ($self, $package);	
		
		$self->{tohost} = $tohost;
		$self->{printfunc} = $FLAGS{PRINTFUNC} || \&_print;
		$self->{debugfunc} = $FLAGS{DEBUGFUNC};
		$self->{debugfunc} = \&_print if ! $self->{debugfunc} and $debug;
		$self->{current_local_directory} = Cwd::cwd();	# "./";
		#$self->{current_local_directory}.="/"
		#	unless $self->{current_local_directory}=~/[\\\/]$/;
	 	if( $developer_debug ) {
	 			print "DEV DBG File: ",__FILE__," Line: ",__LINE__,"\n";use Data::Dumper;print Dumper $self;	
	 	}
		print "DEV DBG File: ",__FILE__," Line: ",__LINE__," connection_mode set to $connection_mode\n" if $debug;
		$local_hash{$self} = $connection_mode;
		$self->myFTPprint( $debug, "myftp->new() new local connection to a local host\n" );
		return $self;
	} elsif( $FLAGS{METHOD} eq "NONE" ) {
			print "myftp->new(SSH) Cant talk to myFTP with NONE specified as comm method\n";			
			print "DEV DBG File: ",__FILE__," Line: ",__LINE__," connection_mode set to $connection_mode\n" if $debug;
			return undef;
	} elsif( $FLAGS{USE_SSH} or $FLAGS{METHOD} eq "SSH" ) {		
		$connection_mode="SSH_MODE";
		do_rsh_method("ssh");
		my($self)={};
		$self->{printfunc} = $FLAGS{PRINTFUNC}  || \&_print;
		$self->{debugfunc} = $FLAGS{DEBUGFUNC};
		$self->{debugfunc} = \&_print if ! $self->{debugfunc} and $debug;
		$self->{tohost} = $tohost;
		$self->{flags}=\%FLAGS;
		$self->{current_local_directory} = "/";
		bless ($self, $package);
		$local_hash{$self} = $connection_mode;
		
		$self->myFTPprint( $debug,  "myftp->new() Using SSH for Connection to $tohost\n" );
		if( $developer_debug ) { print "DEV DBG File: ",__FILE__," Line: ",__LINE__,"\n";use Data::Dumper;print Dumper $self;	}
		print "DEV DBG File: ",__FILE__," Line: ",__LINE__," connection_mode set to $connection_mode\n" if $debug;
		
		return $self;
	} elsif( $FLAGS{USE_RSH} or $FLAGS{METHOD} eq "RSH" ) {		
		$connection_mode="RSH_MODE";
		do_rsh_method("rsh");
		my($self)={};
		$self->{tohost}=$tohost;
		$self->{flags}=\%FLAGS;
		$connection_mode="SSH_MODE";
		$self->{printfunc} = $FLAGS{PRINTFUNC} || \&_print;
		$self->{debugfunc} = $FLAGS{DEBUGFUNC};
		$self->{debugfunc} = \&_print if ! $self->{debugfunc} and $debug;
		$self->{current_local_directory} = "/";
		bless ($self, $package);
		$local_hash{$self} = $connection_mode;
		
		$self->myFTPprint( $debug, "myftp->new() Using RSH for Connection to $tohost\n" );
		if( $developer_debug ) {print "DEV DBG File: ",__FILE__," Line: ",__LINE__,"\n";use Data::Dumper;print Dumper $self;	}
		print "DEV DBG File: ",__FILE__," Line: ",__LINE__," connection_mode set to $connection_mode\n" if $debug;
		
		return $self;
	} else {
		$connection_mode="FTP_MODE";		
		print "myftp->new() connection_mode set to $connection_mode\n" if $debug;
		if( $debug ) {
			foreach ( keys %FLAGS ) {
				print "myftp->new() param $_ is $FLAGS{$_} \n";
			}
		}

		print "myftp->new() calling Super::new() \n" if $debug;
		my $ftp= $package->SUPER::new($tohost,%FLAGS);
		return undef if $ftp eq "";
		$$ftp->{tohost}=$tohost;
		
		$$ftp->{current_local_directory} = "/";
		$$ftp->{printfunc} = $FLAGS{PRINTFUNC} || \&_print;
		$$ftp->{debugfunc} = $FLAGS{DEBUGFUNC};
		$$ftp->{debugfunc} = \&_print if ! $$ftp->{debugfunc} and $debug;
		$$ftp->{flags}=\%FLAGS;		
		
		$local_hash{$ftp} = $connection_mode;
		
		$ftp->myFTPprint( $debug, "myftp->new() new ftp connection to remote host $tohost\n" );
		print "myftp->new() new() completed \n" if $debug;
		return $ftp;
	}
}

sub myFTPprint {
	my $package = shift;
	my $print_flag = shift;
	
	my($connection_mode) = $local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
		
	if( $connection_mode eq "FTP_MODE" ) {
		#my($p)=$$package->{printfunc};
		#my($d)=$$package->{debugfunc};
		#print "$p ",ref $p," \n";
		#print "$d ",ref $d," \n";
		#&$d(@_) if defined $d;
		#&$p(@_) if defined $p;		
		if( $print_flag ) {
			&{$$package->{printfunc}}(@_) if defined $$package->{printfunc};	 		
		} else {
			&{$$package->{debugfunc}}(@_) if defined $$package->{debugfunc};
		}
		return;	 		
	}
	
	#use Data::Dumper;
	#print Dumper $package;
	if( $print_flag ) {
		&{$package->{printfunc}}(@_) if defined $package->{printfunc};	 		
	} else {
		&{$package->{debugfunc}}(@_) if defined $package->{debugfunc};
	}	
}

sub chmod {
	my $package = shift;
	my($mode,$file)=@_;
	my($connection_mode)=$local_hash{$package};
	print "DEV DBG: chmod($mode,$file) mode=$connection_mode \n" if $developer_debug;
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	return undef if $connection_mode eq "FTP_MODE";	
	
	if( $connection_mode eq "LOCAL" ) {
		$file=$package->_localdir().$file
			 	unless $file =~ /^[\\\/]/ or $file =~ /^\w:/;
			 	
		if( ! -e $file ) {
			warn "chmod() File $file Does Not Exist\n";
			return undef;
		}
		
		#$mode=oct($mode) unless $mode=~/^0/;
		$package->myFTPprint( $debug, "system(chmod $mode $file) LOCAL\n" );
		print "DEV DBG: Running system(chmod $mode $file)\n" if $developer_debug;
		system("chmod $mode $file");
		return( 1 );
	}
	
	$package->myFTPprint( $debug, "myftp->chmod($mode,$file) mode=$connection_mode\n" );
	my($cmd)="\"(cd ".$package->_localdir().";chmod $mode $file)\"";
	print "DEV DBG: Running $cmd\n" if $developer_debug;
	my($rc)= do_rsh(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>$cmd,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
	if(   $rc=~/^failure/ ) {
		$package->myFTPprint( $debug, "myftp->chmod() $cmd returned $rc\n" );
		return( 0 );
	}
	return( 1 );	
}
sub reconnect {
	my $package = shift;
	
	my($connection_mode)=$local_hash{$package};
	#print "DEV DBG: reconnect() mode=$connection_mode \n" if $developer_debug;
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	# no point disconnecting unless you have a persistent connection
	return $package unless $connection_mode eq "FTP_MODE";	
		
	my($ftp)= Net::myFTP->new($$package->{tohost}, %{$$package->{flags}});
	$ftp->login( $$package->{myLogin}, $$package->{myPassword} );
	$ftp->binary() if $$package->{mode} eq "binary";
	$ftp->ascii()  if $$package->{mode} eq "ascii";
	$ftp->cwd($$package->{cwd}) if defined $$package->{cwd};
	return $ftp;
}

sub get_local
{
	my $package = shift;
	return $local_hash{$package};
}

sub login {
	my $package = shift;	
	
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
		
	#print __LINE__," DEV DBG: login() localhash=",$local_hash{$package}," local=",$connection_mode,"\n" if $developer_debug;
	#print Dumper $package;
	
	if( $connection_mode eq "LOCAL" ) {
		$package->myFTPprint( $debug, "myftp->login() LOCAL FUNCTION login() CALLED\n" );
		return( 1 );
	} elsif( $connection_mode ne "FTP_MODE"  ) {
		$package->myFTPprint( $debug, "myftp->login() CALLED - mode=$connection_mode\n" );
		$package->{myLogin}=$_[0];
		$package->{myPassword}=$_[1];
		
		if( $connection_mode eq "RSH_MODE" or $connection_mode eq "SSH_MODE" ){		
			print __LINE__," DEV DBG: Double Testing $connection_mode - echo helloworld\n" if $developer_debug;
			my($helloworld)=do_rsh(
				-debugfunc=>$package->{debugfunc},
				-hostname =>$package->{tohost},
				-command=>"echo helloworld",
				-debug=>$debug,
				-login=>$package->{myLogin},
				-show_resultset=>1,
				-password=>$package->{myPassword});
			if( $developer_debug ) {
				print __LINE__," DEV DBG : echo returned (".$helloworld.")\n";
				print __LINE__," DEV DBG : WOAH - its not even defined\n" unless defined $helloworld;
			}
			if( $helloworld =~ /^failure/ ) {
				$package->myFTPprint( $debug, "myftp->login() $helloworld\n" );
				return wantarray ? (0,$helloworld) : 0;
			}
						
			if( $helloworld !~ /^helloworld/ ) {
				if( ! $helloworld or $helloworld=~/^\s*$/ ) {
						print __LINE__," DBG DBG : how odd - i am rerunning echo helloworld ($helloworld) - debug mode set\n";
						#$helloworld.=" (rerunning echo) ";
						# how odd... traping illegal seek failures
						$helloworld=do_rsh(
							-debugfunc=>$package->{debugfunc},
							-hostname =>$package->{tohost},
							-command=>"echo helloworld",
							-debug=>1,
							-login=>$package->{myLogin},
							-password=>$package->{myPassword});
						print __LINE__," DBG DBG : helloworld is now ($helloworld)\n";
						
				}
				if( $helloworld !~ /^helloworld/ ) {
					print __LINE__," DBG DBG: YIKES! echo helloworld returned $helloworld\n";		
					$package->myFTPprint( $debug, "myftp->login() echo helloworld returned $helloworld ??? \n" );
					return wantarray ? (0,"failure : echo helloworld returned (".$helloworld.")") : 0;
				}
			}
		}
		$package->myFTPprint( $debug, "myftp->login() base connect ok!\n" );					
		$package->_localdir( do_rsh(
				-debugfunc=>$package->{debugfunc},
				-hostname =>$package->{tohost},
				-command=>"pwd",
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword}));
		my($d)=$package->_localdir();
		if( $d =~ /^failure / ) {
			$package->myFTPprint( $debug, "myftp->login() error connecting : ",$d,"\n" );
			return wantarray ? (0,$d) : 0 ;
		}
		$d .= "/" 	unless $d=~/[\\\/]$/;
		$package->_localdir( $d );
		$package->myFTPprint( $debug, "myftp->login() curdir set to $d\n" );

		return( 1 );
	}
	
	$$package->{myLogin}=$_[0];
	$$package->{myPassword}=$_[1];
	#print __LINE__," DBG DBG... $_[0] $_[1] \n";
	my($rc) = $package->SUPER::login(@_);
	#print __LINE__," DBG DBG... $! \n";
	return wantarray ? ($rc,$!) : $rc ;	
}

sub pwd {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	#if( $connection_mode eq "LOCAL" ) {
	#	$package->myFTPprint( $debug, "myftp->pwd(".$package->_localdir().")\n" );
	#	return $package->_localdir();
	#} els
	if( $connection_mode ne "FTP_MODE"  ) {
		my($d)=$package->_localdir();
		$package->myFTPprint( $debug, "myftp->pwd($connection_mode) - pwd will return $d\n" );
		$package->myFTPprint( $debug, "myftp->pwd(".$d.")\n" ) ;
		return $d;
	}
	$package->myFTPprint( $debug, "myftp->pwd($connection_mode) - calling Net::FTP::pwd\n" );
	return $package->SUPER::pwd(@_);
}

sub rename {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( $connection_mode eq "LOCAL" ) {
		my($from)=shift;
		my($to)=shift;
		return rename($from,$to) or $package->myFTPprint( 1, "Error: Cant Rename $from to $to : $!\n" );
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		my($cmd)="\"(cd ".$package->_localdir().";mv @_ )\n";
		my($rc)=do_rsh(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>$cmd,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $rc=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->rename() $cmd returned $rc\n" );
			return( 0 );
		}
		return( 1 );
	}
	return $package->SUPER::rename(@_);
}

sub cwd {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
		
	my($d)=$package->_localdir();
	if( $connection_mode ne "FTP_MODE" ) {
	
		return 1 if $_[0] eq ".";
		die "myftp->cwd(..) Fatal Error - cant currently cwd(..) in ssh/rsh mode\n" if $_[0] eq "..";
		if( $_[0] =~ /^[\\\/]/ or $_[0] =~ /^\w:/ ) {
			$d=$_[0];			
		} else {
			$d=$d.$_[0];			
		}		
		$d.="/" unless $d=~/[\\\/]$/;			
		$package->myFTPprint( $debug, "myftp->cwd( $d ) - Connection mode = $connection_mode \n" );
		if( $connection_mode ne "LOCAL" ) {			
			my($rc)=do_rsh(	
					-hostname	=>$package->{tohost},
					-debugfunc	=>$package->{debugfunc},
					-command		=>"cd $d",
					-debug		=>$debug,
					-login		=>$package->{myLogin},
					-password	=>$package->{myPassword});		
			
			if( $rc=~/^failure/ ) {
				$package->myFTPprint( $debug, "myftp->cwd($d) returned $rc\n" );
				return( 0 );
			}	
		} else {			
			if( ! -d $d ) {				# does directory exist?
				$package->myFTPprint( $debug, "myftp->cwd($d) directory does not exist\n" );
				return( 0 );
			}
		}
		$package->_localdir($d);
			
		#$package->_localdir($d."/") ;		
		$package->myFTPprint( $debug, "myftp->cwd($d) $connection_mode\n" );
		return(1);
	}
	
	$package->myFTPprint( $debug, "myftp->cwd( $_[0] ) - connection mode = $connection_mode \n" );
	my($rc)=$package->SUPER::cwd($_[0]);
	if( ! $rc ) {
		$package->myFTPprint( $debug, "myftp->cwd( $_[0] ) - failed - returning $rc\n" );
		#print "DBG : cwd FAILED to $_[0]\n";
		return $rc;
		#$package->SUPER::close();
		#$package->SUPER::quit();
		#$!=undef;
		#my($rc2)= $package->SUPER::login( $$package->{myLogin}, $$package->{myPassword} );
		#return undef unless $rc2;
		#$package->myFTPprint( 1, "myftp->cwd(): ($rc2) cant reconnect as ",$$package->{myLogin}," $!\n" ) unless $rc2;
		#$rc=$package->SUPER::cwd(@_);

		#$package->binary() if $$package->{mode} eq "binary";
		#$package->ascii()  if $$package->{mode} eq "ascii";
		#$package->myFTPprint( $debug, "myftp->cwd() DBG: Done Resetting mode\n" );
		#die ("DONE");
	}
	$$package->{cwd}=$_[0];
	$package->_localdir($_[0]);	
	return $rc;
}

sub get {
	my $package = shift;
	my($from_file,$to_file)=@_;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	$package->myFTPprint( $debug, "myftp->get($from_file,$to_file) mode=$connection_mode\n" );
	return undef if $from_file eq "" or ! defined $from_file;

	if( $connection_mode eq "LOCAL" ) {
		$package->myFTPprint( $debug, "myftp->pwd() LOCAL FUNCTION get($from_file,$to_file) CALLED\n" );
		$from_file=$package->_localdir().$from_file
		 	unless $from_file =~ /^[\\\/]/ or $from_file =~ /^\w:/;
		if( ! -e $from_file ) {
			 $!="Source File Not Found";
			 return undef;
		}
		copy($from_file,$to_file) or return undef;
		return $to_file;
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		$to_file=$from_file unless $to_file;
		$from_file=$package->_localdir().$from_file
		 	unless $from_file =~ /^[\\\/]/ or $from_file =~ /^\w:/;
		my($rc)=do_rcp(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-mode => "get",
				-remotefile=>$from_file,
				-localfile=>$to_file,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $rc=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->put: $rc \n" );
			return undef;
		}
		return $to_file;
	}
	return $package->SUPER::get(@_);
}

sub put {
	my $package = shift;
	my($from_file,$to_file)=@_;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	$package->myFTPprint( $debug, "myftp->put($from_file,$to_file) mode=$connection_mode\n" );
	if( $connection_mode eq "LOCAL" ) {		
		if( ! -e $from_file ) {
			warn "From File $from_file Does Not Exist\n";
			return undef;
		} elsif( ! -r $from_file ) {
			warn "From File $from_file Exists but Is Not Readable\n";
			return undef;
		}
		if( ! $to_file ) {
			$to_file = $package->_localdir();
			$to_file.="/" unless $to_file=~/\/$/;
			$to_file.=basename($from_file) 
		} else {
			$to_file=$package->_localdir().$to_file
			 	unless $to_file =~ /^[\\\/]/ or $to_file =~ /^\w:/;
		}
		#print "DBG DBG: Local Mode Copy\nFrom=$from_file\nTo=$to_file\n";
		#my($dir)=dirname($to_file);	# DBG DBG
		#print "DBG DBG $from_file Exists\n" if -r $from_file;
		#print "DBG DBG $dir Exists\n" 		if -d $dir;
		#print "DBG DBG $dir Writable\n" 		if -w $dir;
		#print "DBG DBG $to_file Exists\n" 	if -e $to_file;
		#print "DBG DBG $to_file Writable\n" if -w $to_file;
	
		my($rc)=copy($from_file,$to_file);
		if( $rc == 0 ) {
		 	warn "Cant Copy $from_file to $to_file : $!\n";
		 	
		 	#open(TST2,"$from_file") or warn "CANT DIRECT READ DBG DBG TO $from_file\n";
		 	#close(TST2);
		 	
		 	#open(TST,"> $to_file") or warn "CANT DIRECT WRITE DBG DBG TO $to_file\n";
		 	#close(TST);
		 	
		 	return undef;
		}
		return $to_file;
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		if( ! $to_file ) {
			$to_file = $package->_localdir();
			$to_file.="/" unless $to_file=~/\/$/;
			$to_file.=basename($from_file) 
		} else {
			$to_file=$package->_localdir().$to_file
			 	unless $to_file =~ /^[\\\/]/ or $to_file =~ /^\w:/;
		}
		#$to_file=$package->_localdir().$from_file unless $to_file;
		#$to_file=$package->_localdir().$to_file
		# 	unless $to_file =~ /^[\\\/]/ or $to_file =~ /^\w:/;
		my($rc)=do_rcp(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-mode => "put",
				-remotefile=>$to_file,
				-localfile=>$from_file,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $rc=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->put: $rc \n" );
			return undef;
		}
		$package->myFTPprint( $debug, "myftp->put() rc =$rc\n" );
		return $to_file;
	}
	return $package->SUPER::put(@_);
}

sub quit {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( $connection_mode ne  "FTP_MODE" ) {
		$package->myFTPprint( $debug, "myftp->quit() mode=$connection_mode\n" );
		return( 1 );
	}
	return $package->SUPER::quit(@_);
}

# rmdir [ dir ]
sub rmdir {
	my $package = shift;
	my($directory)=shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	$package->myFTPprint( $debug, "myftp->rmdir($directory) mode=$connection_mode\n" );
	if( $connection_mode eq "LOCAL" ) {
		return if ! -d $directory;
		rmdir($directory) or croak "Cant rmdir $directory";
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		my($cmd)="\"(cd ".$package->_localdir().";rmdir $directory ) ";
		my($rc)=do_rsh(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>"rmdir $directory",
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $rc=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->rmdir() $cmd returned $rc\n" );
			return( 0 );
		}
		return( 1 );

	} else {
		return $package->SUPER::rmdir($directory);
	}
}

# delete [ dir, recurse ]
sub delete {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( $connection_mode eq "LOCAL" ) {
		my($file)=shift;
		$package->myFTPprint( $debug, "myftp->delete($file) mode=$connection_mode\n" );

		return if ! -e $file;
		if( ! unlink($file) ) {
			warn "Cant unlink $file";
			return( 0 );
		}
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		my($file)=shift;

		my($cmd)="\"(cd ".$package->_localdir().";rm -f ";
		$cmd.="$file";
		$cmd.=")\"";

		$package->myFTPprint( $debug, "myftp->delete($file) mode=$connection_mode\n" );
		my($rc)= do_rsh(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>$cmd,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $rc=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->delete() $cmd returned $rc\n" );
			return( 0 );
		}
		return( 1 );

	} else {
		return $package->SUPER::delete(@_);
	}
	return( 1 );
}

# mkdir [ dir, recurse ]
sub mkdir {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( $connection_mode eq "LOCAL" ) {
		my($directory)=shift;
		$package->myFTPprint( $debug, "myftp->mkdir($directory) mode=$connection_mode\n" );
		my($recurse)=shift;
		$package->myFTPprint( $debug, "myftp->mkdir() LOCAL FUNCTION mkdir() CALLED\n" );
		return if -d $directory;
		croak "error: $directory is not a directory\n"  if -e $directory;
		mkdir($directory,0775)
		 or croak "Cant Mkdir $directory";
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		my($file)=shift;
		$package->myFTPprint( $debug, "myftp->mkdir($file) mode=$connection_mode\n" );
		my($cmd)="\"(cd ".$package->_localdir().";mkdir ";
		$cmd.="$file";
		$cmd.=")\"";
		my($rc)= do_rsh(	-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>$cmd,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $rc=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->mkdir() $cmd returned $rc\n" );
			return( 0 );
		}
		return( 1 );
	} else {
		return $package->SUPER::mkdir(@_);
	}
}

sub ls {
	my $package = shift;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
#	print __LINE__," DBG DBG: localhash=",$local_hash{$package}," local=",$connection_mode,"\n";
	#$package->myFTPprint( $debug, "myftp->ls(@_) mode=$connection_mode\n" );

	if( $connection_mode eq "LOCAL" ) {		
		$package->myFTPprint( $debug, "myftp->ls() LOCAL DIR=".$package->_localdir()."\n" );
	   opendir(DIR,$package->_localdir())
				|| croak("Can't open directory ".$package->_localdir()." for reading\n");
	   my(@files) = grep(!/^\./,readdir(DIR));
	   closedir(DIR);
	   return @files;
	}
	if( $connection_mode ne "FTP_MODE"  ) {
		my($filename) = $_[0];
		$package->myFTPprint( $debug, "myftp->ls($filename) -- mode=$connection_mode \n" );
		my($cmd)="\"(cd ".$package->_localdir().";ls -1 ";
		$cmd.="$filename" if $filename;
		$cmd.=")\"";
		my($output)= do_rsh(	
				-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>$cmd,
				-debug=>$debug,
				-noprintresults=>1,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if(   $output=~/^failure/ ) {
			$package->myFTPprint( $debug, "myftp->ls($filename) returning undef ($output) \n" );
			return;
		}
		my(@files)=split(/\n/,$output);
		return undef if $#files<0;
		return @files;
	}
	$package->myFTPprint( $debug, "myftp->ls(@_) mode=$connection_mode\n" );
	return $package->SUPER::ls(@_);
}

sub binary {
	my $package = shift;
	
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;
	
	if( $connection_mode ne "FTP_MODE"  ) {
		$package->myFTPprint( $debug, "myftp->binary() $connection_mode FUNCTION binary() CALLED\n" );
		return( 1 );
	}
	#use Data::Dumper;
	#$package->myFTPprint( $debug, Dumper $package;
	#use Carp;
	#confess("OOPS");
	$$package->{mode}="binary";
	return $package->SUPER::binary(@_);
}

sub ascii {
	my $package = shift;
	
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( $connection_mode ne "FTP_MODE"  ) {
		$package->myFTPprint( $debug, "myftp->ascii() -- mode=$connection_mode \n" );
		return( 1 );
	}
	$$package->{mode}="ascii";
	return $package->SUPER::ascii(@_);
}

sub close {
	my $package = shift;
	
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( $connection_mode ne "FTP_MODE"  ) {
		$package->myFTPprint( $debug, "myftp->close() -- mode=$connection_mode \n" );
		return( 1 );
	}
	return $package->SUPER::close(@_);
}

sub _localdir {
	my( $package, $dir) = @_;	
	
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	if( ! defined $dir ) {		
		if( $connection_mode eq "FTP_MODE" ) {			
#			print "DBG DBG File: ",__FILE__," Line: ",__LINE__," _localdir() is ",$$package->{current_local_directory},"\n" if $debug;
			return $$package->{current_local_directory};
		} else {
#			print "DBG DBG File: ",__FILE__," Line: ",__LINE__," _localdir() is ",$package->{current_local_directory},"\n" if $debug;
			return $package->{current_local_directory};
		}
	} else {
		print "DBG DBG File: ",__FILE__," Line: ",__LINE__," _localdir() set to $dir\n" if $debug;
		if( $connection_mode eq "FTP_MODE" ) {
			$$package->{current_local_directory}=$dir;
		} else {
			$package->{current_local_directory}=$dir;
		}
	}
}
	
sub fspec {
	my $package = shift;
	my $dir     = shift || $package->_localdir();
	
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;;
	
	#print "DIR is $dir PACAKGEx = $package\n";
	#use Data::Dumper;
	#print Dumper $package;
	$dir = $package->_localdir() if $dir eq ".";
	print "DEV DBG File: ",__FILE__," Line: ",__LINE__," dir is $dir\n" if $developer_debug;
	my(@rc);
	my(@dirlist);
	if( $connection_mode eq "LOCAL" ) {
		#$dir = $package->_localdir() if $dir eq ".";
		$package->myFTPprint( $debug, "myftp->fspec($dir) -- mode=$connection_mode \n" );
		opendir(DIR,$dir) || croak("Can't open directory $dir for reading\n");
	      	my(@files) = grep(!/^\./,readdir(DIR));
	      	closedir(DIR);
		foreach (@files) {
			my($modtime)= -M "$dir/$_";
			#$package->myFTPprint( 1, "DBG1 : ",time," $modtime\n" );
			$modtime = time() - $modtime*24*60*60;
			#$package->myFTPprint( 1, "DBG2: $_ $modtime ", scalar(localtime($modtime)),"\n" );
			my($filetype);
			if( -d "$dir/$_" ) {
				$filetype="Directory";
			} elsif( -f "$dir/$_" ) {
				$filetype="File";
			} elsif( -l "$dir/$_" ) {
				$filetype="SymLink";
			} else {
				$filetype="Unknown";
			}
			push @rc,[ $_,$modtime, -s "$dir/$_", $filetype ];
		}
		return @rc;
	} elsif( $connection_mode ne "FTP_MODE"  ) {
		print "DEV DBG File: ",__FILE__," Line: ",__LINE__," dir is $dir\n" if $developer_debug;
		$package->myFTPprint( $debug, "myftp->fspec($dir) -- mode=$connection_mode \n" );
		die "Cant stat .. using fspec - must pass full path name" if $dir eq "..";

		my($cmd)="\"(cd $dir;ls -l )\"";
		my($rc) = do_rsh(	
				-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>$cmd,
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		if( $rc =~ /^failure / ) {
			$package->myFTPprint( $debug, "myftp->fspec() error connecting : $rc\n" );
			return wantarray ? (0,$rc) : 0 ;
		}		
		@dirlist = split(/\n/,$rc);
	} else {
		# its remote
		print "DEV DBG File: ",__FILE__," Line: ",__LINE__," dir is $dir\n" if $developer_debug;
		$package->myFTPprint( $debug, "myftp->fspec($dir) -- mode=$connection_mode \n" );
		die "Cant stat .. using fspec - must pass full path name" if $dir eq "..";
		use Time::Local;
		$! = undef;
		@dirlist= $package->SUPER::dir($dir);
		print "DEV DBG File: ",__FILE__," Line: ",__LINE__," dirlist=\n\t",join("\n\t",@dirlist),"\n" if $developer_debug;
		if( defined $! and $! ne "" ) {
			$package->myFTPprint( 1, "WARNING: Net::FTP function dir($dir) Error Message: $!\n" );
			$package->myFTPprint( 1, "     reconnecting... $$package->{myLogin}/$$package->{myPassword}\n" );
			my($rc2)=$package->login( $$package->{myLogin}, $$package->{myPassword} );
			warn "DBG:  cant reconnect as ",$$package->{myLogin},"\n" unless $rc2;
			$! = undef;
			@dirlist= $package->SUPER::dir($dir);
			if( $#dirlist<0 ) {
				@dirlist= $package->SUPER::ls($dir);
				$package->myFTPprint( 1, "DBG: dirlist: ",join(" ",@dirlist),"\n" );
				if( defined $! and $! ne "" ) {
					$package->myFTPprint( 1, "WARNING: Attempt number two failed as well : $!\n" );
					$package->myFTPprint( 1, "should reconnect $$package->{myLogin}/$$package->{myPassword}\n" );
					return @rc;
				}
			}
		};
	}

	print "DEV DBG File: ",__FILE__," Line: ",__LINE__," dir is $dir\n" if $developer_debug;
		
	# the following will parse all remote cases (case 2 and 3 above)
	my(@mon)=("Jan","Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
	my($in_dir_section); # apparently sometimes this goes recursive???
	foreach (@dirlist) {
		next if /\s\.$/ or /\s..$/ or /^total/;
		if( /^\s*$/ ) {
			$in_dir_section=1;
			next;
		}
		if( $in_dir_section ) {
			next unless /:$/;
			s/:$//;
			push @rc,[ $_,"",0,"Directory" ];
			next;
		}

		my(@row)=split;
		#next if $row[0] eq "total";
		my($filetype)="Unknown";
		$filetype="Directory" if /^d/;
		$filetype="File" if /^-/;
		$filetype="SymLink" if /^l/;
		my $filetime;
		my($m)=0;
		if( ! $row[5] ) {
			foreach (@dirlist) { $package->myFTPprint( 1, "myFTP bad time",$_,"\n"); }
			die "warning file $_ has bad time!\n";
		}

		foreach (@mon) { last if $_ eq $row[5]; $m++; }
		if( $m == 12 ) { croak "Month $row[5] Not Found\n->data is ".join("|",@row),"\n" };
		if( $row[7] =~ /:/ ) {
			my($hr,$min)=split(/:/,$row[7]);
			my(@ctm)=localtime(time);
			$filetime=timelocal(0,$min,$hr,$row[6],$m,$ctm[5]+1900);
			if( $filetime > time() ) { $filetime-=60*60*24*365; }
		} else {
			$filetime=timelocal(0,0,0,$row[6],$m,$row[7]);
		}

		# $package->myFTPprint( 1, "DBG: filetime for file is $filetime and time =",time," ",time-$filetime,"\n";
		push @rc,[ $row[8],$filetime,$row[4],$filetype ];
	}
	if( $developer_debug ) {
		use Data::Dumper;
		print "SPECIAL FSPEC RC\n";
		print Dumper \@rc;		
	}
	print "DEV DBG File: ",__FILE__," Line: ",__LINE__," dir is $dir\n" if $developer_debug;		
	return @rc;
}

# returns undef if not allowed
sub runcmd {
	my $package = shift;
	my($cmd)=@_;
	my($connection_mode)=$local_hash{$package};
	confess "ERROR - FAILED CONNECTION MODE at ",__FILE__," line ",__LINE__,"\n" unless $connection_mode;
	
	if( $connection_mode eq "LOCAL" ) {		
		$package->myFTPprint( $debug, "myftp->pwd() Exec Local $cmd (unimplemented)\n" );
		return "failure: cant runcmd() as mode=$connection_mode\n";	
	} elsif( $connection_mode ne  "FTP_MODE" ) {
		my($rc)=do_rsh(	
				-hostname=>$package->{tohost},
				-debugfunc=>$package->{debugfunc},
				-command=>"$cmd",				
				-debug=>$debug,
				-login=>$package->{myLogin},
				-password=>$package->{myPassword});
		return $rc;
	}
	return "failure: cant runcmd() as mode=$connection_mode\n";	
}

1;
