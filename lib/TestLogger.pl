#!/usr/local/bin/perl-5.6.1
use Logger;
use strict;

logger_init(
  -debug						=> 1,
  -logfile					=> "/tmp/logfile.tmp",
  -errfile					=> "/tmp/errfile.tmp",
  -database				   => "MYDB",
  -command_run 			=> $0,
  # -mail_host 				$CONFIG{MAIL_HOST}
  -mail_host 				=> "mail1.mlp.com",
  -mail_to 				   => "ebarlow\@mlp.com",
  -success_mail_to 		=> "ebarlow\@mlp.com" );

debug("TEST DEBUG MESSAGE\n");
info("TEST INFO MESSAGE\n");
warning("TEST WARNING MESSAGE\n");
alert("TEST ALERT MESSAGE\n");
logdie("TEST LOGDIE MESSAGE\n");
