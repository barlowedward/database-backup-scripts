#######################################################################
# Standard Header
#  - Begin block is useful
#######################################################################

# copyright (c) 2005-6 by SQL Technologies.  All Rights Reserved.

package  CommonHeader;

use MlpAlarm;
use File::Basename;

use      strict;
use      Carp;

BEGIN {
   # alarm unsupported in perl 5.6 from activestate
   alarm(7200) if $]>5.007001 or ! defined $ENV{PROCESSOR_LEVEL};
   
   if( ! defined $ENV{SYBASE} or ! -d $ENV{SYBASE} ) {
   	
   	if( -d "/export/home/sybase" ) {			
         $ENV{SYBASE}="/export/home/sybase";      
      # *****************************
   	# WARNING - DO NOT REMOVE THIS COMMENT
   	# *****************************
      } elsif( -d "/XXXX" ) {			
         $ENV{SYBASE}="/XXXX";	
      } elsif( -d "/XXXX" ) {		
         $ENV{SYBASE}="/XXXX";	
      } elsif( -d "/XXXX" ) {		
         $ENV{SYBASE}="/XXXX";	
      # *****************************
   	# WARNING - THANK YOU FOR NOT REMOVING THIS EITHER
   	# *****************************         
      } elsif( -d "/usr/local/sybase" ) {
         $ENV{SYBASE}="/usr/local/sybase";
      } elsif( -d "/apps/sybase" ) {
         $ENV{SYBASE}="/apps/sybase";
      } elsif( -d "C:/sybase" ) {
         $ENV{SYBASE}="C:/sybase";
      } elsif( -d "D:/sybase" ) {
         $ENV{SYBASE}="D:/sybase";
      } elsif( -d "E:/sybase" ) {
         $ENV{SYBASE}="E:/sybase";
      }
   }
}

$SIG{ALRM} = sub {
	my($cmd)=$0." ".join(" ",@ARGV);
	chomp $cmd;
	$cmd=~s/-P\w+/-PXXX/;
	$cmd=~s/-PASSWORD=\w+/-PASSWORD=XXX/;

	MlpEvent(
		-monitor_program=>"GEM",
		-system=>"GEM",
		-message_text=>"* * * 2 HOUR TIMEOUT DETECTED * * *\nCommand = $cmd\n",
	 	-severity => "ERROR",
	);
	die "* * * TIMEOUT DETECTED * * *\n Applications may run for 2 Hours Maximum.\nCommand = $cmd\n";
};

1;

__END__

=head1 NAME

CommonHeader.pm - Misc Common Functions

=head2 DESCRIPTION

This module will be included in most batch jobs.  It is currently used to set
an alarm to reap processes that get stuck and to define your environment variables.

