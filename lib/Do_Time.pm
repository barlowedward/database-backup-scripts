#######################################################################
# Standard Time Formatting
#######################################################################

# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package  Do_Time;

use      strict;
use      Carp;
use      vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw(do_time do_diff get_month_id);
$VERSION = '1.0';

# sub get_time
# {
	# my(%OPT)=@_;
	# croak("Must Pass -fmt argument to get_time") unless defined $OPT{-fmt};
	# return time unless defined $OPT{-time};
	# my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime($now);
	# return timelocal( $sec,$min,$hour,$mday,$mon,$year);
# }

my(%mon_hash);
sub get_month_id {
	my($mon,$twochar )=@_;
	if( ! defined $mon_hash{$mon} ) {
		my($c)=1;
		foreach ("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec") {
			$mon_hash{$_}=$c++;
		}
	}
	return "0".$mon_hash{$mon} if $twochar and length $mon_hash{$mon} == 1;
	return $mon_hash{$mon};
}



sub do_time
{
	my(%OPT)=@_;
	croak("Must Pass -fmt argument to do_time") unless defined $OPT{-fmt};

	my($now)=$OPT{-time};
	return "" if defined $OPT{-nullonnull} and ! defined $OPT{-time};
	$now=time unless defined $now;

	my($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)=localtime($now);

	$mon++;
	$year+=1900;
	if( defined $OPT{-spnozeroes} ) {
		print "ADDING SPACE\n";
		substr($mon,0,0)=' ' 	if length($mon)==1;
		substr($mday,0,0)=' ' 	if length($mday)==1;
		substr($hour,0,0)=' ' 	if length($hour)==1;
		substr($min,0,0)=' ' 	if length($min)==1;
		substr($sec,0,0)=' ' 	if length($sec)==1;
	} else {
		substr($mon,0,0)='0' 	if length($mon)==1;
		substr($mday,0,0)='0' 	if length($mday)==1;
		substr($hour,0,0)='0' 	if length($hour)==1;
		substr($min,0,0)='0' 	if length($min)==1;
		substr($sec,0,0)='0' 	if length($sec)==1;
	}

	my($rc)=$OPT{-fmt};
	$rc =~ s/yyyy/$year/g;
	substr($year,0,2)="";
	$rc =~ s/yy/$year/g;
	$rc =~ s/mm/$mon/g;
	$rc =~ s/dd/$mday/g;
	$rc =~ s/hh/$hour/g;
	$rc =~ s/mi/$min/g;
	$rc =~ s/dw/$wday/g;
	$rc =~ s/ss/$sec/g;

	return $rc;
}

# takes a seconds and returns a string
sub do_diff
{
	my($t,$long_format)=@_;
	my($str)="";
	return $t unless $t=~/^\d+$/;
	if( $t>24*60*60 ) {
		my($d)=int($t/(24*60*60));
		$str.=$d."+ Days";
		return $str;
	}
	if( $t>60*60 ) {
		my($h)=int($t/(60*60));
		$str.=$h;
		if( defined $long_format ) { $str.="Hours "; } else { $str.="Hr "; }
		$t -= $h*60*60;
	}
	if( $t>60 ) {
		my($m)=int($t/60);
		$str.=$m;
		if( defined $long_format ) { $str.="Minutes"; } else { $str.="Min"; }
		$t -= $m*60;
	}
	if( $str eq "" ) {
		$str=$t;
		if( defined $long_format ) { $str.="Seconds"; } else { $str.="Sec"; }
	}
	return $str;
}
1;

__END__

=head1 NAME

Do_Time.pm - Misc Time Functions

=head2 DESCRIPTION

Very simple time functions.

  do_time(-time=>secs, -fmt=>formatstring);

  do_diff() - pretty print dime differences
