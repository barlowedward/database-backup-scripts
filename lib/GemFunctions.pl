# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

use   strict;
use   Carp qw(confess);
use 	XML::Simple;
use 	Data::Dumper;
use	Do_Time;
use	Tk;
use 	Tk::Balloon;
use	Tk::JComboBox;
use	Tk::Pane;
use 	Sys::Hostname;
use 	Tk::LabFrame;
use	RunCommand;
use	File::Compare;
use	GemData;
use	DBIFunc;
use 	Tk::ErrorDialog;
use 	Tk::Listbox;
use 	Net::Ping::External qw(ping);

our( $sql_text, $log_text, $debug_text); 	# text widgets on Sql and Log
our(%CONFIG,%XMLDATA,%server_info,%NEEDS_REFRESH,$UPGRADE,$NOLOGFILE,$opt_q,
	$DEBUG,$print_stdout,$statusmsgbar,$mf,$connectionmanager,$DO_DUMP_XML,$gem_data,@perl_includes);
our(%graphics_object);			# graphics_objects - for colorization and show/hide

#
# LOCAL STUFF
#
#my($DATA_NEEDS_WRITE)="FALSE";

###################### BACKUP STUFF ##################################
my(%plantab_objects);	# graphics objects used by plan tab - for enabling and disabling etc

my($Plan_Planname_lb,  $Plan_Leftside_Width, $Plan_Server_lb);

# the next section deal with the jcombo boxes... @ is choices $ is curval
my($plantab_text_allsvr)="[ ALL SERVERS ]";	# constant
#PL# my(@plantab_servers, @plantab_plans);
my(@plantab_plans);
#PL# my($plantab_servers_curval)=$plantab_text_allsvr;
my($plantab_plans_curval)=undef;

my $cur_backup_plan;		# The Plan that stuff is drawn for.  Dont set ... call update to set

my $dflt_color = "yellow";
my %DsqueryByPlanName;
my @BackupTabList_DATA;
my %BKP_DATACELLS;
my %BKP_CELL_TYPE;
my %BKP_DATAVALS;
my %BKP_ISDEFAULT_TEXT;
my %BKP_ISDEFAULT_SAVE;		# saved values of above - the above can get overwritten
my $PlanNbLabel;		# host labels on backup tab
my $filetab_hlist;		# for the files tab - this is the main data section
my $filetab_frm;	# ditto
my $filetab_master;		# ditto
#
#
# THE  ARE READ FROM THE __DATA__ SECTION FOR THE backuptab items
#
#
my %bt_TabHelp_DATA;			# help messages per vbl
my @bt_KeyList_DATA;			# Array of the backup variables (DO_DUMP,DO_COMPRESS...)
my %bt_TabByKey_DATA;			# Tab for each of the variables - can double as list of the variables too
#my %BackupRowByKey_DATA;		# what row is this thing in
my %bt_ElementLength_DATA;		# Length of teh element or CK if it is a checkbox
my %bt_TabHash_DATA;			# a hash of the tab names - used temporarily to set the array
#
################### END BACKUP STUFF #################################
my $balloon;

my(%add_file_labels)=(
		"Sybase Server \$SYBASE Directory     " => "SYBASE",
		"Ignore This RUN_\* File              " => "IGNORE_RUNFILE",
		"Sybase Open Client \$SYBASE Directory" => "SYBASE_CLIENT",
		"SQL Server Maintenance Plan Logs     " => "SQL_SVR_MAINTPLAN_DIR",
		"Application Log File                 " => "APP_LOG_FILE",
		"Operating System Log File            " => "OS_LOG_FILE",
		"Oracle ORAHOME                       "	=> "ORAHOME"	);

# Generic GUI Objects
my( $hosts_frame, %procs_frame_sv);
our($configure_tab_hosttype);
#my($InstProc_btn, $InstDoc_btn, $InstSydump_btn);


# LOGGING FUNCTIONS
my(@message_log);
sub start_msg_logging {
	my($msg)=@_;
	@message_log=();
	push @message_log,$msg;
	push @message_log,"started at ".localtime(time)."\n";
}

sub end_msg_logging {
	my(@rc);
	push @rc,@message_log;
	@message_log=();
	return @rc;
}

sub _debugmsg {
	my($msg)=join("",@_);
	if( defined $DEBUG ) {
		$msg=~s/\] /\]\t\t\<dbg\> /;
		_statusmsg($msg);
	} else {	
		if( ! defined $NOLOGFILE ) {
			foreach( split(/\n/,$msg) ) { chomp; print LOGFP "\t\t<dbg> $_\n"; }
		}
	}
	if( defined $debug_text ) {
	   	$debug_text->insert('end',do_time(-fmt=>"yyyy.mm.dd hh:mi"),'blue');
		if( $_[0] eq "noprint" ) {
	   		$debug_text->insert('end',": ".$msg."\n",'green');
		} else {
	   		$debug_text->insert('end',": ".$msg."\n");
	   	}
	 }
}

our($status_bar_ready);
sub _statusmsg {
    my $cmd = shift;
    my $msg = join("",@_);
    if( $cmd ne "noprint" ) {
    	$msg = $cmd.$msg;
    	$cmd = "";
    }

    chomp $msg;
    if( $msg =~ /\[/ ) {
    	my($p,$m)=split(/\s/,$msg,2);
    	$msg=sprintf("%-12.12s| %s",$p,$m);
    }
    
    if( $cmd eq "" ) {
    	if( $print_stdout eq "TRUE" ) {
    		foreach( split(/\n/,$msg) ) {
    			print ">> $_\n" unless $opt_q;
    			print LOGFP ">> $_\n" unless defined $NOLOGFILE;
    		}
    	} else {
	    	foreach( split(/\n/,$msg) ) {
	    		print LOGFP ">> $_\n" unless $NOLOGFILE;
	    	}
    	}
    }

    if ( $status_bar_ready eq "TRUE" and defined $statusmsgbar and $msg !~ /^\#\#\#\#/ ) {
    	if( ! $msg ) {
    		$msg = "Ready";
    	} else {
    		push @message_log, $msg if $#message_log != -1;
    		$msg = reword($msg,50, 90);
    	}

    	#if( length($msg)>100 ) {	# split it on white spaces
    	#	my(@m)=split(/\n/,$msg);
    	#	my($mout)="";
    	#	foreach (@m){
    	#		if( length < 80 ) {
    	#			$mout.=$_."\n";
    	#		} else {
    	#			my(
    	#		}
    	#	}
    	#}

      $statusmsgbar->configure( -text => "$msg" );
      $statusmsgbar->update;
    }
}
# END LOGGING FUNCTIONS

################### new - plan functions ##################
my %BackupPlanNames;			# Plan list
my %DeletedPlans;

sub plan_init {
	my(@x);
	$XMLDATA{Backup_Plan_Aryptr} = \@x;
}

sub plan_add 	{
	my(%args)=@_;
	mydie("Cant add plan without a name!!!") unless $args{name};
	push @{$XMLDATA{Backup_Plan_Aryptr}}, $args{name};
	foreach ( sort keys %args ) {
		next if $_ eq "name";
		_statusmsg("[PlanAdd] ".$args{name}."_".$_." = ".$args{$_}."\n");
		$CONFIG{$args{name}."_".$_} 	= $args{$_};
	}
	$CONFIG{$args{name}."_DSQUERY"}=$_ unless defined $CONFIG{$args{name}."_DSQUERY"};			
	$BackupPlanNames{$args{name}}=1;
	delete $DeletedPlans{$args{name}} if defined $DeletedPlans{$args{name}};
}

sub plan_delete {
	my(%args)=@_;
	delete $BackupPlanNames{$args{name}};
	$DeletedPlans{$args{name}}=1;
}

sub plan_get 	{
	my(%args)=@_;

	# if name passed - return 1/0 for existence
	return $BackupPlanNames{$args{name}} if $args{name};

	if( $args{type} ) {		# return only the plans of the appropriate type
		my(@x);
		my(%found);
		if( $args{type} eq "Sybase" ) {
			foreach(get_server_by_type(-type=>"sybase")) { $found{$_}=1; }
		} else {
			foreach(get_server_by_type(-type=>"sqlsvr")) { $found{$_}=1; }
		}

		foreach ( @{$XMLDATA{Backup_Plan_Aryptr}} ) {			# the default is the plan name...
			$CONFIG{$_."_DSQUERY"}=$_ unless defined $CONFIG{$_."_DSQUERY"};
			push @x, $_ if $found{$CONFIG{$_."_DSQUERY"}};
 		}
 		return sort @x if $args{sort};
 		return @x;
   }
	
	return sort(@{$XMLDATA{Backup_Plan_Aryptr}}) if $args{sort};
	return @{$XMLDATA{Backup_Plan_Aryptr}};	
}

############################## end new ####################
#sub server_add 	{ my(%args)=@_; }
#sub server_del 	{ my(%args)=@_; }
#sub server_get 	{ my(%args)=@_; }
#sub server_set 	{ my(%args)=@_; }

sub get_server_types {
	return ( "sybase","sqlsvr","oracle","unix","win32servers");
}

sub get_label_by_type  {
	my($type)=shift;
	my( %type_labels ) = (
		sybase		=> "Sybase ASE",
		unix		=> "Unix System",
		sqlsvr		=> "Sql Server",
		win32servers 	=> "NT Server",
		oracle  	=> "Oracle"
	);
	return $type_labels{$type};
}

sub get_type_by_label {
	my($label)=shift;
	my( %toi ) = (
		"Sybase ASE"	=> "sybase",
		"Unix System"	=> "unix",
		"Sql Server"	=> "sqlsvr",
		"NT Server"	=> "win32servers",
		"Oracle"	=> "oracle"
	);
	return $toi{$label} || $toi{lc($label)};
}


sub read_gem_xml {
	my($h)=hostname();	
	$gem_data = new GemData( -printfunc=>\&_statusmsg, -debugfunc=>\&_debugmsg, -hostname=>$h );	# -file=>"conf/gem.xml" );
	if( defined $DO_DUMP_XML ) {
		my($file)=$gem_data->dump();
		print "Xml data dumped to $file\n";
		print LOGFP "Xml data dumped to $file\n" unless defined $NOLOGFILE;
		exit();
	}
}
sub global_data { 	return $gem_data->get_set_data(@_); }
sub write_XMLDATA {	return $gem_data->save(@_); };

sub dialog_addvar {
	my($dlg,$var,$label,$key,$row,$fldtype,$colspan,$bgcolor,$entrywidth)=@_;
	$key=$label unless defined $key;
	$bgcolor='beige' unless $bgcolor;
	$entrywidth=35 unless $entrywidth;

	#$$var{$key}="Dflt" unless defined $$var{$key} or $fldtype eq "entry";

	return $dlg->add("Label",-font=>"large",-text=>" ")->grid(-column=>1, -row=>$row, -sticky=>'nsew', -columnspan=>2)
		 if $fldtype eq "Blank";

	return $dlg->add("Label",-font=>"large",-text=>$label, -justify=>'center',
		-bg=>$bgcolor, -relief=>'sunken')->grid(-column=>1, -row=>$row, -sticky=>'nsew', -columnspan=>2)
		if( $colspan ) ;

	$dlg->add("Label",-text=>$label, -font=>"large",-justify=>'right', -bg=>$bgcolor, -relief=>'sunken')->grid(-column=>1, -row=>$row, -sticky=>'nsew');

	# The variable part of this thing
	if( ! defined $fldtype or $fldtype eq "entry" ) {
		my($r)=$dlg->add("Entry",-bg=>'white',-font=>"large",-textvariable=>\$$var{$key},
				-width=>$entrywidth, -relief=>'groove')->grid(-column=>2,-row=>$row, -sticky=>'ew');
		$r->focus() if ! defined \$$var{$key};
		return $r;
	}

	return $dlg->Checkbutton(
     				-font=>"large",-variable=>\$$var{$key},
   				-onvalue=>'Y',
				-offvalue=>'N',
				-text=>"")->grid(-column=>2, -row=>$row, -sticky=>'ew')
		if $fldtype eq "checkbox";

	return $dlg->add("Label",-font=>"large",-text=>$$var{$key}, -justify=>'left', -bg=>'white', -relief=>'groove')->grid(-column=>2, -row=>$row, -sticky=>'ew')
		if $fldtype eq "label";

	if( $fldtype eq "password" ) {
		my($r)=$dlg->add("Entry",-font=>"large",-bg=>'white',-textvariable=>\$$var{$key},-show=>'*',-width=>$entrywidth, -relief=>'groove')->grid(-column=>2,-row=>$row, -sticky=>'ew');
		$r->focus();
		return $r;
	}

	if( $fldtype eq "SybaseType" ) {	
		my @tmp;
		push @tmp,"Backup Server";
		push @tmp,"ASE Server";
		push @tmp,"Historical Server";
		push @tmp,"Sybase IQ";
		$$var{$key}=$tmp[1] unless defined $$var{$key};

		$dlg->add("JComboBox",
		   	-choices=> \@tmp,
		   	-relief=>'sunken',
		   	-background=>'white',
#		   	-entrywidth=>$Plan_Leftside_Width,
#		   	-selectcommand=>\&plantab_sel_server,
		   	-textvariable=>\$$var{$key},
		   	-maxrows=>4
		   )->grid(-column=>2,-row=>$row++, -sticky=>'ew');

	} elsif( $fldtype eq "CommMethod" ) {	
		my @tmp;
		push @tmp,"FTP";
		push @tmp,"NONE";
		push @tmp,"SSH";
		push @tmp,"RSH";
		$$var{$key}=$tmp[1] unless defined $$var{$key};

		$dlg->add("JComboBox",
		   	-choices=> \@tmp,
		   	-relief=>'sunken',
		   	-background=>'white',
		   	-entrywidth=>10,
#		   	-selectcommand=>\&plantab_sel_server,
		   	-textvariable=>\$$var{$key},
		   	-maxrows=>4
		   )->grid(-column=>2,-row=>$row++, -sticky=>'ew');
	
	} elsif( $fldtype eq "ServerProdBox" ) {
		my @tmp;
		push @tmp,"DEVELOPMENT";
		push @tmp,"PRODUCTION";
		push @tmp,"CRITICAL";
		push @tmp,"QA";
		push @tmp,"DR";
		$$var{$key}=$tmp[1] unless defined $$var{$key};

		$dlg->add("JComboBox",
		   	-choices=> \@tmp,
		   	-relief=>'sunken',
		   	-background=>'white',
		   	-entrywidth=>15,
#		   	-selectcommand=>\&plantab_sel_server,
		   	-textvariable=>\$$var{$key},
		   	-maxrows=>4
		   )->grid(-column=>2,-row=>$row++, -sticky=>'ew');

#		foreach (@tmp) {
#			$dlg->add("Radiobutton",
#			   	-font=>"large",
#			   	-text=>$_,
#			   	-anchor=>'e',
#			   	-activebackground=>'pink',
#			   	-selectcolor => 'yellow',
#			   	-highlightbackground=>'yellow',
#			   	-justify=>'left',
#			   	-anchor=>'e',
#			   	-value => $_,
#			   	-relief=>'sunken',
#			   	-background=>'white',
#			   	-variable=>\$$var{$key}
#		   	)   ->grid(-column=>2,-row=>$row++, -sticky=>'ew');
#		}
	} elsif( $fldtype eq "ServerCatBox" ) {
		my @tmp;
		push @tmp,"Unix or Linux Server";
		push @tmp,"Windows Server";
		push @tmp,"Sybase ASE Server on Unix";
		push @tmp,"Sybase ASE Server on Windows";
		push @tmp,"Microsoft SQL Server on Windows";
		push @tmp,"Oracle Server on Windows";
		push @tmp,"Oracle Server on Unix";
		if( is_nt() ) {
			$$var{$key}=$tmp[4] unless defined $$var{$key};
		} else {
			$$var{$key}=$tmp[2] unless defined $$var{$key};
		}

		$dlg->add("JComboBox",
		   	-choices=> \@tmp,
		   	-relief=>'sunken',
		   	-background=>'white',
#		   	-entrywidth=>$Plan_Leftside_Width,
#		   	-selectcommand=>\&plantab_sel_server,
		   	-textvariable=>\$$var{$key},
#		   	-maxrows=>4
		   )->grid(-column=>2,-row=>$row++, -sticky=>'ew');

#		foreach (@tmp) {
#			$dlg->add("Radiobutton",
#			   	#-choices=> \get_server_by_type(-type=>"sqlsvr"),
#			   	#-choices=> \@tmp,
#			   	-font=>"large",
#			   	-text=>$_,
#			   	-anchor=>'e',
#			   	-activebackground=>'pink',
#			   	-selectcolor => 'yellow',
#			   	-justify=>'left',
#			   	-anchor=>'e',
#			   	-value => $_,
#			   	-relief=>'sunken',
#			   	-background=>'white',
#			   	#-entrywidth=>30,
#			   	-variable=>\$$var{$key}
#		   	)   ->grid(-column=>2,-row=>$row++, -sticky=>'ew');
#		}
	}

#	return 	$dlg->add("JComboBox",
#	   	#-choices=> \get_server_by_type(-type=>"sqlsvr"),
#	   	#-choices=> \@tmp,
#	   	-relief=>'sunken',
#	   	-background=>'white',
#	   	-entrywidth=>30,
#	   	-variable=>\$$var{$key}
#	   	)   ->grid(-column=>2,-row=>$row, -sticky=>'ew');
}

sub tk_add_a_server_baseinfo {
	my( $dlg_ref ) = @_;	
	my $dlg_addPlan= $XMLDATA{mainWindow}->DialogBox(
		-title => 'Enter Server Information',
		-buttons => [ "Ok", "Cancel" ] );
	dialog_addvar($dlg_addPlan,$dlg_ref,"Server Name",		undef,	1,"entry");
	dialog_addvar($dlg_addPlan,$dlg_ref,"Host Name",  		undef,	2,"entry");	
	dialog_addvar($dlg_addPlan,$dlg_ref,"Server Group",	undef,	4,"ServerProdBox");	
	dialog_addvar($dlg_addPlan,$dlg_ref,"Server Type",  	undef,	10,"ServerCatBox");
	return $dlg_addPlan->Show();
}

sub tk_add_a_system_baseinfo {
	my($dlg_ref)=@_;
	#print "DBG: tk_add_a_system_baseinfo($dlg_ref)\n";
	my $dlg_addPlan= $XMLDATA{mainWindow}->DialogBox(
		-title => 'Enter System Information',
		-buttons => [ "Ok", "Cancel" ] );

	dialog_addvar($dlg_addPlan,$dlg_ref,"Host Name",  	undef,	1,"entry");
	dialog_addvar($dlg_addPlan,$dlg_ref,"Server Group",	undef,	3,"ServerProdBox");
	dialog_addvar($dlg_addPlan,$dlg_ref,"Server Type",  	undef,	9,"ServerCatBox");
	return $dlg_addPlan->Show();
}

sub tk_add_a_server_unixlp {
	my($dlg_ref)=@_;
	#print "DBG: tk_add_a_server_unixlp($dlg_ref)\n";
	my $dlg_addPlan= $XMLDATA{mainWindow}->DialogBox(
		-title => 'Enter Host Connectivity Information For '.$$dlg_ref{"Host Name"},
		-buttons => [ "Ok", "Cancel" ] );

	dialog_addvar($dlg_addPlan,$dlg_ref,'Enter Host FTP Connectivity Information For '.$$dlg_ref{"Host Name"},
		undef,	1,"Label",2, "orange");
	dialog_addvar($dlg_addPlan,$dlg_ref,"Host Login",	undef,	2,"entry", undef, "orange");
	dialog_addvar($dlg_addPlan,$dlg_ref,"Host Password",  	undef,	3,"password", undef, "orange");
	return $dlg_addPlan->Show();
}

sub tk_add_a_server_sybaselp {
	my($dlg_ref)=@_;
	# print "DBG: tk_add_a_server_sybaselp($dlg_ref)\n";
	my $dlg_addPlan= $XMLDATA{mainWindow}->DialogBox(
		-title => 'Enter Database Admin Login Information For '.$$dlg_ref{"Server Name"},
		-buttons => [ "Ok", "Cancel" ] );
	dialog_addvar($dlg_addPlan,$dlg_ref,'Enter Database Admin Login Information For '.$$dlg_ref{"Server Name"},	undef,	1,"Label",2);
	dialog_addvar($dlg_addPlan,$dlg_ref,"DB Admin Login",	undef,	2,"entry");
	dialog_addvar($dlg_addPlan,$dlg_ref,"DB Admin Password",undef,	3,"password");
	dialog_addvar($dlg_addPlan,$dlg_ref,'[ note click cancel to not register this server ]',undef,	4,"Label",2,undef);
	return $dlg_addPlan->Show();
}

#sub tk_add_a_server_bkplaninfo {
#	my($dlg_ref)=@_;
#	# print "DBG: tk_add_a_server_bkplaninfo($dlg_ref)\n";
#	my $dlg_addPlan= $XMLDATA{mainWindow}->DialogBox(
#		-title 		=> 'Enter Backup Plan Information',
#		-buttons 	=> [ "Ok", "Cancel" ] );
#	dialog_addvar($dlg_addPlan,$dlg_ref,"Plan Name",  undef,	1,"entry");
#
#	dialog_addvar($dlg_addPlan,$dlg_ref,"Make Separate Weekend DBCC Plan",  undef,	2,"checkbox");
#	dialog_addvar($dlg_addPlan,$dlg_ref,"",  		undef,	3,"Blank");
#
#	dialog_addvar($dlg_addPlan,$dlg_ref,"DIRECTORY TO STORE BACKUPS IN",  undef,	4,"Label",2);
#	dialog_addvar($dlg_addPlan,$dlg_ref,"Local Directory Name (from db server) ",  undef,	5,"entry");
#	dialog_addvar($dlg_addPlan,$dlg_ref,"[optional] Remote Directory Name (from anywhere)",undef,	6,"entry");
#
#	dialog_addvar($dlg_addPlan,$dlg_ref,"",  		undef,	7,"Blank");
#	dialog_addvar($dlg_addPlan,$dlg_ref,"PLAN NOTIFICATIONS",  undef,	8,"Label",2);
#	dialog_addvar($dlg_addPlan,$dlg_ref,"Error Mail",  undef, 9,"entry");
#	dialog_addvar($dlg_addPlan,$dlg_ref,"Success Mail",undef, 10,"entry");
#	return $dlg_addPlan->Show();
#}

#sub tk_add_a_server_planmail {
#	my($dlg_ref)=@_;
#	# print "DBG: tk_add_a_server_planmail($dlg_ref)\n";
#	my $dlg_addPlan= $XMLDATA{mainWindow}->DialogBox(
#		-title => 'Enter Backup Plan Information',
#		-buttons => [ "Ok", "Cancel" ] );
#	return $dlg_addPlan->Show();
#}

#my(%tk_add_a_server_vars);
#sub tk_add_a_server {
#	my(%new_server_config)=@_;
#	my($str)="[func] tk_add_a_server(";
#	foreach (keys %new_server_config) { $str.=$_."=".$new_server_config{$_}.", " unless /password/i; }
#	$str=~s/, $/)/;
#	_statusmsg( $str );
#
	#
	# at the end of this we will have the above hash populated with some vars
	# Screen Num 1 - tk_add_a_server_baseinfo
	#   KEY=Server Name
	#   KEY=Server Type
	#	Unix or Linux Server
	#	Windows Server
	#	Sybase ASE Server on Unix
	#	Sybase ASE Server on Windows
	#	Microsoft SQL Server
	#	Oracle Server on Windows
	#	Oracle Server on Unix
	#   KEY=Host Name
	#   KEY=Server Group
	#	DEVELOPMENT
	#	PRODUCTION
	#	QA
	#	DR
	# Screen tk_add_a_server_unixlp
	#   KEY=Host Login
	#   KEY=Host Password
	# Screen tk_add_a_server_sybaselp
	#   KEY=DB Admin Login
	#   KEY=DB Admin Password
	# Screen tk_add_a_server_bkplaninfo
	#   KEY=Plan Name
	#   KEY=Make Separate Weekend DBCC Plan
	#   KEY=Local Directory Name (from db server)
	#   KEY=[optional] Remote Directory Name (from anywhere)
	# Screen tk_add_a_server_planmail
	#   KEY=Error Mail
	#   KEY=Success Mail

	# Before Adding - we will need to compute
	#Server Cat
	#	Unix Server
	#	Microsoft SQL
	#	NT Server
	#	Sybase Server
	#	Oracle Server
	# is_remote
	#	Y or N

	# restore some defaults if needed
#	if( ! $new_server_config{"Server Type"} 
#	or $new_server_config{"Server Type"} eq $tk_add_a_server_vars{"Server Type"} ) {
#		foreach ( keys %tk_add_a_server_vars) {
#			next if ! $tk_add_a_server_vars{$_} or $new_server_config{$_};
#			$new_server_config{$_} = $tk_add_a_server_vars{$_};			
#		}
#	}
#
#	foreach ( sort keys %new_server_config ) {
#		if( $new_server_config{$_} =~ /^\s*$/ ) { delete $new_server_config{$_}; next; }		
#	}

	# if sybase then lets get the interface information for the server
#	if( ! $new_server_config{"Server Type"} or $new_server_config{"Server Type"} =~ /^Sybase/ ) {		
#		my($host,$port)=$gem_data->get_server_interfaces($new_server_config{"Server Name"});
#		$new_server_config{"Host Name"}  =$host if $host;
#		$new_server_config{"Port Number"}=$port if $port;
#	}
	
# SCREEN 1
#   check variables then get then validate...	
#	if( $new_server_config{"Server Type"} eq "Windows Server"
#	or $new_server_config{"Server Type"} eq "Unix or Linux Server" ){		
#		$new_server_config{"Host Name"}=$new_server_config{"Server Name"}
#			if ! defined $new_server_config{"Host Name"};
#		$new_server_config{"Server Name"} = "";
#		if( !  defined $new_server_config{"Host Name"}
#		or  !  defined $new_server_config{"Server Type"}){
#			if( tk_add_a_system_baseinfo(\%new_server_config) ne "Ok" ) {
#				_statusmsg("[func]  Exiting Server Registration based on user cancel\n");
#				return 0;
#			}
#		} else {
#				_statusmsg("[func] All Basic Info Found\n");
#		}
#	} else {
#		$new_server_config{"Host Name"}=$new_server_config{"Server Name"}
#			if ! defined $new_server_config{"Host Name"}
#			and $new_server_config{"Server Type"}=~/on Windows/;
#		if( ! defined $new_server_config{"Server Name"}
#		or !  defined $new_server_config{"Host Name"}
#		or !  defined $new_server_config{"Server Type"}){
#			if( tk_add_a_server_baseinfo(\%new_server_config) ne "Ok" ) {
#					_statusmsg("[func]  Exiting Server Registration based on user cancel\n");
#					return 0;
#			}
#		}
#	}

#	if( $new_server_config{"Server Type"} =~ /Oracle/ ) {
#		_statusmsg("[func]  Sorry.  Oracle Services Delayed until V2.0\n");
#		_statusmsg("[func] Displaying message box - select a button to continue\n");
#		$XMLDATA{mainWindow}->messageBox(
#			-title => 'Data Validation Error',
#			-message => "ERROR:  Oracle Server Management is Delayed until V2.0\nWe welcome working with you on our oracle solution!\n",
#			-type => "OK" );
#		_statusmsg("[func] GEM Ready / Continuing...\n");
#		return 0;
#	}
#	
#	my($found) = "FALSE";
#	if( $new_server_config{"Server Type"} =~ /Unix/ ) {
#		_statusmsg("[func] Working On Unix Server\n");
#		# check if this is a registered unix server ... if not register one
#		
#		foreach ( get_server_by_type(-type=>"unix") ) {
#			next if $_ ne $new_server_config{"Host Name"};
#			_statusmsg("[func] Note: Skipping Server ",$new_server_config{"Host Name"}," allready registered\n");
#			return 0;
#		}
#		
#		_statusmsg("[func] Note: Server ",$new_server_config{"Host Name"}," is not a registered unix server\n");
#
#		# Now we need to get login and password for unix!
#		$new_server_config{"Host Login"}="sybase" if $new_server_config{"Server Type"}=~/Sybase/;
#		tk_add_a_server_unixlp(\%new_server_config)
#			unless $new_server_config{"Host Login"} and $new_server_config{"Host Password"};
#		$new_server_config{"MustAddUnix"}="TRUE";
#		
#	} else {
#		_statusmsg("[func] Working On Non-Unix Server\n");
#		# check if this is a registered windows server ... if not register one
#		foreach ( get_server_by_type(-type=>"win32servers") ) {
#			next if lc($_) ne lc($new_server_config{"Host Name"});			
#			if( $_ ne $new_server_config{"Host Name"} ) {	$new_server_config{"Host Name"}=$_; }
#			$found="windows";
#			_statusmsg("[func] Note: Server ",$new_server_config{"Host Name"}," is allready a registered windows server\n");
#			# return 0;
#		}
#		
#		if( $found ne "windows" ) {
#			_statusmsg("[func] Note: Server ",$new_server_config{"Host Name"}," is not a registered windows server\n");
#			$new_server_config{"MustAddWindows"}="TRUE";
#			$new_server_config{"Local Directory Name (from db server) "} = 'C:\backups';
#			$new_server_config{"[optional] Remote Directory Name (from anywhere)"} = "\\\\".$new_server_config{"Host Name"}."\\c\$\\backups";
#		}
#	}
#
#	if( $new_server_config{"Server Type"} =~ /Sybase ASE/ ) {
#		foreach ( get_server_by_type(-type=>"sybase") ) {
#			next if $_ ne $new_server_config{"Server Name"};
#			_statusmsg("[func] Note: Server ",$new_server_config{"Host Name"}," is a registered sybase server\n");
#			return 0;
#		}
#	} elsif( $new_server_config{"Server Type"} =~ /Microsoft SQL Server/ ) {
#		foreach ( get_server_by_type(-type=>"sqlsvr") ) {
#			next if $_ ne $new_server_config{"Server Name"};
#			_statusmsg("[func] Note: Server ",$new_server_config{"Server Name"}," is a registered SQL server\n");
#			return 0;
#		}
#	}
#
#	#foreach ( sort keys %new_server_config ) {
#	#	print "DBGX: ",__LINE__," Key $_ Val=$new_server_config{$_} \n";
#	#}
#
#	if( $new_server_config{"Server Type"} =~ /Sybase ASE/
#		or $new_server_config{"Server Type"} =~ /Microsoft SQL Server/ ) {
#
#		if( !  defined $new_server_config{"DB Admin Login"}
#		or  !  defined $new_server_config{"DB Admin Password"}){
#			_statusmsg("[func] Note: Getting Login Info For DB Server ",$new_server_config{"Server Name"},"\n");
#			$new_server_config{"DB Admin Login"}="sa" unless defined $new_server_config{"DB Admin Login"};
#			tk_add_a_server_sybaselp(\%new_server_config);
#		}
#
#		if( $new_server_config{"Server Type"} =~ /Windows/ ) {
#			$new_server_config{"[optional] Remote Directory Name (from anywhere)"}
#				= "\\\\".$new_server_config{"Host Name"}."\\c\$\\backups";
#			$new_server_config{"Local Directory Name (from db server) "}
#				= "c:\\backups";
#		}

		#PLAN# $new_server_config{"Plan Name"}=$new_server_config{"Server Name"}
		#PLAN# 	unless $new_server_config{"Plan Name"};
		#PLAN# $new_server_config{"Make Separate Weekend DBCC Plan"}='Y'
		#PLAN# 			unless $new_server_config{"Make Separate Weekend DBCC Plan"};
		#PLAN# $new_server_config{"Error Mail"}   = $XMLDATA{admin_email};
		#PLAN# $new_server_config{"Success Mail"} = $XMLDATA{admin_email};

#		_statusmsg("[func] Validating Variables For DB Server ",$new_server_config{"Server Name"},"\n");
#		foreach (keys %new_server_config) {
#			next unless $new_server_config{$_} =~ /^\s*$/;
#			next if /(\[optional\])/;
#			_statusmsg("[func] Displaying message box - select a button to continue\n");
#			$XMLDATA{mainWindow}->messageBox(
#				-title => 'Data Entry Error',
#				-message => "ERROR: not all information entered\nVariable $_ Contains No Data\nNot Creating Plan\n",
#				-type => "OK" );
#			_statusmsg("[func] GEM Ready / Continuing...\n");
#			return 0;
#		}
#	}
#
#	# SAVE some defaults
#	foreach ( keys %new_server_config) {		
#		$tk_add_a_server_vars{$_} = $new_server_config{$_}
#			unless /Server Name/ or /Host Name/ or /Password/ or /Plan Name/ or /MustAdd/;
#	}
#
#	if( $new_server_config{"MustAddUnix"}) {
#		_statusmsg("[func] Adding Unix Server ".$new_server_config{"Host Name"}."\n");
#		add_a_server_to_gem("Unix Server",
#				$new_server_config{"Host Name"},
#				$new_server_config{"Host Login"},
#				$new_server_config{"Host Password"},
#				$new_server_config{"Server Group"});
#	}
#
#	if( $new_server_config{"MustAddWindows"}) {
#		_statusmsg("[func] Adding Dependent Windows Server ".$new_server_config{"Host Name"}."\n");
#		add_a_server_to_gem("NT Server",
#				$new_server_config{"Host Name"},
#				undef,
#				undef,
#				$new_server_config{"Server Group"});
#	}
#
#	if( $new_server_config{"Server Type"} =~ /Microsoft SQL Server/ ) {
#		_statusmsg("[func] Adding Microsoft SQL Server ".$new_server_config{"Server Name"}."\n");
#		add_a_server_to_gem("Microsoft SQL",
#			$new_server_config{"Server Name"},
#			$new_server_config{"DB Admin Login"},
#			$new_server_config{"DB Admin Password"},
#			$new_server_config{"Server Group"},
#			undef,undef, $new_server_config{'Host Name'} );
#	} elsif( $new_server_config{"Server Type"} =~ /Sybase/ ) {
#		_statusmsg("[func] Adding Sybase Server ".$new_server_config{"Server Name"}."\n");
#		add_a_server_to_gem("Sybase Server",
#			$new_server_config{"Server Name"},
#			$new_server_config{"DB Admin Login"},
#			$new_server_config{"DB Admin Password"},
#			$new_server_config{"Server Group"},
#			undef,undef, $new_server_config{'Host Name'},
#			$new_server_config{'Port Number'});
#	} elsif( $new_server_config{"Server Type"} =~ /Oracle/ ) {
#		_statusmsg("[func] Adding Oracle Server ".$new_server_config{"Server Name"}."\n");
#		add_a_server_to_gem("Oracle Server",
#			$new_server_config{"Server Name"},
#			$new_server_config{"DB Admin Login"},
#			$new_server_config{"DB Admin Password"},
#			$new_server_config{"Server Group"},
#			undef,undef, $new_server_config{'Host Name'});
#	}	
#
#	_statusmsg("[func] Displaying message box - select a button to continue\n");
#	$XMLDATA{mainWindow}->messageBox(
#		-title => 'Success',
#		-message => "Server(s) Created Successfully\n",
#		-type => "OK" );
#	_statusmsg("[func] GEM Ready / Continuing...\n");
#
#	return 1;
#}

my($plantab_rb_value)="SQL Server";
my($sv_plantab_rb_value)="SQL Server";
#PL# my($sv_plantab_servers_curval)=$plantab_text_allsvr;
my($sv_plantab_plans_curval)=undef;
sub plantab_select_radio_btn {
	# ok a server button has been selected... first lets figure out if it has changed
	if( $plantab_rb_value ne $sv_plantab_rb_value ) {
   		$sv_plantab_rb_value = $plantab_rb_value;
   		$sv_plantab_plans_curval = undef;
        	@plantab_plans=plan_get(type=>$plantab_rb_value, sort=>1);
   		$plantab_plans_curval = $plantab_plans[0];
   		update_planinfo($plantab_plans_curval);
	}
}

sub plantab_select_plan {
	my($combobox, $indx, $value, $tvalue)=@_;
	#print "plantab_select_plan() ($combobox, $indx, $value, $tvalue)\n";
	#print "plantab_select_plan ".join(" ",@_)."\n";
	update_planinfo($value);
}

#PL# sub plantab_sel_server {
	#PL# my($combobox, $indx, $value, $tvalue)=@_;
	#print "DBGX plantab_sel_server( server=$plantab_servers_curval plan=$plantab_plans_curval)\n";

	#PL# @plantab_plans	= ();
	#PL# $plantab_plans_curval = undef;
	#PL# foreach ( plan_get(sort=>1) ) {
		#print "DBGX: checking plan $_ => dsquery=",$CONFIG{$_."_DSQUERY"}," = looking for $plantab_servers_curval\n";
 		#PL# if( $plantab_servers_curval eq $plantab_text_allsvr
 				#PL# or $CONFIG{$_."_DSQUERY"} eq $plantab_servers_curval ) {
	 		#PL# $plantab_plans_curval=$_	if ! defined $plantab_plans_curval;
	 		#PL# push @plantab_plans, $_;
	 		#print "DBGX FOUND PLAN=$_ DSQ= ",$CONFIG{$_."_DSQUERY"},"\n";
 		#PL# }
   	#PL# }

	#PL# return;

#	my($plantab_servers_curval,$svr_sel)=backuptab_get_cur_srvr();
#	my($plantab_plans_curval,$pln_sel)=backuptab_get_cur_plan();
#	print "Current Server=$plantab_servers_curval Plan=$plantab_plans_curval : $svr_sel/$pln_sel\n";
#
#	# the radio button changed or is initialized
#   	if( $plantab_rb_value ne $sv_plantab_rb_value ) {
#   		print "Radio Button Has Changed\n";
#   		$sv_plantab_rb_value = $plantab_rb_value;
#   		$sv_plantab_servers_curval = undef;
#   		$sv_plantab_plans_curval = undef;
#
#        	$Plan_Server_lb->delete(0,'end');
#   		if( $plantab_rb_value eq "SQL Server" ) {
#	   		foreach (get_server_by_type(-type=>"sqlsvr",-sorted=>1)) {
#	   			$plantab_servers_curval = $_ unless $plantab_servers_curval;
#	   			$Plan_Server_lb->insert('end',$_);
#	   		}
#
#   		} else {
#	   		foreach (get_server_by_type(-type=>"sybase",-sorted=>1)) {
#	   			$plantab_servers_curval = $_ unless $plantab_servers_curval;
#	   			$Plan_Server_lb->insert('end',$_);
#	   		}
#   		}
#   		$Plan_Server_lb->selectionSet(0,0);
#   	}
#
#	if( $plantab_servers_curval ne $sv_plantab_servers_curval ) {
#   		print "Server Has Changed to $plantab_servers_curval\n";
#   		$sv_plantab_servers_curval = $plantab_servers_curval;
#	        $Plan_Planname_lb->delete(0,'end');
#		$Plan_Planname_lb->insert(0,"DEFAULT");
#   		foreach (sort @{$XMLDATA{Backup_Plan_Aryptr}}) {
#   			next unless $CONFIG{$_."_DSQUERY"} eq $plantab_servers_curval;
#	   		$plantab_plans_curval = $_ unless $plantab_plans_curval;
#			$Plan_Planname_lb->insert('end',$_); #if $CONFIG{$_."_DSQUERY"} eq ;
#   			print "PLAN=$_ DSQ= ",$CONFIG{$_."_DSQUERY"},"\n";
#   		}
#   		$Plan_Planname_lb->selectionSet(0,0);
#   	}
#
#	if( $plantab_plans_curval ne $sv_plantab_plans_curval ) {
#		print "Plan Has Changed\n";
#   		$sv_plantab_plans_curval = $plantab_plans_curval;
#		print "Drawing Plan $plantab_plans_curval\n";
#	}

#   foreach( keys %BackupPlanNames ) {
#   	$DsqueryByPlanName{$_}=$CONFIG{$_."_DSQUERY"} || $_;
#   	#print "PLAN $_ DSQUERY=$DsqueryByPlanName{$_}\n";
#   }
#PL# }

sub plantab_create_plan_wizard {
	
	my(@searchdirs);
	foreach my $i (1..8) {
		push @searchdirs, $CONFIG{"TARGET_BACKUP_DIR".$i} if $CONFIG{"TARGET_BACKUP_DIR".$i} !~ /^\s*$/;
	}

	if( $#searchdirs == -1 ) {
		_statusmsg("[func] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
				-title => 'No Backup Directories Found',
				-message => 'Please specify one or backup directories on the Backups/locations tabs before continuing',
				-type => "OK" );
		_statusmsg("[func] GEM Ready / Continuing...\n");
		return 0;
	}	
	
	my(%found);
	
	# CHECK OUT THE DIRECTORIES ON YOUR SERVERS
	foreach(get_server_by_type(-type=>"sybase")) { $found{$_}="sybase"; }	
	foreach(get_server_by_type(-type=>"sqlsvr")) { $found{$_}="sqlsvr"; }

	# list out the existing plans...
	foreach ( plan_get() ) {
		my($dsquery)=	$CONFIG{$_."_DSQUERY"} || $_;
		if( $found{$dsquery} ) {
			$found{$dsquery} ="DEL";		
		} else {
			_statusmsg( "[Wizard] Warning Server Not Registered: Plan=$_ (server=",$dsquery,")\n");
		}
	}
	
	foreach my $s (keys %found ) {
		next if $found{$s} eq "DEL";
		
		my $href = $server_info{$s.":".$found{$s}};
		if( ! is_nt() and $found{$s} eq "sqlsvr" ) {
			_debugmsg( "[Wizard] Skipping Server $s (type=",$found{$s},") \n");
			#foreach ( keys %{$href} ) {
			#	_statusmsg( "[Wizard] -- (IGNORING) KEY $_ VAL=",$$href{$_},") \n");				
			#}		
		} else {
			_statusmsg( "[Wizard] Searching $found{$s} Server $s For Backup Locations\n");			
			if( !  $$href{hostname} ) {
				_statusmsg("[func] Displaying message box - select a button to continue\n");
				$XMLDATA{mainWindow}->messageBox(
						-title => 'No Host Found For Server '. $s,
						-message => 'No Host Found For Server '.$s.' - skipping',
						-type => "OK" );
				_statusmsg("[func] GEM Continuing...\n");
				next;
			}				
			_statusmsg( "[Wizard] Connecting to Host $$href{hostname}\n");			
			
			my($svr_type) ="unix" 					if $server_info{$$href{hostname}.":unix"};
			$svr_type = "win32servers" 			if $found{$s} eq "sqlserver" or $server_info{$$href{hostname}.":win32servers"};
				
			if( ! $svr_type ) {
				_statusmsg("[Wizard] Displaying message box - select a button to continue\n");				
				$XMLDATA{mainWindow}->messageBox(
						-title => 'No host type Found For Server '. $$href{hostname},
						-message => 'No Registered Host For '.$$href{hostname}."\nDiscovered Host=".$$href{hostname}."\nSkipping Server\nPerhaps This Host Is Not Registered\n",
						-type => "OK" );
				_statusmsg("[Wizard] GEM Continuing...\n");
				next;
			}
			
			my($c,$errmsg)=$connectionmanager->connect( -type=>$svr_type,-name=>$$href{hostname});
			if( ! defined $c ) {
				_statusmsg("[Wizard] Failed To Connect to $$href{hostname}\n");				
				next;
			}
			
			my($found)="";
			foreach my $dir ( @searchdirs ) {
				my($rc) = $c->cwd( $dir );
				if( $rc == 0 ) {
					_statusmsg("[Wizard] cwd($dir) failed - skipping\n");				
					next;
				}
				_statusmsg("[Wizard] $dir exists\n");				
				$found=$dir;
				last;
			}			
			
			if( $found eq "" ) {
				_statusmsg("[Wizard] Can Not Create Plan - No Backup Directory Found\n" );
				_statusmsg("[Wizard] Skipping ",$$href{hostname}," and continuing wizard\n" );
				next;
			} else {
				_statusmsg("[Wizard] Creating Maintenance Plan $s\n");				
				$plantab_plans_curval=$s;
				if( plan_get(name=>$s) ) {
					$XMLDATA{mainWindow}->messageBox(
						-title => 'Data Entry Enter',
						-message => 'A Duplicate Plan Has Been Entered '.join("\n",plan_get()),
						-type => "OK" );
					return 0;
				}			
				
				my(%args);					
				if( $c->get_local() eq "LOCAL" ) {
					$args{LOCAL_DIR}=$found;
					$args{IS_REMOTE}	= 'n';
				} elsif( $svr_type eq "unix" ) {
					$args{LOCAL_DIR}	= "";
					$args{IS_REMOTE}	= 'y'; 					
				} else {		# this would be windows
					$found=~/^([A-Z])\:(.+)$/;				
					$args{LOCAL_DIR} = "//". $$href{hostname}."/".$1."\$".$2;
					$args{IS_REMOTE}	= 'n';
				}
				if(  $found{$s} eq "sybase" ) {
					$args{COMPRESSION_LEVEL}=1;
					$args{UPD_STATS_FLAGS} = -i;
				} else {
					$args{COMPRESSION_LEVEL}=0;
				}
				
#	plan_add( name=>$plantab_plans_curval,
#			REMOTE_HOST 	=> $args{-host},
#			REMOTE_DIR 	=> $args{'Local Directory Name (from db server) '},
#			LOCAL_DIR 	=> "",
#			MAIL_TO 	=> $XMLDATA{admin_email},
#			SUCCESS_MAIL_TO => $XMLDATA{admin_email},
#			IS_REMOTE	=> $isr );
				
				plan_add( name			=> $s,
					DSQUERY 				=> $s,
					REMOTE_HOST 		=> $$href{hostname},
					REMOTE_DIR 			=> $found,					
					NUM_BACKUPS_TO_KEEP	=>	1,
					DO_COMPRESS			=> "n",
					DO_AUDIT				=> "y",
					DO_DBCC				=> "y",
					DO_UPDSTATS				=> "y",
					DO_PURGE				=> "y",
					DO_LOAD				=> "y",
					MAIL_TO 				=> $XMLDATA{admin_email},
					SUCCESS_MAIL_TO 	=> $XMLDATA{admin_email},					
					%args );
			}			
		}	
	}
	_statusmsg( "[Wizard] Rebuilding Batch Scripts\n");
	build_local_scripts();
	
	_statusmsg( "[Wizard] Add Plan Wizard Completed\n");
}

sub plantab_draw {
   my($tabB)=@_;
   _debugmsg("[func] plantab_draw()\n" );
   
   my $tabB_lft  = $tabB->LabFrame(-label=>"Maintenance Plans",
   				-labelside=>'acrosstop')->pack(qw/-fill y -side left -ipadx 15 -ipady 15 -anchor n/);
   $PlanNbLabel  = $tabB->LabFrame(-label=>"Current Plan = DEFAULT",
   				-labelside=>'acrosstop')->pack(qw/ -fill both -expand 1 -side right -anchor n/);

   my $rb_frm = $tabB_lft->Frame()->pack(-side=>'top', -fill=>'x');

   dohelp(
   	$rb_frm->Button(-width=>24,-bg=>'white',
   		-text=>"Create Plan Wizard",
   		-command=>\&plantab_create_plan_wizard )->pack( -side=>"top",-pady=>2, -padx=>2),
   	"Maintenance Plan Wizard");

   dohelp(
	$rb_frm->Radiobutton(-font=>"small",-variable=>\$plantab_rb_value, -value=>"SQL Server",
		-text=>"SQL Server",-command=>\&plantab_select_radio_btn )->pack( -side=>"left" ),
   	"Select Server Type");

   dohelp(
	$rb_frm->Radiobutton(-font=>"small",-variable=>\$plantab_rb_value, -value=>"Sybase",
		-text=>"Sybase",-command=>\&plantab_select_radio_btn )->pack( -side=>"left" ),
   	"Select Server Type");

   $Plan_Leftside_Width=0;
   foreach ( plan_get() ) {
   	$Plan_Leftside_Width = length if length > $Plan_Leftside_Width;
   }
   $Plan_Leftside_Width++; 	# to give more width to the box
   $Plan_Leftside_Width++;
   $Plan_Leftside_Width = 20 if $Plan_Leftside_Width < 20;

   dohelp(
   	$tabB_lft->Button(-width=>24,-bg=>'white',
   		-text=>"Add Maintenance Plan",
   		-command=>\&tk_add_a_server )->pack( -side=>"top",-pady=>2, -padx=>2),
   	"Add New Maintenance Plan");
   dohelp(
   	$tabB_lft->Button(-width=>24,-bg=>'white',
   		-text=>"Delete Maintenance Plan",
   		-command=>\&plantab_delete_plan)->pack( -side=>"top", -pady=>2, -padx=>2),
   	"Delete Current Maintenance Plan");
  dohelp(
   	$tabB_lft->Button(-width=>24,-bg=>'white',-text=>"Show Default Plan",
   		-command=>\&update_planinfo)->pack( -side=>"top", -pady=>2, -padx=>2),
   	"Show Default Plan");

   @plantab_plans=plan_get(type=>$plantab_rb_value, sort=>1);
   $plantab_plans_curval = $plantab_plans[0];

   $tabB_lft->Label(-font=>"bold",-text=>"MAINTENANCE PLAN:",-anchor=>'w')->pack(-fill=>'x',-side=>'top', -pady=>2, -padx=>2);
   $Plan_Planname_lb=$tabB_lft->JComboBox(
   	-choices=>\@plantab_plans,
   	-relief=>'sunken',
   	-background=>'white',
   	-entrywidth=>$Plan_Leftside_Width,
   	-selectcommand=>\&plantab_select_plan,
   	-textvariable=>\$plantab_plans_curval,
   	-maxrows=>18
   )->pack(-side=>'top', -fill=>'x', -pady=>2, -padx=>2);
	
   plantab_paint_notebook($plantab_plans_curval,$PlanNbLabel);     
   update_planinfo($plantab_plans_curval);
}

#my(@compress_items, @logship_items);
my($use_syb_internal)="n";

#sub plantab_compressitems_button {
#	foreach( @compress_items) { $_->gridForget(); }
#	if( $use_syb_internal eq 'y' ) {
#		$compress_items[0]->grid(-sticky=>'ew',  -row=>3, -column=>1, -padx=>1);
#		$compress_items[1]->grid(-sticky=>'ew', -row=>3, -column=>2);
#		$compress_items[2]->grid(-sticky=>'ew', -row=>3, -column=>3);
#		$BKP_DATAVALS{COMPRESSION_LEVEL}=1;
#	} elsif( $BKP_DATAVALS{DO_COMPRESS} eq "y" ) {
#		$compress_items[3]->grid(-sticky=>'ew',  -row=>3, -column=>1, -padx=>1);
#		$compress_items[4]->grid(-sticky=>'ew', -row=>3, -column=>2);
#		$compress_items[5]->grid(-sticky=>'ew', -row=>3, -column=>3);
#
#		$compress_items[6]->grid(-sticky=>'ew',  -row=>4, -column=>1, -padx=>1);
#		$compress_items[7]->grid(-sticky=>'ew', -row=>4, -column=>2);
#		$compress_items[8]->grid(-sticky=>'ew', -row=>4, -column=>3);
#
#		$compress_items[9]->grid(-sticky=>'ew', -row=>5, -column=>1, -padx=>1);
#		$compress_items[10]->grid(-sticky=>'ew', -row=>5, -column=>2);
#		$compress_items[11]->grid(-sticky=>'ew', -row=>5, -column=>3);
#		$BKP_DATAVALS{COMPRESSION_LEVEL}=0;
#	}
#}

#sub plantab_logship_button {
#	foreach( @logship_items) { $_->gridForget(); }
#	if( $BKP_DATAVALS{DO_LOAD} eq 'y' ) {
#		my($count)=0;
#		foreach (@logship_items) {
#			#print "POSITION OF ITEM $count: ",2+int($count/3)," ",1+$count-3*int($count/3),"\n";
#			$_->grid(-sticky=>'ew',   -row=>2+int($count/3), -column=>1+$count-3*int($count/3));
#			$count++;
#		}
#	}
#}

sub plantab_hilight {
	if( $plantab_rb_value eq "SQL Server" ) {
		$BKP_DATACELLS{use_syb_internal}->configure(-state=>'disabled');
		$BKP_ISDEFAULT_TEXT{use_syb_internal}="[ FIELD DISABLED ]";
		$use_syb_internal='n';
	} else {
		$BKP_DATACELLS{use_syb_internal}->configure(-state=>'normal');
		$BKP_ISDEFAULT_TEXT{use_syb_internal}="[ FIELD ENABLED ]";
		$use_syb_internal="y" if defined $BKP_DATAVALS{COMPRESSION_LEVEL}
   					and  $BKP_DATAVALS{COMPRESSION_LEVEL}>0;
   	}

	if( $BKP_DATAVALS{DO_LOAD} eq 'y' ) {
		$plantab_objects{Notebook}->pageconfigure("LOG_SHIPPING",-state=>'normal');
	} else {
		$plantab_objects{Notebook}->pageconfigure("LOG_SHIPPING",-state=>'disabled');
	}
	
	if( $BKP_DATAVALS{DO_PURGE} eq 'y' ) {
		$plantab_objects{Notebook}->pageconfigure("PURGE",-state=>'normal');
	} else {
		$plantab_objects{Notebook}->pageconfigure("PURGE",-state=>'disabled');
	}

	if( $use_syb_internal eq "y" ) {
		$BKP_DATACELLS{COMPRESSION_LEVEL}->configure(-state=>'normal');
		$BKP_DATACELLS{COMPRESS_LATEST}->configure(-state=>'disabled');
		$BKP_DATACELLS{COMPRESS}->configure(-state=>'disabled');
		$BKP_DATACELLS{UNCOMPRESS}->configure(-state=>'disabled');

		$BKP_ISDEFAULT_TEXT{COMPRESSION_LEVEL}=$BKP_ISDEFAULT_SAVE{COMPRESSION_LEVEL};
		$BKP_ISDEFAULT_TEXT{COMPRESS_LATEST}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{COMPRESS}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{UNCOMPRESS}="[ FIELD DISABLED ]";
	} else {
		$BKP_DATACELLS{COMPRESSION_LEVEL}->configure(-state=>'disabled');
		$BKP_DATACELLS{COMPRESS_LATEST}->configure(-state=>'normal');
		$BKP_DATACELLS{COMPRESS}->configure(-state=>'normal');
		$BKP_DATACELLS{UNCOMPRESS}->configure(-state=>'normal');

		$BKP_ISDEFAULT_TEXT{COMPRESSION_LEVEL}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{COMPRESS_LATEST}=$BKP_ISDEFAULT_SAVE{COMPRESS_LATEST};
		$BKP_ISDEFAULT_TEXT{COMPRESS}=$BKP_ISDEFAULT_SAVE{COMPRESS};
		$BKP_ISDEFAULT_TEXT{UNCOMPRESS}=$BKP_ISDEFAULT_SAVE{UNCOMPRESS};
	}
	if( $BKP_DATAVALS{DO_COMPRESS} eq 'n' ) {
		$BKP_DATACELLS{COMPRESS_LATEST}->configure(-state=>'disabled');
		$BKP_DATACELLS{COMPRESS}->configure(-state=>'disabled');
		$BKP_DATACELLS{UNCOMPRESS}->configure(-state=>'disabled');

		$BKP_ISDEFAULT_TEXT{COMPRESS_LATEST}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{COMPRESS}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{UNCOMPRESS}="[ FIELD DISABLED ]";
	}

	if( $BKP_DATAVALS{IS_REMOTE} eq 'y' ) {		
		$BKP_DATACELLS{LOCAL_DIR}->configure(-state=>'disabled');
		$BKP_DATACELLS{REMOTE_HOST}->configure(-state=>'normal');
		$BKP_ISDEFAULT_TEXT{LOCAL_DIR}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{REMOTE_HOST}=$BKP_ISDEFAULT_SAVE{REMOTE_HOST};
	} else {
		$BKP_DATACELLS{LOCAL_DIR}->configure(-state=>'normal');
		$BKP_DATACELLS{REMOTE_HOST}->configure(-state=>'disabled');
		$BKP_ISDEFAULT_TEXT{REMOTE_HOST}="[ FIELD DISABLED ]";
		$BKP_ISDEFAULT_TEXT{LOCAL_DIR}=$BKP_ISDEFAULT_SAVE{LOCAL_DIR};
	}
}

sub plantab_paint_notebook {
   my($BboxCurSelection,$PlanNbLabel)=@_;
   _debugmsg("[func] plantab_paint_notebook($BboxCurSelection)\n" );

   # BUILD NOTEBOOK
   $plantab_objects{Notebook}  = $PlanNbLabel->NoteBook();
   my(%TabsByCat);
   $TabsByCat{WELCOME} = $plantab_objects{Notebook}->add("WELCOME", -label=>"Welcome",  -underline=>0);
   $TabsByCat{WELCOME}->Label(-font=>"bold",-text=>"\nGEM Maintenance Plan Manager Setup\n",-bg=>'beige',
				-justify=>'left',
   			-relief=>"groove")->pack(-pady=>2, -anchor=>'n', -fill=>'x',	-side=>'top');#	-expand=>1
   $TabsByCat{WELCOME}->Label(	-font=>"norm",-text=>"
The Maintenance Plan Manager for Sybase and Sql Server allows you define your routine
database maintenance tasks.  Plan data is stored in the file conf/configure.cfg. Like 
all GEM Configuration files, this self-documenting file can be hand-editited as needed.

You should read the GEM Maintenance Plan Documentation before setting up your Maintenance Plans.

We provide a wizard to help set up your plans.  Before running, please set up your preferred
backup directory search list on the Locations tab.  After plans are created by the wizard,
you should rebuild your Batch Scripts (see Install Software Tab).  As with other batch
scripts, Plan scripts are created you in the unix_batch_scripts and win32_batch_scripts 
subdirectories.  plan_batch (what you should schedule) and plan_interactive (interactive use)
subdirectories separate these from normal GEM scritps. You are responsible for scheduling. 

The DEFAULT plan defines enterprise wide variable defaults.  If a field is hilighted in
yellow, that plan uses values from the DEFAULT plan - which will appear bracketed i.e. <1>.
",
   			#-bg=>'beige',
				-justify=>'left',
   			#-relief=>"groove"
   			)->pack(-pady=>2, -side=>'top',
   					-anchor=>'n',
						#-fill=>'y',
						#-expand=>1
						);

	$TabsByCat{DIRECTORIES} = $plantab_objects{Notebook}->add("DIRECTORIES", -label=>"Locations",  -underline=>0);
	$TabsByCat{DIRECTORIES}->Label(-font=>"bold",-text=>"Directory Locations For Backups",-bg=>'beige',
				-justify=>'left',
   			-relief=>"groove")->pack(-pady=>2, -anchor=>'n', -fill=>'x');

   $TabsByCat{DIRECTORIES}->Label(-font=>"norm",-text=>"Please enter all the possible directories to place backup dump files.  Include both UNIX and Win32\ndirectories as appropriate. The Create Plan Wizard will check existence of these directories (in order)\nfor your system and creates plans using the first writable directory it finds.\nYou can override on a plan by plan basis using the Definition tab.",
   			#-bg=>'beige',
				-justify=>'left',
   				)->pack(-pady=>2, -anchor=>'n',-side=>'top',);
   my $locfrm = $TabsByCat{DIRECTORIES}->LabFrame(-label=>"Backup Directories",-labelside=>'acrosstop')->pack(-side=>'top', -fill=>'both', -padx=>5);
   
   for my $i (1..8) {
   	#$CONFIG{"TARGET_BACKUP_DIR".$i}="" unless defined $CONFIG{"TARGET_BACKUP_DIR".$i};
   	$locfrm->Label(
   			-anchor=>'w',
   			-background=>'beige', 
   			-relief=>'groove', 
   			-justify=>'left',
   			-font=>"smbold",
   			-text=>"Directory #".$i)->grid(-sticky=>'ew', -row=>$i, -column=>1, -padx=>1);
   	$locfrm->Entry(
   				-width=>60,
   				-textvariable=>\$CONFIG{"TARGET_BACKUP_DIR".$i})->grid(-sticky=>'ew', -row=>$i, -column=>2, -padx=>1);
   }		
   	

   foreach my $tab ( @BackupTabList_DATA ) { 	   	# create a tab
   	my($label)="";
   	foreach ( split(/[\_\s]/,$tab) ) { $label.=ucfirst(lc($_))." "; }
   	$TabsByCat{$tab} = $plantab_objects{Notebook}->add($tab, -label=>$label,  -underline=>0);
		my $lbltxt;
		if( $tab =~ /definition/i ) {
			$lbltxt = "Core Plan Variables\n";
		} elsif( $tab =~ /STEPS/i ) {
			$lbltxt    = "The Following Steps Are Run As Part of This Plan\n";
		} elsif( $tab =~ /COMPRESSION/i ) {
			$lbltxt    = "Define Backup Compression Strategy.  \n";
		} elsif( $tab =~ /Purge/i ) {
			$lbltxt    = "Define File Purge Policy\n";
		} elsif( $tab =~ /Overrides/i ) {
				$lbltxt    = "Variable Overrides \n";
		} elsif( $tab =~ /MAIL/i ) {
			$lbltxt    = "Mail / Notification Details\n";
		} elsif( $tab =~ /LOG_SHIPPING/i ) {
			$lbltxt    = "Log Shipping Definition\n";
		} else {
			mydie( "ERROR: TAB $tab NOT SET UP - DATA SECTION MAY BE WRONG\n" );
		}

 		#$TabsByCat{$tab}->Label(-font=>"bold",-text=>$lbltxt)->pack(-side=>'top', -fill=>'x', -padx=>5);
 		my $frm = $TabsByCat{$tab}->LabFrame(-label=>$lbltxt,-labelside=>'acrosstop')->pack(-side=>'top', -fill=>'both', -expand=>1, -padx=>5);
   	my($row)=1;

   	if( $tab =~ /COMPRESSION/i ) {
   		$use_syb_internal="y" if defined $BKP_DATAVALS{COMPRESSION_LEVEL}
   					and  $BKP_DATAVALS{COMPRESSION_LEVEL}>0;
   		$frm->Label(
   			-anchor=>'w',-background=>'beige', -relief=>'groove', -justify=>'left',
   			-font=>"smbold",
   			-text=>"Use Sybase Internal Compression\n  (ASE 12.5 or Later Only)")->grid(-sticky=>'ew', -row=>$row, -column=>1, -padx=>1, -pady=>2);
			my $smallfrm = $frm->Frame()->grid(-sticky=>'ew', -row=>$row, -column=>2, -padx=>1, -pady=>2);

   		$BKP_DATACELLS{use_syb_internal} = $smallfrm->Checkbutton(
				-variable=>\$use_syb_internal,
				-onvalue=>'y',
				-offvalue=>'n',
				-command => \&plantab_hilight
				#-command=> sub { plantab_compressitems_button();	}
			)->pack(-side=>'top', -fill=>'x', -padx=>5);	#grid(-sticky=>'ew', -row=>$row, -column=>2);
			
			$smallfrm->Label(
				-textvariable=>\$BKP_ISDEFAULT_TEXT{use_syb_internal},
				-font=>'norm', -justify=>'left')->pack(-side=>'bottom', -fill=>'x', -padx=>5, -pady=>2);
			$BKP_ISDEFAULT_TEXT{use_syb_internal}="[ FIELD ENABLED ]";
			$row++;
		}

   	foreach ( @bt_KeyList_DATA ) {
   		next unless $bt_TabByKey_DATA{$_} eq $tab;
			my($lbl);

   		if( defined $bt_TabHelp_DATA{$_} ) {
   			$lbl=$frm->Label(
   				-anchor=>'w',-background=>'beige', -relief=>'groove', -justify=>'left',
   				-font=>"smbold",
   				-text=>$bt_TabHelp_DATA{$_})->grid(-sticky=>'nsew', -row=>$row, -column=>1, -padx=>1, -pady=>2);
   		} else {
   			$lbl=$frm->Label(-font=>"smbold",-text=>$_." = ")->grid(-sticky=>'nsew', -row=>$row, -column=>1, -padx=>1, -pady=>2);
   		}

   		my $smallfrm = $frm->Frame()->grid(-sticky=>'ew', -row=>$row, -column=>2, -padx=>1, -pady=>2);

   		if( $bt_ElementLength_DATA{$_} eq "CK" ) {
   			$BKP_CELL_TYPE{$_} = "CK";
   			my($on,$off)=('Y','N');
   			($on,$off)=('y','n')	if /^DO_/ or /^IS_REMOTE/ or /^COMPRESS_LATEST/;

#			if( $_ eq "DO_LOAD" ) {
#				$BKP_DATACELLS{$_} = $smallfrm->Checkbutton(
#   					-variable=>\$BKP_DATAVALS{$_},
#   					-onvalue=>$on,
#					-offvalue=>$off,
#					-command => \&plantab_hilight
#				)->pack(-side=>'top', -fill=>'x', -padx=>5);;
#			} elsif( $_ eq "DO_COMPRESS" ) {
#				$BKP_DATACELLS{$_} = $smallfrm->Checkbutton(
#   					-variable=>\$BKP_DATAVALS{$_},
#   					-onvalue=>$on,
#					-offvalue=>$off,
#					-command => \&plantab_hilight
#					#-command=> sub { plantab_compressitems_button();	}
#				)->pack(-side=>'top', -fill=>'x', -padx=>5);
#
#
#			} else {
				$BKP_DATACELLS{$_} = $smallfrm->Checkbutton(
   					-variable=>\$BKP_DATAVALS{$_},
   					-onvalue=>$on,
					-offvalue=>$off,
					-command => \&plantab_hilight
				)->pack(-side=>'top', -fill=>'x', -padx=>5);
#			}
   		} else {
				$BKP_CELL_TYPE{$_} = "ENTRY";
				#$smallfrm->Label("row ".$row." col1")->pack();

   			$BKP_DATACELLS{$_} = $smallfrm->Entry(
   				-width=>$bt_ElementLength_DATA{$_},
   				-textvariable=>\$BKP_DATAVALS{$_})->pack(-side=>'top', -fill=>'x', -padx=>5);
		}

		my($dfltrow)=$smallfrm->Label(
			#-width=>10, -background=>'beige', -relief=>'groove'
			-textvariable=>\$BKP_ISDEFAULT_TEXT{$_},
			-font=>'norm', -justify=>'left')->pack(-side=>'bottom', -fill=>'x', -padx=>5);

#		if( $tab =~ /COMPRESSION/i ) {
#			push @compress_items,$lbl;
#			push @compress_items,$BKP_DATACELLS{$_};
#			push @compress_items,$dfltrow;
#		}
#
#		if( $tab =~ /LOG_SHIPPING/i and $_ ne "DO_LOAD" ) {
#			push @logship_items,$lbl;
#			push @logship_items,$BKP_DATACELLS{$_};
#			push @logship_items,$dfltrow;
#		}

		dohelp( $BKP_DATACELLS{$_}, $bt_TabHelp_DATA{$_} );
		$row++;
		#$row++ if $tab =~ /COMPRESSION/i and $row==2;	# skip because 2 is allready taken
   	}
   }

   foreach( plan_get() ) { $DsqueryByPlanName{$_}=$CONFIG{$_."_DSQUERY"} || $_;   }
   $plantab_objects{Notebook}->pack(-side=>'left',-anchor=>'w',-fill=>'both', -expand=>1);
   update_planinfo($BboxCurSelection);
   return $plantab_objects{Notebook};
}

sub plantab_delete_plan
{
	if( $plantab_plans_curval eq "" ) {
		_statusmsg("[func] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
				-title => 'Cant Delete',
				-message => 'Cant Delete Current Plan - No Plan Selected',
				-type => "OK" );
		_statusmsg("[func] GEM Ready / Continuing...\n");
		return 0;
	}

	if( $plantab_plans_curval eq "DEFAULT" ) {
			_statusmsg("[func] Displaying message box - select a button to continue\n");
			$XMLDATA{mainWindow}->messageBox(
					-title => 'Cant Delete DEFAULT',
					-message => 'Cant Delete DEFAULT Plan',
					-type => "OK" );
			_statusmsg("[func] GEM Ready / Continuing...\n");
			return 0;
	}

	# remove from box
	my($indx)  =0;
	foreach (@plantab_plans) {
		if( $_ eq $plantab_plans_curval ) { $indx = -1; last; }
		$indx++;
	}
	return if $indx == -1;
	splice( @plantab_plans, $indx, 1 );
	foreach (keys %bt_TabByKey_DATA) {
		my($x) = $plantab_plans_curval."_".$_;
		delete $CONFIG{$x} if defined $CONFIG{$x};
	}

	plan_delete(name=>$plantab_plans_curval);

	# remove from arrays
	_debugmsg( "[func] [func] Plan $plantab_plans_curval Deleted\n" );
	print LOGFP "Plan $plantab_plans_curval Deleted\n" unless defined $NOLOGFILE;
	#$Plan_Planname_lb->selectionSet(0,0);
	#update_planinfo($Plan_Planname_lb->curselection());
	update_planinfo("DEFAULT");
}

sub mydie {
	_statusmsg("[func] Displaying message box - select a button to continue\nFATAL ERROR: ",@_,"\n");
	$XMLDATA{mainWindow}->messageBox(
					-title => 'FATAL ERROR',
					-message => 'FATAL ERROR: '.join("\n",@_),
					-type => "OK" ) if defined $XMLDATA{mainWindow};
	confess("") if $DEBUG;
	exit(0);
}

sub mywarn {
	_statusmsg( "WARNING: ",@_,"\n" );
	_statusmsg("[func] Displaying message box - select a button to continue\nWARNING: ",@_,"\n");
	$XMLDATA{mainWindow}->messageBox(
					-title => 'WARNING',
					-message => 'WARNING: '.join("\n",@_),
					-type => "OK" ) if defined $XMLDATA{mainWindow};
	unbusy(1);	
	return undef;
}

#sub backuptab_get_cur_srvr {
#	my($rc);
#	my($sel)=$Plan_Server_lb->curselection();
#	if( $sel eq "" ) {
#		$rc = $cur_backup_plan;
#	} else {
#		$rc = $Plan_Server_lb->get($sel);
#	}
#	return ($rc,$sel);
#}
#
#sub backuptab_get_cur_plan {
#	my($rc);
#
#	if( ! defined $Plan_Planname_lb or ! Tk::Exists($Plan_Planname_lb) ) {
#		#die "WARNING - update_planinfo() called with no list box associated\n";
#		# this is ok in the context of gem.pl...
#		return (undef,undef);
#	}
#
#	my($sel)=$Plan_Planname_lb->curselection();
#	if( $sel eq "" ) {
#		$rc = $cur_backup_plan;
#	} else {
#		$rc = $Plan_Planname_lb->get($sel);
#	}
#	return ($rc,$sel);
#}

# Called On Click Or Whatever
sub update_planinfo {
	shift if ref $_[0];
	return if $UPGRADE;
	my($next_backup_plan)=@_;	
	
	_debugmsg( "[func] update_planinfo(",join(" ",@_),") called!\n" );

	# you can either have a current plan or not...
	$next_backup_plan="DEFAULT" if ! defined $next_backup_plan;

	# Reset Text In Top Label
	$PlanNbLabel->configure( -label=>"Current Plan = ".$next_backup_plan )	if defined $PlanNbLabel;

	if( ! defined $cur_backup_plan ) {
		_debugmsg("[func] update_planinfo():   Loading plan data for plan $next_backup_plan\n" );
	} else {
		_debugmsg("[func] update_planinfo(): Flushing $cur_backup_plan Loading $next_backup_plan\n" );
	}

	foreach my $key ( sort keys %bt_TabByKey_DATA ) {
		my($val)= $BKP_DATAVALS{$key};
				
		if( defined $cur_backup_plan ) {
			if( $cur_backup_plan eq "DEFAULT" or $cur_backup_plan eq "" or ! defined $cur_backup_plan ) {
				# its the default plan
				$val="" unless defined $val;
				if( ! defined $CONFIG{$key} or $CONFIG{$key} ne $val ) {
					$CONFIG{$key} = $val if $val !~ /^</ and $val !~ />$/;
				}
			} else {
				if( defined $CONFIG{$cur_backup_plan."_".$key} ) {
					if( $val eq "" and $CONFIG{$cur_backup_plan."_".$key} ne "" ) {
						# no value so undef it so you can use the default
						undef $CONFIG{$cur_backup_plan."_".$key};
					} elsif( $val !~ /^</ and $CONFIG{$cur_backup_plan."_".$key} ne $val ) {
						_debugmsg( "[func] Creating & Setting $cur_backup_plan key $key to $val\n" );
						$CONFIG{$cur_backup_plan."_".$key} = $val;
					};
				} else {
					$CONFIG{$cur_backup_plan."_".$key} = $val if $val !~ /^</;
				}
			}
		}
		
		if( $next_backup_plan eq "DEFAULT" ) {
			$BKP_DATAVALS{$key} = $CONFIG{$key};
			$BKP_DATACELLS{$key}->configure( -bg=>"white" )
				unless $BKP_CELL_TYPE{$key} eq "CK";
			$BKP_DATACELLS{$key}->configure( -selectcolor=>"white" ) if $BKP_CELL_TYPE{$key} eq "CK";
			$BKP_ISDEFAULT_TEXT{$key}="";	#  if $BKP_CELL_TYPE{$key} eq "CK";
		} else {
			my($jobkey)=$next_backup_plan."_".$key;
			if( ! defined $CONFIG{$jobkey} ) {
				$BKP_ISDEFAULT_TEXT{$key}="[DEFAULT]";
				if( $BKP_CELL_TYPE{$key} eq "CK" ) {
					$BKP_DATAVALS{$key} = $CONFIG{$key};
					$BKP_DATACELLS{$key}->configure( -selectcolor=>$dflt_color );
				} else {
					if( $CONFIG{$key} ) {
						$BKP_DATAVALS{$key} = "<".$CONFIG{$key}.">";
					} else {
						$BKP_DATAVALS{$key} = "<>";
					}
					$BKP_DATACELLS{$key}->configure( -bg=>$dflt_color );
				}

			} else {
				$BKP_DATAVALS{$key} = $CONFIG{$jobkey};
				mydie( "Woah... cell $key is not yet painted!!!\n" )
					unless defined $BKP_DATACELLS{$key};
				$BKP_DATACELLS{$key}->configure( -bg=>"white" ) unless $BKP_CELL_TYPE{$key} eq "CK";
				$BKP_DATACELLS{$key}->configure( -selectcolor=>"white" ) if $BKP_CELL_TYPE{$key} eq "CK";
				$BKP_ISDEFAULT_TEXT{$key}="[NOT DEFAULT]";# if $BKP_CELL_TYPE{$key} eq "CK";
			}
		}
		$BKP_ISDEFAULT_SAVE{$key}=$BKP_ISDEFAULT_TEXT{$key};
	}
	
	$cur_backup_plan=$next_backup_plan;

	#_debugmsg("[func] done plantab_compressitems_button()\n" );
	#plantab_compressitems_button();
   	#_debugmsg("[func] done plantab_logship_button()\n" );
	#plantab_logship_button();
	plantab_hilight();
	_debugmsg("[func] done update_planinfo()\n" );

}

sub read_backup_layout_data {
	# READ DATA SECTION OF THE PROGRAM
	my($key,$tab,$len,$text);
	while (<DATA>) {
		next if /^\s*$/ or /^#/;
		chomp;
		s/\s+$//;
		if( /^\s/ ) {
			s/^\s+//;
			$bt_TabHelp_DATA{$key}.="\n   ".$_;
			next;
		}
		($key,$tab,$len,$text)=split(/\s+/,$_,4);
		push @bt_KeyList_DATA,$key unless defined $bt_TabByKey_DATA{$key};
		$bt_TabByKey_DATA{$key}=$tab;
		$bt_ElementLength_DATA{$key}=$len;
		$bt_TabHelp_DATA{$key}=$text;
		push @BackupTabList_DATA,$tab unless defined $bt_TabHash_DATA{$tab};
		$bt_TabHash_DATA{$tab}=	1;		# doesnt matter what you set this to
	}
}

sub dohelp {
	my($widget,$help)=@_;

	if( ! defined $XMLDATA{mainWindow} ) {
		foreach (keys %XMLDATA) {
			print "$_ -> $XMLDATA{$_}\n";
		}
		print __FILE__," ",__LINE__," ",\%XMLDATA,"\n";
		mydie("mainWindow undefined\n");
	}
	if( ! defined $widget or ! Tk::Exists($widget) ) {
		confess("Fatal error in dohelp($help) at ",__FILE__," ",__LINE__,"\n");
		print __FILE__," ",__LINE__," ",\%XMLDATA,"\n";
		mydie("widget $widget not a tk widget \n");
	}
	$balloon = $XMLDATA{mainWindow}->Balloon()	#-statusbar=>$statusmsgbar)
		unless defined $balloon;

	$balloon->attach($widget,-msg=>$help);

	my(@txt);

#	foreach ( split(/\n/,$help) ) {
#		if( length > 100 ) {
#			my($msg) = reword($_,50, 90);
#			foreach ( split(/\n/,$msg) ) { push @txt,$_; }
#		} else {
#			push @txt, $_;
#		}
#	}

	if( $#txt >= $main::NUM_STATUS_LINES ){
		$help="";
		for my $i (0..$main::NUM_STATUS_LINES){
			$help.=$txt[$i]."\n";
		}
		chomp $help;
	}

	#if( ! $msg ) {
    	#	$msg = $_statusmsg_idle;
    	#} else {
    	#	$msg = reword($msg,50, 90);
    	#}

	$widget->bind('<Enter>',[\&set_status_label, $help]);
	$widget->bind('<Leave>', [\&set_status_label, ""]);

	return $widget;
}

my($busy_count)=0;

sub is_busy
{
	_statusmsg("[func] skipping button click as application is busy\n" ) if $busy_count;
	_debugmsg("[func] DBG DBG is_busy() - count is $busy_count\n" );	
	return $busy_count;
}
sub busy
{
	$busy_count++;
	_debugmsg("[func] DBG DBG busy() - count is now $busy_count\n" );	
	$XMLDATA{mainWindow}->configure(-cursor=>'watch');
}

sub unbusy
{
	my($clear)=@_;
	$busy_count-- if $busy_count>0;
	$busy_count=0 if $clear;
	_debugmsg("[func] DBG DBG unbusy() - count is now $busy_count\n" );	
	$XMLDATA{mainWindow}->configure(-cursor=>'arrow') if $busy_count==0;
}

sub set_status_label {
	my($widget,$txt)=@_;
	chomp $txt;
	$statusmsgbar->configure(-text=>$txt);

#	if( $txt =~ /^\s*$/ ) {
#		$cnt=0;
#		while( $cnt++ < $NUM_STATUS_LINES ) { $txt.=" \n"; }
#		$statusmsgbar->configure(-text=>$txt);
#		return;
#	}
#	my(@lines)=split(/\n/,$txt);
#	my($cnt)=$#lines;
#	$cnt=0 if $cnt<0;
#	print "Setting Status Label: $#lines , ",($NUM_STATUS_LINES-$cnt),", $txt\n";
#	while( $cnt++ < $NUM_STATUS_LINES ) { $txt.="\n"; }
}

sub gemrep_allowedkeys {
	my($vartype)=@_;
	my(@rc);
	push @rc, "SERVER_TYPE";								
	if( $vartype eq "sybase" ) {
		push @rc, "TYPE";	
		push @rc, "backupservername";
		push @rc, "hostname";
		push @rc, "portnumber";
	} elsif( $vartype eq "sqlsvr" ) {
		push @rc, "hostname";
		push @rc, "portnumber";
	} elsif( $vartype eq "unix" ) {
		push @rc, "SYBASE";
		push @rc, "SYBASE_CLIENT";
		push @rc, "IGNORE_RUNFILE";
		push @rc, "UNIX_COMM_METHOD";
		push @rc, "WIN32_COMM_METHOD";
		push @rc, "DF";
		push @rc, "DFCOL";
	}	
	return @rc;
}

sub get_type_by_servername {
	my($server)=shift;
	foreach ( get_server_types() ) {		
		return $_ if defined $server_info{$server.":".$_};	
	}
	return undef;
}

sub gemrep_initialize {
	my(@svr_list)				=	get_server_info('sybase');
	my(@host_list)				=	get_server_info('unix');
	my(@win32servers_list)	=	get_server_info('win32servers');
	my(@sqlsvr_list)			=	get_server_info('sqlsvr');
	my(@oracle_list)			=	get_server_info('oracle');

	$XMLDATA{sybase_server_list_ptr}	= \@svr_list;
	$XMLDATA{host_list_ptr}		= \@host_list;
	$XMLDATA{windows_server_list_ptr}	= \@win32servers_list;
	$XMLDATA{sqlsvr_list_ptr}	= \@sqlsvr_list;
	$XMLDATA{oracle_list_ptr}	= \@oracle_list;
	
	# correct win32 hostnames
	foreach (@sqlsvr_list) {
		my $hostname = $server_info{$_.":sqlsvr"}{hostname};
		next if $server_info{$hostname.":win32servers"};
		if( $server_info{lc($hostname).":win32servers"} ) {
			_statusmsg("[func] Correcting Host for ",$_," from $hostname => ".lc($hostname)."\n");
			$server_info{$_.":sqlsvr"}{hostname}=lc($hostname);
			next;
		}
		if( $server_info{uc($hostname).":win32servers"} ) {
			_statusmsg("[func] Correcting Host for ",$_," from $hostname => ".uc($hostname)."\n");
			$server_info{$_.":sqlsvr"}{hostname}=uc($hostname);
			next;
		}
		_statusmsg("[func] Warning : Host for Server ",$_," is $hostname (not registered)\n");
	}	
	
	# part2
	# THE FOLLOWING CHECK MUST BE DONE FOR BACKWARDS COMPATIBILITY...
	#  - this corrects gem.xml values by adding values form the password files.
	my($filelist_ref)=global_data(-class=>"fileinfo", -keys=>1, -noprint=>1);
	my(%files_found);
	if( defined $filelist_ref ) { foreach (@$filelist_ref) {$files_found{$_}=1; }}
	foreach my $hostname ( get_server_by_type(-type=>"unix") ) {
		my( $dathash ) = $server_info{$hostname.":unix"};
		if( ! defined $dathash ) {
			print "WARNING: Unix Server $hostname of type unix Has been Deleted\n";
			print LOGFP "WARNING: Unix Server $hostname of type unix Has been Deleted\n" 
				if ! defined $NOLOGFILE;
			next;
		}
		my( %dat ) = %$dathash;
		if( defined $dat{SYBASE} ) {
			foreach( split(/\s+/,$dat{SYBASE} )) {
				next if $files_found{$hostname.":".$_};
				print "Correcting xml data for file $hostname:$_\n";
				print LOGFP "Correcting xml data for file $hostname:$_\n" 
					if defined $NOLOGFILE;
				my(%fileinfo);
				$fileinfo{filepath}=		$_;
				$fileinfo{filepath_fromlocal}=	"";
				$fileinfo{filetype}=		"Sybase Server \$SYBASE Directory";
				$fileinfo{is_local}=0;
				$fileinfo{frequency}=		"Hourly";
				$fileinfo{hostname}=		$hostname;
				$fileinfo{hosttype}=		"unix";
				global_data(-class=>'fileinfo',-name=>$hostname.":".$_,-value=>\%fileinfo);
			}
		}

		if( defined $dat{IGNORE_RUNFILE} ) {
			foreach( split(/\s+/,$dat{IGNORE_RUNFILE} )) {
				next if $files_found{$hostname.":".$_};
				print "Correcting xml data for file $hostname:$_\n";
				print LOGFP "Correcting xml data for file $hostname:$_\n" 
					if defined $NOLOGFILE;
				my(%fileinfo);
				$fileinfo{filepath}=		$_;
				$fileinfo{filepath_fromlocal}=	"";
				$fileinfo{filetype}=		"Ignore This RUN_\* File";
				$fileinfo{is_local}=0;
				$fileinfo{frequency}=		"Hourly";
				$fileinfo{hostname}=		$hostname;
				$fileinfo{hosttype}=		"unix";
				global_data(-class=>'fileinfo',-name=>$hostname.":".$_,-value=>\%fileinfo);
			}
		}

		if( defined $dat{SYBASE_CLIENT} ) {
			foreach( split(/\s+/,$dat{SYBASE_CLIENT} )) {
				next if $files_found{$hostname.":".$_};
				print "Correcting xml data for file $hostname:$_\n";
				print LOGFP "Correcting xml data for file $hostname:$_\n" 
					if defined $NOLOGFILE;
				my(%fileinfo);
				$fileinfo{filepath}=		$_;
				$fileinfo{filepath_fromlocal}=	"";
				$fileinfo{filetype}=		"Sybase Open Client \$SYBASE Directory";
				$fileinfo{is_local}=0;
				$fileinfo{frequency}=		"Hourly";
				$fileinfo{hostname}=		$hostname;
				$fileinfo{hosttype}=		"unix";
				global_data(-class=>'fileinfo',-name=>$hostname.":".$_,-value=>\%fileinfo);
			}
		}
	}
}

sub read_all_configfiles {
	my($readconfig,$debug)=@_;
	_statusmsg("[func] Reading Configuration Files - $readconfig\n" );

	my($AM_I_INITIALIZED)=copy_sample_files() if $readconfig eq "FULL";
	print "... Reading xml data\n" if defined $debug;
	print LOGFP "... Reading xml data\n" 
		if defined $debug and ! defined $NOLOGFILE;
	_statusmsg("[func] Reading XML Data - gem.xml\n" );
	read_gem_xml();
	_statusmsg("[func] Parsing Key Values from xml\n" );

	$XMLDATA{admin_name}			= global_data(-class=>'administrator',-name=>'name');
	$XMLDATA{admin_company}			= global_data(-class=>'administrator',-name=>'company');
	$XMLDATA{admin_email}			= global_data(-class=>'administrator',-name=>'email');
	$XMLDATA{phone_number}			= global_data(-class=>'administrator',-name=>'phone');

	if( $readconfig ne "FULL" and global_data(-class=>'install',-name=>'current_hostname') ne hostname() ) {
		$XMLDATA{current_hostname}	= global_data(-class=>'install',-name=>'current_hostname');
		$XMLDATA{admin_scripts_dir}	= global_data(-class=>'install',-name=>'admin_scripts_dir');
		$XMLDATA{VERSION} 		= global_data(-class=>'version',-name=>'VERSION');
		$XMLDATA{sht_version} 		= global_data(-class=>'version',-name=>'sht_version');
		$XMLDATA{isnt}			= global_data(-class=>'install',-name=>'isnt');
	} else {
		$XMLDATA{current_hostname}			=	hostname();
		$XMLDATA{admin_scripts_dir}			=	$XMLDATA{gem_root_directory}."/ADMIN_SCRIPTS";
		($XMLDATA{VERSION},$XMLDATA{sht_version})	=	get_version();
		$XMLDATA{isnt}  				= 	is_nt();
	}

	$XMLDATA{installtype}		= global_data(-class=>'install',-name=>'installtype');
	$XMLDATA{win32tounix}		= global_data(-class=>'install',-name=>'win32tounix');
	$XMLDATA{unixtounix}			= global_data(-class=>'install',-name=>'unixtounix');

	$XMLDATA{use_sybase_ase}	= global_data(-class=>'install',-name=>'use_sybase_ase') || "Y";
	$XMLDATA{use_windows_servers}	= global_data(-class=>'install',-name=>'use_windows_servers') || "Y";
	$XMLDATA{use_ms_sql_server}	= global_data(-class=>'install',-name=>'use_ms_sql_server') || "Y";
	$XMLDATA{use_sybase_rep}	= global_data(-class=>'install',-name=>'use_sybase_rep') || "Y";
	$XMLDATA{use_oracle}		= global_data(-class=>'install',-name=>'use_oracle') || "Y";
	$XMLDATA{use_sybase_iq}		= global_data(-class=>'install',-name=>'use_sybase_iq') || "Y";

	# initialize this because its needed for one of the tabs
   	if( ! defined $XMLDATA{installtype} ) {
   		if( $XMLDATA{isnt} ) {
	   		$XMLDATA{installtype}="WINDOWS"
   		} else {
	   		$XMLDATA{installtype}="UNIX"
   		}
   	}

   	$XMLDATA{win32tounix}="FTP" unless defined $XMLDATA{win32tounix};
   	$XMLDATA{unixtounix} ="SSH" unless defined $XMLDATA{unixtounix};

	$XMLDATA{is_mail_ok}		= global_data(-class=>'install',-name=>'is_mail_ok');
	$XMLDATA{documenter_do_copy}	= global_data(-class=>'plugin:documenter',-name=>'documenter_do_copy');
	$XMLDATA{copy_using_ftp}	= global_data(-class=>'plugin:documenter',-name=>'copy_using_ftp');
	$XMLDATA{documenter_do_copy} = "N" unless defined $XMLDATA{documenter_do_copy};
	$XMLDATA{copy_using_ftp} = "N" unless defined $XMLDATA{copy_using_ftp};

	gemrep_initialize();
	
	print "... Reading configfile\n" if defined $debug;
	print LOGFP "... Reading configfile\n" unless defined $NOLOGFILE;
	%CONFIG=read_configfile(-nodie=>1, -debug=>$debug);

	print "... Done reading configfile\n" if defined $debug;
	print LOGFP "... Done reading configfile\n" unless defined $NOLOGFILE;
	if( defined $CONFIG{ERRORS} and $AM_I_INITIALIZED ne "TRUE" and $XMLDATA{program_name} ne "setup.pl" ) {
		print "*******************************************************\n";
		print "*\n";
		print "* THE FOLLOWING CONFIGURATION ERRORS WERE FOUND        \n";
		my(@errs) = @{$CONFIG{ERRORS}};
		foreach( @errs ){
			chomp;
			print "* ",join("\n* ",split(/\n/,$_)),"\n* \n";
			print LOGFP "* ",join("\n* ",split(/\n/,$_)),"\n* \n" unless defined $NOLOGFILE;
		}
		print "*******************************************************\n";
		#print "<HIT ENTER KEY TO CONTINUE>";
		#<STDIN>;
	}
	# set it to ...dat/BACKUP_LOGS
	$CONFIG{BASE_BACKUP_DIR} = $XMLDATA{gem_root_directory}."/data/BACKUP_LOGS"
		unless defined $CONFIG{BASE_BACKUP_DIR};

	print "... Reading read_backup_layout_data()\n" if defined $debug;
	read_backup_layout_data();

	print "... get_job_list()\n" if defined $debug;
	my(@Backup_Plan_Ary) = get_job_list(%CONFIG);
	plan_init();
	foreach ( @Backup_Plan_Ary ) {
		plan_add(name=>$_);
	}
	
	$XMLDATA{downarrow}=Tk->findINC('cbxarrow.xbm');
	foreach (@INC) {
		if( $_ !~ /$XMLDATA{gem_root_directory}/ ){			
				last;
		}		
		push @perl_includes, $_;
		print "  Adding Perl Include Lib : $_\n" if defined $DEBUG;
		print LOGFP "  Adding Perl Include Lib : $_\n" unless defined $NOLOGFILE;
	}
	if( defined $ENV{perlargs} ) {
		my($x)=$ENV{perlargs};
		$x=~s/^\s+//;
		$x=~s/\s+$//;
		$x=~s/^-I//;
		push @perl_includes, $x;
	}
	
	# part2
	#my(@file_hashptr)=global_data(-class=>'fileinfo',-noprint=>1,-keys=>1);
	#print "File List: >",join(" ",@file_hashptr),"\n";
	#print Dumper \@file_hashptr;
	#print "Setting One XXX\n";

#	my(%fileinfo);
#	$fileinfo{filepath}=		"A";
#	$fileinfo{filepath_fromlocal}=	"B";
#	global_data(-class=>'fileinfo',-name=>"test1",-value=>\%fileinfo);

#	print "YYY\n";
#	my($ref)=global_data(-class=>"fileinfo", -noprint=>1);
#	print Dumper $ref;

#	my(%fileinfo2);
#	$fileinfo2{filepath}=		"A";
#	$fileinfo2{filepath_fromlocal}=	"B";
#	global_data(-class=>'fileinfo',-name=>"test2",-value=>\%fileinfo2);
#
#	my(@file_hashptr)=global_data(-class=>'fileinfo',-noprint=>1,-keys=>1);
#	print "File List: >",join(" ",@file_hashptr),"\n";
#	print Dumper \@file_hashptr;
#	print "Setting One XXX\n";

#	print "YYY\n";
#	my($ref)=global_data(-class=>"fileinfo");
#	print Dumper $ref;

	print "completed reading files\n" if defined $debug;
	print LOGFP "completed reading files\n" if defined $debug and ! defined $NOLOGFILE;	
}

sub get_server_by_type {
	my(%args)=@_;
	Carp::confess "Must pass -type to get_server_by_type()\n" unless defined $args{-type};
	my($ptr)=get_svrlist_ptr($args{-type});
	mydie("Unknown type $args{-type} passed to get_server_by_type()\n") unless defined $ptr;
	return @$ptr unless $args{-sorted};
	return sort @$ptr
}

sub get_svrlist_ptr {
	my($type)=@_;
	return $XMLDATA{host_list_ptr} 				if $type eq "unix";
	return $XMLDATA{windows_server_list_ptr} 	if $type eq "win32servers";
	return $XMLDATA{sybase_server_list_ptr} 	if $type eq "sybase";
	return $XMLDATA{sqlsvr_list_ptr} 			if $type eq "sqlsvr";
	return $XMLDATA{oracle_list_ptr} 			if $type eq "oracle";
	print "ERROR : INVALID TYPE ($type) PASSED TO get_svrlist_ptr()\n";
	mydie("ERROR : INVALID TYPE ($type) PASSED TO get_svrlist_ptr()\n");
	exit(0);
	return undef;
}

sub set_svrlist {
	my($type,$ptr)=@_;
	return $XMLDATA{host_list_ptr}=$ptr 		if $type eq "unix";
	return $XMLDATA{windows_server_list_ptr}=$ptr 	if $type eq "win32servers";
	return $XMLDATA{sybase_server_list_ptr}=$ptr 	if $type eq "sybase";
	return $XMLDATA{sqlsvr_list_ptr}=$ptr 		if $type eq "sqlsvr";
	return $XMLDATA{oracle_list_ptr}=$ptr 		if $type eq "oracle";
	mydie("ERROR : INVALID TYPE ($type) PASSED TO get_svrlist_ptr()\n");
	return undef;
}
	
sub update_begin_block {	# edit CommonHeader.pm	or DBIFunc.pm Begin Block For $SYBASE contents
	my($cfg_file)=@_;
	_statusmsg("[func] Saving $cfg_file\n" );
	open(ICFG1,     $cfg_file ) 			or return mywarn("Cant Open $cfg_file : $!");
	open(OCFG1, ">".$cfg_file.".tmp" ) 	or return mywarn("Cant Open $cfg_file.tmp");
	my($state)=0;
	while(<ICFG1>) {			
		chomp;
		s/\s+$//;
		$_ .= "\n";
		if( $state==2 ) {
			# WE ARE DONE MODDING BLOCK... PRINT EVERYTHING
			print OCFG1 $_;
		} elsif( $state ) {
			# NOT DONE BUT ALSO NOT JUST STARTING
			if( /\s*# WARNING - THANK YOU FOR NOT REMOVING THIS EITHER/ ) {
				$state=2;
				print OCFG1 $_;
			}
			next;
		} elsif( /\s*# WARNING - DO NOT REMOVE THIS COMMENT/ ) {
			# NOT DONE AND STARTING AND WE FOUND OUR START PATERN
			$state=1;
			print OCFG1 $_,"\t#******************\n";
			for my $i (1..9) {	
				next unless $CONFIG{"SYBASE".$i};
				$CONFIG{"SYBASE".$i} =~ s/\s+$//;
				if( defined $CONFIG{"SYBASE".$i} and $CONFIG{"SYBASE".$i} =~ /\// ) {
						print OCFG1 "\t\t} elsif( -d \"".$CONFIG{"SYBASE".$i}."\" ) {\n\t\t\t\$ENV{SYBASE}=\"".$CONFIG{"SYBASE".$i}."\";\n";
				}	
			}	
			print OCFG1 "\t#******************\n"
		}	else {
			# NOT DONE AND STARTING AND NO START PATERN YET
				print OCFG1 $_;
		}	
	}
	close(ICFG1);
	close(OCFG1);
	if( compare($cfg_file.".tmp",$cfg_file) != 0 ) {
		_statusmsg("[func] Saving $cfg_file\n" );
		archive_file($cfg_file,2);
		rename($cfg_file.".tmp",$cfg_file)
			or return mywarn("Cant rename $cfg_file.tmp to $cfg_file : $!");
	} else {
		_statusmsg( "[func] $cfg_file Unchanged (not saving)\n" );
		print LOGFP "Identical Files Found\n" unless defined $NOLOGFILE;
		unlink $cfg_file.".tmp";
	}
	chmod( 0666, $cfg_file ) or warn "Cant Chmod $cfg_file $!\n";
}

sub save_all_configfiles {
	my($noshow)=@_;
	if( ! defined $NOLOGFILE ) {
		print LOGFP __LINE__," Dumper \%CONFIG run by save_all_configfiles\n";
		print LOGFP Dumper \%CONFIG ;
	}	
	
	$CONFIG{INSTALLTYPE} = $XMLDATA{installtype};
	
	$CONFIG{NT_PERL_LOCATION}		=~ s.\\.\/.g;
	$CONFIG{NT_CODE_LOCATION}		=~ s.\\.\/.g;
	$CONFIG{UNIX_PERL_LOCATION}	=~ s.\\.\/.g;
	$CONFIG{UNIX_CODE_LOCATION} 	=~ s.\\.\/.g;
	$CONFIG{CODE_LOCATION_ALT1} 	=~ s.\\.\/.g;
	$CONFIG{CODE_LOCATION_ALT2} 	=~ s.\\.\/.g;
	$CONFIG{BASE_BACKUP_DIR} 		=~ s.\\.\/.g;
	
	for my $i (1..9) {
		next unless $CONFIG{"SYBASE".$i};
		$CONFIG{"SYBASE".$i} 		=~ s/\s+$//;
		$CONFIG{"SYBASE".$i}			=~ s.\\.\/.g;
	}	

	_statusmsg("[save] Saving All Configfiles That Have Changed\n" );
	global_data(-class=>'administrator',-name=>'name',		-value=>$XMLDATA{admin_name});
	global_data(-class=>'administrator',-name=>'company',	-value=>$XMLDATA{admin_company});
	global_data(-class=>'administrator',-name=>'email',	-value=>$XMLDATA{admin_email});
	global_data(-class=>'administrator',-name=>'phone',	-value=>$XMLDATA{phone_number});

	#global_data(-class=>'mail',-name=>'SMTPMAILSERVER',-value=>$XMLDATA{SMTPMAILSERVER});
	global_data(-class=>'version',-name=>'VERSION',-value=>$XMLDATA{VERSION});
	global_data(-class=>'version',-name=>'sht_version',-value=>$XMLDATA{sht_version});
	my($build)=$XMLDATA{sht_version};
	$build=~s/\D+//g;
	global_data(-class=>'version',-name=>'Build',-value=>$build);
	my($datestr)=$XMLDATA{VERSION};
	$datestr=~s/^.+\(//;
	$datestr=~s/\)\s*$//;
	global_data(-class=>'version',-name=>'Date',-value=>$datestr);

	global_data(-class=>'install',-name=>'cf_dir',-value=>$XMLDATA{gem_configuration_directory});
	global_data(-class=>'install',-name=>'root_dir',-value=>$XMLDATA{gem_root_directory});
	global_data(-class=>'install',-name=>'admin_scripts_dir',-value=>$XMLDATA{admin_scripts_dir});
	global_data(-class=>'install',-name=>'isnt',-value=>$XMLDATA{isnt});
	global_data(-class=>'install',-name=>'installtype',-value=>$XMLDATA{installtype});
	global_data(-class=>'install',-name=>'win32tounix',-value=>$XMLDATA{win32tounix});
	global_data(-class=>'install',-name=>'unixtounix',-value=>$XMLDATA{unixtounix});

	global_data(-class=>'install',-name=>'use_sybase_ase', -value=>$XMLDATA{use_sybase_ase} );
	global_data(-class=>'install',-name=>'use_windows_servers', -value=>$XMLDATA{use_windows_servers} );

	global_data(-class=>'install',-name=>'use_ms_sql_server', -value=>$XMLDATA{use_ms_sql_server} );
	global_data(-class=>'install',-name=>'use_sybase_rep', -value=>$XMLDATA{use_sybase_rep} );
	global_data(-class=>'install',-name=>'use_oracle', -value=>$XMLDATA{use_oracle} );
	global_data(-class=>'install',-name=>'use_sybase_iq', -value=>$XMLDATA{use_sybase_iq} );

	#global_data(-class=>'install',-name=>'ENABLE_SQLSERVER',-value=>$XMLDATA{ENABLE_SQLSERVER});
	#global_data(-class=>'install',-name=>'ENABLE_SYBASE',-value=>$XMLDATA{ENABLE_SYBASE});
	#global_data(-class=>'install',-name=>'ENABLE_ORACLE',-value=>$XMLDATA{ENABLE_ORACLE});
	global_data(-class=>'install',-name=>'is_mail_ok', -value=>$XMLDATA{is_mail_ok});

	global_data(-class=>'install',-name=>'current_hostname',-value=>$XMLDATA{current_hostname});

	global_data(-class=>'plugin:documenter',-name=>'documenter_do_copy',	-value=>$XMLDATA{documenter_do_copy});
	global_data(-class=>'plugin:documenter',-name=>'copy_using_ftp',	-value=>$XMLDATA{copy_using_ftp});

	print LOGFP __LINE__," CONFIG at save_all_configfiles\n" unless defined $NOLOGFILE;
	print LOGFP Dumper \%CONFIG unless defined $NOLOGFILE;

	update_planinfo();

	if( ! defined $NOLOGFILE ) {
		print LOGFP "bt_KeyList_DATA\n";
		print LOGFP Dumper \@bt_KeyList_DATA;
		print LOGFP  "bt_TabByKey_DATA\n";
		print LOGFP Dumper \%bt_TabByKey_DATA;
		print LOGFP "DeletedPlans\n";
		print LOGFP Dumper \%DeletedPlans;
		print LOGFP "BackupPlanNames\n";
		print LOGFP Dumper \%BackupPlanNames;
		print LOGFP "bt_TabHash_DATA\n";
		print LOGFP Dumper \%bt_TabHash_DATA;
		print LOGFP "CONFIG\n";
		print LOGFP Dumper \%CONFIG;
	}

	write_XMLDATA();	

# A BUTTON TO DO THIS	
#	$graphics_object{FILEMOD_BUTTON}=$bframe->Button(
#		-width=>25,
#		-text=>'Modify Sybase Paths',
#		-command=> sub {
#			return if is_busy();
#			busy();
#			update_begin_block("$XMLDATA{gem_root_directory}/lib/CommonHeader.pm");
#			update_begin_block("$XMLDATA{gem_root_directory}/lib/DBIFunc.pm");
#			unbusy();
#		},
#		-bg=>$color )->pack( -side=>"right", -pady=>2, -padx=>2);
#	dohelp($graphics_object{FILEMOD_BUTTON},
#		"By clicking here you will test Mime::Lite by sending mail to yourself $XMLDATA{admin_email}.");

	update_begin_block("$XMLDATA{gem_root_directory}/lib/CommonHeader.pm");
	update_begin_block("$XMLDATA{gem_root_directory}/lib/DBIFunc.pm");

	# save_config_dat
	my($cfg_file)="$XMLDATA{gem_configuration_directory}/configure.cfg";

	# delay a bit if needed
	my($count)=0;
	while( $count++ < 5 and -e $cfg_file.".tmp" ) {
		print "waiting on $cfg_file write\n";
		print LOGFP "waiting on $cfg_file write\n" unless defined $NOLOGFILE;
		sleep 1;
	}

	open(ICFG98,     $cfg_file ) 		or return mywarn("Cant Open $cfg_file : $!");
	open(OCFG98, ">".$cfg_file.".tmp" ) 	or return mywarn("Cant Open $cfg_file.tmp");
	my(%backup_key_was_printed);
	foreach (<ICFG98>) {
		# print comments and blank lines from the sample file verbatim
		if( /^\s*\#/ or /^\s*$/ or ! /=/ ) {
			if( /END CONFIGURATION OF BACKUP SCRIPTS/ ) {
				# print any missing variables
				foreach ( @bt_KeyList_DATA ) {
					next if $backup_key_was_printed{$_} or $_ eq "SYBASE";
					print OCFG98 "$_=$CONFIG{$_}\n" if defined $CONFIG{$_};
					my($var)=$_;
					foreach ( plan_get() ) {
						my($x)=$_."_".$var;
						next unless defined $CONFIG{$x};
						print OCFG98 "$x=$CONFIG{$x}\n";
					}
				}
			}
			print OCFG98 $_;

		# if its a variable
		} else {
			my($var,$val)=split("=",$_,2);
			$var=~s/^\s+//;
			$var=~s/\s+$//;
			$val=~s/^\s+//;
 			$val=~s/\s+$//;
			my($found)=0;
			foreach ( plan_get() ) {
				if( $var =~ /^$_\_/ ) {
					my($x)=$var;
					$x=~s/^$_//;
					$x=~s/\_//;
					#print "DBG: Skipping $var=$val as $x is plan var\n" if defined $bt_TabByKey_DATA{$x};
					if( defined $bt_TabByKey_DATA{$x} ) {
						#_debugmsg("[save] Skipping $var=$val as $x is plan var\n" );
						$found++;
					}
				}
			}

			foreach ( keys %DeletedPlans ) {
				if( $var =~ /^$_/ ) {
					my($x)=$var;
					$x=~s/^$_//;
					$x=~s/\_//;
					#print  "DBG: Skipping $_ as plan was deleted\n"  if defined $bt_TabByKey_DATA{$x};
					_debugmsg("[save] Skipping $_ as plan was deleted\n" )
						 if defined $bt_TabByKey_DATA{$x};
					$found++ if defined $bt_TabByKey_DATA{$x};
				}
			}
#print "Skipping it was found\n" if $found;
			next if $found;

			# Foreach Backup Variable, Print it and all plan specific overrides
			if( defined $bt_TabByKey_DATA{$var} ) {
				if( $CONFIG{$var} ) {
					print OCFG98 "$var=$CONFIG{$var}\n";
				} else {
					print OCFG98 "$var=\n";
				}
				foreach ( plan_get() ) {
					my($x)=$_."_".$var;
					next unless defined $CONFIG{$x};
					#_debugmsg("[save] saving $x=$CONFIG{$x}\n" );
					print OCFG98 "$x=$CONFIG{$x}\n";
				}
				$backup_key_was_printed{$var}=1
			} else {
				if( $CONFIG{$var} ) {
					print OCFG98 "$var=$CONFIG{$var}\n";
				} else {
					print OCFG98 "$var=\n";
				}
			}
		};
	}
	close(ICFG98);
	close(OCFG98);

	if( compare($cfg_file.".tmp",$cfg_file) != 0 ) {
		_statusmsg("[save] Saving $cfg_file\n" );
		archive_file($cfg_file,10);
		rename($cfg_file.".tmp",$cfg_file)
			or return mywarn("Cant rename $cfg_file.tmp to $cfg_file : $!");
	} else {
		_statusmsg( "[save] Identical $cfg_file Files Found\n" );
		print LOGFP "Identical Files Found\n" unless defined $NOLOGFILE;
		unlink $cfg_file.".tmp";
	}

	_statusmsg( "[save] Saving sybase_passwords..dat\n" );
	save_a_config_file("$XMLDATA{gem_configuration_directory}/sybase_passwords.dat",
		"sybase",
		get_server_by_type(-type=>"sybase"));
	_statusmsg( "[save] Saving unix_passwords.dat\n" );
	save_a_config_file("$XMLDATA{gem_configuration_directory}/unix_passwords.dat",
		"unix",
		get_server_by_type(-type=>"unix"));
	_statusmsg( "[save] Saving pc_password.dat\n" );
	save_a_config_file("$XMLDATA{gem_configuration_directory}/pc_password.dat",
		"win32servers",
		get_server_by_type(-type=>"win32servers"));
	_statusmsg( "[save] Saving sqlsvr_password.dat\n" );
	save_a_config_file("$XMLDATA{gem_configuration_directory}/sqlsvr_password.dat",
		"sqlsvr",
		get_server_by_type(-type=>"sqlsvr"));
	_statusmsg( "[save] Saving oracle_password.dat\n" );
	save_a_config_file("$XMLDATA{gem_configuration_directory}/oracle_password.dat",
		"oracle",
		get_server_by_type(-type=>"oracle"));

	purge_config_dir();
	_statusmsg("[save] Displaying message box - select a button to continue\n");
	$XMLDATA{mainWindow}->messageBox( -title => 'Done Configuration',
				-message => 'Configuration values saved.',
				-type => "OK" ) unless $UPGRADE or $noshow;
				
	if( ! defined $NOLOGFILE ) {
		print LOGFP "FILE: ",__FILE__," LINE: ",__LINE__," Dumper \%CONFIG run by read_all_configfiles\n";
		print LOGFP Dumper \%CONFIG ;
	}
	_statusmsg( "[save] Configuration Values Saved\n" );
}

sub save_a_config_file {
	#print "DBG: save_a_config_file(",join(" ",@_),")\n";
	my($file, $typ, @array )=@_;
	#print "DBG: saving config file $file\n";
	my($str)="";

	my($count)=0;
	while( $count++ < 5 and -e $file.".tmp" ) { print "waiting on $file write\n"; sleep 1; }

	if( -r $file.".sample" ) {
		open(F97,$file.".sample") or return mywarn("Cant read $file.sample\n");
		while(<F97>) {
			next unless /^#/;
			chop; $str.=$_."\n";
		}
		close(F97);
	}

	foreach ( sort @array ) {
		my( $dathash ) = $server_info{$_.":$typ"};
		if( ! defined $dathash ) {
			print "WARNING: Server $_ of type $typ Has been Deleted\n";
			next;
		}
		my( %dat ) = %$dathash;
		if( $typ eq "win32servers" ) {
			$str.="$_ $dat{DISKS}\n";
		} else {
			$str.="$_ $dat{login} $dat{password}\n";
		}
		foreach ( keys %dat ) {
			last if $typ eq "win32servers";			
			$str.="\t$_=$dat{$_}\n" unless $_ eq "login" or $_ eq "password" or $_ eq "type";		
		}
	}

	open(O96,">".$file.".tmp") or return mywarn("Cant write $file.tmp : $!\n");
	print O96 $str;
	close(O96);

	if( compare($file.".tmp",$file) != 0 ) {
		_debugmsg("[save] Saving $file.tmp\n" );
		archive_file($file,10);
		rename($file.".tmp",$file) or return mywarn("Cant rename $file.tmp to $file : $!");
	} else {
		_debugmsg("[save] Skipping ".basename($file).": file unchanged\n" );
		unlink $file.".tmp";
	}
}

sub get_server_info {
	my($type)=@_;
	my(@s)=get_password(-type=>$type);
	foreach (@s) {
		my($l,$p,$c)=get_password(  -type=>$type, -name=>$_);
		my %dat = get_password_info(-type=>$type, -name=>$_);
		$dat{type}=$type;
		$dat{login}=$l;
		$dat{password}=$p;
		
		# fix up any missing hostnames / port numbers
		if( $type eq "sybase" ) {
			if( ! defined $dat{hostname} or ! defined $dat{portnumber} ) {
				($dat{hostname},$dat{portnumber})=$gem_data->get_server_interfaces($_);
				_debugmsg( "[serverInfo] set $_ hostname to $dat{hostname}\n" );
				_debugmsg( "[serverInfo] set $_ portnumber to $dat{portnumber}\n" );
			}
		}
		$server_info{$_.":".$type} = \%dat;
	}	
	return @s;
}

sub copy_sample_files {
	opendir(DIR,$XMLDATA{gem_configuration_directory})
		or mydie("copy_sample_files() Cant open directory $XMLDATA{gem_configuration_directory} for reading : $!\n");
	my(@dirlist)=grep(!/^\./,readdir(DIR));
	closedir(DIR);
	my(%conf_file);
	my($num_copies)=0;
	foreach( @dirlist ) { $conf_file{$_}=1;	}
	foreach( @dirlist ) {
		next unless /.sample$/;
		next if /crontab.sample/;
		my($f)=$_;
		$f =~ s/.sample$//;
		next if $conf_file{$f};
		_statusmsg( "[func] creating conf/$f from sample file\n" );
		print LOGFP "creating conf/$f from sample file\n"
			 unless defined $NOLOGFILE;
		$num_copies++;
		use File::Copy;
		copy("$XMLDATA{gem_configuration_directory}/$f.sample","$XMLDATA{gem_configuration_directory}/$f") or mydie("cant copy $f : $!");
	}

	return "TRUE" if $num_copies>0;
	return "FALSE";
}


my(@server_tab_objects , @server_tab_blank_objects);
sub ServerTab_addframe {
	my($rgtfrm,$lftfrmB,$de_servercatref, $htyperef)=@_;
	# data entry variables
	my($de_hostname);	# Server Name
	my($de_login);		# Login
	my($de_password);	# Password
	my($de_servertype);	# View

	$$de_servercatref="Unix Server";
	push @server_tab_objects,my_label( $lftfrmB, $de_servercatref, \&repaint_add_server_tab,
		undef, "yellow", "Unix Server", "NT Server","Sybase Server","Microsoft SQL", "Oracle Server" );
	dohelp(	$server_tab_objects[$#server_tab_objects],	"Select Server/System Type" );
	push @server_tab_objects,$lftfrmB->Label(-text=>"Server Name",-font=>'bold');
	push @server_tab_objects,$lftfrmB->Entry( -bg=>'yellow', -font=>"small", -textvariable=>\$de_hostname, -width=>20 );
	dohelp(	$server_tab_objects[$#server_tab_objects],	"Enter Server To Add" );

	push @server_tab_objects,$lftfrmB->Label(-text=>"Login",   -font=>'bold');
	push @server_tab_objects,$lftfrmB->Entry( -bg=>'yellow', -font=>"small", -textvariable=>\$de_login,    -width=>20 );
	dohelp(	$server_tab_objects[$#server_tab_objects],	"Enter Login To Add" );

	push @server_tab_objects,$lftfrmB->Label(-text=>"Password",-font=>'bold');
	push @server_tab_objects,$lftfrmB->Entry( -bg=>'yellow', -font=>"small",
		-show=>'*', -textvariable=>\$de_password, -width=>20 );
	dohelp(	$server_tab_objects[$#server_tab_objects],	"Enter Password To Add" );

	$de_servertype="DEVELOPMENT";
	push @server_tab_objects,$lftfrmB->Label(-text=>"View",-font=>'bold');
	push @server_tab_objects,my_label( $lftfrmB, \$de_servertype, undef,undef,"yellow",  "PRODUCTION","CRITICAL","DEVELOPMENT","QA","DR" );
	dohelp(	$server_tab_objects[$#server_tab_objects],	"Select Server/System View" );

	push @server_tab_objects,
		$lftfrmB->Button(  -bg=>'white', -font=>'small',-pady=>-2, -text=>"Add Server",-command=>sub {
			
		return if is_busy();
		_statusmsg("[func] Add Server Button Selected\n");
			
		my $typ;
		$typ="unix" 			if $$de_servercatref eq "Unix Server";
		$typ="win32servers" 	if $$de_servercatref eq "NT Server";
		$typ="sybase" 			if $$de_servercatref eq "Sybase Server";
		$typ="sqlsvr" 			if $$de_servercatref eq "Microsoft SQL";
		$typ="oracle" 			if $$de_servercatref eq "Oracle Server";
		
		Register_Server(	-name			=> $de_hostname,
								-type 		=>	$typ, 
								-is_prod		=> $de_servertype,
								-login		=> $de_login,
								-password	=>	$de_password	 );
								
		unbusy(1);			
		
#			_statusmsg( "[func] Add Server Button Pressed\n" );
#			my(%configopts);
#			if( $$de_servercatref eq "Unix Server" or $$de_servercatref eq "NT Server") {
#				$configopts{"Host Name"}	= $de_hostname;
#				$configopts{"Host Login"}	= $de_login;
#				$configopts{"Host Password"}	= $de_password;
#				if( $$de_servercatref eq "Unix Server" ) {
#					$configopts{"Server Type"} = "Unix or Linux Server";
#				} else {
#					$configopts{"Server Type"} = "Windows Server";
#				}
#			} else {
#				$configopts{"Server Name"}	= $de_hostname;
#				$configopts{"DB Admin Login"}	= $de_login;
#				$configopts{"DB Admin Password"}= $de_password;
#				if( $$de_servercatref eq "Sybase Server" ) {
#					$configopts{"Server Type"} = "Sybase ASE Server on Unix";
#				} elsif( $$de_servercatref eq "Microsoft SQL" ) {
#					$configopts{"Server Type"} = "Microsoft SQL Server";
#				} else {
#					$configopts{"Server Type"} = "Oracle Server on Unix";
#				}
#			}
#			$configopts{"Server Group"}=$de_servertype;
#
#			tk_add_a_server(%configopts);
#			_statusmsg( "[func] Completed tk_add_a_server()\n" );
#			
			$NEEDS_REFRESH{Connect}=1;
			$NEEDS_REFRESH{ProcLibInstall}=1;	
			$NEEDS_REFRESH{Files}=1;
			busy();
			ServerTab_draw( $rgtfrm,$$htyperef);
			filetab_create() if $$de_servercatref eq "Unix Server";
			unbusy();
			_statusmsg( "[func] Add Server Button Done\n" );
		});

	dohelp($server_tab_objects[$#server_tab_objects],"Add A New Server" );
	repaint_add_server_tab($lftfrmB,$$de_servercatref);
}

sub repaint_add_server_tab {
	my($object,$de_servercat)=@_;
	foreach my $obj (@server_tab_objects,@server_tab_blank_objects) {	$obj->gridForget(); }
	@server_tab_blank_objects=();
	return if $de_servercat eq "Unregistered";
	my($row)=0;
	foreach my $obj (@server_tab_objects) {
		if(  ($de_servercat eq "NT Server" and $row>=3 and $row<=6)) {
			push @server_tab_blank_objects,
				$object->Label(-text=>" ")->grid(-column=>1,-row=>$row,-sticky=>'ew');
		} else {
			$obj->grid(-column=>1,-row=>$row,-sticky=>'ew');
		}
		$row++;
	}
}

sub do_delete_server {
	my($nm,$typ)=@_;
	_statusmsg( "[func] Deleting Server $nm\n" );
	if( ! defined $server_info{$nm.":".$typ} ) {
		print "Warning - no hash found for $nm / $typ\n";
		print "Found Hashes Include",join("\n\t",keys %server_info),"\n";
		print LOGFP "Warning - no hash found for $nm / $typ\n" unless defined $NOLOGFILE;
		print LOGFP "Found Hashes Include",join("\n\t",keys %server_info),"\n" unless defined $NOLOGFILE;
	}
	delete $server_info{$nm.":".$typ};
	
	$NEEDS_REFRESH{Connect} 		= 1;
	$NEEDS_REFRESH{ProcLibInstall}= 1 if $typ eq "sqlsvr" or $typ eq "sybase";	

	# remove host from the array...
	my($hostaryptr)=get_svrlist_ptr($typ);
	my @hostary;
	foreach ( @$hostaryptr ) {
		push @hostary,$_ unless $_ eq $nm;
	}
	set_svrlist($typ,\@hostary);
	_statusmsg( "[func] Done Deleting Server $nm\n" );
	#print "DBG: $_ => ",Dumper \%server_info,"\n";
}

# add_a_server_to_gem
#    Category=Unix Server|NT Server|Microsoft SQL|Sybase Server|Oracle Server
#	Type = PRODUCTION|DEVELOPMENT|QA
sub add_a_server_to_gem {	
	my($de_servercat,$de_hostname,$de_login,$de_password,$de_servertype,
		$ignore_errors,$win32disks,$hostname,$portnumber,$unixmethod,$win32method)=@_;
			
	_statusmsg("[addserver] add_a_server_to_gem($de_servercat,$de_hostname,$de_login,$unixmethod,$win32method)\n");
	
	# add that server...
	if( $de_hostname =~ /^\s*$/ or $de_hostname=~ /\s/ ) {
		_statusmsg("[addserver] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
			-title => 'Invalid Server Name/Hostname',
			-message => 'Name can not contain white space.',
			-type => "OK" );
		_statusmsg("[addserver] GEM Ready / Continuing...\n");
		return undef;
	}

	$NEEDS_REFRESH{Connect}=1;
	$NEEDS_REFRESH{ProcLibInstall}=1;	

	my $type_to_add;
	my($ERROR)="";
	if( $de_servercat  eq "Unix Server" or $de_servercat eq "unix" ) {
		$type_to_add="unix";
		foreach (get_server_by_type(-type=>"unix")) { $ERROR="DUPLICATE UNIX HOST" if $_ eq $de_hostname; }
		$ERROR="Must Enter Login" 	if $de_login =~ /^\s*$/;
		$ERROR="Must Enter Password" 	if $de_password =~ /^\s*$/;
		if( $ERROR eq "" ) {
			#	unshift get_server_by_type(-type=>"unix"), $de_hostname;
			my($ptr)=get_svrlist_ptr('unix');
			unshift @$ptr, $de_hostname;
			my %dat;
			$dat{type}=$type_to_add;
			$dat{login}=$de_login;
			$dat{password}=$de_password;
			$dat{SERVER_TYPE}=$de_servertype;
			$dat{UNIX_COMM_METHOD} =	$unixmethod;	#  || $XMLDATA{unixtounix};
			$dat{WIN32_COMM_METHOD} =	$win32method;	#  || $XMLDATA{win32tounix};
			$server_info{$de_hostname.":".$type_to_add} = \%dat;			
		}
	} elsif( $de_servercat  eq "Microsoft SQL" or $de_servercat eq "sqlsvr" ) {
		$type_to_add="sqlsvr";
		foreach (get_server_by_type(-type=>"sqlsvr")) {
			$ERROR="DUPLICATE SQL SERVER $de_hostname Current Servers include".join("\n",sort get_server_by_type(-type=>"sqlsvr"))
				if $_ eq $de_hostname;
		}
		$ERROR="Must Enter Login" if $de_login =~ /^\s*$/;
		$ERROR="Must Enter Password" if $de_password =~ /^\s*$/;
		if( $ERROR eq "" ) {
			my($ptr)=get_svrlist_ptr('sqlsvr');
			unshift @$ptr, $de_hostname;

			#unshift get_server_by_type(-type=>"sqlsvr"), $de_hostname;

			my %dat;
			$dat{type}=$type_to_add;
			$dat{login}=$de_login;
			$dat{password}=$de_password;
			$dat{SERVER_TYPE}=$de_servertype;
			$dat{hostname}=$hostname;
			#C#$dat{conn_type}=$de_conntype;
			#print __LINE__," DBG: Setting server_info{$de_hostname $type_to_add}\n";
			$server_info{$de_hostname.":".$type_to_add} = \%dat;
			#ServerTab_draw( $rgtfrm,$type_to_add);
#			my($rc)= tk_add_a_server(
#				"Local Directory Name (from db server) " => 'C:\backups',
#				"[optional] Remote Directory Name (from anywhere)" => "\\\\".$de_hostname."\\c\$\\backups",
#				"Plan Name"=>$de_hostname,
#				"Server Name" =>$de_hostname );
#			$ERROR="Server Entry Cancelled" unless $rc;
		}

	} elsif( $de_servercat  eq "NT Server" or $de_servercat eq "win32servers" ) {
		$type_to_add="win32servers";

		foreach (get_server_by_type(-type=>"win32servers")) {
			$ERROR="DUPLICATE WIN32 SERVER"  if $_ eq $de_hostname;
		}
		if( $ERROR eq "" ) {
			my(@disks);
			if( is_nt() ) {
				# DETECT DISK DRIVES IF WINDOWS SERVER
				if( defined $win32disks ) {
					push @disks, @$win32disks;
				} else {
					_statusmsg("[addserver]  Checking Disk Drives On $de_hostname");
					@disks=get_win32_disks($de_hostname);
					_statusmsg("[addserver]  Found Disk Drives ",join(" ",@disks));
				}
			} else {
				_statusmsg("[addserver] Displaying message box - select a button to continue\n");
				$XMLDATA{mainWindow}->messageBox(
					-title => 'Warning',
					-message => 'Please Note: Win32 Servers should have their disk
drives registered but they cant be AutoDetected from UNIX/Linux.
Set drive shares (C$, D$...) by hand or rerun configure.pl on win32
to identify the disks (needed for win32 disk monitoring).',
					-type => "OK" );
				_statusmsg("[addserver] GEM Ready / Continuing...\n");

			}
			my($ptr)=get_svrlist_ptr('win32servers');
			unshift @$ptr, $de_hostname;

			#unshift @{$XMLDATA{windows_server_list_ptr}}, $de_hostname;
			my %dat;
			$dat{type}=$type_to_add;
			$dat{SERVER_TYPE}=$de_servertype;
			#C#$dat{conn_type}=$de_conntype;
			$dat{DISKS} = join(" ",@disks) if $#disks>=0;
			$server_info{$de_hostname.":".$type_to_add} = \%dat;
		}
	} elsif( $de_servercat  eq "Sybase Server" or $de_servercat eq "sybase" ) {
		$type_to_add="sybase";
		foreach (get_server_by_type(-type=>"sybase")) { $ERROR="DUPLICATE SYBASE"  if $_ eq $de_hostname; }
		$ERROR="Must Enter Login" if $de_login =~ /^\s*$/;
		$ERROR="Must Enter Password" if $de_password =~ /^\s*$/;
		if( $ERROR eq "" ) {
			#unshift get_server_by_type(-type=>"sybase"), $de_hostname;
			my($ptr)=get_svrlist_ptr('sybase');
			unshift @$ptr, $de_hostname;

			my %dat;
			$dat{type}=$type_to_add;
			$dat{login}=$de_login;
			$dat{password}=$de_password;
			$dat{SERVER_TYPE}=$de_servertype;
			$dat{hostname}=$hostname;
			$dat{portnumber}=$portnumber;			
			
			if( ! defined $dat{hostname} or ! defined $dat{portnumber} ) {
				($dat{hostname},$dat{portnumber})=$gem_data->get_server_interfaces($de_hostname);
				_debugmsg( "[serverInfo] set $de_hostname hostname to $dat{hostname}\n" );
				_debugmsg( "[serverInfo] set $de_hostname portnumber to $dat{portnumber}\n" );
			}		
		
			#C#$dat{conn_type}=$de_conntype;
			#print __LINE__," DBG: Setting server_info{$de_hostname $type_to_add}\n";
			$server_info{$de_hostname.":".$type_to_add} = \%dat;
#			my($rc)=tk_add_a_server(
#				"Plan Name"=>$de_hostname,
#				 "Server Name" =>$de_hostname,
#				 "Host Name"=>undef );
#
#			$ERROR="Server Entry Cancelled" unless $rc;
		}
	} elsif( $de_servercat  eq "Oracle Server" or $de_servercat eq "oracle" ) {
		$type_to_add="oracle";
		foreach (get_server_by_type(-type=>"oracle")) { $ERROR="DUPLICATE ORACLE"  if $_ eq $de_hostname; }
		$ERROR="Must Enter Login" if $de_login =~ /^\s*$/;
		$ERROR="Must Enter Password" if $de_password =~ /^\s*$/;
		if( $ERROR eq "" ) {
			#unshift get_server_by_type(-type=>"oracle"), $de_hostname;
			my($ptr)=get_svrlist_ptr('oracle');
			unshift @$ptr, $de_hostname;
			my %dat;
			$dat{type}=$type_to_add;
			$dat{login}=$de_login;
			$dat{password}=$de_password;
			$dat{SERVER_TYPE}=$de_servertype;
			#C#$dat{conn_type}=$de_conntype;
			#print __LINE__," DBG: Setting server_info{$de_hostname $type_to_add}\n";
			$server_info{$de_hostname.":".$type_to_add} = \%dat;
			#ServerTab_draw( $rgtfrm,$type_to_add);
		}
	} else {
		$ERROR="Code Line Error - Illegal Server Type $de_servercat - Contact Development\n";
	}

	if( $ERROR eq "" ) {
		_statusmsg("[addserver] Checking connection to $de_hostname (type=$type_to_add)");
		# pwd is a hack - a shortcut - only relevvant if type=unix... gets you the current directory.  We
		# can add this as a default $SYBASE i guess... works for a  start
		my($pwd);
		my($c,$errmsg)=$connectionmanager->connect(
				-type			=>$type_to_add, 
				-name			=>$de_hostname, 
				-returnpwd	=>\$pwd,
				-unixmethod	=>$unixmethod,
				-win32method=>$win32method);
				
		if( ! defined $c ) {
			_statusmsg("[addserver] Displaying message box - select a button to continue\n");
			$XMLDATA{mainWindow}->messageBox(
				-title => 		'Warning',
				-message => 	"Unable to connect to $de_hostname: $errmsg.",
				-type => 		"OK" );
			_statusmsg("[addserver]  $de_hostname : Not Connected: $errmsg\n" );			
		} else {		
			if( $type_to_add eq "unix" ) {
				# TEST OTHER POTENTIAL SYBASE DIRECTORIES!
				my(@testlist);			
				my(%found);	
				if( defined $pwd and $pwd !~ /^\s*$/ ) { 	
						# print "DBG DBG: ADDING cwd($pwd) to testlist\n";
						$pwd=~s/[\\\/]$//;
						push @testlist, $pwd; 
						$found{$pwd}=1;
				}
				for my $i (1..9) {					
					next unless defined $CONFIG{"SYBASE".$i} and $CONFIG{"SYBASE".$i} =~ /[\\\/]/;
					$CONFIG{"SYBASE".$i} =~ s/\s+$//;
					next if $found{$CONFIG{"SYBASE".$i}};
					# next if $pwd eq $CONFIG{"SYBASE".$i};
					_statusmsg("[addserver] $de_hostname: Testing Directory ".$CONFIG{"SYBASE".$i}."\n");									
					my($rc) = $c->cwd( $CONFIG{"SYBASE".$i} );
					if( $rc == 0 ) {
						_statusmsg("[addserver]    $de_hostname: cwd(".$CONFIG{"SYBASE".$i}.") failed - skipping\n");
						next;
					}
					_statusmsg("[Wizard]    $de_hostname: cwd(".$CONFIG{"SYBASE".$i}.") succeeded\n");
					# print "DBG DBG: ADDING ",$CONFIG{"SYBASE".$i}," to testlist\n";
					push @testlist, $CONFIG{"SYBASE".$i};	
				}				
				
				if( $#testlist >= 0 ) {
					my( $dathash ) = $server_info{$de_hostname.":unix"};					
					foreach my $d ( @testlist ) {
						next if $d =~ /^\s*$/;
						if( ! defined $dathash ) {
							return mywarn("ERROR: Server $de_hostname doesnt exist???\n");						
						} elsif( defined $$dathash{SYBASE} and $$dathash{SYBASE}!~/^\s*$/ ) {
							_statusmsg("[addserver] Adding Sybase Directory $d to unix host $de_hostname\n");				
							$$dathash{SYBASE}.=" ".$d;
							
							# add $d to xml data too
							#my(%fileinfo);
							#$fileinfo{filepath}=		$d;
							#$fileinfo{filepath_fromlocal}=	"";
							#$fileinfo{filetype}=		"Sybase Server \$SYBASE Directory";
							#$fileinfo{is_local}=0;
							#$fileinfo{frequency}=		"Hourly";
							#$fileinfo{hostname}=		$de_hostname;
							#$fileinfo{hosttype}=		"unix";
							#global_data(-class=>'fileinfo',-name=>$de_hostname.":".$d,-value=>\%fileinfo);
						} else {
							_statusmsg("[addserver] $de_hostname: Found Sybase Directory $d\n");				
							$$dathash{SYBASE}=$d;
						}
						$NEEDS_REFRESH{Files}=1;							
						#$$dathash{SYBASE}=~s/^\s+//;
			
						my(%fileinfo);						
						$fileinfo{filepath}=		$d;
						$fileinfo{filetype}=		"Sybase Server \$SYBASE Directory";
						$fileinfo{hostname}=		$de_hostname;
						$fileinfo{hosttype}=		"unix";
						global_data(-class=>'fileinfo',-name=>$de_hostname.":".$d,-value=>\%fileinfo);
						#filetab_addrow($itm,$de_hostname,"unix",$d,"Sybase Server \$SYBASE Directory" );
					}
					_statusmsg( "[debug] DBG DBG: ".Dumper($dathash)."\n" );							
				} else {
					_statusmsg("[Wizard] $de_hostname: No Sybase Directories Found\n");
				}
			}
		}

		main::colorize_tree( $de_hostname );
	}

	if( $ERROR ne "" and ! defined $ignore_errors) {
		_statusmsg("[addserver] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
			-title => 'Can Not Save Server',
			-message => "An Error Was Found: $ERROR",
			-type => "OK" );
		_statusmsg("[addserver] GEM Ready / Continuing...\n");
		return undef;
	}
	_statusmsg("[addserver] Server $de_hostname ($type_to_add) added successfully\n");
	return $type_to_add;
}

sub get_unknown_servers {
	#print "DSN LIST FOR ".hostname()."\n";
	my(%DSNS);
	my($keyptr)=global_data( -class=>'available_dsns', -keys=>1);
	#foreach ( @$keyptr ) {
		#print "HOST ",$_,"\n";
		#my($cstrings)=global_data( -class=>'available_dsns', -name => $_ );
		my($cstrings)=global_data( -class=>'available_dsns', -name => hostname() );
		#print "Cstring=$cstrings\n";
		foreach (split(/,/,$cstrings) ) {
			next if /MS Access Database/ or /Excel Files/ or /MQIS/ or /Visual FoxPro/
				or /Files$/ or /Demo$/ or /LocalServer$/ or /FoodMart 2000$/;
			if( /dbi:Sybase:/i ) {
				s/dbi:Sybase://ig;
				s/server=//ig;
				$DSNS{$_}=1 unless /_BACK$/i or /_BACKUP$/i or /_MON$/i or /_XP$/i or /_HS$/i or /_RS$/i;
			} elsif( /dbi:ODBC:/i ) {
				s/dbi:ODBC://ig;
				$DSNS{$_}=1 unless /_BACK$/i or /_BACKUP$/i or /_MON$/i or /_XP$/i or /_HS$/i or /_RS$/i;
			} else {
		#		print "UNKNOWN DSN $_\n";
			}
		}
	#}

	foreach ( get_server_by_type(-type=>"sybase") ) {
			delete $DSNS{uc($_)} if exists $DSNS{uc($_)};
			delete $DSNS{lc($_)} if exists $DSNS{lc($_)};
			delete $DSNS{$_} if exists $DSNS{$_};
	}
	foreach ( get_server_by_type(-type=>"sqlsvr") ) {
			delete $DSNS{uc($_)} if exists $DSNS{uc($_)};
			delete $DSNS{lc($_)} if exists $DSNS{lc($_)};
			delete $DSNS{$_} if exists $DSNS{$_};
	}
	foreach ( get_server_by_type(-type=>"oracle") ) {
			delete $DSNS{uc($_)} if exists $DSNS{uc($_)};
			delete $DSNS{lc($_)} if exists $DSNS{lc($_)};
			delete $DSNS{$_} if exists $DSNS{$_};
	}

	#print "DBG: printing Unknown DSNS\n";
	my(@dsn);
	push @dsn,sort keys %DSNS;
	#print "DSN: ",join(" ",@dsn),"\n";
	#print "=======================\n";
	return \@dsn;
}

sub edit_definition_box
{
	my($hosttype, $host, $datptr)=@_;
	my(%dat)=%$datptr;
	my $d= $XMLDATA{mainWindow}->DialogBox(-title =>'Enter '.$hosttype.' Server Information',-buttons => [ "Ok", "Cancel" ] );
	
	dialog_addvar($d,\%dat,"Server Name: ".$host,undef,	1,"label", 2, "orange");
	dialog_addvar($d,\%dat,"Server Type: ".$hosttype,undef,	2,"label", 2,"orange");
	dialog_addvar($d,\%dat,"Admin Login","login",	3,undef);
	dialog_addvar($d,\%dat,"Admin Password","password",	4,"password");
	
	if( $hosttype eq "sybase" and $host ) {		# fill in port/hostname  from interfaces if unavailable.
			if( ! defined $dat{hostname} or ! defined $dat{portnumber} ) {
				($dat{hostname},$dat{portnumber})
					=$gem_data->get_server_interfaces($host);
				_debugmsg( "[edit] autoset hostname to $dat{hostname}\n" );
				_debugmsg( "[edit] autoset portnumber to $dat{portnumber}\n" );
			}
	} 

	my($entrywidth)=35;
	$entrywidth=60 if $hosttype eq "unix";
	my($i)=1;				
	foreach my $k ( sort(gemrep_allowedkeys($hosttype)) ) {					
		#next if $k eq "SERVER_TYPE";					
		my($label)=join(" ",grep($_=ucfirst(lc($_)),split(/\_/,$k)));					
		my($fieldtype);
		$fieldtype="ServerProdBox" if $k eq "SERVER_TYPE";
		$fieldtype="SybaseType"		if $k eq "TYPE";
		$fieldtype="CommMethod" 	if $k eq "UNIX_COMM_METHOD" or $k eq "WIN32_COMM_METHOD";
							
		if( $k=~/^SYBASE/ or $k=~/^IGNORE/ ) {
			my($j)=1;
			foreach my $l ( split(/\s+/,$dat{$k} )) {
				$dat{$k."_".$j} = $l;
				dialog_addvar($d,\%dat,$label." Directory #".$j, $k."_".$j,	4 + $i++,$fieldtype,undef,undef,$entrywidth);						
				$j++;
			}						
			
			$dat{$k."_".$j} = "";											
			dialog_addvar($d,\%dat,$label." Directory #".$j, $k."_".$j,	4 + $i++,$fieldtype,undef,undef,$entrywidth);										
			$j++;
			
			$dat{$k."_".$j} = "";											
			dialog_addvar($d,\%dat,$label." Directory #".$j, $k."_".$j,	4 + $i++,$fieldtype,undef,undef,$entrywidth);
			$j++;
		} else {
			dialog_addvar($d,\%dat,$label, $k,	4 + $i++,$fieldtype,undef,undef,$entrywidth);						
		}
	}
	
	my $button2=$d->Show();
	if( $button2 eq "Ok" ) {										
		_debugmsg( "[edit] CHANGES NOTED!\n" );
		
		# glue back the separated items
		delete $dat{SYBASE};
		delete $dat{IGNORE_RUNFILE};
		delete $dat{SYBASE_CLIENT};
		foreach (sort keys %dat) {
			next unless /^SYBASE/ or /^IGNORE/;
			next unless /_\d+$/;
			my($k)=$_;
			$k=~s/_\d+$//;			
			if( $dat{$k} ) {
				$dat{$k}.=" ".$dat{$_};
			} else {
				$dat{$k}=$dat{$_};
			}
			delete $dat{$_};
		}		
		
		foreach (sort keys %dat) {
			if( defined $dat{$_} and $dat{$_} !~ /^\s*$/ ) {
				
				_debugmsg( "[edit] K=$_ Dat=$dat{$_} \n" );
				$server_info{$host.":".$hosttype}{$_}=$dat{$_};
			} else {
				_debugmsg( "[edit] K=$_ Dat is not defined \n" );						
				delete $server_info{$host.":".$hosttype}{$_}
			}
		}
			
	#	$server_info{$host.":".$hosttype}{login} 		= $dlg_svrvar{'Admin Login'};
	#	$server_info{$host.":".$hosttype}{password}  = $dlg_svrvar{'Admin Password'};
	} elsif( $button2 eq "Cancel" ) {
		return 0;
	}
}

sub ServerTab_draw {
	my($mainTab,$hosttype)=@_;
	##print "DBGX: HOST TYPE for ServerTab_draw is $hosttype\n";
	_debugmsg( "[func] ServerTab_draw($hosttype)\n" );

	my($hostaryptr);
	if( $hosttype eq "Unregistered") {
		$hostaryptr=get_unknown_servers();
	} else {
		$hostaryptr=get_svrlist_ptr($hosttype);
	}
	
	#print "DBGX: =======================\n";
	#print "DBGX: Retrieved $hosttype Servers\n";
	#print "DBGX: HOSTS ARE: ",join(" ",@$hostaryptr),"\n";
	#print "DBGX: =======================\n";

	$hosts_frame->destroy() if defined $hosts_frame and Tk::Exists($hosts_frame);
	
	$hosts_frame=$mainTab->Scrolled(qw/Pane -scrollbars osoe -gridded xy -sticky nw/)->pack(qw/-expand 1 -side top -anchor w -fill both/);
	my($col)=0;
	my($row)=1;

	my(@iargs) = qw/-ipadx 2 -ipady 2 -padx 2 -pady 2 -sticky ew/;
	my(%unregservers);
	_debugmsg( "[func] Refreshing Screen - This may take a few seconds\n" );
	foreach my $host ( @$hostaryptr ) {
		if( $row == 1 and $hosttype ne "Unregistered") {	# ADD THE HEADER
			#_debugmsg( "[func] painting header\n" );
			if( $hosttype eq "unix" or $hosttype eq "win32servers" ) {
				$hosts_frame->Label(-text=>"Hostname",-font=>'smbold')->grid(-row=>$row,-column=>1,-sticky=>'ew');
			} else {
				$hosts_frame->Label(-text=>"Server",-font=>'smbold')->grid(-row=>$row,-column=>1,-sticky=>'ew');
			}
			$col=2;
			$hosts_frame->Label(-text=>"Login",   -font=>'smbold')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				unless $hosttype eq "win32servers" or  $hosttype eq "Unregistered";
			$hosts_frame->Label(-text=>"Disks",-font=>'smbold')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				if $hosttype eq "win32servers";
			$hosts_frame->Label(-text=>"Category",-font=>'smbold')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				unless $hosttype eq "win32servers"  or  $hosttype eq "Unregistered";
			$hosts_frame->Label(-text=>"UnxComm",-font=>'smbold')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				if $hosttype eq "unix";
			$hosts_frame->Label(-text=>"WinComm",-font=>'smbold')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				if $hosttype eq "unix";
			$row++;
			$col=1;
		}

		if( $hosttype eq "Unregistered") {
			my %hash;
			$unregservers{$host}=\%hash;
			$hash{host}=$host;
			# col is 1 or 3 at this point
			$hosts_frame->Label(
				-text=>$host,
				-font=>'smbold',
				-bg=>'white',
				-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);

			# col would be 2 or 4 at this point
			dohelp($hosts_frame->Button(  -text=>'Register This Server',-background=>'white',-command=>sub {
					return if is_busy();
					_statusmsg("[func] REGISTER SERVER BUTTON PUSHED - host=$host\n" );
					my($hashptr)=$unregservers{$host};
					#my(%new_info) = tk_add_a_server("Server Name"=>$host);
					Register_Server( -name => $host );
					ServerTab_draw($mainTab,$hosttype);
					unbusy(1);
				})->grid(-column=>$col++,-row=>$row,-sticky=>'ew'),
					"Register Server $host (pulled from local server list)" );

			# a blank!
			$hosts_frame->Label(-text=>"   ")->grid(-row=>$row,-column=>$col++,@iargs)
				if $col == 2;

			if( $col>=5 ) { $row++; $col=0; }
			next;
		}
		$col=1;

		my($current_color)="white";
		$hosts_frame->Label(-text=>$host,-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);

		my( $dathash ) = $server_info{$host.":$hosttype"};
		if( ! defined $dathash ) {
			print "WARNING: Server $host of type $hosttype Has been Deleted\n";
			print LOGFP "WARNING: Server $host of type $hosttype Has been Deleted\n"
				 unless defined $NOLOGFILE;
			next;
		}
		my( %dat ) = %$dathash;

		$hosts_frame->Label(-text=>$dat{login},-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs)
			unless $hosttype eq "win32servers";
		$hosts_frame->Label(-text=>$dat{DISKS},-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs)
			if $hosttype eq "win32servers";
		#$hosts_frame->Label(-text=>$dat{password},-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);

		#C#if( $hosttype eq "sybase" or $hosttype eq "oracle" ) {
		#C#my @tmp;
		#C#push @tmp,"ODBC";
		#C#push @tmp,"Sybase" if $hosttype eq "sybase";
		#C#push @tmp,"Oracle" if $hosttype eq "oracle";
		#C#my($x)=my_label( $hosts_frame, \$server_info{$host.":".$hosttype}{conn_type},  undef,undef,"white",  @tmp );
		#C#$x->grid(-row=>$row,-column=>$col++,@iargs);

		#$hosts_frame->Label(-text=>$dat{conn_type},-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs)
		#		if $hosttype eq "sybase"
		#		or $hosttype eq "sqlsvr"
		#		or $hosttype eq "oracle";
		#C#}
		if( $hosttype ne "win32servers" ) {
			my($x)=my_label( $hosts_frame, \$server_info{$host.":".$hosttype}{SERVER_TYPE},  undef,undef,$current_color,  "PRODUCTION","CRITICAL","DEVELOPMENT","QA","DR" );
			dohelp(
			$x->grid(-row=>$row,-column=>$col++,@iargs),
			"PRODUCTION/CRITICAL servers are treated with priority and out of hours notification.");
		}

		if( $hosttype eq "unix") {
			my($x)=my_label( $hosts_frame, \$server_info{$host.":".$hosttype}{UNIX_COMM_METHOD},  undef,undef,$current_color,  "RSH","SSH","FTP","NONE" );
			dohelp(
			$x->grid(-row=>$row,-column=>$col++,@iargs),
			"How unix programs talk to $host.");
			
			$x=my_label( $hosts_frame, \$server_info{$host.":".$hosttype}{WIN32_COMM_METHOD},  undef,undef,$current_color,  "FTP","NONE" );
			dohelp(
			$x->grid(-row=>$row,-column=>$col++,@iargs),
			"How windows programs talk to $host. Set unix_passwords.dat by hand if you have SSH/RSH.");
		}

		# button oversized - bah
		#$hosts_frame->Button(  -bg=>'beige', -pady=>-1,-text=>"Delete",-command=>\&del_host )->grid(-column=>$col++,-row=>$row,-sticky=>'ew');
		dohelp (
		#$hosts_frame->Button(  -bitmap=>'error',-background=>$current_color,-relief=>'groove',-command=>sub {}
		$hosts_frame->Button(  -text=>'Deregister',-background=>$current_color,-command=>sub {
				return if is_busy();
				do_delete_server($host,$hosttype);
				my($hostaryptr)=get_svrlist_ptr($hosttype);
				#print "DBG: Remaining ",join(" ",@$hostaryptr ),"\n";
				busy();
				ServerTab_draw($mainTab,$hosttype);
				filetab_create() if $hosttype eq "unix";
				unbusy();
				_statusmsg("[func] Deregister Server Process Completed\n");
		 })->grid(-column=>$col++,-row=>$row,-sticky=>'ew'),
			"Deregister $host From GEM" );

		dohelp (
		$hosts_frame->Button(  -text=>'Edit Definition',-background=>$current_color,-command=>sub {
				return if is_busy();
				edit_definition_box($hosttype, $host, \%dat);
				#_statusmsg("[func] Redrawing Table\n");	
				#ServerTab_draw($mainTab,$hosttype);
				_statusmsg("[func] Edit Definition Completed\n");	
			} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
			"Edit Server Setup") unless $hosttype eq "win32servers";

		dohelp (
		$hosts_frame->Button(  -text=>'Set Disks',-background=>$current_color,-command=>sub {
			return if is_busy();
			my $d= $XMLDATA{mainWindow}->DialogBox(	-title =>'Enter Disk Information',-buttons => [ "Ok", "Cancel" ] );
			my(%dlg_svrvar);
			$dlg_svrvar{'Server Name'} 	= $host;
			$dlg_svrvar{'Space Separated Disk Drives'} = $dat{DISKS};

			dialog_addvar($d,\%dlg_svrvar,"Server Name",undef,	1,"label");
			dialog_addvar($d,\%dlg_svrvar,"Space Separated Disk Drives",undef,	2,undef);

			my $button=$d->Show();
			if( $button eq "Ok" ) {
				print "Saving Changes to($dat{login},$dat{password}) and Redrawing\n";
				#print __LINE__," DBG: Setting server_info{$host $hosttype}\n";
				$server_info{$host.":".$hosttype}{DISKS} = $dlg_svrvar{'Space Separated Disk Drives'};
				busy();
				ServerTab_draw($mainTab,$hosttype);
				unbusy();
			}
		} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
			"Add Disk Shares For This Server - these will be monitored. e.g. C\$ D\$ E\$" ) if $hosttype eq "win32servers";

		dohelp (
		$hosts_frame->Button(  -text=>'AutoDetect Disks',-background=>$current_color,-command=>sub {
			return if is_busy();
			if( is_nt() ) {
				# DETECT DISK DRIVES IF WINDOWS SERVER
				_statusmsg("[func]  Checking Disk Drives On $host");
				busy();
				my(@disks)=get_win32_disks($host);
				#print __LINE__," DBG: Setting server_info{$host $hosttype}\n";
				$server_info{$host.":".$hosttype}{DISKS} = join(" ",@disks);
				ServerTab_draw($mainTab,$hosttype);
				unbusy();
				_statusmsg("[func] Drive Detection Completed\n");
			} else {
				_statusmsg("[func] Displaying message box - select a button to continue\n");
				$XMLDATA{mainWindow}->messageBox(
					-title => 'Warning',
					-message => 'You Can Not Auto-Detect Windows Disks From UNIX/Linux.',
					-type => "OK" );
				_statusmsg("[func] GEM Ready / Continuing...\n");
			}
		} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
			"Add Disk Shares For This Server - these will be monitored. e.g. C\$ D\$ E\$" ) if $hosttype eq "win32servers";

		dohelp (
		$hosts_frame->Button(  -text=>'Connect',-background=>$current_color,-command=>sub {
			return if is_busy();
			busy();
			my($c,$errmsg)=$connectionmanager->connect(-type=>$hosttype, -name=>$host);
			main::colorize_tree($host);
			unbusy();
			if( defined $c ) {
				_statusmsg("[func] Displaying message box - select a button to continue\n");
				$XMLDATA{mainWindow}->messageBox(
					-title => "Ok",
					-message => "Successful Connection To $host: $errmsg",
					-type => "OK" );
				_statusmsg("[func] GEM Ready / Continuing...\n");
			} else {
				_statusmsg("[func] Displaying message box - select a button to continue\n");
				$XMLDATA{mainWindow}->messageBox(
					-title => "Failed To Connect to $host",
					-message => "Failed To Connect to $host\n$errmsg",
					-type => "OK" );
				_statusmsg("[func] GEM Ready / Continuing...\n");
			}
			busy();
			ServerTab_draw($mainTab,$hosttype);
			unbusy();
			_statusmsg("[func] Connection Process Completed\n");

		} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
			"Connect To This Server" );

		$row++;
	}
	_debugmsg( "[func] Done Refreshing Screen\n" );

	if( $row == 1 ) {
		$hosts_frame->Label(-text=>"NO SERVERS FOUND",   -font=>'smbold')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
		$row++;
	}


}

#sub rt_clicked {
#  	my($lb,$x,$y)=@_;
#  	my $entryPath = $lb->nearest($y-$lb->rooty);
#   	print( "rt button pressed at (",$x-$lb->rootx,",",$y-$lb->rooty,") $entryPath\n");
#
#   	my($type,$srv,$cnt)=split(/\./,$entryPath);
#    	my($pmenu) = $XMLDATA{mainWindow}->Menu(
#   		#-title=>'Server $srv',
#   		-tearoff=>0,
#   		-disabledforeground=>'blue',
#                -activebackground=>'white',
#                -activeforeground=>'black' );
#        $pmenu->command(-label => "Add File",    -command => sub { print "add file(@_)"; } );
#    	$pmenu->command(-label => "Delete File", -command => sub { print "add file(@_)"; } );
#
#   	$pmenu->post($x+5,$y);
#}

sub unimplemented {
	my($string)=@_;
	_statusmsg("[func]  (not) $string");
   	my $dialog = $XMLDATA{mainWindow}->DialogBox(-title=>"Unimplemented", -buttons=>["Ok"]);
   	$dialog->Label(-text=>"<To Do> $string")->pack();
   	$dialog->Show;
}

my(%filetab_itemcount);
my(%nullitemlist);	# items that have no data

sub filetab_discovered_file
{
	my($itm,$style,$hostname,$rowid,$filename,$filetype)=@_;
	my($entryPath) = $itm.".".($filetab_itemcount{$itm}+$rowid);
	#print "DBG: Adding disc item=$itm path=$entryPath Rowid=$rowid\n";
	$filetab_hlist->add($entryPath, -style=>$style );
	$filetab_hlist->itemCreate($entryPath, 2, -text=>$hostname, -style=>$style);
	$filetab_hlist->itemCreate($entryPath, 3, -text=>"Discovered", -style=>$style);
	$filetab_hlist->itemCreate($entryPath, 4, -text=>$filename, -style=>$style);
	$filetab_hlist->itemCreate($entryPath, 5, -text=>$filetype, -style=>$style);

	#print "FILE: $filename type=$filetype\n";
	#foreach (keys %fileinfo) { print "K=$_ v=$fileinfo{$_}\n"; }

	return unless $filetype=~/Run File/;

	my $del = $filetab_hlist->Button(
			-background=>'beige',
			-text=>'Ignore',
			-font=>'tiny',-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
			-command=>sub {
				return if is_busy();
				my($hosttype,$hostname)=split(/\./,$itm,2);


				if( $hosttype eq "Unix" ) {
					my $dialog = $XMLDATA{mainWindow}->DialogBox(-title=>"Note", -buttons=>["Ok"]);
	   				$dialog->Label(-text=>"Ignoring $filename on $hostname.\n\tThis file will be ignored in the next survey.\n\tFiles discovered in this Run File will appear until then.")->pack();
   					$dialog->Show;

					my( $dathash ) = $server_info{$hostname.":unix"};
					if( $$dathash{IGNORE_RUNFILE} =~ /^\s*$/ ) {
						$$dathash{IGNORE_RUNFILE} = $filename;
					} else {
						$$dathash{IGNORE_RUNFILE} .= " ".$filename
							unless $$dathash{IGNORE_RUNFILE} =~ $filename;
					}

					my($dat)=global_data(-class=>'unix',-name=>$hostname,-arg=>'discovered_files');
					delete  $$dat{$filename};
					global_data(-class=>'unix',-name=>$hostname,-arg=>'discovered_files',-value=>$dat);

					my(%fileinfo);
					$fileinfo{filepath}=		$filename;
					$fileinfo{filetype}=		"Ignore This RUN_\* File";
					$fileinfo{hostname}=		$hostname;
					$fileinfo{hosttype}=		"unix";
					#print "DBG: adding fileinfo for $hostname:$filename\n";
					global_data(	-class=>'fileinfo',
							-name=>$hostname.":".$filename,
							-value=>\%fileinfo);

					filetab_create();
					if( $filetab_hlist->infoExists( $itm ) ) {
						$filetab_hlist->see($itm);
    						$filetab_hlist->selectionSet($itm);
    					}

				} else {
					unimplemented("This should only work on Unix!!! type=$hosttype svr=$hostname");
				}
			}
		);
	dohelp($del,"Ignore This Run File");
	$filetab_hlist->itemCreate($entryPath,1,-itemtype=>'window',-widget=>$del);
}

sub filetab_addrow
{
	#print "DBG: filetab_addrow(",join(" ",@_),"\n";
	my($itm,$hostname,$hosttype,$file,$type)=@_;	#,$frequency, $is_local, $filepath_fromlocal)=@_;
	my($entryPath);

	my( $dathash ) = $server_info{$hostname.":$hosttype"};
	if( ! defined $dathash ) {
		print "WARNING: Unix Server $hostname of type $hosttype Has been Deleted\n";
		next;
	}

	if( $filetab_itemcount{$itm} != 0 and ! defined $nullitemlist{$itm} ) {
		$entryPath=$itm.".".$filetab_itemcount{$itm};
		#print "DBG: Adding item=$itm path=$entryPath count=$filetab_itemcount{$itm} \n";
		$filetab_hlist->add($entryPath);
	} else {
		#print "DBG: Item $itm does not exist\n";
		$filetab_itemcount{$itm}=1;
		$entryPath=$itm;
		delete $nullitemlist{$entryPath} if defined $nullitemlist{$entryPath};
	}
	$filetab_itemcount{$itm}++;

	$filetab_hlist->itemCreate($entryPath, 2, -text=>$hostname);
	$filetab_hlist->itemCreate($entryPath, 3, -text=>"Unix Server")    if $hosttype eq "unix";
	$filetab_hlist->itemCreate($entryPath, 3, -text=>"NT Server") if $hosttype ne "unix";
	$filetab_hlist->itemCreate($entryPath, 4, -text=>$file);
	$filetab_hlist->itemCreate($entryPath, 5, -text=>$type);
	#$filetab_hlist->itemCreate($entryPath, 6, -text=>$frequency);
	#if( $is_local ) {
	#	$filetab_hlist->itemCreate($entryPath, 7, -text=>"Local");
	#} else {
	#	$filetab_hlist->itemCreate($entryPath, 7, -text=>"Ftp"  );
	#}
	#$filetab_hlist->itemCreate($entryPath, 8, -text=>$filepath_fromlocal);

	$nullitemlist{$itm}=1 if $type eq "";
	my $del = $filetab_hlist->Button(
			-background=>'beige',
			-text=>'Delete',
			-font=>'tiny',-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
			-command=>sub {
				return if is_busy();
				print "Deleting $type $file for host $hostname\n";

			# MOD FOR DELETES
			global_data(-class=>"fileinfo", -name=>"$hostname:$file", -delete=>1,-debug=>$DEBUG);

				if( defined $add_file_labels{$type} ) {
					$$dathash{$add_file_labels{$type}} =~ s/$file//;
					$$dathash{$add_file_labels{$type}} =~ s/\s\s+/ /;
					$$dathash{$add_file_labels{$type}} =~ s/^\s+//;
					$$dathash{$add_file_labels{$type}} =~ s/\s+$//;
#				if( $type eq "Sybase Server \$SYBASE Directory" ) {
#					$$dathash{SYBASE} =~ s/$file//;
#					$$dathash{SYBASE} =~ s/\s\s+/ /;
#					$$dathash{SYBASE} =~ s/^\s+//;
#					$$dathash{SYBASE} =~ s/\s+$//;
#				} elsif( $type eq "Ignore This RUN_\* File" ) {
#					$$dathash{IGNORE_RUNFILE} =~ s/$file//;
#					$$dathash{IGNORE_RUNFILE} =~ s/\s\s+/ /;
#					$$dathash{IGNORE_RUNFILE} =~ s/^\s+//;
#					$$dathash{IGNORE_RUNFILE} =~ s/\s+$//;
#				} elsif( $type eq "Sybase Open Client \$SYBASE Directory"	) {
#					$$dathash{SYBASE_CLIENT} =~ s/$file//;
#					$$dathash{SYBASE_CLIENT} =~ s/\s\s+/ /;
#					$$dathash{SYBASE_CLIENT} =~ s/^\s+//;
#					$$dathash{SYBASE_CLIENT} =~ s/\s+$//;
#				}
				} else {
					print "ERROR - Cant Delete\n";
					return;
				}

				# ok now we need a bit of logic
				if( $entryPath !~ /\.\d+$/ ) {
					# we need to leave the base item...
	#				$filetab_hlist->itemDelete($entryPath, 8);
	#				$filetab_hlist->itemDelete($entryPath, 7);
	#				$filetab_hlist->itemDelete($entryPath, 6);
					$filetab_hlist->itemDelete($entryPath, 4);
					$filetab_hlist->itemDelete($entryPath, 5);
					$filetab_hlist->itemCreate($entryPath, 4, -text=>"[No File or Directory Specified]");
					$filetab_hlist->itemDelete($entryPath, 1);
					#print "DBG: Setting $itm to null\n";
					$nullitemlist{$itm}=1;
				} else {
					$filetab_hlist->delete("entry",$entryPath);
				}
			}
		);
	dohelp($del,"Delete The Current File");
	$filetab_hlist->itemCreate($entryPath,1,-itemtype=>'window',-widget=>$del) if $type ne "";

	my $add = $filetab_hlist->Button(
			-background=>'beige',
			-text=>'Add',
			-font=>'tiny',-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
			-command=>sub {
				return if is_busy();
				filetab_add_dialog($hostname,$hosttype, $dathash, $filetab_hlist, $itm);
			}
		);
	dohelp($add,"Add A New File");
	$filetab_hlist->itemCreate($entryPath,0,-itemtype=>'window',-widget=>$add);
}


sub filetab_add_dialog {
	my($hostname,$hosttype, $dathash, $tree, $itm)=@_;

	my($filepath,$filetype);

	my $dlg= $XMLDATA{mainWindow}->DialogBox( -title =>'Enter File/Directory Information',
		-buttons => [ "Ok", "Cancel" ] );
	$dlg->add("Label",-text=>"Host Name")->grid(-column=>1, -row=>1);
	$dlg->add("Label",-text=>$hostname)->grid(-column=>2, -row=>1,-sticky=>'w');
	$dlg->add("Label",-text=>"Host Type")->grid(-column=>1, -row=>2);
	$dlg->add("Label",-text=>ucfirst($hosttype))->grid(-column=>2, -row=>2, -sticky=>'w');

	$dlg->add("Label",-text=>"File Type")->grid(-column=>1, -row=>3);
	my(@items)=keys %add_file_labels;
	if( ! defined $filetype ) {
		foreach ( @items ) {
			next unless $add_file_labels{$_} eq "SYBASE";
			$filetype=$_ ;
		}
	}
	my($p)=my_label( $dlg, \$filetype,  undef,undef, "white",@items )->grid(-column=>2,-row=>3,-sticky=>'w');

	$dlg->add("Label",-text=>"Path On $hostname")->grid(-column=>1, -row=>4);
	$dlg->add("Entry",-bg=>'white',-textvariable=>\$filepath,-width=>45)->grid(-column=>2,-row=>4,-sticky=>'w');

	my $button=$dlg->Show();
	if( $button eq "Ok" and $filepath!~/^\s*$/ ) {
		#ok so first thing we do is add it to the variables

		if( defined $add_file_labels{$filetype} ) {
			if( defined $$dathash{$add_file_labels{$filetype}}
			and $$dathash{$add_file_labels{$filetype}}!~/^\s*$/ ) {
				$$dathash{$add_file_labels{$filetype}}.=" ".$filepath;
			} else {
				$$dathash{$add_file_labels{$filetype}}=" ".$filepath;
			}
			$$dathash{$add_file_labels{$filetype}}=~s/^\s+//;
#		}
#		if( $filetype eq "Sybase Server \$SYBASE Directory" ) {
#			if( defined $$dathash{SYBASE} and $$dathash{SYBASE}!~/^\s*$/ ) {
#				#print "Syb Defined $$dathash{SYBASE}\n";
#				$$dathash{SYBASE}.=" ".$filepath;
#			} else {
#				#print "Syb Not Defined\n";
#				$$dathash{SYBASE}=" ".$filepath;
#			}
#			$$dathash{SYBASE}=~s/^\s+//;
#		} elsif( $filetype eq "Ignore This RUN_\* File" ) {
#			if( defined $$dathash{IGNORE_RUNFILE} and $$dathash{IGNORE_RUNFILE}!~/^\s*$/ ) {
#				$$dathash{IGNORE_RUNFILE}.=" ".$filepath;
#			} else {
#				$$dathash{IGNORE_RUNFILE}=" ".$filepath;
#			}
#			$$dathash{IGNORE_RUNFILE}=~s/^\s+//;
#		} elsif( $filetype eq "Sybase Open Client \$SYBASE Directory"	) {
#			if( defined $$dathash{SYBASE_CLIENT} and $$dathash{SYBASE_CLIENT}!~/^\s*$/ ) {
#				$$dathash{SYBASE_CLIENT}.=" ".$filepath;
#			} else {
#				$$dathash{SYBASE_CLIENT}=" ".$filepath;
#			}
#			$$dathash{SYBASE_CLIENT}=~s/^\s+//;
		} else {
			$filetype=~s/\s+$//;
			print "ERROR - Cant Save - ($filetype) not found in list\n";
			return;
		}

		my(%fileinfo);
		$fileinfo{filepath}=		$filepath;
		$fileinfo{filetype}=		$filetype;
		$fileinfo{hostname}=		$hostname;
		$fileinfo{hosttype}=		$hosttype;
		global_data(-class=>'fileinfo',-name=>$hostname.":".$filepath,-value=>\%fileinfo);
		filetab_addrow($itm,$hostname,$hosttype,$filepath,$filetype );
	}
}

sub filetab_create {
	if( ! defined $filetab_master ) {
		($filetab_master)=@_;
	}
	my($tabF)=$filetab_master;
	$NEEDS_REFRESH{Files} = 0;

	mydie("NO TAB FRAME FOUND in filetab_create()\n") unless defined $tabF;
	if( defined $filetab_frm and defined $filetab_hlist ) {
		
		$filetab_frm->destroy() 	if Tk::Exists($filetab_frm);
		$filetab_frm=undef;
		$filetab_hlist->destroy()  if Tk::Exists($filetab_hlist);;
		$filetab_hlist=undef;
		
	}

	_debugmsg("[func] filetab_create()\n");
	$filetab_frm = $tabF->LabFrame(
		-label=>"Unix File and Directory Registration",
		-labelside=>'acrosstop'	)->pack(qw/-side top -fill both -expand 1/);

	$filetab_hlist = $filetab_frm->Scrolled("HList",
			      #-width            => 80,
			      #-height           => 5,
			      -background       => "white",
			      -selectbackground => "beige",
			      -itemtype         => 'text',
			      -separator        => '.',
			      -columns		=> 6,
			      -header		=> 1,
			      -selectmode       => 'single',
			      -relief           => 'sunken',
			      -scrollbars       => 'osoe',
			      -drawbranch	=> 0,
			      #-command 		=> sub { print "Browse @_\n"; },
			      # -command          => \&command,
			      )->pack(-side     => 'top',
				      -fill     => 'both',
				      -expand   => 1)->pack(qw/-side top -fill both -expand 1/);

	my($disc_style)=$filetab_hlist->ItemStyle('text',
		-selectforeground=>'blue',
		-foreground=>'blue',
		-background =>'white',
	);

	#$filetab_hlist->bind('<Button-3>',[ \&rt_clicked ,Ev('X'),Ev('Y') ] );

	$filetab_hlist->headerCreate(0, -text=>"Add");
	$filetab_hlist->headerCreate(1, -text=>"Delete");
	$filetab_hlist->headerCreate(2, -text=>"Server Name");
	$filetab_hlist->headerCreate(3, -text=>"Server Type");
	$filetab_hlist->headerCreate(4, -text=>"File or Directory Name");
	$filetab_hlist->headerCreate(5, -text=>"File Type");

	#$filetab_hlist->headerCreate(6, -text=>"Frequency");
	#$filetab_hlist->headerCreate(7, -text=>"Method");
	#$filetab_hlist->headerCreate(8, -text=>"Mapping");

	my($filelist_ref)=global_data(-class=>"fileinfo", -keys=>1, -noprint=>1);

	# FOR EACH UNIX HOST
	$filetab_hlist->add("Unix");
	_debugmsg("[func] getting discovered file statistics\n");
	foreach my $host (get_server_by_type(-type=>"unix", -sorted=>1)) {
		return mywarn("Hosts Names may not have periods embedded.  Host=$host") if $host=~/\./;
		my($itm)="Unix.".$host;
		#_debugmsg("[func] Adding path=$itm\n" );
		$filetab_hlist->add($itm);
		$filetab_itemcount{$itm}=0;

		if( defined $filelist_ref ) {
			foreach (@$filelist_ref) {
				next unless /^$host:/;
				#print "DBG HOST $host - FOUND FILE $_\n";
				#print "------------------------\n";
				my($r)=global_data(-class=>"fileinfo", -name=>$_);
				#$filelist_found{$_}=$r;
				mydie("WOAH: can not recover fileinfo for $_ from xml repository\n")
					unless $r;
				#print Dumper $r if $host eq "riskdb";
				#print "------------------------\n";
				my($fname)=$_;
				$fname=~s/$host://;
				filetab_addrow($itm,	$$r{hostname},
							$$r{hosttype},
							$$r{filepath},
							$$r{filetype},
							#$$r{frequency},
							#$$r{is_local},
							#$$r{filepath_fromlocal}
						);
			}
		}

		if( $filetab_itemcount{$itm} == 0 ) {
			filetab_addrow($itm,$host,"unix","[No File or Directory Specified]","");	#,"","","");
		}
		#foreach( keys %dat ) { print "\t$_\t$dat{$_}\n"; }

		my($dat)=global_data(-class=>'unix',-name=>$host,-arg=>'discovered_files');
		my($rowid)=1;
		if( defined $dat ) {
			#my(%dathash)=%{$dat};
			my(%dathash)=%$dat;
			foreach ( keys %dathash ) {
				my($note)=$dathash{$_}->{File_Type};
				$note.= " for \[".$dathash{$_}->{Server_Type}."\] ".$dathash{$_}->{Server_Name}
					if defined $dathash{$_}->{Server_Type} and defined $dathash{$_}->{Server_Name};
				filetab_discovered_file($itm,$disc_style,$host,$rowid++,$_,$note);
			}
		}
		$filetab_itemcount{$itm}+=$rowid;
	}
	_debugmsg("[func] filetab_create completed\n");

	# add windows stuff
	#$filetab_hlist->add("Windows", -text=>"Windows");

#	foreach my $host ( @$hostaryptr ) {
#		$col=1;
#		$hosts_frame->Label(-text=>$host,-font=>'bold',-bg=>'white',-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
#		$hosts_frame->Label(-text=>$l,-font=>'norm',-bg=>'white',-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
#		$hosts_frame->Label(-text=>$p,-font=>'norm',-bg=>'white',-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
#		# button oversized - bah
#		#$hosts_frame->Button(  -bg=>'beige', -pady=>-1,-text=>"Delete",-command=>\&del_host )->grid(-column=>$col++,-row=>$row,-sticky=>'ew');
#		$hosts_frame->Button(  -bitmap=>'error',-background=>'white',-command=>sub {
#			host_del( $host ) },-relief=>'groove' )->grid(-column=>$col++,-row=>$row,-sticky=>'ew');
#		$row++;
#	}

	#$filetab_hlist->autosetmode();
}

sub my_label {
	my($widget,$varref, $funcref, $bgcolor, $bglabel, @list)=@_;
	my $prodfrm;
	if( defined $bgcolor ) {
		$prodfrm = $widget->Frame(-bg=>$bgcolor, -relief=>'ridge');
	} else {
		$prodfrm = $widget->Frame( -relief=>'ridge');
	}

	if( defined $bglabel ) {
		$prodfrm->Label(-textvariable=>$varref, -bg=>$bglabel, -relief=>'ridge')->pack(-side => "left", -pady=>2, -expand=>1, -fill=>'x');
	} else {
		$prodfrm->Label(-textvariable=>$varref, -relief=>'ridge')->pack(-side => "left", -pady=>2, -expand=>1, -fill=>'x');
	}

	$prodfrm->Button(-bitmap=>'@'.$XMLDATA{downarrow}, -bg=>'white', -command=> sub {
		return if is_busy();
		my $var= $$varref;
		my $i=0;
		foreach (@list) { last if $_ eq $var; $i++; }
		$i++;
		$i=0 if $i>$#list;
		$$varref = $list[$i];
		&$funcref($widget,$$varref) if defined $funcref;
		})->pack(-side=>'right', -pady=>1);
	return $prodfrm;
}


sub do_installprocs {
	my(%args)=@_;	# -name and -type for finer grain control

	#foreach ( keys %args ) { #print "DBG: do_installprocs($_) => $args{$_}\n"; };

	# get version of stored proc library
	my($PROCVERSION,$PROCSHTVERSION)=get_version("$XMLDATA{admin_scripts_dir}/procs/VERSION");
	#$PROCVERSION=~s/v//i;

	#
	# Install Stored Procedure Library
	#
	my($sv_print_stdout )= $print_stdout;
	$print_stdout = "TRUE";
        if( ! defined $args{-type} or $args{-type} eq "sybase" ) {
            	foreach my $svr ( get_server_by_type(-type=>"sybase", -sorted=>1) ) {
            		next if defined $args{-name} and $args{-name} ne $svr;
            		my( $dathash ) = $server_info{$svr.":sybase"};
            		if( ! defined $dathash ) {
            			_statusmsg(  "WARNING: Server $svr of type sybase Has been Deleted\n" );
            			next;
            		}
            		my( %dat ) = %$dathash;

            		my($typ);
            		if( is_nt() ) {
            			$typ = "ODBC";
            		} else {
            			$typ = "Sybase";
            		}

            		my($str)="$^X -I".join(" -I",@perl_includes).
            			" $XMLDATA{admin_scripts_dir}/procs/configure.pl --SERVER=$svr --USER=$dat{login}".
            			" --PASSWORD=$dat{password} ".
            			"--TYPE=$typ ";
            		$str.="--MINVERSION=\"$PROCSHTVERSION\"" if $CONFIG{DONT_SKIPPROCS}==1;

            		busy();
            		my(%subargs);
            		$subargs{-showinwin}="Stored Procedure Installer" if defined $args{-name};
            		my($rc,$datptr)=RunCommand(            			
            			-cmd=>$str,
            			-mainwindow=>$XMLDATA{mainWindow},
            			-statusfunc=>\&_statusmsg,
            			-printfunc =>\&_statusmsg,
            			-ignore_string=>"^Installing",
            			-printhdr  => "[installer] : ",
            			%subargs                     
            		);
						unbusy();
						if( ! defined $args{-name} and ! $UPGRADE ){
                    		foreach( @$datptr ) {
                    			if( /^\* Cant connect to/ ) { #  or /^Skipping installation as current version/ ) {}
                    				my($errmsg)="[func]".join("\n[func]",@$datptr)."\n\nReturn Code=$rc\n";
                    				last if $errmsg=~/Skipping installation as current version/;
                    				_statusmsg($errmsg);
                    				#_statusmsg("[func] Please Select 'OK' on the pop-up Results display\n");
                    				#my_rotext_box(
        					#	-title => "Stored Procedure Installer",
        					#	-text => $errmsg );
        					#_statusmsg("[func] GEM Ready / Continuing...\n");
        					last;
        				}
        			}
			}
           	}
	}

	if( $XMLDATA{isnt} ) {
         	if( ! defined $args{-type} or $args{-type} eq "sqlsvr" ) {
               	foreach my $svr (get_server_by_type(-type=>"sqlsvr", -sorted=>1)) {
               		next if defined $args{-name} and $args{-name} ne $svr;
               		my( $dathash ) = $server_info{$svr.":sqlsvr"};
               		if( ! defined $dathash ) {
               			_statusmsg(  "WARNING: Server $svr of type sqlsvr Has been Deleted\n" );
               			next;
               		}
               		my( %dat ) = %$dathash;
               		my($str)="$^X -I".join(" -I",@perl_includes)." $XMLDATA{admin_scripts_dir}/procs/configure.pl --SERVER=$svr --USER=$dat{login} --PASSWORD=$dat{password} --TYPE=ODBC ";
               		$str.="--MINVERSION=\"$PROCSHTVERSION\"" if $CONFIG{DONT_SKIPPROCS}==1;

               		#_statusmsg(  $str,"\n" );
               		#system($str);
               		my(%subargs);
		      			$subargs{-showinwin}="Stored Procedure Installer" if defined $args{-name};
		      			busy();
		               my($rc,$outptr)=RunCommand(
               			-cmd=>$str,
               			-mainwindow=>$XMLDATA{mainWindow},
               			-ignore_string=>"^Installing",
      						-statusfunc=>\&_statusmsg,
               			-printhdr  => "[installer] : ",
               			-printfunc =>\&_statusmsg,
               			%subargs
               		);
							unbusy();
               		if( ! defined $args{-name} ){
	              			foreach( @$outptr and !$UPGRADE) {
	              				if( /^\* Cant connect to/ ) { #or /^Skipping installation as current version/ ) {}
	              					my($errmsg)=join("\n",@$outptr);
	              					last if $errmsg=~/Skipping installation as current version/;
	              					_statusmsg("[func] Please Select 'OK' on the pop-up Results display\n");
				  						my_rotext_box(
				  							-title => "Stored Procedure Installer Error",
				  							-text => $errmsg );
				  						_statusmsg("[func] GEM Ready / Continuing...\n");
	
	
				  						#$XMLDATA{mainWindow}->messageBox(
				  						#-title => "$_",
				  						#-message => $errmsg,
				  						#-type => "OK" );
				  						last;
									}
								}
							}
              		}
         	}
	}

	_statusmsg("[func] Done With Stored Procedure Library Installation\n\n" );
	$print_stdout = $sv_print_stdout;
	return "" if defined $args{-name};

	set_install_state( "FINAL_INSTALL_PROCS" );
	colorize_install_buttons();
	_statusmsg("[configure] GEM Ready / Continuing...\n");
	return "";
}

# -title, -label (optional), -text
sub my_rotext_box {
	my(%args)=@_;

	my $d = $XMLDATA{mainWindow}->DialogBox(-title=>$args{-title}, -buttons=>["OK"]);
	$d->add('Frame')->grid;
	my $frame = $d->Subwidget('frame');
	$args{-label} = $args{-title} unless $args{-label};

	$frame->Label(-font=>"large",-text=>$args{-label}, -justify=>'left', -bg=>'beige',
		-relief=>'groove')->pack(-side=>'top', -fill=>'x');

	my($textObject) = $frame->Scrolled("ROText",
			-relief => "sunken",
			-wrap=>'none',
			-bg=>'white',
			-scrollbars => "osoe")->pack(-side=>'bottom', -fill=>'x', -expand=>1);
	#$textObject->tagConfigure('header',-font=>'Ariel 14 bold', -foreground=>'blue');
	#$textObject->tagConfigure('listitem',-font=>'Ariel 12 bold');
	$textObject->tagConfigure('normal',-font=>'Ariel 12');
	$textObject->insert("end",$args{-text},"normal");
	$d->Show();
}

sub do_connect_all {
	my(%args)=@_;
	my(@errs);
	_statusmsg("[func] do_connect_all()\n" );
	foreach my $type ( get_server_types() ) {
		next if  ! is_nt() and ($type eq "sqlsvr" or $type eq "win32servers");
			
		push @errs, "[func] *** CONNECTING TO TYPE $type\n";
		push @errs, "\n";
		my($numok)=0;
		foreach my $svr ( get_server_by_type(-type=>$type ) ) {
			_statusmsg("[func] do_connect_all() Connecting To $svr\n" );
			my($c,$errmsg)=$connectionmanager->connect(-type=>$type, -name=>$svr);
			main::colorize_tree($svr);
			if( defined $c ) {
				push @errs,"[func]  $svr : Connected\n" if $DEBUG;
				$numok++;
				_statusmsg("[func]  $svr : Connected\n" );
			} else {					
				# RECONNECT IF UNIX/LINUX - STEP DOWN FROM SSH CONNECTION TO RSH or FTP
				
		 		if( ! $c and  $type eq "unix" ) {					 			
			 		# CASE #1 : target=unix and is_nt() and not FTP
			 		if( is_nt() and  $server_info{$svr.":".$type}{WIN32_COMM_METHOD} ne "FTP" ) {
			 			_statusmsg("[func] Reconnecting using FTP\n");	 	
						($c,$errmsg)=$connectionmanager->connect(
							-type				=>	$type, 
							-name				=>	$svr,
							#-login			=>	$dat{login}, 
							#-password		=>	$dat{password}, 						
							-win32method	=> "FTP");						
						if( $c ) {
							 $server_info{$svr.":".$type}{WIN32_COMM_METHOD}="FTP";
							 push @errs,"[func] RESETTING $svr Windows Connection Type To FTP \n";		
							_statusmsg("[func] RESETTING $svr Windows Connection Type To FTP \n");		
						} 										
					}		
		 			
		 			# CASE #2 : target=unix and ! is_nt() 
					if( ! $c and ! is_nt() ) {				
						# CASE #2a : IF SSH THEN TRY RSH
			 			if( $server_info{$svr.":".$type}{UNIX_COMM_METHOD} eq "SSH" ) {
			 				_statusmsg("[func] Reconnecting using RSH\n");
							($c,$errmsg)=$connectionmanager->connect(
								-type				=>	$type, 
								-name				=>	$svr,
								#-login			=>	$dat{login}, 
								#-password		=>	$dat{password}, 
								-unixmethod		=> "RSH"				);
							if( $c ) {
								$server_info{$svr.":".$type}{UNIX_COMM_METHOD}="RSH";
								push @errs,"[func] RESETTING $svr Unix Connection Type To RSH \n";		
								_statusmsg("[func] RESETTING $svr Unix Connection Type To RSH \n");		
							} 
						}
						
						if( ! $c) {		
							_statusmsg("[func] Reconnecting using FTP\n");	 	
							($c,$errmsg)=$connectionmanager->connect(
								-type				=>	$type, 
								-name				=>	$svr,
								#-login			=>	$dat{login}, 
								#-password		=>	$dat{password}, 
								-unixmethod		=> "FTP");
							if( $c ) {						
								$server_info{$svr.":".$type}{UNIX_COMM_METHOD}="FTP";
								push @errs,"[func] RESETTING $svr Unix Connection Type To FTP \n";		
								_statusmsg("[func] RESETTING $svr Unix Connection Type To FTP \n");		
							} 
						}						
					}
				}
				if( ! $c ) {
					push @errs,"[func]  $svr : Not Connected: $errmsg\n" ;
					_statusmsg("[func]  $svr : Not Connected: $errmsg\n" );
				}				
 			}
		}
		push @errs, "Successful Connection to $numok Servers\n";
		push @errs, "\n";
	}

	if( $#errs>=0) {
		unshift @errs,"The following connection errors were found:\n\n";
		_statusmsg("[func] Please Select 'OK' on the pop-up Results display\n");
		my_rotext_box(-title=>'Connection Errors', -label=>'Connect To All' , -text=>join("",@errs));
		_statusmsg("[func] GEM Ready / Continuing...\n");
		#$XMLDATA{mainWindow}->messageBox(
		#		-title => "Connection Error",
		#		-message => join("",@errs),
		#	 	-type => "OK" );
	}
	set_install_state( 'CONNECT_TO_ALL' );
	return;
}

sub set_install_state {
	my($key,$unsetflag)=@_;
	my($t) = $gem_data->SetInstallState($key,$unsetflag);
	if( ! $UPGRADE ) {
		# might not be defined if u are doing an upgrade			
		if( $key eq "CONNECT_SURVEY" ) {
			mydie("NO CONNECT SURVEY") unless defined $graphics_object{CONNECT_SURVEY};
			$graphics_object{FINAL_SURVEY}->configure(-bg=>"white");
		} elsif( $key eq "FINAL_INSTALL_PROCS" ) {
			mydie("NO FINAL_INSTALL_PROCS") unless defined $graphics_object{FINAL_INSTALL_PROCS};
			$graphics_object{CONNECT_INSTALL_PROCS}->configure(-bg=>"white");
		} elsif( $key eq "CONNECT_TO_ALL" ) {
			mydie("NO CONNECT SURVEY") unless defined $graphics_object{CONNECT_TO_ALL};
			$graphics_object{CONNECT_TO_ALL}->configure(-bg=>"white");
		} elsif( $key eq "CONNECT_INSTALL_PROCS" ) {
			mydie("NO CONNECT SURVEY") unless defined $graphics_object{CONNECT_INSTALL_PROCS};
			$graphics_object{FINAL_INSTALL_PROCS}->configure(-bg=>"white");
		} elsif( $key eq "FINAL_INSTALL_SOFTWARE" ) {
			$gem_data->SetInstallState("FINAL_REFORMAT_CODE",$unsetflag);
			$gem_data->SetInstallState("FINAL_CREATE_BATCHES",$unsetflag);
			$gem_data->SetInstallState("FINAL_INSTALL_BACKUP_MGR",$unsetflag);
			$gem_data->SetInstallState("FINAL_BUILD_CONSOLE",$unsetflag);
		} else {
			if( 	! $gem_data->get_install_state('FINAL_INSTALL_SOFTWARE')
			and 	$gem_data->get_install_state('FINAL_REFORMAT_CODE')
			and 	$gem_data->get_install_state('FINAL_CREATE_BATCHES')
			# and 	$gem_data->get_install_state('FINAL_INSTALL_PROCS')
			and 	$gem_data->get_install_state('FINAL_INSTALL_BACKUP_MGR')
			and 	$gem_data->get_install_state('FINAL_BUILD_CONSOLE')) {
				$gem_data->SetInstallState("FINAL_INSTALL_SOFTWARE",$unsetflag);
				$graphics_object{FINAL_INSTALL_SOFTWARE}->configure(-bg=>"white");
			}

			if( defined $graphics_object{FINAL_INSTALL_SOFTWARE} ) {
				$graphics_object{FINAL_INSTALL_SOFTWARE}->configure(-bg=>"white")
					if $key eq "FINAL_INSTALL_SOFTWARE";
				$graphics_object{FINAL_REFORMAT_CODE}->configure(-bg=>"white")
					if $key eq "FINAL_REFORMAT_CODE"
					or $key eq "FINAL_INSTALL_SOFTWARE";
				$graphics_object{FINAL_CREATE_BATCHES}->configure(-bg=>"white")
					if $key eq "FINAL_CREATE_BATCHES"
					or $key eq "FINAL_INSTALL_SOFTWARE";
				$graphics_object{FINAL_INSTALL_BACKUP_MGR}->configure(-bg=>"white")
					if $key eq "FINAL_INSTALL_BACKUP_MGR"
					or $key eq "FINAL_INSTALL_SOFTWARE";
				$graphics_object{FINAL_BUILD_CONSOLE}->configure(-bg=>"white")
					if $key eq "FINAL_BUILD_CONSOLE"
					or $key eq "FINAL_INSTALL_SOFTWARE";
			}
		}

		if( $key eq "ALARM_INSTALL_UPDATE" ) {
			foreach ( keys %graphics_object ) {
				$graphics_object{$_}->configure(-bg=>"white") if /^ALARM/ and $_ ne "ALARM_VALIDATE_ALARM";
			}
		}

		$graphics_object{$key}->configure(-bg=>"white") if defined $graphics_object{$key};# and $key=~/^Console/;
		$graphics_object{SAVE_CONFIGURATION_CHANGES}->configure(-bg=>"pink")
			if defined $graphics_object{$key};

		# turn off Final_ matching Console_
		if( $key =~ /Console/ ) {
			$key=~s/Console/Final/;
			$graphics_object{$key}->configure(-bg=>"white") if defined $graphics_object{$key};;
		}
	}

	return $t;
}

sub do_survey_all {
	my(%args)=@_;

	my(@errs);

	my($sv_print_stdout )= $print_stdout;
	$print_stdout = "TRUE";
	foreach my $stype ( get_server_types() ) {
		next if  ! is_nt() and ($stype eq "sqlsvr" or $stype eq "win32servers");
		my(%a);		
		for my $i (1..9) { 
			next unless $CONFIG{"SYBASE".$i};		
			$CONFIG{"SYBASE".$i} =~ s/\s+$//;
			$a{"SYBASE".$i}	= $CONFIG{"SYBASE".$i}; 
		}		
				
		foreach my $svr ( get_server_by_type( -type=>$stype, -sorted=>1 ) ) {			
			if( $stype eq "unix" ) {
				# deal with ftp timeouts
				#busy();
				_statusmsg("[func] Conducting Survey of $svr \n" );
				push @errs,"[func] Conducting Survey of $svr \n";							
				
				my($c,$errmsg)=$connectionmanager->connect(-type=>$stype, -name=>$svr, -reconnect=>1);
				if( ! defined $c ) {
					_statusmsg("[func] Displaying message box - select a button to continue\n");
					unbusy();
					$XMLDATA{mainWindow}->messageBox(
						-title => "Failed To Connect to $svr",
						-message => "Failed To Connect to $svr\n$errmsg",
						-type => "OK" );
					busy();					
					_statusmsg("[func] GEM Ready / Continuing...\n");						
				} else {		
						
						my(@rc2)=$gem_data->survey(
							-type					=> $stype,
							-name					=> $svr,							
							-since				=> (time-3600)*$CONFIG{dont_skip},
							-unixmethod			=> $server_info{$svr.":".$stype}{UNIX_COMM_METHOD},
							-win32method		=> $server_info{$svr.":".$stype}{WIN32_COMM_METHOD},
							-login				=> $server_info{$svr.":".$stype}{login},
							-password			=> $server_info{$svr.":".$stype}{password},
							-sybase_dirs		=>	$server_info{$svr.":".$stype}{SYBASE}||"",
							-sybase_client_dirs=>$server_info{$svr.":".$stype}{SYBASE_CLIENT}||"",
							-ignore_runfile	=> $server_info{$svr.":".$stype}{IGNORE_RUNFILE}||"",
							-local_directory 	=> $XMLDATA{gem_root_directory}.'/data/system_information_data',					
							-connection			=> $c,							
							%a );
							
						foreach ( @rc2 ) {
							push @errs,$_;
							chomp;															
							if( /Directory Exists : (.+)$/ ) {
								_statusmsg("[func] (2) $svr $stype Adding SYBASE Directory $1\n");
								$NEEDS_REFRESH{Files}=1;
								$server_info{$svr.":".$stype}{SYBASE}.=" " if $server_info{$svr.":".$stype}{SYBASE};
								$server_info{$svr.":".$stype}{SYBASE}.=$1;	
								
								# add $d to xml data too
								my(%fileinfo);
								$fileinfo{filepath}=		$1;
								$fileinfo{filepath_fromlocal}=	"";
								$fileinfo{filetype}=		"Sybase Server \$SYBASE Directory";
								$fileinfo{is_local}=0;
								$fileinfo{frequency}=		"Hourly";
								$fileinfo{hostname}=		$svr;
								$fileinfo{hosttype}=		"unix";
								global_data(-class=>'fileinfo',-name=>$svr.":".$1,-value=>\%fileinfo);
							}
						}						
								
#				========
#				my(@rc2)= $gem_data->survey(
#					-type				=> $stype,
#					-name				=> $svr,
#					-since			=> (time-3600)*$CONFIG{dont_skip},
#					-unixmethod		=> $server_info{$svr.":".$stype}{UNIX_COMM_METHOD},
#					-win32method	=> $server_info{$svr.":".$stype}{WIN32_COMM_METHOD},					
#					-login			=> $server_info{$svr.":".$stype}{login},
#					-password		=> $server_info{$svr.":".$stype}{password},
#					-sybase_dirs	=>	$server_info{$svr.":".$stype}{SYBASE}||"",
#					-sybase_client_dirs	=>	$server_info{$svr.":".$stype}{SYBASE_CLIENT}||"",
#					-ignore_runfile	=> 	$server_info{$svr.":".$stype}{IGNORE_RUNFILE}||"",
#					-local_directory 	=> $XMLDATA{gem_root_directory}."/data/system_information_data",
#					%a
#				);
#				
#				foreach ( @rc2 ) {
#					if( /Searching Unknown Sybase Directory/ ) {
#						chomp;
#						print "DBG DBG: HEY $_\n";
#					}
#				}
#							
				#unbusy();
				_statusmsg("[func] $stype Survey of $svr Completed\n" );
				push @errs, "[func] $stype Survey of $svr Completed\n";
				}
				#_statusmsg("[func]  Survey of $svr Returned:\n".join("\n",@rc)."\n" );
			} else {
				my($c,$errmsg)=$connectionmanager->connect(-type=>$stype, -name=>$svr);
				main::colorize_tree($svr);
				push @errs,"[func]  Surveying $stype server $svr\n";
				if( defined $c ) {
					#busy();
					_statusmsg("[func]  Conducting Survey of $svr \n" );
					push @errs,"[func]  Conducting Survey of $svr \n";
					my(@rc2) = $gem_data->survey(-type=>$stype,
						-name=>$svr, 
						-connection=>$c,
						-since=>(time-3600)*$CONFIG{dont_skip},				
						-local_directory => $XMLDATA{gem_root_directory}.'/data/system_information_data',
						%a );						
						
					foreach ( @rc2 ) {
						push @errs,$_;
						chomp;															
						if( /Directory Exists : (.+)$/ ) {
							_statusmsg("[func] (1) $svr $stype Adding SYBASE Directory $1\n");
							$NEEDS_REFRESH{Files}=1;
							$server_info{$svr.":".$stype}{SYBASE}.=" " if $server_info{$svr.":".$stype}{SYBASE};
							$server_info{$svr.":".$stype}{SYBASE}.=$1;		
							
							# add $d to xml data too
							my(%fileinfo);
							$fileinfo{filepath}=		$1;
							$fileinfo{filepath_fromlocal}=	"";
							$fileinfo{filetype}=		"Sybase Server \$SYBASE Directory";
							$fileinfo{is_local}=0;
							$fileinfo{frequency}=		"Hourly";
							$fileinfo{hostname}=		$svr;
							$fileinfo{hosttype}=		"unix";
							global_data(-class=>'fileinfo',-name=>$svr.":".$1,-value=>\%fileinfo);												
						}
					}						
						
					if( $stype eq "sybase" ) {
						$server_info{$svr.":sybase"}{backupservername}=global_data( -class=>"sybase",-name=>$svr, -arg=>'Backupserver_Name');						
					} elsif( $stype eq "sqlsvr" ) {
						$server_info{$svr.":sqlsvr"}{hostname}
							=global_data( -class=>"sqlsvr",-name=>$svr, -arg=>'Host_Name');						
					}
					#unbusy();
					push @errs, "[func]  $stype Survey of $svr Completed\n";
					#_statusmsg("[func]  Survey of $svr Returned:\n".join("\n",@rc)."\n" );
				} else {
					_statusmsg("[func]  $svr : Not Connected: $errmsg\n" );
					push @errs,"[func]  Unable to connect to $stype server $svr: $errmsg\n";
				}
			}
		}
	}
	$NEEDS_REFRESH{Files}=1;
	_statusmsg("[func] Please Select 'OK' on the pop-up Results display\n");
	my_rotext_box(-title=>'Survey Results', -label=>'Survey All' , -text=>join("",@errs));
	_statusmsg("[func] GEM Ready / Continuing...\n");

	set_install_state( 'CONNECT_SURVEY' );
	$print_stdout=$sv_print_stdout;
}

sub gem_uses_type {
	my($type)=@_;
	if( $XMLDATA{installtype} eq "UNIX" ) {
		return 0 if $type eq "win32servers" or $type eq "sqlsvr";
	} elsif( $XMLDATA{installtype} eq "WINDOWS" ) {
		return 0 if $type eq "unix";
	}
	return 1;
}

sub main_tab_paint {
	my($mainTab,$hosttype_arg,$view)=@_;	
	my($do_survey,$do_procs);
	if( ! defined $view ) {
		$do_procs=1;
		$do_survey=1;
		$NEEDS_REFRESH{Connect} 		= 0;
		$NEEDS_REFRESH{ProcLibInstall}= 0;	
	} elsif( $view eq "survey" ) {
		$do_survey=1;
		$NEEDS_REFRESH{Connect} 		= 0;
	} elsif( $view eq "procs" ) {
		$do_procs=1;
		$NEEDS_REFRESH{ProcLibInstall}= 0;	
	} else {
		die "Invalid args passed to main_tab_paint\n";
	}

	_statusmsg("[func] main_tab_paint($hosttype_arg,$view)\n" );
	if( ! Tk::Exists($mainTab) ) {
		die("[func] Warning - $mainTab is not a Tk Object\n" );
		return;
	}
	
	$configure_tab_hosttype = $hosttype_arg;
	
	# THE NEXT LINE - THE DESTROY - SOMETIMES APPEARS TO CAUSE tk::Ballon to have problems
	# i dunno why - the object exists and is a tk object... and the errors are random so im so confused
	# perhaps its because im trying to repaint / destroy the main frame from a button within the frame???
	$procs_frame_sv{$mainTab}->destroy()
		if defined $procs_frame_sv{$mainTab} and Tk::Exists($procs_frame_sv{$mainTab});
	$procs_frame_sv{$mainTab}=$mainTab->Scrolled(qw/Pane -scrollbars osoe -gridded xy -sticky nw/)->pack(qw/-expand 1 -side top -anchor w -fill both/);
	my($procs_frame)=$procs_frame_sv{$mainTab};
	my($row)=1;

#	if( ! Tk::Exists($procs_frame)) {
#		print "Line=",__LINE__,"\n";
#		print "DBG DBG:WOAH: procs_frame is NOT a Tk Widget";
#		print "DBG DBG:",Dumper $procs_frame;
#		confess("DBG DBG:");
#	}
	
	my(@ht);
	if($hosttype_arg eq "all" ) {
		if( $do_survey ) {
			@ht=("unix","win32servers","sybase","sqlsvr","oracle");
		} else {
			@ht=("sybase","sqlsvr");
		}
	} else {
		push @ht,$hosttype_arg;
	}
			
	foreach my $hosttype (@ht) {
		my($col)=1;
		my($hostaryptr) = get_svrlist_ptr($hosttype);
		
		if( $#$hostaryptr<0 ) {
			$procs_frame->Label(-text=>"No Servers of type $hosttype have been Registered",-font=>'smbold',
				-bg=>"pink",-relief=>'groove')->grid(-row=>$row++,-columnspan=>6,-column=>$col++,-sticky=>'ew')
					if gem_uses_type($hosttype);
			next;
		}
		
		$procs_frame->Label(-text=>"Hosttype",-font=>'smbold',
			-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
		if( $hosttype eq "unix" or $hosttype eq "win32servers" ) {
			$procs_frame->Label(-text=>"Hostname",-font=>'smbold',
				-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
		} else {
			$procs_frame->Label(-text=>"Server",-font=>'smbold',
				-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
		}
		
		if( $do_survey ) {
			$procs_frame->Label(-text=>"Last Survey",-font=>'smbold',
					-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
			$procs_frame->Label(-text=>"Run Survey",-font=>'smbold',
					-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');			
			$procs_frame->Label(-text=>"Connection Info",-font=>'smbold',
					-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
			$procs_frame->Label(-text=>"Connect",-font=>'smbold',
					-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');				
		} else {
			if( $hosttype eq "sybase" or $hosttype eq "sqlsvr" ) {
					$procs_frame->Label(-text=>"Lib Version",-font=>'smbold',
						-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
					$procs_frame->Label(-text=>"Install Library",-font=>'smbold',
						-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
					$procs_frame->Label(-text=>"Srvr Version",-font=>'smbold',
						-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
			}
		}

		$procs_frame->Label(-text=>"Edit Definition",-font=>'smbold',
			-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
 			#unless $hosttype eq "win32servers";
 		$procs_frame->Label(-text=>"UnxComm",-font=>'smbold',-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				if $hosttype eq "unix" and $do_survey;
		$procs_frame->Label(-text=>"WinComm",-font=>'smbold',-bg=>"beige",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew')
				if $hosttype eq "unix" and $do_survey;			
		$row++;

		#
		# BODY
		#
		$col=0;
		my(@iargs) = qw/-ipadx 2 -ipady 2 -padx 2 -pady 2 -sticky ew/;
		foreach my $host ( sort (@$hostaryptr) ) {
			$col=1;
			my( $dathash ) = $server_info{$host.":$hosttype"};
			if( ! defined $dathash ) {
				print "WARNING: Server $host of type $hosttype Has been Deleted\n";
				print LOGFP "WARNING: Server $host of type $hosttype Has been Deleted\n"
					 unless defined $NOLOGFILE;
				next;
			}
			
			my($current_color, $Last_Survey_Time);
			if( $do_survey ) {
				my($t)=global_data( -class=>$hosttype,-name=>$host, -arg=>'Last_Survey_Time');				
				if( ! defined $t ) {
					$Last_Survey_Time = "Never";
					$current_color="yellow";
				} else {
					$Last_Survey_Time = do_time(-fmt=>'yyyy/mm/dd',-time=>$t);
					if(  time - $t > 3600*24*3 ) {
						$current_color="pink";
					} else {
						$current_color="white";
					}
				}			
			} else {
				$current_color="white";
			}
			
			$procs_frame->Label(-text=>$hosttype,-font=>'smbold',
					-bg=>"white",-relief=>'groove')->grid(-row=>$row,-column=>$col++,-sticky=>'ew');
			$procs_frame->Label(-text=>$host,
					-font=>'smbold',-bg=>"white",-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);
			
			if( $do_survey ) {
				$procs_frame->Label(-text=>$Last_Survey_Time,
					-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);
				dohelp (
					$procs_frame->Button(  -text=>'Survey',-background=>$current_color,-command=>sub {
						return if is_busy();
						
					my(%args)= (	
							-sybase_dirs			=>	$server_info{$host.":unix"}{SYBASE}				||	"",
							-sybase_client_dirs	=>	$server_info{$host.":unix"}{SYBASE_CLIENT}	||	"",
							-ignore_runfile		=> $server_info{$host.":unix"}{IGNORE_RUNFILE}	||	""
							) if $hosttype eq "unix";
							
					if( $hosttype eq "unix" ) {
						for my $i (1..9) {
							next unless $CONFIG{"SYBASE".$i};
							$CONFIG{"SYBASE".$i} =~ s/\s+$//;
							$args{"SYBASE".$i} = $CONFIG{"SYBASE".$i}; 
						}
					}
					
					_statusmsg("[func] Conducting Survey of $host \n" );				
					busy();
					
					my($c,$errmsg)=$connectionmanager->connect( -type=>$hosttype, -name=>$host, -reconnect=>1);
					if( ! defined $c ) {
						_statusmsg("[func] Displaying message box - select a button to continue\n");
						unbusy();
						$XMLDATA{mainWindow}->messageBox(
							-title => "Failed To Connect to $host",
							-message => "Failed To Connect to $host\n$errmsg",
							-type => "OK" );
						busy();
						_statusmsg("[func] GEM Ready / Continuing...\n");						
					} else {
						my(@rc)=$gem_data->survey(
							-type=>$hosttype,
							-unixmethod=>$server_info{$host.":".$hosttype}{UNIX_COMM_METHOD},
							-win32method=>$server_info{$host.":".$hosttype}{WIN32_COMM_METHOD},
							-name=>$host,							
							-connection=>$c,
							-login=>$server_info{$host.":".$hosttype}{login},
							-since=>(time-3600)*$CONFIG{dont_skip},
							-password=>$server_info{$host.":".$hosttype}{password},
							
							-sybase_dirs	=>	$server_info{$host.":".$hosttype}{SYBASE}||"",
							-sybase_client_dirs	=>	$server_info{$host.":".$hosttype}{SYBASE_CLIENT}||"",
							-ignore_runfile	=> 	$server_info{$host.":".$hosttype}{IGNORE_RUNFILE}||"",
					
							-local_directory => $XMLDATA{gem_root_directory}.'/data/system_information_data',
							%args );
							
						foreach ( @rc ) {
							chomp;															
							if( /Directory Exists : (.+)$/ ) {
								_statusmsg("[func] (3) $host $hosttype Adding SYBASE Directory $1\n");								
								$server_info{$host.":".$hosttype}{SYBASE}.=" " if $server_info{$host.":".$hosttype}{SYBASE};
								$server_info{$host.":".$hosttype}{SYBASE}.=$1;			
								
								# add $d to xml data too
								my(%fileinfo);
								$fileinfo{filepath}=		$1;
								$fileinfo{filepath_fromlocal}=	"";
								$fileinfo{filetype}=		"Sybase Server \$SYBASE Directory";
								$fileinfo{is_local}=0;
								$fileinfo{frequency}=		"Hourly";
								$fileinfo{hostname}=		$host;
								$fileinfo{hosttype}=		$hosttype;
								global_data(-class=>'fileinfo',-name=>$host.":".$1,-value=>\%fileinfo);											
							}
						}
						
						$NEEDS_REFRESH{Files}=1;
						_statusmsg("[func] Completed Survey of $host\n" );				
					}
					
					unbusy();
	
					#$XMLDATA{mainWindow}->messageBox(
					#	-title => "Survey Results For On $host",
					#	-message => join("",@rc),
					#	-type => "OK" );
	
					main_tab_paint($mainTab,$hosttype_arg,$view);
					_statusmsg( "[setup] GEM Ready For Input\n" );
	
				} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
					"Survey This Server - Last Survey : $Last_Survey_Time" );
			}
	
			#$t=global_data( -class=>$hosttype,-name=>$host, -arg=>'Last_Ok_Conn_Time');
			#if( ! defined $t ) {
			#	$current_color = "yellow";
			#} elsif( time - $t > 3600*24*3 ) {
			#	$current_color = "pink";
			#} else {
			#	$current_color = "white";
			#}

			my($cstat);
			if( $do_survey ) {
				$cstat=$connectionmanager->connection_status(-name=>$host, -type=>$hosttype);
				if( $cstat eq "Ok" ) {
					$current_color="white";
				} else {
					$current_color="yellow";
				}
				$procs_frame->Label(-text=>$cstat, -font=>'smbold', -bg=>$current_color, -relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);
	
				dohelp (
						$procs_frame->Button(  -text=>'Connect',-background=>$current_color,-command=>sub {
						return if is_busy();
						my($c,$errmsg)=$connectionmanager->connect(-type=>$hosttype, -name=>$host);
						main::colorize_tree($host);
						if( defined $c ) {
							_statusmsg("[func] Displaying message box - select a button to continue\n");
							$XMLDATA{mainWindow}->messageBox(
								-title => "Ok",
								-message => "Successful Connection To $host: $errmsg",
								-type => "OK" );
							_statusmsg("[func] GEM Ready / Continuing...\n");
						} else {
							_statusmsg("[func] Displaying message box - select a button to continue\n");
							$XMLDATA{mainWindow}->messageBox(
								-title => "Failed To Connect to $host",
								-message => "Failed To Connect to $host\n$errmsg",
								-type => "OK" );
							_statusmsg("[func] GEM Ready / Continuing...\n");
						}
						busy();
						main_tab_paint($mainTab,$hosttype_arg,$view);
						unbusy();
						_statusmsg( "[setup] GEM Ready For Input\n" );
				} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
					"Connect To This Server : $cstat" );
			}

			$current_color="white";

			#if( $hosttype eq "sybase" or $hosttype eq "sqlsvr" ) {
				if(  $do_procs ) {
           			my($Proc_Lib_Version) = global_data(	-class=>$hosttype, -name=>$host, -arg=>"Proc_Lib_Version");
           			$Proc_Lib_Version = "N.A." unless defined $Proc_Lib_Version;
           			$procs_frame->Label(-text=>$Proc_Lib_Version,
           				-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);
           			dohelp (
           				$procs_frame->Button(  -text=>'Install Library',
           					-background=>$current_color,-command=>sub {
           						return if is_busy();
	           					do_installprocs(-type=>$hosttype, -name=>$host);           					
	           					main_tab_paint($mainTab,$hosttype_arg,$view);
	           					_statusmsg( "[setup] GEM Ready For Input\n" );
           			} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
           				"Install Procedures For This Server : $cstat" );
           				
						# print server version
						my($Server_Version) = global_data(	-class=>$hosttype, -name=>$host, -arg=>"Server_Version");
						$Server_Version = "N.A." unless defined $Server_Version;
						$procs_frame->Label(-text=>$Server_Version,
							-font=>'smbold',-bg=>$current_color,-relief=>'groove')->grid(-row=>$row,-column=>$col++,@iargs);
				}
			#}

			# new addition
			dohelp (
			$procs_frame->Button(  -text=>'Edit Definition',-background=>$current_color,-command=>sub {
				return if is_busy();
				edit_definition_box($hosttype, $host,  $server_info{$host.":$hosttype"});
				#_statusmsg("[func] Redrawing Table\n");	
				#ServerTab_draw($mainTab,$hosttype);
				_statusmsg("[func] Edit Definition Completed\n");
				
#				return if is_busy();
#				my( $dathash ) = $server_info{$host.":$hosttype"};
#				if( ! defined $dathash ) {
#					print "WARNING: Server $host of type $hosttype Has been Deleted\n";
#					print LOGFP "WARNING: Server $host of type $hosttype Has been Deleted\n"
#					 	unless defined $NOLOGFILE;
#					next;
#				}
#				my( %dat ) = %$dathash;
#
#				my $d= $XMLDATA{mainWindow}->DialogBox(	-title =>'Enter Sybase Server Information',-buttons => [ "Ok", "Cancel" ] );
#				my(%dlg_svrvar);
#				$dlg_svrvar{'Server Name'} 	= $host;
#				$dlg_svrvar{'Admin Login'} 	= $dat{login};
#				$dlg_svrvar{'Admin Password'} 	= $dat{password};
#				dialog_addvar($d,\%dlg_svrvar,"Server Name",undef,	1,"label");
#				dialog_addvar($d,\%dlg_svrvar,"Admin Login",undef,	2,undef);
#				dialog_addvar($d,\%dlg_svrvar,"Admin Password",undef,	3,"password");
#				if( $d->Show() eq "Ok" ) {
#					print "Saving Changes to login $dat{login} and Redrawing\n";
#					print LOGFP "Saving Changes to login $dat{login} and Redrawing\n"
#						 unless defined $NOLOGFILE;
#					$server_info{$host.":".$hosttype}{login} = $dlg_svrvar{'Admin Login'};
#					$server_info{$host.":".$hosttype}{password}  = $dlg_svrvar{'Admin Password'};
#
#					main_tab_paint($mainTab,$hosttype_arg,$view);
#					_statusmsg("[func] Password Change Completed\n");
#				}
			} )->grid(-column=>$col++,-row=>$row,-sticky=>'ew',-padx=>2),
				"Edit Server Setup") unless $hosttype eq "win32servers";
			# end new addition

			# addition of comm method for unix 			
			if( $hosttype eq "unix") {
				# unixcomm
				my($x)=my_label( $procs_frame, \$server_info{$host.":".$hosttype}{UNIX_COMM_METHOD},  undef,undef,$current_color,  "RSH","SSH","FTP","NONE" );
				dohelp(
				$x->grid(-row=>$row,-column=>$col++,@iargs),
				"Is unix rsh/ssh to $host enabled?\nWill be used for disk space monitoring on unix.");
				
				#win32 comm
				my($y)=my_label( $procs_frame, \$server_info{$host.":".$hosttype}{WIN32_COMM_METHOD},  undef,undef,$current_color,  "FTP","NONE" );
				dohelp(
				$y->grid(-row=>$row,-column=>$col++,@iargs),
				"If you are a windows program, how will you talk to $host.\nTechnically, this can be RSH/SSH as well as FTP/NONE but thats not default.\nSet by hand in the config file.");
			}			
			
			dohelp (
					$procs_frame->Button(  -text=>'Deregister',-background=>"white",-command=>sub {
							return if is_busy();
							do_delete_server($host,$hosttype);
							#my($hostaryptr)=get_svrlist_ptr($hosttype);
							#ServerTab_draw($mainTab,$hosttype);
							#filetab_create() if $hosttype eq "unix";
							main_tab_paint($mainTab,$hosttype_arg,$view);
							_statusmsg("[func] Deregister Server Process Completed\n");
					 })->grid(-column=>$col++,-row=>$row,-sticky=>'ew'),
						"Delete $host" );
			
			$row++;
		}
		
	}	
	
	_debugmsg( "[func] done main_tab_paint\n" );
}

sub do_isql_cmd {
	my($connection,$file)=@_;
	_statusmsg("[schema.sql] loading file $file\n");
	my($rc)=open(FILE95,$file);
	if( ! $rc ) {
		_statusmsg("[func] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
			-title => "Error With Sql Load",
			-message => "Cant read $file : $!\n",
			-type => "OK" );
		_statusmsg("[func] GEM Ready / Continuing...\n");
		return;
	}
	my($batchsql)="";
	my($errormsgs)="";
	my($batchid)=0;

	while(<FILE95>) {
		chomp;chomp;
		if(/^go/) {
			$batchid++;
			#print "Running Batch Number $batchid\n";
			_statusmsg("[schema] Running Batch Number $batchid\n");
			#_debugmsg("RUNNING $batchsql\n" );
			my(@rc) = $connectionmanager->query(
				-query=>$batchsql,
				-connection=>$connection,
				-db=>$CONFIG{ALARM_DATABASE} );

			foreach (@rc) {
				my(@d)=$connectionmanager->decode_row($_);
				my($rowdat)=join(" ",@d);
				next if $rowdat =~ /because it does not exist in the system catalog/;
				next if $rowdat =~ /because it doesn't exist in the system catalog/;

				#$XMLDATA{mainWindow}->messageBox(
				#	-title => "Error With Sql Load",
				#	-message => "Batch $batchsql\nError $rowdat\n",
				#	-type => "OK" );

				print "Batch $batchsql\nError $rowdat\n\n";
				print LOGFP "Batch $batchsql\nError $rowdat\n\n"
				 	unless defined $NOLOGFILE;
				#print "[schema.sql] ".$rowdat."\n";
			}
			$batchsql="";
		} else {
			$batchsql.=$_."\n";
		}
	}
	close(FILE95);
}


## -cmd
## -err
## -print
#sub run_cmd
#{
#   my(%OPT)=@_;
#   my(@rc);
#   chomp $OPT{-cmd};
#
#   $OPT{-cmd} .= " 2>&1 |" unless defined $XMLDATA{isnt};
#   $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;
#
#   my $cmd_no_pass=$OPT{-cmd};
#   $cmd_no_pass=~s/-P\w+/-PXXX/;
#   print $cmd_no_pass."\n" if  defined $OPT{-print};
#   $OPT{-cmd} .= " |" 		    if defined $XMLDATA{isnt};
#
#	my(@data);
#   open( SYS2STD,$OPT{-cmd} ) || logdie("Unable To Run $OPT{-cmd} : $!\n");
#   while ( <SYS2STD> ) {
#      chop;
#		push 		@data,$OPT{-status}." ".$_."\n";
#		print 	$OPT{-status}." ".$_."\n" if defined $OPT{-print};
#		shift 	@data if $#data>=4;
#		my($l) = join("",@data);
#		$statusmsgbar->configure(-text=>$l);
#      push @rc, $_;
#   }
#   close(SYS2STD);
#
#	my($cmd_rc)=$?;
#   if( $cmd_rc != 0 ) {
#		if( defined $OPT{-err} ) {
#				$XMLDATA{mainWindow}->messageBox(
#					-title => 'Command Failed',
#					-message => $OPT{-err}."Command Failed - return code=$cmd_rc\n".
#      							"Command: ".$OPT{-cmd}."\n".
#									"\n\nResults:\n".join("\n",@rc),
#					-type => "OK" ) if defined $OPT{-err};
#		}
#		return @rc;
#	}
#	set_status_label( undef, "Command completed at ".localtime(time)."\n");
#	return @rc;
#}

sub archive_file {
	my($from, $versions,$do_statusmsg)=@_;
	my($curdate)=do_time(-fmt=>'yyyymmdd_hhmiss');
	my($tofile)=$from.".".$curdate;
	if( ! $do_statusmsg ) {
		_statusmsg("[save] Archived $from\n" );
		_statusmsg("[save]      to $tofile\n" )if defined $DEBUG;
	}
	rename( $from , $tofile )
		or mydie("Cant rename $from to $tofile : $!\n");

	if( defined $versions ) {
		my($dir)=dirname($from);
		my($file)=basename($from);
		opendir(DIR,$dir)
			|| die("Can't open directory $dir for reading\n");
		my(@dirlist) = sort grep(/^$file\.\d\d\d\d\d\d\d\d/,readdir(DIR));
		closedir(DIR);

		if( $#dirlist>$versions ) {
			$#dirlist-=$versions;
			foreach (sort @dirlist) {
				_statusmsg("[save] Removing $dir/$_\n" ) unless $do_statusmsg;
				unlink $dir."/".$_;
			}
		}
		#print "DBG ARCHIVER - versions=$versions dir=$dir file=$file\n\t==>",join("\n\t==>",@dirlist),"\n";
	}
	return $tofile;
}

sub purge_config_dir {
	_statusmsg("[func] Purging Saved Configfiles\n" );

	my($cfg_file)="/configure.cfg";
	opendir(DIR,$XMLDATA{gem_configuration_directory})
		|| die("Can't open directory $XMLDATA{gem_configuration_directory} for reading\n");
	my(@dirlist) = sort grep(!/^\./,readdir(DIR));
	closedir(DIR);

	# purge old gem.xml files
	my(@pfiles)=grep(/^gem.xml.\d\d\d\d\d\d\d/,@dirlist);
	if( $#pfiles>10 ) {
		$#pfiles-=10;
		foreach (sort @pfiles) {
			_statusmsg("[func] Removing $XMLDATA{gem_configuration_directory}/$_\n" );
			unlink $XMLDATA{gem_configuration_directory}."/".$_;
		}
	}
	foreach my $spec ( 'unix_passwords.dat.\d\d\d\d',
			'sqlsvr_password.dat.\d\d\d\d',
			'sybase_password.dat.\d\d\d\d',
			'oracle_password.dat.\d\d\d\d',
			'pc_password.dat.\d\d\d\d' ){
        	# purge old gem.xml files
        	@pfiles=grep(/^$spec/,@dirlist);
        	if( $#pfiles>10 ) {
        		$#pfiles-=10;
        		foreach (sort @pfiles) {
        			_statusmsg("[func] Removing $XMLDATA{gem_configuration_directory}/$_\n" );
        			unlink $XMLDATA{gem_configuration_directory}."/".$_;
        		}
        	}
	}
}

my(%qs_pingstate);
my(%qs_ignorelist);	# key is $ty:$name
# Register_Server()
#
# REQUIRED ARGS (required only if the server type requires it - ignored otherwise)
#		-type => type of server
# 		-name => name of server
#		-host
#		-port
#		-disks
#		-is_prod
#
# OPTIONAL ARGS
#		-login
#		-password
#		-win32method
#		-unixmethod
#				
sub Register_Server {
	my(%args)=@_;
	
	if( ! $args{-host} and ( ! $args{-type} or $args{-type} eq "sybase" )) {
		# guess from the interfaces file
		_statusmsg("[register] Register_Server() - Guessing Host/Port\n");
		my($host,$port)=$gem_data->get_server_interfaces($args{-name});
		$args{-host}  = $host 		if $host;
		$args{-port}  = $port 		if $port;		
		$args{-type} = "sybase" 	if $host and ! $args{-type};	
	}
	
	# -type will only be undefined if called from the UnRegistered Server Tab
	if( ! $args{-type} ) {
		_statusmsg("[register] Register_Server() - Determining Server Type\n");
		$args{'Server Name'} = $args{-name};
		
		$args{'Host Name'}	= "";
		$args{'Server Group'}= "PRODUCTION";
		if( $args{-host} ) {			
			$args{'Server Type'} = "Sybase ASE Server on Unix";
			$args{'Host Name'}	= $args{-host};
		} else {
			$args{'Server Type'} = "Microsoft SQL Server on Windows";
			$args{'Host Name'}	= $args{-name};
		}
		
		if( tk_add_a_server_baseinfo( \%args ) ne "Ok" ) {
			_statusmsg("[register]  Exiting Server Registration based on user cancel\n");
			return 0;
		}				
		
		$args{-is_prod} = $args{'Server Group'};
		
		$args{-type} = "unix" 			if $args{"Server Type"} eq "Unix or Linux Server";
		$args{-type} = "win32servers" if $args{"Server Type"} eq "Windows Server";
		$args{-type} = "sybase" 		if $args{"Server Type"} eq "Sybase ASE Server on Unix";
		$args{-type} = "sybase" 		if $args{"Server Type"} eq "Sybase ASE Server on Windows";
		$args{-type} = "sqlsvr" 		if $args{"Server Type"} eq "Microsoft SQL Server on Windows";
		$args{-type} = "oracle" 		if $args{"Server Type"} eq "Oracle Server on Windows";
		$args{-type} = "oracle" 		if $args{"Server Type"} eq "Oracle Server on Unix";
		
		if( $args{'Host Name'} ) {
			Register_Server(-type=>"unix",-name=>$args{'Host Name'},-is_prod=>$args{-is_prod})
				 if $args{"Server Type"} eq "Sybase ASE Server on Unix";
			Register_Server(-type=>"win32servers",-name=>$args{'Host Name'},-is_prod=>$args{-is_prod})
				  if $args{"Server Type"} eq "Sybase ASE Server on Windows";
			Register_Server(-type=>"win32servers",-name=>$args{'Host Name'},-is_prod=>$args{-is_prod})
				  if $args{"Server Type"} eq "Microsoft SQL Server on Windows";
			Register_Server(-type=>"win32servers",-name=>$args{'Host Name'},-is_prod=>$args{-is_prod})
				  if $args{"Server Type"} eq "Oracle Server on Windows";
			Register_Server(-type=>"unix",-name=>$args{'Host Name'},-is_prod=>$args{-is_prod})
				  if $args{"Server Type"} eq "Oracle Server on Unix";
		}		
	}
			
	return if $qs_ignorelist{$args{-type}.":".$args{-name}};
	$qs_ignorelist{$args{-type}.":".$args{-name}}=1;	
	
	if( $args{-type} eq "oracle" ) {
		_statusmsg("[register] Sorry.  Oracle Services Delayed until V2.0\n");
		_statusmsg("[register] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
			-title => 'Data Validation Error',
			-message => "ERROR:  Oracle Server Management is Delayed until V2.0\nWe welcome working with you on our oracle solution!\n",
			-type => "OK" );
		_statusmsg("[register] GEM Ready / Continuing...\n");
		return 0;
	}
	
	my($str)="[register] Register_Server ";
	foreach (keys %args) { $str.=$_."=>".$args{$_}." " unless /^-password$/; }
	#$str.=" Ht=$args{-host}"  if defined $args{-host};
	#$str.=" Pt=$args{-port}"  if defined $args{-port};
	#_debugmsg("[register]\n");
	_statusmsg("[register] ************** REGISTER SERVER *************\n");
	_statusmsg("$str\n" );

	#
   # PASSWORD GUESSER!!!
	#
	# GO THROUGH ALL EXISTING PASSWORDS AND TEST THEM!
	my($unixtounix,$win32tounix);
	if( $args{-type} eq "unix" or $args{-type} eq "sqlsvr" or $args{-type} eq "sybase" ) {		
		my(@to_try_login);
		my(@to_try_password);
		my(@to_try_unixmeth);
		my(@to_try_ntmeth);
		
		if( $args{-login} ) {		# ok you passed it in 
			push @to_try_login, 			$args{-login};
			push @to_try_password, 		$args{-password};
			push @to_try_unixmeth, 		$XMLDATA{unixtounix};
			push @to_try_ntmeth, 		$XMLDATA{win32tounix};
			
			if( ! is_nt() and $args{-type} eq "unix" and $XMLDATA{unixtounix} eq "SSH") {
				push @to_try_login, 			$args{-login};
				push @to_try_password, 		$args{-password};
				push @to_try_unixmeth, 		"RSH";
				push @to_try_ntmeth, 		$XMLDATA{win32tounix};
				
				push @to_try_login, 			$args{-login};
				push @to_try_password, 		$args{-password};
				push @to_try_unixmeth, 		"FTP";
				push @to_try_ntmeth, 		$XMLDATA{win32tounix};
			}		
				
		} else {
			_statusmsg("[register] ... GUESSING LOGIN/PASSWORD FOR $args{-name}\n");
			my(%tried_logins);
			foreach ( keys %server_info ) {
				next unless /:$args{-type}$/;
				my( $dathash ) = $server_info{$_};
				next unless defined $dathash;
				my( %dat ) = %$dathash;						
				next if $tried_logins{$dat{login}.".".$dat{password}};
				$tried_logins{$dat{login}.".".$dat{password}} = 1;
				push @to_try_login, 			$dat{login};
				push @to_try_password, 		$dat{password};
				push @to_try_unixmeth, 		$XMLDATA{unixtounix};
				push @to_try_ntmeth, 		$XMLDATA{win32tounix};
				
				if( ! is_nt() and $args{-type} eq "unix" and $XMLDATA{unixtounix} eq "SSH") {
					push @to_try_login, 			$dat{login};
					push @to_try_password, 		$dat{password};
					push @to_try_unixmeth, 		"RSH";
					push @to_try_ntmeth, 		$XMLDATA{win32tounix};
					
					push @to_try_login, 			$dat{login};
					push @to_try_password, 		$dat{password};
					push @to_try_unixmeth, 		"FTP";
					push @to_try_ntmeth, 		$XMLDATA{win32tounix};
				}		
			}			
		}
		
		my($passcnt)=0;
		foreach ( @to_try_login ) {
			if( is_nt() ) {
				_statusmsg("[register] Testing Login $to_try_login[$passcnt] $to_try_password[$passcnt] method=$to_try_ntmeth[$passcnt] \n");
			} else {
				_statusmsg("[register] Testing Login $to_try_login[$passcnt] $to_try_password[$passcnt] method=$to_try_unixmeth[$passcnt] \n");
			}
			
			my( $c, $errmsg ) = $connectionmanager->connect(
				-type				=>	$args{-type}, 
				-name				=>	$args{-name},
				-login			=>	$to_try_login[$passcnt], 
				-password		=>	$to_try_password[$passcnt], 
				-quiet			=>	1,
				-win32method	=> $to_try_ntmeth[$passcnt],
				-unixmethod		=>	$to_try_unixmeth[$passcnt]	
 			);
 			
 			if( defined $c ) {
				_statusmsg("[register] Connected\n");	
				$args{-LOGINdefined}=1 if $args{-login};					
				$args{-login}			= $to_try_login[$passcnt]	;
				$args{-password}		= $to_try_password[$passcnt];
				$unixtounix				= $to_try_unixmeth[$passcnt];
				$win32tounix			= $to_try_ntmeth[$passcnt]	;
				
				$args{-ok}=1;
				last;			
			}
			
 			if( ! $c and $errmsg =~ /is not an ASE or SQL server$/ ) { # not a sql server/sybase! 
 				_statusmsg("[register] Not Registering $args{-name} Is not An ASE or SQL Server\n");
 				return 0;
 			}
 			_statusmsg("[register] Failed To Connect: $errmsg\n");						
 			$passcnt++;			
		}
		
#		my($unixtounix,$win32tounix)=($XMLDATA{unixtounix},$XMLDATA{win32tounix});
#		foreach ( keys %server_info ) {
#			next unless /:$args{-type}$/;
#			my( $dathash ) = $server_info{$_};
#			next unless defined $dathash;
#			my( %dat ) = %$dathash;						
#			next if $tried_logins{$dat{login}.".".$dat{password}};
#			$tried_logins{$dat{login}.".".$dat{password}} = 1;
#			_statusmsg("[register] Testing Login $dat{login} WinMethod=$win32tounix unixmethod=$unixtounix\n");			
#			_statusmsg("[register] $args{-name} : Testing Password Number ",$passcnt++,"...\n" );			
#			my($c,$errmsg)=$connectionmanager->connect(
#				-type				=>	$args{-type}, 
#				-name				=>	$args{-name},
#				-login			=>	$dat{login}, 
#				-password		=>	$dat{password}, 
#				-quiet			=>	1,
#				-win32method	=> $win32tounix,
#				-unixmethod		=>	$unixtounix	
# 			);
# 			
# 			if( ! $c and $errmsg =~ /is not an ASE or SQL server$/ ) { # not a sql server/sybase! 
# 				_statusmsg("[register] Not Registering $args{-name} Is not An ASE or SQL Server\n");
# 				return 0;
# 			}
#			
#			_statusmsg("[register] Unable to connect to $args{-name} - $errmsg\n") unless $c;
#			# RECONNECT IF UNIX/LINUX - STEP DOWN FROM SSH CONNECTION TO RSH or FTP
#	 		if( ! $c and  $args{-type} eq "unix" ) {
# 			 		
#		 		# CASE #1 : target=unix and is_nt() and not FTP
#		 		if( is_nt() and  $win32tounix ne "FTP" ) {
#		 			$win32tounix = "FTP";					
#	 				_statusmsg("[register] Reconnecting using FTP\n");	 	
#					($c,$errmsg)=$connectionmanager->connect(
#						-type				=>	$args{-type}, 
#						-name				=>	$args{-name},
#						-login			=>	$dat{login}, 
#						-password		=>	$dat{password}, 
#						-unixmethod		=> $unixtounix,
#						-win32method	=> "FTP");						
#					if( $c ) {
#						_statusmsg("[register] Unable to connect using SSH/RSH but successful FTP connection \n");		
#					} 										
#				}		
#	 			
#	 			# CASE #2 : target=unix and ! is_nt() 
#				if( ! $c and ! is_nt() ) {				
#					# CASE #2a : IF SSH THEN TRY RSH
#		 			if( $unixtounix eq "SSH" ) {
#		 				_statusmsg("[register] Reconnecting using RSH\n");
#						$unixtounix="RSH";
#						($c,$errmsg)=$connectionmanager->connect(
#							-type				=>	$args{-type}, 
#							-name				=>	$args{-name},
#							-login			=>	$dat{login}, 
#							-password		=>	$dat{password}, 
#							-unixmethod		=> "RSH",
#							-win32method	=> $win32tounix);
#						if( $c ) {
#							_statusmsg("[register] Unable to connect using SSH but successful RSH connection \n");		
#						} 
#					}
#					
#					if( $unixtounix eq "RSH" and ! $c) {		
#						$unixtounix = "FTP";
#						_statusmsg("[register] Reconnecting using FTP\n");	 	
#						($c,$errmsg)=$connectionmanager->connect(
#							-type				=>	$args{-type}, 
#							-name				=>	$args{-name},
#							-login			=>	$dat{login}, 
#							-password		=>	$dat{password}, 
#							-unixmethod		=> "FTP",
#							-win32method	=> $win32tounix);
#						if( $c ) {						
#							_statusmsg("[register] Unable to connect using SSH/RSH but successful FTP connection \n");		
#						} 
#					}						
#				}
#			}
# 				
#			if( defined $c ) {
#				$args{-login}	=$dat{login};
#				$args{-password}=$dat{password};
#				$args{-win32method}=$win32tounix;
#				$args{-unixmethod}=$unixtounix;
#				$args{-ok}=1;
#				last;			
#			}
#		}
		
		if( $args{-ok} ) {
			if( $args{-type} eq "unix" ) {
				_statusmsg("[register] Successfully Guessed The Password Method=$unixtounix/$win32tounix\n" );
			} else {
				_statusmsg("[register] Successfully Guessed The Password\n" );
			}				
		} else {
			_statusmsg("[register] Cant Guess $args{-type} Password - Enter it yourself\n" );
		}
		
		# GET MISSING INFORMATION IF YOU NEED IT
		if( $args{-type} eq "unix" ) {
			$args{"Host Name"} 		= $args{-name};
			$args{"Host Login"} 		= $args{-login} || "sybase";
			$args{"Host Password"} 	= $args{-password} || "";

			unbusy();
			if( ! $args{-ok} and tk_add_a_server_unixlp(\%args) ne "Ok" ) {
				_statusmsg("[register] Not Registering $args{-name} user cancel\n");
				busy();
				return 0;
			}
			
			if( ! $args{-ok} and $args{"Host Password"} =~ /^\s*$/ ) {
				_statusmsg("[register] Bad Password \n");
				busy();
				return 0;
			}
			busy();
			$args{-login}	=$args{"Host Login"};
			$args{-password}=$args{"Host Password"};
		} elsif( $args{-type} eq "sybase" or  $args{-type} eq "sqlsvr" ) {
			$args{"Server Name"} = $args{-name};
			$args{"DB Admin Login"} = $args{-login} || "sa";
			$args{"DB Admin Password"} = $args{-password} || "";
			# in Host Name, Host Login, Host Password
			unbusy();
			if( ! $args{-ok} and tk_add_a_server_sybaselp(\%args) ne "Ok" ) {
				_statusmsg("[register] Not Registering $args{-name} user cancel\n");
				busy();
				return 0;
			}
			busy();
			$args{-login}=$args{"DB Admin Login"};
			$args{-password}=$args{"DB Admin Password"};
		}
	} elsif( $args{-type} eq "win32servers" ) {
	} else {
		_statusmsg("[register] Displaying message box - select a button to continue\n");
		$XMLDATA{mainWindow}->messageBox(
				-title => 'Data Validation Error',
				-message => "ERROR: invalid server type $args{-type}.\n",
				-type => "OK" );
		_statusmsg("[register] GEM Ready / Continuing...\n");
		return;
	}
	
#	$str= "[register] Registering $args{-name} -type=>".$args{-type}."";
#	foreach ( keys %args ) {
#		next if $_ eq "-name" or $_ eq "-type" or $_ eq "-login" or /password/i or /^DB Admin/ or /^Host /;
#		if( ref $args{$_} ) {
#			$str.=" ".$_."=>".join(" ",@{$args{$_}});
#		} else {
#			$str.=" ".$_."=>".$args{$_};
#		}
#	}
#	_statusmsg($str."\n");

	if( ! $args{-is_prod} ) {
		$args{-is_prod} =  "PRODUCTION";
		$args{-is_prod} = "DEVELOPMENT" 	if $args{-name} =~ /dev/i;
		$args{-is_prod} = "QA" 				if $args{-name} =~ /qa/i;
		$args{-is_prod} = "PRODUCTION" 	if $args{-name} =~ /prod/i;
	}	
	
	_debugmsg("[register] Ready to Add Server\n");	
	my($rc)=add_a_server_to_gem($args{-type}, $args{-name}, 
			$args{-login}, $args{-password}, $args{-is_prod}, undef, $args{-disks}, $args{-host}, $args{-port},
			$unixtounix, $win32tounix	 );
						
	_statusmsg( "[register] Registration of $args{-name} Completed \n" );

#	if( $args{-type} eq "sqlsvr" or $args{-type} eq "sybase" ) {
#		my($isr)="n";
#		$isr = "y" if $args{-host} eq hostname() or $args{-type} eq "sqlsvr";
#
#		plan_add( name=>$plantab_plans_curval,
#			DSQUERY 	=> $args{'Server Name'},
#			REMOTE_HOST 	=> $args{-host},
#			REMOTE_DIR 	=> $args{'Local Directory Name (from db server) '},
#			LOCAL_DIR 	=> "",
#			MAIL_TO 	=> $XMLDATA{admin_email},
#			SUCCESS_MAIL_TO => $XMLDATA{admin_email},
#			IS_REMOTE	=> $isr );
#	}
	return $rc;	
}

# run net_view and osql -L and parse interfaces file
# ignore sybase servers on hosts/ports that have allready been registered
sub Add_Server_Wizard {
	my(@discovery_messages);	
	my($UPDATE,$REGISTER)=@_;
	busy();
	undef %qs_ignorelist;
	start_msg_logging("STARTING WIZARD");
	_statusmsg( "[func] Starting Add Server Wizard!\n" );
	$XMLDATA{mainWindow}->messageBox( -title => 'Wizard Starting',
			-message => "Wizard Started! 

This Wizard will register your servers for you, connecting using the passwords you have entered on previously

\"Cancel\" non database servers (Backup Servers, Rep Servers etc...) that show up

After databases (beige) we ask your system passwords (orange)",
			-type => "OK" );

	foreach my $ty ("unix","win32servers","sybase","oracle","sqlsvr") {
		foreach ( get_server_by_type(-type=>$ty) ) {
			$qs_ignorelist{$ty.":".$_}=1;		
		}
	}

	_statusmsg( "[func] Fetching Windows Servers\n" ) 	if is_nt();
	my(%winhosts)	= qs_run_net_view() 		if is_nt();

	_statusmsg( "[func] Fetching SQL Servers\n" ) 	if is_nt();
	my(@sqlservers)	= qs_run_osql() 		if is_nt();

	_statusmsg( "[func] Fetching DBI Dsn's\n" );
	my($dsn_list, %avail_dsns) = $connectionmanager->qs_get_dbi();

	my(%ignore_hostports);

	# find existing servers and block their host/ports
	_statusmsg( "[func] Creating Hash of Registered Servers\n" );
	my(@sybase_interfaces) = $gem_data->get_server_interfaces();	
	foreach (@sybase_interfaces) {
		my($server, $host,$port)=( $_->[0], $_->[1], $_->[2] );
		# server allready registered
		# hey we want to ignore this server... add it to our list of host:ports
		next unless $qs_ignorelist{"sybase:".$server};
		next unless $server and $host and $port;
		
		$host=~s/\..+$//;
		$ignore_hostports{$host.":".$port}=$server;			
	}

	my(%unixhosts);		# key=hostname val=Pingable|Unpingable
	my(%sybasehosts);
	_statusmsg( "[func] Processing Sybase interfaces file\n" );
	my($NumSybToRegister)=0;
	foreach (@sybase_interfaces) {
		my($server, $host,$port)=( $_->[0], $_->[1], $_->[2] );
		next unless $server and $host and $port;
		$host=~s/\..+$//;
		
		$unixhosts{$host}="" unless $unixhosts{$host} or $host =~ /\+/;
		
		if( $server=~/_BACK$/i or $server=~/_BACKUP$/i
		or $server=~/_BS$/i or $server=~/_MON$/i  or $server=~/_MS$/i
		or $server=~/_RS$/i or $server=~/_HS$/i or $server=~/_JSAGENT$/i or $server=~/^ws/
		or $server=~/_XP$/i ) {
			_statusmsg( "[func] Processing $server - ignoring (type)\n" );
			next;
		}

	   if( $server=~/^[\d\.]*$/ ) {	# digits and periods... no way
			_statusmsg( "[func] Processing $server - ignoring (all digits and dots)\n" );
			next;
		}

		if( $UPDATE and $qs_ignorelist{"sybase:".$server} ) {
			_debugmsg( "[func] Processing $server - ignoring (qs_ignorelist)\n" );
			next;
		}

		if( defined $ignore_hostports{$host.":".$port} ) {
			if( $ignore_hostports{$host.":".$port} eq $server ) {
				_debugmsg( "[func] Processing $server - allready registered\n" );
			} else {
				_debugmsg( "[func] Processing $server - duplicated - registered as ", $ignore_hostports{$host.":".$port}," \n" );
			}
			next;
		}
		_statusmsg( "[func] Processing $server\n" );

		my(@h,@p);
		if( $host =~ /\+/ ) {
			@h=split(/\+/,$host);
			@p=split(/\+/,$port);
		} else {
			push @h, $host;
			push @p, $port;
		}

		foreach (@h) {
			s/\..+$//;
			my($prt) = shift @p;
			if ( /^[\d\.]+$/ ) {
				_debugmsg( "[func] ignoring - host is an ip address\n" );
				next;
			}
						
			if( $prt == 1433 ) {
				_debugmsg( "[func] ignoring sql server\n" );
				next;
			}
			_statusmsg( "[func] Testing host $_ port $prt\n" );		

			my $alive_svr = tcp_portping($_,$prt);
			_debugmsg( "[func] tcp_portping($_,$prt) returned $alive_svr\n"	);

			if( ! $alive_svr ) {
				my $alive = qs_do_ping($_);
				_statusmsg( "[func] qs_do_ping($server) - host=$_ returned $alive\n");			
				$unixhosts{$_}="[ Unpingable ]" unless $alive;
				$unixhosts{$_}="Pingable" if $alive;
			} else {
				$unixhosts{$_}="Pingable";
			}

			if( $alive_svr ) {
				$sybasehosts{$server}=$_;
				if( $avail_dsns{$server} ) {
					$NumSybToRegister++;
					my($rc)=Register_Server(-name=>$server,-type=>"sybase", -host=>$_, -port=>$prt );
					push @discovery_messages,"* Sybase Server $server added successfully" if $rc and $DEBUG;	
					last;
				} elsif( $avail_dsns{lc($server)} ) {
					$NumSybToRegister++;
					my($rc)=Register_Server(-name=>lc($server),-type=>"sybase", -host=>$_, -port=>$prt );
					push @discovery_messages,"* Sybase Server $server added successfully" if $rc and $DEBUG;
					last;
				} elsif($avail_dsns{uc($server)} ) {
					$NumSybToRegister++;
					my($rc)=Register_Server(-name=>uc($server),-type=>"sybase", -host=>$_, -port=>$prt );
					push @discovery_messages,"* Sybase Server $server added successfully" if $rc and $DEBUG;
					last;
				} else {
					push @discovery_messages,"* Ignoring Sybase Server $_ : no ODBC DSN";
					_statusmsg( "[func] Cant Register Sybase $server - No DSN\n" );
				}
			}
		}
		$sybasehosts{$server}=$h[0] unless $sybasehosts{$server};	# unpingable
	}
	_statusmsg( "[func] Registered $NumSybToRegister Sybase Servers\n" );
	push @discovery_messages,"* $NumSybToRegister Sybase Servers Were Registered";

	_statusmsg( "[func] Processing Discovered WINDOWS SERVERS\n" );
	my($NumWinToRegister)=0;
	foreach ( sort keys %winhosts ) {
		s/\..+$//;
		_debugmsg( "[func] Working on windows host $_\n");
		if( $UPDATE and $qs_ignorelist{"win32servers:".$_} ) {
			_debugmsg( "[func] ignoring previously registered server $_\n");
			next;
		}
		#_debugmsg( "[func] Doing Ping $_\n");
		my $alive = qs_do_ping($_);
		if( $alive ) {
			_statusmsg( "[func] Testing File System Access For Windows Server $_\n");
			
  			# GENERIC ALARM BLOCK
   		local $SIG{ALRM} = sub { die "CommandTimedout"; };
			my(@disks);
			eval {
				alarm(2);
		   	@disks=get_win32_disks($_);
		   	alarm(0);
			};
			if( $@ ) {
				if( $@=~/CommandTimedout/ ) {
					_statusmsg( "[func] Command Timed Out\n" );
					#return 0;
				} else {
					_statusmsg( "[func] Command Returned $@\n" );
					#print __FILE__," ",__LINE__," RESULTS ARE $@\n";
					#return 0;
					#alarm(0);
				}
			}
   		
			#my(@disks)=get_win32_disks($_);
			if( $#disks>=0 ) {
				_statusmsg( "[func] Access to server $_ Is Ok ". join(" ",@disks) );
				my($rc)=Register_Server(-name=>$_,-type=>"win32servers", -disks=>\@disks);
				push @discovery_messages,"* Windows Server $_ added successfully" if $rc and $DEBUG;
				$NumWinToRegister++ if $rc;
			} else {
				_debugmsg( "[func]   You do not have windows access to $_]" );
			}
		} else {
			_debugmsg ( "[func] ".sprintf( "%-30s %s\n", $_, "[ Not Pingable ]" ));
		}
	}
	push @discovery_messages,"* $NumWinToRegister Windows Servers Were Registered";

	_statusmsg( "[func] No Unregistered SQL SERVERS Found\n" )if $#sqlservers<0;
	_statusmsg( "[func] Processing Discovered SQL SERVERS\n" )if $#sqlservers>=0;
	my($NumSqlToRegister)=0;
	foreach ( @sqlservers ) {
		next if $UPDATE and $qs_ignorelist{"sqlsvr:".$_};
		my($string);
		my($isok)="TRUE";
		$string = sprintf( "%-22s ", $_);
		my($windows_host)="";
		if( $winhosts{$_} ) {
			$windows_host=$_;
			$string .= sprintf( "%-24s ", $_ );
		} elsif( $winhosts{lc($_)}  ) {
			$windows_host=lc($_);
			$string .= sprintf( "%-24s ", lc($_) );
		} elsif( $winhosts{uc($_)} ) {
			$windows_host=uc($_);
			$string .= sprintf( "%-24s ", uc($_) );
		} else {
			# for kicks just try the server name!
			my $alive = qs_do_ping($_);
			if( $alive ) {
				$windows_host=$_;
				$string .= sprintf( "%-24s ", $_ );
			} else {
				$alive = qs_do_ping(lc($_))
					unless lc($_) eq $_;
				if( $alive ) {
					$windows_host=lc($_);
					$string .= sprintf( "%-24s ", lc($_) );
				} else {
					$alive = qs_do_ping(uc($_))
						unless uc($_) eq $_;;
					if( $alive ) {
						$windows_host=uc($_);
						$string .= sprintf( "%-24s ", uc($_) );
					} else {
						$isok="FALSE";
						$string .= sprintf( "%-24s ", "[Unknown Host]" );
					}
				}
			}
		}
		if( $avail_dsns{$_} ) {
			next if $UPDATE and $qs_ignorelist{"sqlsvr:".$_};
			$string .= "DBI connection exists";
			my($rc)=Register_Server(-name=>$_,-host=>$windows_host,-type=>"sqlsvr") if $isok eq "TRUE";
			$NumSqlToRegister++ if $rc;
			push @discovery_messages,"* SQL Server $_ added successfully" if $rc and $DEBUG;				
		} elsif( $avail_dsns{lc($_)} ) {
			next if $UPDATE and $qs_ignorelist{"sqlsvr:".lc($_)};
			$string .= "DBI connection exists";
			my($rc)=Register_Server(-name=>lc($_),-host=>$windows_host,-type=>"sqlsvr") if $isok eq "TRUE";
			$NumSqlToRegister++ if $rc;
			push @discovery_messages,"* SQL Server $_ added successfully" if $rc and $DEBUG;
		} elsif( $avail_dsns{uc($_)} ) {
			next if $UPDATE and $qs_ignorelist{"sqlsvr:".uc($_)};
			$string .= "DBI connection exists";
			my($rc)=Register_Server(-name=>uc($_),-host=>$windows_host,-type=>"sqlsvr") if $isok eq "TRUE";
			$NumSqlToRegister++ if $rc;
			push @discovery_messages,"* SQL Server $_ added successfully" if $rc and $DEBUG;
		} else {
			push @discovery_messages,"* Ignoring SQL Server $_ : no ODBC DSN";
			_statusmsg("[func] Ignoring SQL Server $_ : no ODBC connection\n");
			$string .= "[No DBI Connection]";
			$isok="FALSE";
		}
		$string .= "\n";
		_debugmsg( $string ) if ! $UPDATE or $isok eq "TRUE"
	}
	push @discovery_messages,"* $NumSqlToRegister SQL Servers Were Registered";


#	_statusmsg( "[func] \nSYBASE SERVERS\n----------------\n" ) if $#sybase>=0;
#	foreach ( keys %sybasehosts) {
#		next if $UPDATE and $qs_ignorelist{"sybase:".$_};
#		my($server, $host)=( $_, $sybasehosts{$_} );	#$_->[0], $_->[1] );
#		my($isok,$string)=("TRUE","");
#		$string .= sprintf( "%-22s %-24s ", $server, $host );
#		if( $avail_dsns{$server} or $avail_dsns{lc($server)} or $avail_dsns{uc($server)} ) {
#			$string .= "DBI connection ok";
#			Register_Server(-name=>$_,-type=>"sybase",-host=>$host) if $isok eq "TRUE";
#			$isok="TRUE";
#		} else {
#			$string .= "[No DBI Connection]";
#			$isok="FALSE";
#		}
#		$string .= "\n";
#		_debugmsg( $string );
#	}
	
	_statusmsg( "[func] TESTING UNIX SERVERS\n" );
	my($NumUnixToRegister)=0;
	foreach ( sort keys %unixhosts ) {		
		next if $UPDATE and $qs_ignorelist{"unix:".$_};
		
		if( $unixhosts{$_} eq "" ) {
			my $alive = qs_do_ping($_);
			_statusmsg( "[func] qs_do_ping($_) - returned $alive (pass 2)\n");			
			$unixhosts{$_}="[ Unpingable ]" unless $alive;
			$unixhosts{$_}="Pingable" if $alive;
		}
		
		if( $unixhosts{$_} eq "Pingable" ) {
			_debugmsg(sprintf( "%-30s %s\n", $_, $unixhosts{$_} ));
			my($rc)=Register_Server(-name=>$_, -type=>"unix");
			push @discovery_messages,"* Unix Server $_ added successfully" if $rc and $DEBUG;
			$NumUnixToRegister++ if $rc;
		}
	}
	push @discovery_messages,"* $NumUnixToRegister Unix Servers Were Registered";

	_statusmsg( "[func] * WIZARD COMPLETE!\n" );
	_statusmsg( "[func] * CHECK YOUR SERVERS AND REMEMBER TO SAVE CHANGES\n" );
	_statusmsg( "[func] * VALIDATE YOUR SERVERS ON THE NEXT SCREEN\n\nREMEMBER TO SAVE CHANGES BEFORE EXITING\n" );

	my(@rc)=end_msg_logging();

	#unbusy();
	unbusy(1);
	_statusmsg("[func] Please Select 'OK' on the pop-up Results display\n");
	my_rotext_box(-title=>'Add Server Wizard Results', -label=>'Add Server Wizard' , -text=>"SUMMARY RESULTS\n".join("\n",@discovery_messages,"\n---------------\n",@rc));
	_statusmsg("[func] GEM Ready / Continuing...\n");

#	$XMLDATA{mainWindow}->messageBox( -title => 'Wizard Completed',
#		-message => "Wizard Completed.\n\nYou may see the servers you have added on the category screens.\n\nVALIDATE YOUR SERVERS ON THE NEXT SCREEN\n\nREMEMBER TO SAVE CHANGES BEFORE EXITING\n",
#		-type => "OK" );
}

sub tcp_portping {
   #my($name,$port)=@_;
   #local $SIG{__WARN__} = sub { };
   #my $connection = IO::Socket::INET->new(Proto=>'tcp', PeerAddr=>$name, PeerPort=>$port, Timeout=>1);
   #return 0 unless $connection;
   #return 1;  
   
   my($name,$port,$timeout)=@_;
	$timeout=1 unless $timeout;
   #local $SIG{WARN} = sub { };
   local $SIG{ALRM} = sub { die "CommandTimedout"; };

	my($connection);
	_debugmsg("[func] tcp_portping(name=$name,port=$port,timeout=$timeout)\n");	
		
	eval {
		alarm(2*$timeout);
   	$connection = IO::Socket::INET->new(Proto=>'tcp', PeerAddr=>$name, PeerPort=>$port, Timeout=>$timeout);
   	alarm(0);
	};
	if( $@ ) {
		print "PORTPING RETURNED $@\n";
		if( $@=~/CommandTimedout/ ) {
			return 0;
		} else {
			return 0;
			alarm(0);
		}
	}
   return 0 unless $connection;
	return 1;
}

sub qs_run_net_view {
	_debugmsg( "[func] Running Net View to Get Windows Servers\n" );
	my(%winhosts);
				
	busy();
	my(%subargs);
#	if( $DEBUG ) {
#		$subargs{-mainwindow}	= $XMLDATA{mainWindow},
#		$subargs{-debug}			= 1,
#		$subargs{-statusfunc}	= \&_statusmsg,
#		$subargs{-printfunc} 	= \&_statusmsg,
#		$subargs{-ignore_string}= "",
#		$subargs{-printhdr}  	= "[netview] : ",
#		$subargs{-showinwin}  	= "Net View Runner"		
#	}		
	
	my($rc,$datptr);
	local $SIG{ALRM} = sub { die "CommandTimedout"; };
	eval {
		alarm(2);	
		($rc,$datptr)=RunCommand(   -cmd=>"NET VIEW", %subargs	);
	  	alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			_statusmsg( "[func] NET VIEW COMMAND TIMED OUT.\n" );			
		} else {
			_statusmsg( "[func] NET VIEW Command Returned $@\n" );
			alarm(0);
		}
	}   		
	
	unbusy();
	
	foreach ( @$datptr ) {
		chomp;
		next if /^Server/ or /^----/ or /^\s+/ or /^The command completed/ or /^\s*$/;
		my($h,$comment)=split(/\s+/,$_);
		$h=~s/^\\\\//;
		$winhosts{$h}=$comment;
	}
	
#	local $SIG{ALRM} = sub { die "CommandTimedout"; };
#	my(@disks);
#	eval {
#		alarm(20);	
#		open(NETVIEWRESULTS,"NET VIEW |") or die("Cant Execute Net View $!\n");
#		#_debugmsg( "[func] Net View Cmd Completed. Processing Results\n" );
#		while(<NETVIEWRESULTS>) {
#			chomp;
#			 #_debugmsg( "[func] $_" );
#			next if /^Server/ or /^----/ or /^\s+/ or /^The command completed/ or /^\s*$/;
#			my($h,$comment)=split(/\s+/,$_);
#			$h=~s/^\\\\//;
#			$winhosts{$h}=$comment;
#		}
#		close(NETVIEWRESULTS);
#	  	alarm(0);
#	};
#	if( $@ ) {
#		if( $@=~/CommandTimedout/ ) {
#			_statusmsg( "[func] WOAH!!!  NET VIEW COMMAND TIMED OUT???\n" );			
#		} else {
#			_statusmsg( "[func] Net View Command Returned $@\n" );
#			#print __FILE__," ",__LINE__," RESULTS ARE $@\n";
#			#return 0;
#			#alarm(0);
#		}
#	}   		
	
	_debugmsg( "[func] Done Running Net View to Get Windows Servers\n" );	
	return %winhosts;
}

sub qs_do_ping {
	my($host)=@_;
	return $qs_pingstate{$host} if defined $qs_pingstate{$host};
	$! = undef;

	_debugmsg("[func] qs_do_ping(name=$host)\n");		
	#
	# WARNING: YOU CAN *** NOT *** USE AN EVAL BLOCK HERE - Net::Ping::External
	#    aborts ungracefully (no messages or whever) if you do 
	#
	#local $SIG{ALRM} = sub { die "CommandTimedout"; };
	#eval {
	#	alarm(10);	
		
		$qs_pingstate{$host} = ping( host=>$host, timeout=>1 );	
	  	
	#	alarm(0);
	#};		
	#if( $@ ) {
	#	print __FILE__," ",__LINE__," DBG DBG\n";
	#	
	#	if( $@=~/CommandTimedout/ ) {
	#		_statusmsg( "[func] PING COMMAND TO $host TIMED OUT.\n" );			
	#		print __FILE__," ",__LINE__," DBG DBG\n";
	#	
	#	} else {
	#		_statusmsg( "[func] PING Command To $host Returned $@\n" );
	#		alarm(0);
	#	}
	#}   			
		
	$! = undef if $! =~ /Bad file descriptor/ and is_nt();
	
	return mywarn("ping returned \"$!\" - could ping not be in your path\n")
		if $! eq "No such file or directory" or $! =~ /ping: not found/;
	print __FILE__," ",__LINE__," DBG DBG\n";
		
	return $qs_pingstate{$host};
}

#sub qs_get_sql_hostname {
#	my($server)=@_;
#
#	my($host);
#	open(OUT,'osql -E -S$server -Q"select host_name()" |') or return mywarn("Cant Run osql -E $!\n");
#	while(<OUT>) {
#		chomp;
#		s/^\s+//;
#		next if /^$/ or /^--------------/ or /affected/;
#		_statusmsg( "[func] HOST for $server is $host\n" );
#		$host=$_;
#	}
#	close(OUT);
#	return $host;
#}

sub qs_run_osql {
	_debugmsg( "[func] Fetching SQL Server List.\n" );
	my(@sqlservers);
	
	my(%subargs);
	if( $DEBUG ) {
		$subargs{-mainwindow}	= $XMLDATA{mainWindow},
		$subargs{-debug}			= 1,
		$subargs{-statusfunc}	= \&_statusmsg,
		$subargs{-printfunc} 	= \&_statusmsg,
#		$subargs{-ignore_string}= "",
		$subargs{-printhdr}  	= "[osql] : ",
#		$subargs{-showinwin}  	= "Osql Runner"		
	}
	busy();
	
	# GENERIC ALARM BLOCK
	local $SIG{ALRM} = sub { die "CommandTimedout"; };
	my($rc,$datptr);
	eval {
		alarm(20);
		($rc,$datptr)=RunCommand( -cmd=>"osql -L", %subargs	);	
   	alarm(0);
	};
	if( $@ ) {
		if( $@=~/CommandTimedout/ ) {
			_statusmsg( "[func] osql -L Command Timed Out\n" );
			return ();
			
		} else {
			_statusmsg( "[func] Command Returned $@\n" );
			alarm(0);			
		}
	}
   		
	
	unbusy();
	
	foreach ( @$datptr ) {
		chomp;
		s/^\s+//;
		next if /^Server/ or /^----/ or /^\s*$/;
		push @sqlservers,$_;
	}	
	
#	local $SIG{ALRM} = sub { die "CommandTimedout"; };	
#	eval {
#		alarm(20);	
#		
#		open(OSQLRESULTS,"osql -L |") or return mywarn("Cant Run osql -L $!\n");
#	#	_debugmsg( "[func] Command Completed... Processing results\n" );
#		while(<OSQLRESULTS>) {
#			chomp;
#			 #_debugmsg( "[func] $_" );		
#			s/^\s+//;
#			next if /^Server/ or /^----/ or /^\s*$/;
#			push @sqlservers,$_;
#		}
#		close(OSQLRESULTS);
#	
#	  	alarm(0);
#	};
#	if( $@ ) {
#		if( $@=~/CommandTimedout/ ) {
#			_statusmsg( "[func] WOAH!!!  NET VIEW COMMAND TIMED OUT???\n" );			
#		} else {
#			_statusmsg( "[func] Net View Command Returned $@\n" );
#			#print __FILE__," ",__LINE__," RESULTS ARE $@\n";
#			#return 0;
#			#alarm(0);
#		}
#	}   		
	return @sqlservers;
}

1;

__DATA__

#BASE_BACKUP_DIR		GENERAL		50 Base Backup Directory
#	A GEM data directory for logs, dbcc output, and other
#	job output, but not for the backups themselves. Dont change
#	unless this is a local (non GEM) install.
#MASTER_LOG		GENERAL		50	Master Log File
#	[ Filename ]
# 	The master log file.  This log will have one line per job start
# 	and one line per job completion. Defaults to BASE_BACKUP_DIR/log
#IGNORE_SERVER		GENERAL		30	List Of Unavailable Servers
#	pipe separated list of servers ignored
#	by the plan manager.  Use this to disable plans.
#SYBASE			GENERAL		45	Sybase Environment Variable
#	[ Sybase Environment Variable ] for use in
#	cron jobs. You shouldnt need to overridde
#	on a per job basis.
DSQUERY			DEFINITION		30	Server/Connection Name
 	The DSQUERY or ODBC name of the server.
REMOTE_DIR		DEFINITION		40	Target Backup Directory
 	Directory the server will place backups in. e.g. c:/backups
IS_REMOTE		DEFINITION		CK 	Target Dir Is Remote
	The Target Backup Directory can NOT be reached by direct
	file access (NFS or NT Networking).  If Selected, will use FTP
REMOTE_HOST		DEFINITION		30	Host Name
	The system the database resides on.
	Used only if the Target Dir Is Remote checkbox is checked
LOCAL_DIR		DEFINITION		40	Target Backup Directory Filespec
 	[Optional] Direct path to the Target Backup Directory as
 	seen by the scheduling server.  e.g. //mysqlsvr/c$/backups
 	Used only if the Target Dir Is Remote checkbox is NOT checked
DO_DUMP			STEPS		CK	Plan Includes Full or Incremental Backup
DO_COMPRESS		STEPS		CK	Compress Backups When Completed
DO_UPDSTATS		STEPS		CK	Plan Includes Running Update Statistics
DO_AUDIT		STEPS		CK	Plan Includes Running A Configuration Audit
DO_DBCC			STEPS		CK	Plan Includes Running DBCC Checks
DO_PURGE		STEPS		CK	Plan Includes Purging (See the "Purge" Tab)
NUM_BACKUPS_TO_KEEP	PURGE		4	Number of full backups to keep
	[ A Number >=1 ] The number of Full Dumps To Keep
NUMBER_OF_STRIPES	PURGE 4	Number Of Stripes (Sybase Only)
	[ A Number >= 1 ] The number of stripes used for backups.
NUM_DAYS_TO_KEEP	PURGE		4	Days to Keep Other Files
	[ A Number >= 1 ] The number of days to keep log files
COMPRESSION_LEVEL	COMPRESSION		4	Internal Compression Level (Sybase 12.5+ Only)
	Sybase Internal Compression Level. Set to 0 to not use
	internal compression (ie server version < 12.5).  If
	non-zero, external compression utilities will NOT be used.
COMPRESS_LATEST		COMPRESSION		CK	Use External Compression On Latest Backup?
	Do you wish to compress the LATEST backup file?
	Not setting means the latest will not be compressed
	but the older backups will be.
COMPRESS	COMPRESSION		40	Path to External Compression Program
	Full path to external compression program on the
	target system.  Only used if not using sybase
	internal compression. e.g. /usr/local/bin/gzip.
UNCOMPRESS	COMPRESSION		40	Path to External UnCompression Program
	Full path to external uncompression program on the
	target 	system.  Only used if not using sybase
	internal compression. e.g. /usr/bin/gzip -d.
IGNORE_LIST		OVERRIDES		45	Databases Ignored By Backups
	[db1|db2|db3] Pipe separated list of databases to ignore.
	Might include system databases e.g. model|tempdb|pubs
MAIL_TO			MAIL		45	Failure Mail Address
	Comma Separated List of EMail addresses to send
	failure notices. No spaces should exist in the line.
SUCCESS_MAIL_TO		MAIL		45	Success Mail Address
	Comma Separated List of EMail addresses to send
	success notices. No spaces should exist in the line.
DBCC_IGNORE_DB		OVERRIDES	45	Databases Ignored By DBCC
	[ db1|db2|db3 ] In addition to those "Ignored by Backups"
	this pipe separated list of databases are ignored by DBCC
REMOTE_ACCOUNT		OVERRIDES		45	Unix Ftp Account (override)
	[login] The unix login account for your host.  If not defined,
	will use entries from server registration.
REMOTE_PASSWORD		OVERRIDES		45	Unix Ftp Password (override)
	[password] The unix password for your host. If not defined
	will use entries from server registration.
#REBUILD_INDEX_DB 	DEFINITION	30
#REBUILD_INDEX_FILE 	DEFINITION	30
#	[file|file|file]
#   	File to rebuild indexes for given database
#   	If you wish to recreate indexes, please seek help in setting up this feature.
DO_LOAD			STEPS		CK	Ship and Load Dumps Into Another Server
XFER_TO_HOST		LOG_SHIPPING	40	Target System Hostname
XFER_TO_SERVER		LOG_SHIPPING	40	Target Server DSQUERY
	[ pipe separated list of servers you want to copy to ]
XFER_FROM_DB		LOG_SHIPPING	40	Source Database Name(s)
	[ pipe separated list of databases to to log ship (* for all) ]
XFER_TO_DB		LOG_SHIPPING	40	Target Database Name(s)
	[ pipe separated list of databases to copy in to (* for all) ]
XFER_BY_FTP		LOG_SHIPPING	CK	Transfer Using FTP?
	if not checked, will use direct file copy
XFER_SCRATCH_DIR 	LOG_SHIPPING	40	Temporary Directory on local system
XFER_TO_DIR		LOG_SHIPPING	40	Directory on Target System (full pathspec)
	Directory on Target as seen by Caller (ie //mysrv/D$/logship)
XFER_TO_DIR_BY_TARGET	LOG_SHIPPING	40	Directory on Target System (raw)
	Directory on Target as seen by Target (ie D:/logship)

