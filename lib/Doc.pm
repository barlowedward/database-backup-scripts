# Copyright (c) 1999-2006 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package Doc;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Carp qw(confess);
use strict;
use Exporter;
use File::Basename;
use File::Path;
use File::Copy;

$VERSION= 1.0;
@ISA      = ('Exporter');
@EXPORT = ( 'doc_init','doc_navlink','doc_finish','doc_dir' );

my(%DOC);	       # CONFIGURATION VARIBALES

my(%BGCOLOR)=(
	DARKRED => '#cc0066',
	OLIVE	   => '#669933',
	LTBLUE  => '#0066cc',
	RED	     => '#cc0000',
	PINK	    => '#ff3399',
	ORANGE  => '#cc6600',
	LTGREEN => '#00cc66',
	DKBLUE  => '#333399',
	BLUERED => '#663399',
	PURPLE  => '#660033'
);

my(%FGCOLOR)=(
	DARKRED => '#ff99cc',
	OLIVE	   => '#ccff99',
	LTBLUE  => '#33ccff',
	RED	     => '#ccff33',
	PINK	    => '#ccff33',
	ORANGE  => '#ffcc99',
	LTGREEN => '#ccffcc',
	DKBLUE  => '#66cccc',
	BLUERED => '#ccff99',
	PURPLE  => '#cccc00'
);

# DEFAULT COLORS
my($topbgcolor) = $BGCOLOR{"DARKRED"};
my($topfgcolor) = $FGCOLOR{"DARKRED"};
my($rgtbgcolor) = $BGCOLOR{"DARKRED"};
my($rgtfgcolor) = $FGCOLOR{"DARKRED"};

my(%CrossRef);
my(@TOC_ROWS);

my $linkcount = 0;

# =======================================
# INITIIALIZE DOCUMENTATION IN DIRECTORY
#  - create index.html
#  - start toc
#  - deal with ascii files too
# =======================================
sub doc_init
{
	%DOC=@_;
	$DOC{"indent_level"}=0 unless defined $DOC{"indent_level"};

	die "ERROR bad source_dir in doc_init()"
		if defined $DOC{'source_dir'} and ! -d $DOC{'source_dir'};

	undef %CrossRef;

	print "  Starting Documentation Set\n";

	my($dir)=$DOC{'out_dir'};
	die "No Directory $dir\n"       unless -d "$dir";

	if( defined $DOC{'colorscheme'} ) {
		die "ERROR: Illegal Color scheme. Color must be in: ".join(" ",keys %FGCOLOR)."\n" unless defined $FGCOLOR{$DOC{'colorscheme'}};

		$topbgcolor = $BGCOLOR{$DOC{'colorscheme'}};
		$topfgcolor = $FGCOLOR{$DOC{'colorscheme'}};
		$rgtbgcolor = $BGCOLOR{$DOC{'colorscheme'}};
		$rgtfgcolor = $FGCOLOR{$DOC{'colorscheme'}};
	}

	# ====================================
	# Write indexhtm
	# ====================================
	unlink "$dir/index.htm"			 if -e "$dir/index.htm";
	open(INDEX_PAGE,"> $dir/index.htm")
		|| die("Can't write $dir/index.htm: $!\n");
	print INDEX_PAGE "<HTML>\n <HEAD>\n <TITLE>",
							$DOC{'product_name'},
							"</TITLE>\n </HEAD>
<FRAMESET ROWS=\"43,*\" BORDER=NO SCROLLING=NO>
	<FRAME SRC=top.htm NAME=top NORESIZE SCROLLING=NO>
	<FRAMESET COLS=\"24%,*\">
	<FRAME SRC=toc.htm      NAME=contents>
	<FRAME SRC=readme.htm NAME=basefrm>
	</FRAMESET>
</FRAMESET>
</HTML>\n";
	close(INDEX_PAGE);

	# ====================================
	# Write top.htm
	# ====================================
	unlink "$dir/top.htm"		   if -e "$dir/top.htm";
	open(TOP_PAGE,"> $dir/top.htm")
		|| die("Can't write $dir/top.htm: $!\n");

	print TOP_PAGE "<BODY bgcolor=$topbgcolor>
<TABLE WIDTH=100% CELLPADDING=0 CELLSPACING=0><TR><TD>
<font face=sans-serif size=+1 color=$topfgcolor>
".$DOC{'product_name'}."
</TD><TD ALIGN=RIGHT>
<font face=sans-serif size=+1 color=$topfgcolor>
Copyright &#169 ".
$DOC{'copyright_years'}." by ".
$DOC{'company_name'}."
</TD></TR></TABLE>
</body>\n";

	close(TOP_PAGE);

	# =====================================
	# START TABLE OF CONTENTS toc.htm
	# =====================================
	unlink "$dir/toc.htm"
		if -e "$dir/toc.htm";

	open(TBLOFCNTS,"> $dir/toc.htm")
		|| die("Can't write $dir/toc.htm: $!\n");

	if( defined $DOC{'source_dir'} ) {
		print TBLOFCNTS "
<html>
<head>
<link rel=\"stylesheet\" href=\"ftie4style.css\">

<!-- Infrastructure code for the tree -->
<script src=\"ftiens4.js\"></script>

<!-- Execution of the code that actually builds the specific tree -->
<script src=\"mycustomjs.js\"></script>

<script>
initializeDocument()
</script>

</head>
<body bgcolor=white>
</html>
";
		close TBLOFCNTS;

		unlink "$dir/mycustomjs.js"
			if -e "$dir/mycustomjs.js";
		open(TBLOFCNTS,"> $dir/mycustomjs.js")
			|| die("Can't write $dir/mycustomjs.js: $!\n");
	} else {

		# initialize a real table of contents
		push @TOC_ROWS,"<HTML><HEAD><TITLE>$DOC{'product_name'}</TITLE>
<META NAME=AUTHOR CONTENT=\"Edward Barlow\"> </HEAD>
<BODY TEXT=BLACK BGCOLOR=WHITE><FONT SIZE=2>\n";

	}

	die "No product name" unless defined $DOC{'product_name'};

	push @TOC_ROWS, toc_folder($DOC{'product_name'});

	# =====================================
	# START readme.htm
	# =====================================
	unlink "$dir/readme.htm"	if -e "$dir/readme.htm";

	# =====================================
	# START readme.txt
	# =====================================
	unlink "$dir/README.txt"			if -e "$dir/README.txt";

	if( defined $DOC{page_head} ) {
		$DOC{'html'} = "";
		open(F, $DOC{page_head} ) or die "cant read $DOC{page_head}\n";
		while( <F> ) {
			s/CURRENT_TITLE/$DOC{product_name}/;
			#print "Read $_";
			$DOC{'html'} .= $_;
		}
		close(F);
	} else {
		$DOC{'html'} = "<html>\n<head>\n<title>".
			$DOC{'product_name'}.
			"</title>\n</head><body BGCOLOR=WHITE TEXT=BLACK>\n";
	}

	# COPY ALL FILES FROM source_dir
	if( defined $DOC{'source_dir'} )  {
		opendir(DIR,$DOC{'source_dir'})
			|| die("Can't open source_dir directory: $!");
		foreach (readdir(DIR) ) {
			next if /^\./ or -d $DOC{'source_dir'}."/".$_;
			copy($DOC{'source_dir'}."/".$_,$dir."/".$_)
				or die "Cant copy $_: $!\n";

		}
		closedir DIR;
	}
}

sub doc_navlink
{
	my %ARGS=@_;

	# place external link in table of contents
	push @TOC_ROWS,	 toc_item(0,$ARGS{'link'},$ARGS{"name"});
}

# =====================================
# FINISH TABLE OF CONTENTS AND CLOSE STUFF
# =====================================
sub doc_finish
{
	my(%args)=@_;
	my $html   = $DOC{'html'};
	my $outdir = $DOC{'out_dir'};

	push @TOC_ROWS, "</FONT> </BODY>\n" if ! defined $DOC{'source_dir'};

	foreach (@TOC_ROWS) { 
		s/Current title/Document Navigation/g;
		print TBLOFCNTS $_; 
	}
	close (TBLOFCNTS);

	if( defined $args{-translate} ) {
	$html=~s/href="(CHANGE_LOG.html)/href="..\/$1/g;
	$html=~s/href="(FAQ.html)/href="..\/$1/g;
	$html=~s/href="(Features List.doc)/href="..\/$1/g;
	$html=~s/href="(MONITORING SOLUTION AT MLP.doc)/href="..\/$1/g;
	$html=~s/href="(MONITORING SOLUTION.doc)/href="..\/$1/g;
	$html=~s/href="(POST_INSTALL_STEPS.html)/href="..\/$1/g;
	$html=~s/href="(Product.gif)/href="..\/$1/g;
	$html=~s/href="(SalesPresentation.htm)/href="..\/$1/g;
	$html=~s/href="(backup_scripts)/href="..\/$1/g;
	$html=~s/href="(batchjobs)/href="..\/$1/g;
	$html=~s/href="(bin)/href="..\/$1/g;
	$html=~s/href="(console)/href="..\/$1/g;
	$html=~s/href="(gem)/href="..\/$1/g;
	$html=~s/href="(getting_started.html)/href="..\/$1/g;
	$html=~s/href="(index.html)/href="..\/$1/g;
	$html=~s/href="(installation_guide)/href="..\/$1/g;
	$html=~s/href="(procs)/href="..\/$1/g;
	$html=~s/href="(screenshots)/href="..\/$1/g;
	$html=~s/href="(source_files)/href="..\/$1/g;
	}

	# WRITE MAIN SUMMARY PAGE
	$html.= get_web_page_footer();
	$html =~ s/CURRENT_TITLE/$DOC{product_name}/g;

	# DO THE CROSSREFERENCE BUILD
	foreach (keys %CrossRef) {
# print __LINE__,"DBG: Key $_\n";
		next if /^NAME$/
				or /^DESCRIPTION$/
				or /^SYNOPSIS$/
				or /^USAGE$/
				or /^NOTES$/;
# print "DBG $_ -> $CrossRef{$_}\n";
		$html =~ s~([\w\s])$_([\W\s])~$1<A HREF=$CrossRef{$_}>$_</A>$2~g;
	}

	open(MAINHTML,"> $outdir/readme.htm")
		|| die("Can't open $outdir/readme.htm: $!");
	print MAINHTML $html;
	close MAINHTML;

	print "  Documentation Set Completed\n";
}

# $level is level of stack.     Level 1 (From H1) /undef parent foldersTree
#							       Level 2 (From H2) is one level higher
my @folder_stack;
my $folder_num=0;
sub toc_folder
{
	my($folder_name,$url)=@_;
	$url= "readme.htm#link".($linkcount+1) unless defined $url;
	my $str;

	if( ! defined $DOC{'source_dir'} ) {
		$folder_num++;
		$str="<BR>" if $folder_num!=1;
		return "$str<B>$folder_name</B><BR>\n";
	}

	if( $#folder_stack<0  ) {
		$str.= "foldersTree = gFld(\"$folder_name\"";
		$str.= ",\"$url\"" if defined $url;
		$str.= ")\n";
		@folder_stack=('foldersTree');
	} else {
		$folder_num++;
		$str.= "fld$folder_num = insFld(foldersTree,gFld(\"$folder_name\"";
		$str.= ",\"$url\"" if defined $url;
		$str.= "))\n";
		push @folder_stack,"fld$folder_num";
	}
	return $str;
}

# name has 0 spaces if in last folder
# two spaces means one level down
sub toc_item
{
	my($itemtype, $link, $name)=@_;
#print "DBG: $itemtype, $link, $name\n";
	# remove trailing spaces
	$name=~s/\s+$//;

	$link= "readme.htm#link".$linkcount unless defined $link;
	$CrossRef{$name}="#link".$linkcount;

	$name=~s/_/ /g;

	warn "ERROR - INVALID NAME FOR LINK REFERENCE.  Could you have a =head1 line with no description after it?\n" if $name=~/^\s*$/;
	return if $name=~/^\s*$/;
	$CrossRef{$name}="#link".$linkcount unless defined $CrossRef{$name};

	$folder_num++;
	if( defined $DOC{'source_dir'} ) {
		if($name=~/^ / ) {
			# if you are spaced at start then subitem
			return "  itm$folder_num = insDoc($folder_stack[$#folder_stack],gLnk($itemtype,\"=>&nbsp;&nbsp;$name\",\"$link\"))\n";
		}
		return "  itm$folder_num = insDoc($folder_stack[$#folder_stack],gLnk($itemtype,\"$name\",\"$link\"))\n";
	} else {
		my($target)='_parent'   if $itemtype==0;
		$target='basefrm'	       if $itemtype==2;
		my $str="";
		$str.="==>&nbsp;&nbsp;<FONT SIZE=-1>" if $name=~/^ /;
		$str.= "<A HREF=".$link." TARGET=$target>".$name."</A><BR>\n";
#print "DBG: <A HREF=".$link." TARGET=$target>".$name."</A><BR>\n";
		$str.="</FONT>" if $name=~/^ /;
		return $str;
	}
}

#===============================================================
#
# FILL UP THE html and man subdirectories of the $outdir
# The main html page will be readme.htm
#
# Also... create an index.htm, toc.htm, and readme.htm
#  with hyperlinks so that you get summary of the doc!
#
#===============================================================
sub doc_dir
{
	my(%ARGS)=@_;

	my(@files)=();
	my(%ignore)=();

	my($ar)	   = $ARGS{'ignore'};
	if( defined $ar ) {
		foreach ( @$ar ) { $ignore{$_}=1; };
	}

	$ar	    = $ARGS{'files'};
	@files	    = @$ar if defined $ar;

	my($dir)	= $ARGS{'dir'};
	my($section)	= $ARGS{'section'};
	my($outdir)	= $DOC{'out_dir'};

	$DOC{'indent_level'}   = $ARGS{'indent_level'}
		if defined $ARGS{'indent_level'};

	print "  Building Section: $section\n";
	push    @TOC_ROWS, toc_folder($section);

	# ===================
	# Get list of files we can document
	# ===================
	if( defined $dir ) {
		print "  Reading Directory $dir (for .pod/.pl/.pm/.gif/.jpg/.htm/.html)\n";
		opendir(DIR,$dir)
			|| die("Can't open directory: $!");
		my(@rawfiles)=readdir(DIR);

		if( defined $ARGS{-debug} ) {
 			print "IGNORE IS ",$ARGS{'ignore'},"\n";
 			print "IGNORE LIST is\n";
			foreach(keys %ignore) {
				print "$_ -= $ignore{$_}\n";
			}
		}

		push @files, sort( grep((! defined $ignore{$_} and (/\.pod$/i or /\.pl$/i or /\.gif$/i or /\.pm$/ or /\.jpg$/i or /\.htm$/i or /\.html$/i )), @rawfiles));
# print "FILES is ",join(" ",@files),"\n";
		closedir(DIR);
	}

	my $html="";

	# ===================
	# HANDLE readme.pod FIRST IF IT EXISTS
	#       HTML FOR SUMMARY STARTS WITH readme.pod
	#       REGULAR TEXT FROM readme
	# ===================
	if( -e "$dir/readme.pod" and ! defined $ignore{"readme.pod"} ) {
		my $t;
		print "Processing readme.pod";
		($html,$t)=get_htm_from_pod("$dir/readme.pod");
	}

	# =======================
	#  process files
	# =======================
	my($short_file_nm);
	foreach my $file (@files) {

		print "    Processing File $file ";
		my($realfile)=$file if -r $file;;
		$realfile = $dir."/".$file
			if -r "$dir/$file";
		if( ! -r $realfile) {
			print "Hmmm File $realfile Does Not Exist??\n";
			next;
		}

		if($file=~/readme.pod$/i or $file=~/^configure/i or $file=~/\.tar$/i) {
			print "  Ignoring ...\n";
			next;
		}

		# ========================
		# if .htm or .html just copy it unless index.htm or default.htm
		# make links to file (by name) and to any named anchors
		# ========================
		my($until_state)="OK" unless defined $ARGS{-until_string};
		if( $file =~ /\.html*/i ) {


			#
			# HTML FILE => copy verbatim and add
			# line to toc based on filename
print "HTML FILE\n";
			#

			$short_file_nm = $file;
			$short_file_nm =~ s/.html//i;
			$short_file_nm =~ s/.htm//i;

			if($short_file_nm=~/^default/i
				or $short_file_nm=~/^config/i
				or $short_file_nm eq "index" ){
					print "  Ignoring $short_file_nm because of name\n";
					next;
			}

			$short_file_nm =~ s/_/ /g;
print "    searching for cross-references...\n";

print "OPENING $realfile -embed=$ARGS{-embed} state=$until_state $ARGS{-until_string}\n" if defined $ARGS{-debug};
			open(HTMLFILE,$realfile)
				or die "Cant read $realfile: $!\n";

			if( $outdir ne "."  and ! defined $ARGS{-embed}) {
				open(OUTFILE ,"> $outdir/".basename($file))
					or die "Cant write $file in $outdir: $!\n";
			}

			my($num_anchors)=0;
			while (<HTMLFILE>) {
        			if( ! defined $until_state or $until_state ne "OK" ) {
        				next if ! defined $until_state and ! /$ARGS{-until_string}/;
        				if( ! defined $until_state and /$ARGS{-until_string}/ ) {
        					$until_state="FOUND";
        					next;
        				}
        				if( $until_state eq "FOUND" ) { # search forward for <a name=
        					next unless  /\<a name=/i;
        					$until_state="OK";

$html .= "<center>
<TABLE WIDTH=100% BORDER CELLPADDING=3 COLS=1 BGCOLOR=$rgtbgcolor>
<TR><TD><CENTER><font color=$rgtfgcolor><H1>$ARGS{-title}</H1>\n<H3>$ARGS{-subtitle}</H3>
        					</font>\n</TD></TR>\n</TABLE></CENTER>\n";
        				}
				}

				if( /\<A NAME=/i ) {
					last if /<A NAME="END">/i;

					# found an anchor - put it in TOC
print "FOUND AN ANCHOR ON LINE $_" if defined $ARGS{-debug};
					m/A NAME=\"*([\-\.\_\w\s]+)\"*\>/i;
					my($anchor)=$1;
die "LINE=$_\nNO ANCHOR FOUND ON LINE" unless defined $anchor and $anchor!~/^\s*$/;
					$num_anchors++;
					my($atext)=$anchor;
					$atext =~ s/_/ /g;
print "THE ANCHOR IS $anchor\n" if defined $ARGS{-debug};
print "OUTDIR is $outdir\n" if defined $ARGS{-debug};
					my(@words)=split(/\s+/,$atext);
					$atext="";
					foreach (@words) {
						$atext.= ucfirst(lc($_))." ";
					}
					$atext=~s/ $//;
print "THE ANCHOR IS $anchor text is $atext\n" if defined $ARGS{-debug};
					if( defined $ARGS{-embed} ) {
						push @TOC_ROWS, toc_item(2,"readme.htm#".$anchor,$atext);
					} else {
						push @TOC_ROWS, toc_item(2,basename($file)."#".$anchor,$atext);
					}
				}

# added for embed
				if( defined $ARGS{-embed} ) {
					$html.=$_;
				} else {
					print OUTFILE $_ if $outdir ne ".";
				}
			}
			$until_state = undef;
print "Done Reading File\n";

			if( $num_anchors== 0 ) {
				my($f)=basename($short_file_nm);
				$f=~s/.html*$//;
				$f="Product Notes" if $f =~ /^index$/i or $f =~ /^readme$/i;
#print "DBG short file name is $f\n";
				if( defined $ARGS{-embed} ) {
					push @TOC_ROWS, toc_item(2,undef,$f);
				} else {
					push @TOC_ROWS, toc_item(2,$file,$f);
				}
			}

			close HTMLFILE;
			close OUTFILE unless defined $ARGS{-embed};

			print "\n";
			next;

		# ========================
		# GIF FILE
		# ========================
		} elsif( $file =~ /\.gif/i or $file =~ /\.jpg/i ) {

			$short_file_nm = $file;
			$short_file_nm =~ s/.gif//i;
			$short_file_nm =~ s/.jpg//i;
			$short_file_nm =~ s/_/ /g;

			push @TOC_ROWS, toc_item(2,$file,$short_file_nm);

			#copy($dir."/".$file,$outdir."/".$file)
			copy($realfile,$outdir."/".basename($file))
				or die "Cant copy $realfile: $!\n";

			next;
		} else {

		# ========================
		# Pod or regular file
		# ========================
		print "  reading... ";
		my($h,$title);
		# if( $file =~ /^\./ ) {
		# print "DBG: READING - $file \n";
			($h,$title)=get_htm_from_pod($realfile);
		# } else {
		# print "DBG: READING - $dir/$file \n";
			# ($h,$title)=get_htm_from_pod($dir."/".$file);
		# }

		if( $h eq "" ) {
			print " [ no documentation ]\n";
			next;
		} else {
			print "\n";
		}

		# ADD TO THE HTML OF THE MAIN PAGE
		$html .= $h;

		$short_file_nm=basename($file);
		$short_file_nm=~s/\.pod//i;
		$short_file_nm=~s/\.pl//i;
		$short_file_nm=~s/pod\///i;

		#
		# Add to table of contents with basefrm as the total
		#
		$title = $short_file_nm if $title eq "";

		}
	}

	$DOC{'html'} .= $html;
}

sub recapitalize {
	my($x)=@_;
		if( $x =~ /[A-Z][A-Z]+/ ) {
				my(@words)=split(/\s+/,$x);
					$x="";
					foreach (@words) {
						$x.= ucfirst(lc($_));
					}
					$x=~s/ $//;
		}
	return $x;
}
sub system_to_stdout
{
	my($cmd)=@_;
	open(OUTCMD,$cmd." |") || die("Can't run command $cmd: $!\n");
   while ( <OUTCMD> ) {
		chomp;
			print;
   }
   close(OUTCMD);
}

# =================================================
#
# GET_HTM_FROM_POD( FILE )
#       gets and formats html between =head1 and =cut
#       return empty string if no pod in the file
#       second argument is title
#
# =================================================
sub get_htm_from_pod {
	my($input_file)=@_;
	my($output_html_str,$title)=("","");

	my( @infile_lines );
	confess("Error file $input_file does not exist\n")
		unless -r $input_file;
	open(INFILE,$input_file)
		|| die("Can't read $input_file: $!");
	while ( <INFILE> ) {
		chomp;
		chomp;
		s/\t/   /g;
		push @infile_lines, $_;
	}
	close(INFILE);

	my($in_pod_area)=0;
	my($in_verbatim_section)=0;
	my($cnt_to_main_heading)=0;

	foreach ( @infile_lines ) {
		next unless $in_pod_area==1 or /^=head/;
		$in_pod_area=1;
		if( /=cut/ ) {
			$in_pod_area=0;
			next;
		};

		# YOU ARE NOW BETWEEN HEAD & CUT
		if( $cnt_to_main_heading ) {
			next if /^\s*$/;
			$cnt_to_main_heading--;

			# NAME BLOCK PRINT BOX HEADING !
			if( $cnt_to_main_heading==0 ) {
				my($left,$right)=split /-/;
				$title=$left;

				$output_html_str .= mkanchor();
				$output_html_str .= "<br>" if $linkcount>1;
				$output_html_str .= "\n<center>\n<TABLE WIDTH=100% BORDER CELLPADDING=3 COLS=1 BGCOLOR=$rgtbgcolor>\n<TR><TD><CENTER><font color=$rgtfgcolor><H1>$left</H1>\n<H3>$right</H3></font>\n</TD></TR>\n</TABLE></CENTER>\n";

				# MAKE LINK ON LEFT SIDE
				$title=recapitalize($title);
				push @TOC_ROWS, toc_item(2,undef,$title);
			}
			next;
		}

		# VERBATIM IS INDENTED STUFF - END VERBATIM
		if( $in_verbatim_section and ! /^\s/ ) {
			$output_html_str.= "</pre>\n";
			$in_verbatim_section=0;
		}

		if( /^=head1/ ) {
			s/^=head1\s*//;
			s/\s+$//;
			#s/\s//g;

			if( $_ eq "NAME" ) {
				$cnt_to_main_heading=1;
				next;
			} elsif( $_ eq "AUTHOR" ) {
				# Ignore this block until next head so we dont repeat
				if( $input_file !~ /readme.pod/i ) {
					$in_pod_area=0;
					next;
				} else {
					$output_html_str .= "<h2>$_</h2>\n";
					next;
				}
			} else {
				$_ = ucfirst(lc());
				if( ! defined $DOC{"color_h1"} ) {
					$output_html_str .= mkanchor()."<h1>$_</h1>\n";
				} else {
					$output_html_str .= "\n<P>\n".mkanchor()."\n<center>\n<TABLE WIDTH=100% BORDER CELLPADDING=3 COLS=1 BGCOLOR=$rgtbgcolor>\n<TR><TD><CENTER><font color=$rgtfgcolor><H1>$_</H1>\n</font>\n</TD></TR>\n</TABLE></CENTER>\n";
				}
				if( $DOC{"indent_level"} ne "1" ) {
					push @TOC_ROWS, toc_item(2,undef,$_);
				} else {
					push @TOC_ROWS, toc_item(2,undef," ".$_);
				}
			}
			$output_html_str.="<p>\n";
			next;
		} elsif( /^=head2/ ) {
			s/=head2\s*//;
			if( $DOC{"indent_level"} eq "2" and $_ !~ /AUTHOR/ ) {
				$output_html_str .= mkanchor();
				push @TOC_ROWS, toc_item(2,undef,"  ".$_);
			}
			$output_html_str .= "<h3>$_</h3>\n";
			# $output_html_str.="<p>\n";
			next;
		} elsif( /^=over/ ) {
			# Start List
			$output_html_str .= "<UL>\n";
		} elsif( /^=back/ ) {
			$output_html_str .= "</UL>\n";
		} elsif( /^\s*$/ ) {
			$output_html_str .= "<P>\n"
				unless $output_html_str=~/<P>[\n]$/;
		} elsif( /^\s/ ) {

			# text starts with a space verbatim text left justified
			# s/^\s*//;
			s/\</\&lt;/g;
			s/\>/\&gt;/g;
			s/\s+$//;

			$output_html_str.= "<pre>\n"
				if $in_verbatim_section==0;

			s/C\<([\"\'\.\$\w\#\/\\\s\-\|]+)\>/<em>$1<\/em>/g;
			s/B\<([\"\'\.\$\w\#\/\\\s\-\|]+)\>/<b>$1<\/b>/g;

			$in_verbatim_section=1;
			$output_html_str.= $_."\n";

		} else {

			# regular text just print it
			s/C\<([\"\'\.\$\w\#\/\\\s\-\|]+)\>/<em>$1<\/em>/g;
			s/B\<([\"\'\.\$\w\#\/\\\s\-\|]+)\>/<b>$1<\/b>/g;
			if( /^=item/ ) {
				s/^=item\s+\*\s*//;
				$output_html_str.= "<LI><B>$_</B>\n";
			} else {
				$output_html_str.= " $_";
			}
		}
	}

	# END Verbatim Sections
	if( $in_verbatim_section ) {
		$output_html_str.= "</pre>\n";
	}

	return ($output_html_str,$title);
}

sub mkanchor {
	$linkcount++;
	return "\n<A NAME=link".$linkcount."></A>\n";
}

# =================================================
# GET_WEB_PAGE_FOOTER
#       returns string of page footer documentation
# =================================================
sub get_web_page_footer
{
return "<hr><br>\nThis output is documentation for the $DOC{'company_name'} $DOC{'product_name'}.
<ADDRESS>copyright &#169 $DOC{'copyright_years'} By
<A HREF=$DOC{'home_page'}>$DOC{'company_name'}</A>
</ADDRESS></BODY></html>\n";
}

1;

__END__

=head1 NAME

Doc.pm - build html documentation

=head2 SYNOPSIS

Build html based documentation from your perl application using the
perl pod documentation format.  The output of this library is a cross
indexed frames based documentation set based on the multiple files
in your perl application (ie. this works on many perl files instead
of just 1).

=head2 DESCRIPTION

The user will create a small document generator script (using Doc.pm) which will
specify which files to extract pod from and in what order to do it
(as well as other options).  Whenever you wish to rebuild your documentation
set you run the driver program and create them.

=head2 FUNCTIONS

=over 4

=item * doc_init() - Initialize documentation set.

PARAMETERS: out_dir, source_dir, colorscheme, home_page,
copyright_years, company_name, product_name, color_h1

Starts a frameset that can be referenced via the
file index.htm in the directory you pass as out_dir. This file (index.htm)
contains a master frameset,  consisting of three parts:
a page top (top.htm), a table of contents (toc.htm) on the left side,
and a body on the right side (readme.htm).   Filenames are fixed.

=item * doc_navlink() - Add new external link to table of contents.

PARAMETERS: name, link

Places a special hyperlink in the table of contents.
These links are special and will bring up a new browser window.
This is for external links, perhaps to another
set of documentation or to your company home page.
2 breaks appear this after link.

=item * doc_finish() - Finish / Write up the documentation set.

PARAMETERS:  none

Write and close the files opened earlier.

=item * doc_dir() - Documents a section of data from a files in a directory.

PARAMETERS: files, ignore, dir, section

This is the main function.  It documents a section (in the table of contents)
from data in a single directory.  By default it includes all files, but you
can specify files with the 'files' parameter and can ignore files with 'ignore'.
The title is 'section' and the directory used is 'dir'.

=back

=head2 PARAMETERS DESCRIPTIONS

=over 4

=item * source_dir

Source of Folder-Tree input files.  These files are required for
javascript.  The source dir is an optional parameter.  You may think the
documentation looks better if this parameter is not passed :)

=item * out_dir

Output Directory For the web pages. It actually places stuff in the
html subdirectory of this directory.

=item * company_name

Your company Name.

=item * home_page

Your Home Page for links.

=item * copyright_years

Copyright Notice Years

=item * indent_level

Default to 1.  If this is set to 2 then head2's show up in the
documentation table of contents and head1's are not indented.
If it is set to 1 then head1's in pod files are indented
in table of contents (no head2's).  If 0 then all head1's
show up non indented (no head2's).

=item * product_name

Product Name

=item * current_section

Section header - goes in table of contents

=item * color_h1

All first level headings get colored - not just NAME blocks.

=item * colorscheme

Change the color scheme of the documentation.  Currently available color schemes are: DARKRED, PINK, OLIVE, LTBLUE, RED, ORANGE, LTGREEN, DKBLUE, BLUERED, and PURPLE.

=back

=head2 NOTES

The documentation shows in a nice 3 pane window.  The top pane (top.htm)
will give title info.  The left pane (toc.htm) gives table of contents and
navigation information.  The right pane (readme.htm) will give documentation
with anchors embedded so the table of contents can work.

Documentation can be built from directories by using the doc_dir function
to read all files for pod documentation. Files can either be in perl
source code (.pl) files or .pod files.  Documentation can also come from
files with .htm or .html extensions.  These files are parsed so as to
remove everything but the body section.

If reading pod files, the first file is readme.pod.  After that
it is alphabetical.

In addition to regular pod documentation, get any documentation between "^# ===" blocks (comments only).  If line is of format ^#\s*(\w+)\s*: then word is a heading

Specifically the doc subdirectory will contain README.txt and individual doc pages and the html directory will contain x.html files (from x.pl), and an index.html which is composed of a readme.html and toc.html (table of contents)

=head2 CROSSREFERENCE

The application will keep track of any keyword titles (ie items in the table
of contents).  When it is almost done, it will rewrite the main page and
replace any words that match the title with self hyperlinks.

=head2 EXAMPLE

   use strict;
   use Doc;

   doc_init(
      out_dir	 => '../web'			   ,
      home_page       => 'http://www.edbarlow.com',
      copyright_years => '1998-1999'			,
      company_name    => 'IQ Financial'		     ,
      product_name    => 'OPERATIONS MGR'
   );

   # add link to top level
   doc_navlink( name=>"Return To Top Level", link=>"../index.htm");

   #any pod documents in ../web/pod are in DOCUMENTATION section
   doc_dir(
      dir     => "../web/pod"     ,
      section => "DOCUMENTATION"
   );

   # SCREENS section
   doc_dir(
      ignore     => ['func.pl','web_run_cmd.pl',
		     'web_view_file.pl','web_waitfor.pl'],
      dir      => "../web"	 ,
      section    => "SCREENS"
   );

   # INTERNALS section
   doc_dir(
      files     => ['func.pl','web_run_cmd.pl',
		    'web_view_file.pl','web_waitfor.pl'],
      dir      => "../web"	 ,
      section    => "INTERNALS"
   );

   doc_finish();

=cut
