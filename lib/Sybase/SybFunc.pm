# Copyright (c) 1999-2003 by Edward Barlow. All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.
#

package Sybase::SybFunc;

require	  5.000;
use	     Exporter;
use	     Carp;
use	     Socket;			 # needed for gethostbyaddr

$VERSION = 1.01;
use subs qw(myprint mycroak);

@ISA	    = qw(Exporter);

@EXPORT = qw(db_reformat_results db_parse_opt_D db_set_ignore db_add_line_number db_has_login_failed db_close_sybase db_syb_ping db_connect db_use_database db_query_to_hash db_query db_decode_row db_query_to_table db_msg_exclude db_msg_ok db_get_sql_text db_colorize_sql db_get_dirlist db_get_errorlogs db_eachobject db_set_mode db_is_passwd_expired db_set_debug db_set_web_page db_set_password_expire_check db_bcp_table db_get_interfaces db_set_option);

# PERL FUNCTION LIBRARY
# For documentation see POD at the end (or run pod2text etc... on this file)

use vars qw($DEBUG $NL);

db_set_web_page(0);

my($mode)="NORMAL";
my($die_on_error)=1;		    	# croak if you get serious error

my(@syb_msg_stack);		     	# internal message stack

my($db_is_passwd_expired);    # undefined if you want to croak at login
										# if expired password. 0 then password ok
										# or you have not logged in yet.
										# set to 1 if password expired.

my($ignore_error_message)=0;	# Ignore a failure condition.  Required for
										# 2007 messages from the server
my($print_pre_on_msg)=1;

my($login_failed_reason);	   # SET TO A VALUE IF YOU FAILED TO LOGIN

dbmsghandle ("message_handler_mine");
dberrhandle ("error_handler_mine");

sub mycroak { croak @_; }
sub myprint { print join("",@_); }

my($print_handler,$croak_handler)=(\&myprint,\&mycroak);

use Sybase::DBlib;
use strict;
my($dbproc);

sub db_set_option
{
	my(%OPT)=@_;
	$print_handler=$OPT{-print} if defined $OPT{-print};
	$croak_handler=$OPT{-croak} if defined $OPT{-croak};
}

# =========================
# PROC:		 db_parse_opt_D
# DESCRIPTION:	  given opt_D return all matching databases
# =========================
sub db_parse_opt_D
{
	my($db)=@_;
	my(@list)=split(/,/,$db);
	my($cmd)='select name from sysdatabases where ';
	foreach (@list) {
		$cmd .= "name like \"$_\" or ";
	}
	$cmd =~ s/or $//;
	return db_query('master',$cmd);
}

# =========================
# PROC:		 db_close_sybase
# DESCRIPTION:	  cleans up and exits the server
# PARAMETERS:   none
# RETURNS:  no return code
# =========================
sub db_close_sybase
{
	db_debugmsg("Exiting Sybase") ;
	if( defined $dbproc ) { $dbproc->dbclose() };
}

sub db_has_login_failed
{
	return $login_failed_reason;
}

# =========================
# PROC:	 db_syb_ping
# DESCRIPTION:  pings the server
# PARAMETERS:   $svr,$login,$password
# RETURNS:      no return code
# =========================
sub db_syb_ping
{
	my($olddie_on_error)=$die_on_error;
	my($oldmode)=db_set_mode("INLINE");
	$die_on_error=0;

	db_set_password_expire_check();

	my($svr,$login,$password)=@_;

	&dbmsghandle ("message_handler_mine");
	&dberrhandle ("error_handler_mine");
	@syb_msg_stack=();

	undef $login_failed_reason;
	my($newdbproc) = new Sybase::DBlib $login, $password, $svr;

	db_set_mode($oldmode);
	$die_on_error=$olddie_on_error;

	my($msg);
	if( defined $login_failed_reason
		and $login_failed_reason eq "SERVER DOWN" ) {
		$msg=$login_failed_reason;
	} elsif( db_is_passwd_expired() ) {
		$msg="LOGIN EXPIRED";
	} else {
		$msg="";
		$msg="FAILED: ".$login_failed_reason if defined $login_failed_reason;
	}
	$msg.=join("$NL",@syb_msg_stack) if $#syb_msg_stack>=0;
	return $msg unless defined $newdbproc;

	$newdbproc->dbclose();
	return 0;
}

# =========================
# PROC:		 	db_connect($server,$login,$password)
# DESCRIPTION:	connects to sybase using login & password
# PARAMETERS:  server, login, password as per normal
# RETURNS:  	1/0
# =========================
sub db_connect
{
	my($srvr,$login,$passwd,$allow_bcp_in)=@_;
	db_debugmsg("Connecting To Sybase") ;

	BCP_SETL(TRUE) if defined $allow_bcp_in;
	&$croak_handler("Sybase Environment Must Be Set")
		unless defined $ENV{"SYBASE"};

	my($old_die_on_err)=$die_on_error;
	$die_on_error=0;
	&dbmsghandle ("message_handler_mine");
	&dberrhandle ("error_handler_mine");
	undef $login_failed_reason;
	$dbproc = new Sybase::DBlib $login, $passwd, $srvr;
	if( ! defined $dbproc ) {
		db_debugmsg( "Can't connect to the $srvr Sybase server as login $login.");
		db_debugmsg( $login_failed_reason )
			if defined $login_failed_reason;
		$die_on_error=$old_die_on_err;
		return 0;
	}

	$die_on_error=$old_die_on_err;
	db_debugmsg( "Successful Login To Sybase as $login" );
	1;
}

# =========================
# PROC:		 db_use_database($db)
# DESCRIPTION:	  use a database
# PARAMETERS:   $db - database name
# RETURNS:  returns 1 (success) or 0 (fail)
# =========================
sub db_use_database
{
	return $dbproc->dbuse(@_);
}

# =========================
# PROC:		 db_query_to_hash
# DESCRIPTION : Creates a hash output based on keys
# =========================
sub db_query_to_hash
{
	my($db,$query,$keysep,$k,$v)=@_;
	db_debugmsg("Running Cmd To Hash Keys=@$k Vals=@$v");

	my(@keys)=@$k;
	my(@values)=@$v;
	my(%results_hash);
	foreach my $row ( db_query($db,$query) ) {
		my @rc=db_decode_row($row);
		my $key = "";
		foreach (@keys) {
			$key.=$keysep unless $key eq "";
			$key.=$rc[$_];
		}
		my $val = "";
		foreach (@values) {
			$val.="~~" unless $val eq "";
			$val.=$rc[$_];
		}
		db_debugmsg("Res $key => $val");
		$results_hash{$key} = $val;
	}
	return(%results_hash);
}

# =========================
# PROC:		 db_query
# DESCRIPTION:	  run a command
# PARAMETERS:   $db - database name
#			  $query - what to run
#			  $die_on_err (optional) - if defined then mycroak if an error occurrs
#			  $print_hdr - if defined first row will be header info
# RETURNS:  returns array (success) or undef (fail). array must be decoded
# =========================
sub db_query
{
	my(@output_results)=();
	my($old_die_on_err)=$die_on_error;
	@syb_msg_stack=();

	my($db,$query,$print_hdr,$add_batchid,$srv,$login,$pass,$nullstr);
	($db,$query,$die_on_error,$print_hdr,$add_batchid,$srv,$login,$pass,$nullstr) = @_;
	db_debugmsg("Running db_query($db,$query)") ;

	$die_on_error = $old_die_on_err if ! defined $die_on_error;

	my($dbprocess,$oldmsg,$olderr);
	undef $login_failed_reason;
	if( defined $srv ) {
		# NEW LOGIN JUST FOR THIS COMMAND
		db_debugmsg( "CONNECTING TO SERVER $srv LOGIN $login");
		&$croak_handler("Sybase Environment Must Be Set")
			unless defined $ENV{"SYBASE"};
		&$croak_handler("Login Not Set")
			unless defined $login and $login ne "";
		&$croak_handler("Password Not Set")
			unless defined $pass and $pass ne "";
		&$croak_handler("Server Not Set")
			unless defined $srv and $srv ne "";
		$oldmsg=dbmsghandle ("message_handler_mine");
		$olderr=dberrhandle ("error_handler_mine");
		$dbprocess = new Sybase::DBlib $login, $pass, $srv;
	} else {
		$dbprocess=$dbproc;
	}

	# When u return, mycroak if u want to, reset mycroak on error, and return either
	# the message stack or an error
	if( ! defined $dbprocess ) {
		dbmsghandle ($oldmsg) if defined $srv;
		dberrhandle ($olderr) if defined $srv;
		db_debugmsg("No Connection For Query $query" );
		if( defined $srv or defined $login or defined $pass ) {
			&$croak_handler("No Connection To Sybase - You tried to connect to server=$srv using login=$login") if $die_on_error;
		} else {
			&$croak_handler("No Connection Available To Sybase - please connect before calling this function.") if $die_on_error;
		}
		$die_on_error = $old_die_on_err;
		return @syb_msg_stack if $mode eq "INLINE";
		return undef;
	}

	if(! defined $query or ! defined $db or $query eq "" ) {
		db_debugmsg("db_query requires query" );
		dbmsghandle ($oldmsg) if defined $srv;
		dberrhandle ($olderr) if defined $srv;
		$dbprocess->dbclose() if defined $srv;
		&$croak_handler("No Query Passed or db name not Passed")
			if $die_on_error;
		$die_on_error = $old_die_on_err;
		return @syb_msg_stack if $mode eq "INLINE";
		return undef;
	}

	db_debugmsg( "Running $query" );

	my($rc)=$dbprocess->dbuse($db) if $db ne "";
	if( $db ne "" and $rc == 0 ) {
		dbmsghandle ($oldmsg) if defined $srv;
		dberrhandle ($olderr) if defined $srv;
		$dbprocess->dbclose() if defined $srv;
		&$croak_handler("Cant Use Database $db") if $die_on_error;
		$die_on_error = $old_die_on_err;
		return @syb_msg_stack if $mode eq "INLINE";
		return undef;
	}

	$dbprocess->dbcmd($query);
	$rc = $dbprocess->dbsqlexec();
	if(( ! defined $rc or $rc==0 ) and $ignore_error_message==0 ) {
		dbmsghandle ($oldmsg) if defined $srv;
		dberrhandle ($olderr) if defined $srv;
		$dbprocess->dbclose() if defined $srv;
		&$croak_handler("\nERROR: Dbsucceed Failed ($rc $ignore_error_message)")
			if $die_on_error;
		$die_on_error = $old_die_on_err;
		return @syb_msg_stack if $mode eq "INLINE";
		return undef;
	}

	my(@dat)=();
	my($batchid);
	$batchid=1 if defined $add_batchid;
	my( $header_count );
	db_debugmsg("Executing...") ;
	while( $dbprocess->dbresults != NO_MORE_RESULTS  ) {
		$header_count=1 if defined $print_hdr;
		while ( @dat = $dbprocess->dbnextrow()) {
			unshift @dat,"BATCH ".$batchid if defined $add_batchid;
			# ASSUME PRINTS SHOULD COME BEFORE ANYTHING ELSE
			if( $#syb_msg_stack>=0 ) {
					push @output_results, @syb_msg_stack;
					#unshift @output_results, @syb_msg_stack;
					@syb_msg_stack=();
			}

			if( defined $header_count ) {
					# Populate Header
					my @header = ();
					my($ncol) = $dbprocess->dbnumcols;
					for( my($i)=1; $i<=$ncol; $i++ ) {
						push(@header,$dbprocess->dbcolname($i));
					}
					unshift @header,"BATCH ".$batchid if defined $add_batchid;
					push(@output_results,join("~~",@header));
					undef $header_count;
			}

			my($outstr)="";
			foreach (@dat) {
				if( ! defined $_ ) {
					if( defined $nullstr ) {
						$outstr .= $nullstr."~~";
					} else {
						$outstr .= "~~";
					}
				} else {
					$outstr .= $_."~~";
				}
			}
			$outstr =~ s/~~$//;

			push @output_results, $outstr;

			# push(@output_results,join("~~",@dat));
		}

		# If there were no rows returned
		if( $#syb_msg_stack>=0 ) {
				#unshift @output_results, @syb_msg_stack;
				push @output_results, @syb_msg_stack;
				@syb_msg_stack=();
		}

		$batchid++ if defined $add_batchid;
	}
	db_debugmsg("Completed Query ",($#output_results+1)," rows...") ;

	dbmsghandle ($oldmsg) if defined $srv;
	dberrhandle ($olderr) if defined $srv;
	$dbprocess->dbclose() if defined $srv;
	$die_on_error = $old_die_on_err;
	if( $#syb_msg_stack >= 0 ) {
		push @output_results,@syb_msg_stack;
		@syb_msg_stack=();
	}
	return @output_results;
}

# =========================
# PROC:		 db_decode_row
# DESCRIPTION:	  decodes packed row returned by the functions
# PARAMETERS:   packed row
# RETURNS:  array - each element is a cell for row
# =========================
sub db_decode_row
{
	my($rc)=@_;

	$rc =~ s.<PRE>..g;	      # REMOVE PRE, BR TAGS
	$rc =~ s.<BR>\s*..g;
	$rc =~ s.</PRE>..g;
	$rc =~ s.~~$.~~ .;
	$rc =~ s.~~\s*~~.~~~~.g;

	return split("~~",$rc);
}

# =========================
# PROC:		 db_query_to_table
# =========================
sub db_query_to_table
{
	my($db,$query,$print_hdr, $table_options, $die_on_error, @pargs) = @_;
	my($old_mode) = db_set_mode("INLINE");
	die "Invalid Argument List: db=$db query=$query printhdr=$print_hdr tblopts=$table_options die=$die_on_error args=",join(" ",@pargs)," (",$#pargs,")" unless $#pargs%2==1 or $#pargs<0;
	my(%options) = @pargs;

	my($TBLHDRCOLOR,$TBLCELLCOLOR);

	if( defined $options{"TBLHDRCOLOR"} ) {
		$TBLHDRCOLOR = "BGCOLOR=".$options{"TBLHDRCOLOR"};
	} else {
		$TBLHDRCOLOR = "BGCOLOR=#FFFFBB";
	}

	if( defined $options{"TBLCELLCOLOR"} ) {
		$TBLCELLCOLOR = "BGCOLOR=".$options{"TBLCELLCOLOR"};
	} else {
		$TBLCELLCOLOR = "BGCOLOR=#FFFFEE";
	}

	db_debugmsg( "Running Command To Table: $query" );

	# WE HAVE DEFINED BATCHES HERE - OUTPUT OF PRINTS NORMAL
	# OUTPUT OF BATCHES START WITH "BATCH #" IN FIRST FIELD
	my(@rc)=db_query( $db, $query, $die_on_error, $print_hdr,1 );

	my(@output);
	my($current_batch) = "GARBAGE STRING!";
	my($in_table)      = 0;
	my($in_pre_block)  = 0;
	my($table_count)   = 0;

	foreach (@rc) {
		my(@x)=db_decode_row $_;

		if( $current_batch ne $x[0] ) {

			# NEW BATCH OR PRINT STATEMENT SO START NEW TABLE
			if( $in_table ) {
					push @output,"</TABLE><p>\n";
					$table_count--;
					$in_table=0;
			}

			# DIFFERENT FIRST FIELD
			# - is either a print
			# - or the first row of batch - header if it is set
			if( $x[0] =~ /BATCH/ ) {

				if( $in_pre_block ) {
					push @output,"</b></PRE>\n";
					$in_pre_block=0;
				}

				push @output,"<TABLE $table_options>\n";
				$in_table   =1;
				$table_count++;
				$current_batch=$x[0];
				shift @x;
				if( defined $print_hdr )  {
					$options{"TBLHDRFONT"}="" unless defined $options{"TBLHDRFONT"};
					push @output,
						"<TR><TH $TBLHDRCOLOR><b>".$options{"TBLHDRFONT"}.join("</b></TH><TH $TBLHDRCOLOR><b>".$options{"TBLHDRFONT"},@x)."</b></TH></TR>\n";
				} else {
					$options{"TBLCELLFONT"}="" unless defined $options{"TBLCELLFONT"};
					push @output,"<TR><TD $TBLCELLCOLOR>&nbsp;".$options{"TBLCELLFONT"}.join("</TD><TD $TBLCELLCOLOR>&nbsp;".$options{"TBLCELLFONT"},@x)."</TD></TR>\n";
				}
			} else {

				# YUP ITS A PRINT SO JUST PRINT IT!
				# UNLESS IT IS A PLAIN LINE - THEN IGNORE IT
				my $pr_stmt=join(" ",@x);
				next if $pr_stmt =~ /^\s*$/;    # ignore blank lines
				$pr_stmt =~ s/\s*$//;		   # rtrim line

				$pr_stmt = "<PRE><b>".$pr_stmt if ! $in_pre_block;
				$in_pre_block=1;
				push @output,$pr_stmt."\n";
				next;
			}
		} else {
			if( $in_pre_block ) {
				push @output, "</b></PRE>\n";
				$in_pre_block=0;
			}
			# REGULAR ROW IN TABLE SO PRINT IT AS A ROW
			shift @x;
			push @output,"<TR><TD $TBLCELLCOLOR>&nbsp;".join("</TD><TD $TBLCELLCOLOR>&nbsp;",@x)."</TD></TR>\n";
		}
	}
	if( $in_pre_block ) {
		push @output,"</b></PRE>\n";
		$in_pre_block=0;
	}
	push @output,"</TABLE><p>\n" while $table_count-- > 0;
	db_set_mode($old_mode);
	@output;
}

# =========================
# PROC:		 db_msg_exclude
# DESCRIPTION:	  adds to list of messages to exclude
# PARAMETERS:   message number
# RETURNS:  N.A.
# NOTES:  Currently list of messages is
#	       5701,3,4 - use database successful message
#	       4002       - login incorrect message
#	       911,13,16,18,19				 - Unable To Use Db
#    20012     - srv name not in interfaces file
#    20018     - general message
#    20019     - results pending
# =========================
my(@exclude_msgs)=(911,5701,5703,20019,5704,4002,913,918,919,916,20012,20014,20018,20009,SYBESMSG,2409);
sub db_msg_exclude
{
	my($msg)=@_;
	push @exclude_msgs,$msg if defined $msg;
	return @exclude_msgs;
}

# =========================
# PROC:		 db_msg_ok
# DESCRIPTION:	  remove message from list of messages to exclude
# PARAMETERS:   message number
# RETURNS:  N.A.
# =========================
sub db_msg_ok
{
	my($msg)=@_;
	my($cnt) = -1;
	while( defined $msg and $cnt++ <= $#exclude_msgs ) {
		if( $exclude_msgs[$cnt] eq $msg ) {
			undef $exclude_msgs[$cnt];
			return;
		}
	}
	return @exclude_msgs;
}

# =========================
# PROC:		 DEFAULT Message Handler
# DESCRIPTION:	  handle messages sent from server
# NOTES:		Eliminates sp_recompile messages automatically
#			  Eliminates messages on exclude list
#			       Sets @syb_msg and RETURN or PRINT based on $mode
# =========================
sub message_handler_mine
{
	my($db, $message, $state, $severity, $text, $server, $procedure, $line)=@_;
	my(@str)=();				    # FORMATTED MESSAGES - 1 LINE PER ITEM
	chomp $text;				    # MESSAGES HAVE NO NEW LINE

	# IF YOU HAVE PASSWORD EXPIRED (4022) & WANT TO CHECK - DO IT HERE
	if( $message == 4022 and defined $db_is_passwd_expired ) {
		$db_is_passwd_expired=1;
		db_debugmsg( "PASSWORD EXPIRED $message" );
		return 0;
	}

	# IS MESSAGE TO BE EXCLUDED
	foreach (@exclude_msgs) {
		next unless $_  eq $message     # MESSAGE NUMBER EXCLUDED
				or $text =~ /$_/;		       # TEXT EXCLUDED
		db_debugmsg( "IGNORING MESSAGE $message" ) if $message eq $_;
		db_debugmsg( "IGNORING MESSAGE $message $_" ) if $message ne $_;
		$login_failed_reason="REASON: LOGIN INCORRECT"
			if $message==4002;
		$login_failed_reason="REASON: SERVER NOT FOUND IN INTERFACES ($text)"
			if $message==20012;
		$login_failed_reason="SERVER DOWN"
			if $message==20009;
		return 0;
	}

	# Eliminate sp_recompile messages
	return 0
		if $text=~/Each stored procedure and trigger that uses table/;

	db_debugmsg( "MESSAGE HANDLER CALLED (message=$message, $text)" );

	if ($severity > 10) {
		push @str, "Serious or Fatal message ".$message.", Severity ".$severity.", state ".$state;
		push @str,"(mode=$mode, die=$die_on_error)";
		push @str, "Server '".$server."'" if defined ($server);

		# &dbstrcpy returns the command buffer.
		if(defined($db)) {
			my ($lineno, $cmdbuff) = (1, undef);
			$cmdbuff = &Sybase::DBlib::dbstrcpy($db);
			foreach (split (/\n/, $cmdbuff)) {
				push @str , sprintf ("%5d", $lineno ++). "> ". $_;
			}
		}

		push @str, "Procedure '".$procedure."'" 	if defined ($procedure);
		push @str, "Line ". $line 						if defined ($line);
		push @str,  $text;

		if( $mode eq "NORMAL" or $die_on_error ) {
			unshift @str,"<PRE>" if $print_pre_on_msg==1;
			push @str,"</PRE>"   if $print_pre_on_msg==1;
			&$croak_handler(join("$NL",@str)."$NL FATAL DATABASE ERROR\n") if $die_on_error;
			&$print_handler(join("$NL",@str)."$NL");
		} else {
			push @syb_msg_stack,@str;
		}

		push @syb_msg_stack, "-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-";
		return 0;

	#} elsif ($message == 0) {
	} else  {
		# OTHERWISE JUST PUT THE MESSAGE ON THE STACK
		if( $mode eq "NORMAL" ) {
			&$print_handler("<PRE>" )
				if $print_pre_on_msg==1;
			&$print_handler($text,$NL);
			&$print_handler("</PRE>"   )
				if $print_pre_on_msg==1;
		} else {
			push @syb_msg_stack, $text;
		}
	}

	return 0;
}

# =========================
# PROC:		 DEFAULT Error Handler
# DESCRIPTION:	  print Error sent from server
# NOTES:		Handles messages on exclude list
# =========================
sub error_handler_mine {
	my ($db, $severity, $error, $os_error, $error_msg, $os_error_msg) = @_;

	$ignore_error_message=0;

	# IF YOU HAVE PASSWORD EXPIRED (4022) & WANT TO CHECK - DO IT HERE
	if( $error == 4022 and defined $db_is_passwd_expired ) {
		$db_is_passwd_expired=1;
		db_debugmsg( "PASSWORD EXPIRED $error" );
		return INT_CANCEL;
	}

	foreach (@exclude_msgs) {
		next unless $error == $_ and $_ ne $error_msg;
		db_debugmsg( "IGNORING ERROR $error" );
		$login_failed_reason="REASON: LOGIN INCORRECT" if $error==4002;
		$login_failed_reason="REASON: SERVER NOT FOUND IN INTERFACES ($error_msg)" if $error==20012;
		$login_failed_reason="SERVER DOWN" if $error==20009;
		$ignore_error_message=1 if $error == 2007;	      # dependencies
		$ignore_error_message=1 if $error == 20018;	     # general
		return INT_CANCEL;
	}

	db_debugmsg( "SYBASE ERROR HANDLER (error=$error severity=$severity)" );

	if( $mode eq "NORMAL" or $die_on_error ) {
			my $str="";
			$str.= "<PRE>"
				if $print_pre_on_msg==1;
			$str.= "ERROR # $error: $error_msg \n";
			$str.= "OS ERROR $os_error: ".$os_error_msg."\n"
				if defined $os_error_msg;
			$str.= "</PRE>"
				if $print_pre_on_msg==1;
			&$croak_handler($str."$NL FATAL SYBASE ERROR\n") if $die_on_error;
			&$print_handler($str);
	} else {
			push @syb_msg_stack, "ERROR # $error: $error_msg";
			push @syb_msg_stack, "OS ERROR $os_error: ".$os_error_msg
				if defined $os_error_msg;
			&$croak_handler("FATAL SYBASE ERROR") if $die_on_error;
	}

	return INT_CANCEL;
}

# =========================
# PROC:		 db_get_sql_text
# DESCRIPTION:	  return text for an object
# NOTES:	 handles any object type in current db
# PARAMETERS:    $objname: object name in current db
#			 $print_go:  do you wish to include a line with the word go
# =========================
sub db_get_sql_text {
	my($objname,$print_go,$do_permissions,$do_drop) = @_;
	my $text;

	$dbproc->dbcmd ("select type from dbo.sysobjects where id = object_id(\"$objname\")");
	$dbproc->dbsqlexec;
	$dbproc->dbresults;
	my($objtype)="";
	while(($text) = $dbproc->dbnextrow) { $objtype = $text; }
	$objtype =~ s/\s//g;

	if( $objtype eq "" ) {
		return "/* ERROR OBJECT $objname NOT FOUND IN CURRENT DATABASE??? */\n";
	}

	$dbproc->dbcmd ("select text from dbo.syscomments where id = object_id(\"$objname\")");
	$dbproc->dbsqlexec;
	$dbproc->dbresults;

	my($html) = '';
	while(($text) = $dbproc->dbnextrow) { $html .= $text; }

	if( $html eq "" ) {
		# ITS A TABLE
		$dbproc->dbcmd ("exec sp__revtable \"$objname\"");
		$dbproc->dbsqlexec;
		while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
			while(($text) = $dbproc->dbnextrow) { $html .= $text."\n"; }
			$html.="go\n" if defined $print_go and $print_go;
		}

		#$dbproc->dbcmd ("exec sp__revindex \"$objname\"");
		#$dbproc->dbsqlexec;
		#while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
			#while(($text) = $dbproc->dbnextrow) {
			   #if( /^go$/ ) {
					#next if ! defined $print_go;
				#next if ! $print_go;
				#}
				#$html .= $text."\n";
			#}
		#}
	} else {
		$html.="\ngo\n" if defined $print_go and $print_go;
	}

	if( defined $do_permissions ) {
		$dbproc->dbcmd ("exec sp__helprotect \"$objname\", \@groups_only=\"Y\"");
		$dbproc->dbsqlexec;
		while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
			while(($text) = $dbproc->dbnextrow) { $html .= $text."\n"; }
			$html.="\ngo\n" if defined $print_go and $print_go;
		}
	}

	# add drop statement as first line
	if( defined $do_drop and $do_drop ) {
		my $drop;
		$drop = "TABLE"		 if $objtype eq "U";
		$drop = "PROCEDURE"     if $objtype eq "P";
		$drop = "TRIGGER"	       if $objtype eq "TR";
		$drop = "DEFAULT"	       if $objtype eq "D";
		$drop = "RULE"		  if $objtype eq "R";
		$drop = "VIEW"		  if $objtype eq "V";
		if( defined $drop ) {
			$drop = "if exists ( select * from sysobjects where name =\"$objname\" )\n\tDROP $drop $objname\n";
			$drop.="\ngo\n" if defined $print_go and $print_go;
			$html = $drop."\n".$html;
		}
	}

	return $html;
}

# =========================
# PROC:					 db_add_line_number
# DESCRIPTION:	  adds line number to text
# =========================
sub db_add_line_number
{
	my $txt = shift;
	my $out = "";
	my $count=1;
	foreach ( split /\n/,$txt ) {
		$out .= sprintf("%5d %s",$count,$_);
		$count++;
	}
	$out;
}

# =========================
# PROC:		 db_colorize_sql
# DESCRIPTION:	  formats sql text
# =========================
sub db_colorize_sql {

	 my $html = shift;
	 $html =~ s/\n/<br>\n/g;

	 # comments
	 $html =~ s/(\/\*.*?\*\/)/<FONTCOLOR="BLUE">$1<\/FONT>/g;

	 # use FONTCOLOR and then respace out later to FONT COLOR
	 $html =~ s/(\@\w+)/<FONTCOLOR="GREEN">$1<\/FONT>/ig;
	 $html =~ s/\b(not null|null)\b/<i>$1<\/i>/ig;
	 $html =~ s/\b(as|begin|between|declare|delete|drop|else|end|exec|exists|go|if|insert|procedure|return|set|update|values|from|into|select|where|and|or|create|order by)\b/<b>$1<\/b>/ig;
	 $html =~ s/\b(tinyint|smallint|int|char|varchar|float|double|datetime|smalldatetime|money|smallmoney|numeric|decimal|text|binary|varbinary|image)\b/<FONTCOLOR=\"DARKVIOLET\">$1<\/FONT>/gi;

	 $html =~ s/\t/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g;
	 $html =~ s/ /\&nbsp;/sg;
	 $html =~ s/FONTCOLOR/FONT COLOR/g;

	 "<CODE>".$html."</CODE>";
}

# =========================
# db_get_dirlist(): Returns list of files in directory
# =========================
sub db_get_dirlist
{
	my($dir) = @_;
	$dir = '.' unless defined($dir);
	return () unless -d $dir;

	# Get list of files in current directory
	opendir(DIR,$dir)
		|| &$croak_handler("Can't open(directory) $dir for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);
	return @dirlist;
}

# =========================
# PROC:		 db_get_errorlogs
# DESCRIPTION:	  returns a list of sybase errorlogs
# PARAMETERS:      errorlog_type:       if type=0 SYBASE
#									       if type=1 Backupserver
#									       if type=2 Both
# =========================
sub db_get_errorlogs
{
	my($errorlog_type,$SYBASE)=@_;
	my(@errorlogs)=();
	if( ! -d $SYBASE and ! -d $SYBASE."/install" ) {
		my(@files)=db_get_dirlist($SYBASE."/install");
		my(@runfiles)=grep(/RUN_/,@files);
		my($file);
		foreach $file (@runfiles) {

			# search through files for lines like errorlog: based on type of file
			open(RUNFILE,$file) || return undef;
			while(<RUNFILE>) {
				if( /SQL Server Information:/ ) {
					last if $errorlog_type==1;
				} elsif( /Backup Server Information:/ ) {
					last if $errorlog_type==0;
				} elsif( /errorlog:/ ) {
					my(@x)=split;
					push @errorlogs,$x[2];
					last;
				}
			}
			close(RUNFILE)
		}
	}
	@errorlogs;
}

# =========================
# PROC:		 db_debugmsg
# DESCRIPTION:	  debug message - only prints in debug mode
# =========================
sub db_debugmsg
{
	my($msg)=join("",@_);
	return unless defined $DEBUG;
	return if $msg eq "";
	chomp $msg;

	if( $NL eq "\n" ) {
		$msg="debug: $msg";
	} else { $msg="<FONT COLOR=BLUE>debug: $msg</FONT>";
	}

	&$print_handler($msg,$NL);
}

# =========================
# PROC:	 db_eachobject
# DESCRIPTION:  runs cmd on each object of type and print output
# PARAMETERS:   $do_print: print text of what you are doing too
#	       $type: object type
# =========================
sub db_eachobject
{
	my($db,$cmd,$type,$do_print,$die_on_error)=@_;

	&$print_handler("Database $db",$NL) if defined $do_print;
	&$croak_handler("Cant Use Database $db".$NL) unless db_use_database($db);

	# CHECK IF GROUP EXISTS
	my(@obj)=db_query($db,"select name from sysobjects where type=\"$type\" and uid=1");
	my($object);
	foreach $object (@obj) {
		&$print_handler("\t$cmd $object\n") if defined $do_print;
		my(@rc)=db_query($db,"$cmd $object",$die_on_error);
		foreach (@rc) { &$print_handler($_,$NL); }
	}
}

# =========================
# PROC: db_set_mode
# DESCRIPTION: sets the mode to NORMAL or INLINE
#		       returns current mode (before set)
# =========================
sub db_set_mode {
	my($rc)=$mode;
	($mode)=@_ if $#_>=0;
	return $rc;
}

# =========================
# PROC: db_is_passwd_expired
# DESCRIPTION: Returns 1 if Password Expired.  Else 0
#       Valid after login attempt & db_set_password_expire_check()
# =========================
sub db_is_passwd_expired {
	&$croak_handler("Cant check password expiration yet - run db_set_password_expire_check()")
		unless defined $db_is_passwd_expired;
	return $db_is_passwd_expired;
}

# =========================
# PROC: db_set_debug
# DESCRIPTION: Sets Debug Flag
# =========================
sub db_set_debug {
	($DEBUG)=@_;
}

# =========================
# PROC: db_set_web_page
# DESCRIPTION: 1 = web page 0 if not
# =========================
sub db_set_web_page {
	my($webpg)=@_;
	if( $webpg ) {
		$NL="<br>\n";
		$print_pre_on_msg=1;
	} else {
		$NL="\n";
		$print_pre_on_msg=0;
	}
}

# =========================
# PROC: db_set_password_expire_check
# DESCRIPTION: Allows Pasword Expiration Checks
# =========================
sub db_set_password_expire_check {
	$db_is_passwd_expired = 0;
}

sub db_set_ignore {
	my($i)=@_;
	$ignore_error_message=$i;
}

# ==========================
# db_bcp_table(): bcp in a table
# ==========================
sub db_bcp_table
{
	my($tblname,$filename,$field_delimeter,$doprint,$batch_size,$return)=@_;

	$batch_size=1000			unless defined $batch_size;
	$return = "\n"		  		unless defined $return;
	$field_delimeter="~~"   unless defined $field_delimeter;

	&$print_handler( sprintf("%-30s ",$tblname) ) if $doprint;
	$dbproc->dbcmd("truncate table $tblname");
	$dbproc->dbsucceed(1);

	while( $dbproc->dbresults != NO_MORE_RESULTS  ) {
		while ( my @dat = $dbproc->dbnextrow()) {
			&$print_handler("DBG: ",join(" ",@dat),"\n") if $doprint;
		}
	}

	&$print_handler("Truncate ") if $doprint;

	if(! -s $filename) {
		&$print_handler("Ignoring (empty)\n") if $doprint;
		return;
	};

	#$dbproc->dbcmd("set identity_insert $tblname on");
	#$dbproc->dbsucceed(1);

	$dbproc->bcp_init($tblname,'','',DB_IN);

	open(INFILE,$filename) or &$croak_handler("Cant read $filename : $!");

	my($count)=0;
	my(@dat,$rc);
	my($numfields) = -1;

	foreach (<INFILE>) {
		chop;
		$_.=$field_delimeter."AA";
		s/$return/\n/g;
		@dat= split($field_delimeter,$_);
		pop @dat;
		$numfields=$#dat if $numfields == -1;
		push @dat,"" while $numfields>$#dat;
		print $_,"\n";
		foreach (@dat) { print "\t",$_,"\n"; }
		$dbproc->bcp_meminit(($#dat+1)) if $count==0;
		print "$field_delimeter $count $filename $#dat columns\n";
		$rc=$dbproc->bcp_sendrow(\@dat);
		if(( ++$count % $batch_size ) == 0 ) {
			&$print_handler(".") if $doprint;
			$dbproc->bcp_batch();
		}
	}

	$dbproc->bcp_batch();
	$dbproc->bcp_done();

	#$dbproc->dbcmd("set identity_insert $tblname off");
	#$dbproc->dbsucceed(1);

	&$print_handler(" ") if $count>$batch_size and $doprint;
	&$print_handler("Completed ($count rows)\n") if $doprint;
	close INFILE;
}

# Returns Reformatted Query
# expects queries to have been run in batch mode so as to remove prints
#
sub db_reformat_results
{
	my(@indat)=@_;
	my(@output,@rc);

	# remove spaces from @indat
	foreach (@indat) {
		s/\s+\~\~/\~\~/g;
		s/\s+$//g;
		next if /^$/;
		push @rc,$_;
	}

	# BUILD FORMAT STRING
	my(@widths)=(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);
	foreach my $dat ( @rc ) {
		my @rcdat=db_decode_row($dat);
		my $count=0;
		foreach ( @rcdat ) {
			$widths[$count] = length($_) if $widths[$count] < length($_);
			$count++;
		}
	}

	my($fmt)="";
	foreach (@widths) {
		last if $_ == -1;
		$fmt.="%-".$_."s ";
	}

	foreach( @rc ) {
		chomp;
		next if /^\s*$/;
		push @output,sprintf($fmt,db_decode_row($_));
	}
	return @output;
}

sub db_get_interfaces
{
	my($search_string,$filename)=@_;

	die "ERROR: SYBASE ENV not defined" unless defined $ENV{"SYBASE"};

	my(@srvlist,%port,%host,%found_svr);

	if( defined $filename and ! -e $filename ) {
		print "Warning: File $filename Not Found\n";
	}

	if( -e $ENV{"SYBASE"}."/interfaces" or -e $filename ) {

		$filename=$ENV{"SYBASE"}."/interfaces" unless defined $filename;

		# UNIX
		open FILE,$filename or die "ERROR: Cant open $filename : $!";
		my($srv,$dummy);
		while( <FILE> ){
			next if /^#/;
			chop;
			if( /^\s/ ) {
				next unless /query/;
				next if $srv eq "";
				chomp;

				my(@vals)=split;

				# Sun Binary Format
				if( $vals[4] =~ /^\\x/ ) {
					#format \x
					$vals[4] =~ s/^\\x0002//;

					my($p) = hex(substr($vals[4],0,4));
					my($pk_ip) = pack('C4',
						hex(substr($vals[4],4,2)),
						hex(substr($vals[4],6,2)),
						hex(substr($vals[4],8,2)),
						hex(substr($vals[4],10,2)));
					my($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
					my($h) =$name ||
						hex(substr($vals[4],4,2)).".".
						hex(substr($vals[4],6,2)).".".
						hex(substr($vals[4],8,2)).".".
						hex(substr($vals[4],10,2));

					if( defined $host{$srv}  ) {
						$host{$srv}.="+".$h;
						$port{$srv}.="+".$p;
					} else {
						$host{$srv}=$h;
						$port{$srv}=$p;
					}
				} else {
					$port{$srv} = $vals[4];
					$host{$srv} = $vals[3];
				}

				push @srvlist,$srv unless defined $found_svr{$srv};
				$found_svr{$srv} = 1;
				# $srv="";
			} else {
				($srv,$dummy)=split;
			}
		}
		close FILE;

	} elsif( -e $ENV{"SYBASE"}."/ini/sql.ini"  ) {

		# DOS
		# fix up sybase env vbl
		$ENV{"SYBASE"} =~ s#\\#/#g;
		open FILE,$ENV{"SYBASE"}."/ini/sql.ini"
			or die "ERROR: Cant open file ".$ENV{"SYBASE"}."/ini/sql.ini: $!\n";
		my($cursvr)="";
		while( <FILE> ){
			next if /^#/;
			next if /^;/;
			next if /^\s*$/;
			chop;

	#
	# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
	#
	#[NYSISW0035]
	#$BASE$00=NLMSNMP,\pipe\sybase\query
	#$BASE$01=NLWNSCK,nysisw0035,5000
	#MASTER=$BASE$00;$BASE$01;
	#$BASE$02=NLMSNMP,\pipe\sybase\query
	#$BASE$03=NLWNSCK,nysisw0035,5000
	#QUERY=$BASE$02;$BASE$03;

			if( /^\[/ ) {
				# IT A SERVER LINE
				s/^\[//;
				s/\]\s*$//;
				s/\s//g;
				$cursvr=$_;
				push @srvlist,$_ unless defined $found_svr{$_};
				$found_svr{$_} = 1;
			} else {
				# IT A DATA LINE
				next if /^master=/i;
				next if /QUERY=\$BASE/i;
				next if /\,.pipe/i;
				next if /NLMSNMP/i;
				(undef,$host{$cursvr},$port{$cursvr})=split /,/;
			}
		}
		close FILE;
	} elsif( defined $filename ) {
		die "ERROR: $filename error";
	} else {
		die "ERROR: UNABLE TO DETERMINE INTERACE FILE";
	}

	my(@rc);
	foreach (sort @srvlist ) {
		if(defined $search_string) {
			next unless /$search_string/i
				or $host{$_} =~ /$search_string/i
				or $port{$_} =~ /$search_string/i;
		}
		push @rc,[ $_,$host{$_},$port{$_} ];
	}
	return @rc;
}

1;

__END__

=head1 NAME

Sybase::SybFunc Module - Perl Sybase Subroutines

=head2 DESCRIPTION

This is my cut at a generic subroutine library for sybase perl applications.
I have used it for a variety of packages and it seems quite adequate.
It currently uses dblib.
These routines are useful if you only need a single connection to sybase.

=head2 AUTHOR

	Edward Barlow
	mail: edbarlow@mad.scientist.com
	url : http://www.edbarlow.com
	All Rights Reserved

=head2 FUNCTION OVERVIEW

 db_add_line_number
 db_bcp_table
 db_colorize_sql     - format sql text
 db_eachobject
 db_get_sql_text     - get sql text for an object
 db_get_dirlist
 db_has_login_failed
 db_is_passwd_expired
 db_msg_exclude      - tell message handler to ignore this message
 db_msg_ok	   - tell message handler to error out on this message
 db_set_ignore
 db_close_sybase     - close you connection
 db_syb_ping	 - ping your server
 db_connect	  - connect to your server
 db_use_database     - use database
 db_query_to_hash    - run_query_to_hash
 db_query	    - the main query
 db_decode_row       - interpret output from db_query()
 db_query_to_table   - command - output html formatted
 db_set_debug	- set debug option
 db_set_web_page      - set html_on option
 db_set_password_expire_check   - setup password expiration checking
 db_get_errorlogs    - return a list of error log files
 db_set_mode	 - set run mode

=head2 LIBRARY FUNCTIONS

=over 4

=item * db_close_sybase()

 DESCRIPTION: cleans up and exits the server
 RETURNS:     no return code

=item * db_syb_ping($svr,$login,$password)

 DESCRIPTION:    Ping server
 RETURNS:	0 success or $msg string

=item * db_connect($server,$login,$password)

 DESCRIPTION: connects to sybase using login & password.
 RETURNS:     1/0

=item * db_use_database($db)

 DESCRIPTION: use a database
 PARAMETERS:  $db - database name
 RETURNS:     returns 1 (success) or 0 (fail)

=item * db_query($db,$query,$die_on_error,$print_hdr,$add_batchid,$srv,$login,$pass)

 DESCRIPTION:   run a command
 PARAMETERS:
   $db	  - database name
   $query       - what to run
   $die_on_err (optional) - if defined then die if an error
   $print_hdr   - if defined first row will be header info
   $add_batchid - batch id should be added
   $srv	 - OPTIONAL SERVER ONLY FOR THIS COMMAND
   $login       - OPTIONAL LOGIN TO SERVER ONLY FOR THIS COMMAND
   $pass	- OPTIONAL PASSWORD TO SERVER ONLY FOR THIS COMMAND
 RETURNS:  returns array (success) or undef (fail). The array must
	     be decoded

=item * db_decode_row($row)

 DESCRIPTION:  decodes packed row returned by the functions
 PARAMETERS:  $row - packed row
 RETURNS:  array - each element is a cell for row

=item * db_query_to_table($db,$query,$print_hdr, $table_options)

 DESCRIPTION: Run sybase query to table (or multiple tables)
 RETURNS: table formatted output

=item * db_msg_exclude()

 DESCRIPTION:  adds to list of messages to exclude
 PARAMETERS:  message number
 RETURNS:  N.A.
 NOTES:  Currently list of messages is

   5701,3,4 - use database successful message
   4002    - login incorrect message
   911,13,16,18,19     - Unable To Use Db
	2409		 - char set conversion
   20012     - srv name not in interfaces file
   20018     - general message
   20019     - results pending

=item * db_msg_ok($msg)

 DESCRIPTION:  remove message from list of messages to exclude
 PARAMETERS:  message number
 RETURNS:  N.A.

=item * db_get_sql_text($objname,$print_go)

 DESCRIPTION:  return text for an object
 NOTES:  handles any object type in current db
 PARAMETERS:
   $objname: object name in current db
   $print_go:  do you wish to include a line with the word go

=item * db_colorize_sql($html)

 DESCRIPTION:  formats sql text

=item * db_get_dirlist($directory)

 DESCRIPTION:  Returns list of files in directory

=item * db_get_errorlogs($errorlog_type,$SYBASE)

 DESCRIPTION: returns a list of sybase errorlogs.
 PARAMETERS:    errorlog_type:
   if type=0 SYBASE
   if type=1 Backupserver
   if type=2 Both

=item * db_eachobject($db,$cmd,$type,$do_print)

 DESCRIPTION: runs cmd on each object of type and print output
 PARAMETERS:
   $do_print: print text of what you are doing too
   $type: object type

=item *  db_set_mode($mode)

 DESCRIPTION: sets the mode to NORMAL or INLINE.
 RETURNS: Returns current mode (before set).

=item *  db_is_passwd_expired()

 DESCRIPTION: Returns 1 if Password Expired.  Else 0.
 Valid after login attempt & db_set_password_expire_check().

=item *  db_set_debug($debug_mode)

 DESCRIPTION: Sets Debug Flag

=item *  db_set_web_page($NL)

 DESCRIPTION: Sets NEWLINE  & controls printing of pre on errors

=item *  db_set_password_expire_check()

 DESCRIPTION: Allows Pasword Expiration Checks

=back

=cut
