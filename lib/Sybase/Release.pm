# Copyright (c) 1999 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package Sybase::Release;

require 5.000;
require Exporter;

$VERSION=1.0;

@ISA = qw(Exporter);
@EXPORT = qw(do_release);

#
# Release Manager FUNCTIONS By Ed Barlow
#
use strict;
use Cwd;
use File::Basename;
use DBIFunc;

$|=1;

my(%DO_ARGS);
my(%TBL_ROWS);
my(%objtype);
my($die_on_error)=1;
my(%optionpacks);

sub do_release {

    %DO_ARGS = @_;

#foreach (keys %DO_ARGS) { print __LINE__,"  $_ -> $DO_ARGS{$_} \n"; }
    # UNDEFINE KEY IF VALUE UNDEFINED
    foreach (keys %DO_ARGS) {
            delete $DO_ARGS{$_} unless defined $DO_ARGS{$_};
    }

    $die_on_error=1;
    $die_on_error=0         if defined $DO_ARGS{'batch_nodie'};

    # ================================
    # CHANGE TO WORKING DIRECTORY
    # ================================

    # SW DIR IS CODE DIRECTORY
    $DO_ARGS{"cur_directory"}=cwd() unless defined $DO_ARGS{"cur_directory"};

    install_die( "do_release() Option ctlfile and objects mutually exclusive\n")
            if defined $DO_ARGS{"ctlfile"} and defined $DO_ARGS{"objects"};
    install_die( "do_release() Must Pass ctlfile or objects\n")
            unless defined $DO_ARGS{"ctlfile"} or defined $DO_ARGS{"objects"};

    my(@filelist);

    # ==============================
    # YOU HAVE SPECIFIED A CTL FILE so CHECK CURRENT DIR then CODE DIRECTORY
    # ==============================
    if( defined $DO_ARGS{"ctlfile"} ) {

            if( ! -e $DO_ARGS{"ctlfile"} ) {
                    # No control file - is it in the dir?
                    if( defined $DO_ARGS{"dir"} ) {
                            $DO_ARGS{"ctlfile"}= $DO_ARGS{"dir"}."/".$DO_ARGS{"ctlfile"}
                                            if -e  $DO_ARGS{"dir"}."/".$DO_ARGS{"ctlfile"};
                    }
            }

            install_die($DO_ARGS{"ctlfile"}." not a file")
                    unless -e $DO_ARGS{"ctlfile"};

            my($cfgfile_dir)=dirname($DO_ARGS{"ctlfile"});
            chdir $cfgfile_dir or install_die("Cant cd to $cfgfile_dir: $!\n");
            $DO_ARGS{"ctlfile"} = basename($DO_ARGS{"ctlfile"});

            open( CTLFILE , $DO_ARGS{"ctlfile"} )
                    or install_die("cant open ctlfile:".$DO_ARGS{"ctlfile"}.": $!\n");
            foreach (<CTLFILE>) {
                    chomp;
                    push @filelist,$_;
            }
            close CTLFILE;

    }  else {
            # PROCS FROM THE COMMAND LINE
            @filelist=split /\s/,$DO_ARGS{"objects"} if defined $DO_ARGS{"objects"};
    }

    if( defined $DO_ARGS{"dir"} ) {
            install_die($DO_ARGS{"dir"}." not a directory")
                    unless -d $DO_ARGS{"dir"};
            chdir $DO_ARGS{"dir"}
                    or install_die("Cant cd to ".$DO_ARGS{"dir"}.": $!\n");
            $DO_ARGS{"cur_directory"}=$DO_ARGS{"dir"};
    }

    $DO_ARGS{"start_at_fileno"} = 1 unless defined $DO_ARGS{"start_at_fileno"};
    $DO_ARGS{"end_at_fileno"} = 999 unless defined $DO_ARGS{"end_at_fileno"};

    # ================================
    # CONNECT TO DB
    # ================================
    dbi_set_web_page(0);                                                             # not web page

    if( defined $DO_ARGS{connection} ) {
    	dbi_connect(-setconnection=>1, -connection=>$DO_ARGS{connection});
    } else {
    	dbi_connect(
            -srv=>$DO_ARGS{"servername"},
            -login=>$DO_ARGS{"username"},
            -password=>$DO_ARGS{"password"})
            or install_die("Cant connect to ".$DO_ARGS{"servername"}." as ".$DO_ARGS{"username"}."\n");
   }

    # EXCLUDE SILLY MESSAGES
    dbi_msg_exclude("3716");         # Rule bound to column
    dbi_msg_exclude("2714");         # Allready a rule!
    dbi_msg_exclude("2615");         # Key allready done - who cares about dup keys
    dbi_msg_exclude("New primary key added.");
    dbi_msg_exclude("New foreign key added.");
    dbi_msg_exclude("Authorization updated.");
    dbi_msg_exclude("Default database changed.");
    dbi_msg_exclude("Rule bound to datatype.");
    dbi_msg_exclude("Rule bound to table column.");
    dbi_msg_exclude(17348);
    dbi_msg_exclude(17983);
    dbi_msg_exclude("Audit option has been changed and has taken effect immediately.");
    dbi_msg_exclude("Configuration option changed. The SQL Server need not be rebooted since the option is dynamic.");
    dbi_msg_exclude(17982);
    dbi_msg_exclude(17419);
    dbi_msg_exclude("Default bound to datatype.");
    dbi_msg_exclude("Type added.");
    dbi_msg_exclude("User has been dropped from current database.");
    dbi_msg_exclude("The new default has been bound to columns(s) of the specified user datatype.");
    dbi_msg_exclude("New user added.");
    dbi_msg_exclude("New group added.");
    dbi_msg_exclude(2007);
    dbi_msg_exclude(20018);
    dbi_msg_exclude("<<< DROPPED PROCEDURE");
    dbi_msg_exclude("<<< CREATED PROCEDURE");

    # checkpoint your db's
    dbi_msg_exclude(17430);

    # db options
    dbi_msg_exclude(17433);
    dbi_msg_exclude(17434);

    # audit options
    dbi_msg_exclude(17982);
    dbi_msg_exclude(">>> CREATED");

    # ================================
    # GET LIST OF DATABASES
    # ================================
    my($srvtype,@databaselist)=dbi_parse_opt_D($DO_ARGS{"dbname"});
	print "Server type is $srvtype\n";
    %optionpacks=dbi_optionpacks($srvtype);

    #my(@databaselist)=dbi_query(-db=>"master",
            #-query=>"select name from sysdatabases where name like \'".$DO_ARGS{"dbname"}."\'");

    print "SERVER ".$DO_ARGS{"servername"}." WORKING ON DATABASEs: ",join(" ",@databaselist),"\n";
    print "  DIR ".$DO_ARGS{"cur_directory"}
                    if defined $DO_ARGS{"cur_directory"};
    print "  CONTROL FILE ".$DO_ARGS{"ctlfile"}
                    if defined $DO_ARGS{"ctlfile"};
    print "\n"
                    if defined $DO_ARGS{"ctlfile"} or defined $DO_ARGS{"cur_directory"};

    print "hmmmm.... no databases found matching ".$DO_ARGS{"dbname"}."\n"
                     unless $#databaselist >= 0;
    return unless $#databaselist >= 0;

    if( !defined $DO_ARGS{"batch_die"} and ! defined $DO_ARGS{batch_nodie} ) {
            print "< HIT ENTER KEY TO CONTINUE >";
            <STDIN>;
    }

    # ================================
    # DO AN INSTALLATION
    # ================================
    my($db,$file);
    foreach $db (@databaselist) {
            print "  DATABASE: ",$db,"\n" if $#databaselist>0;

            my(@is_selectinto)=dbi_query(-db=>$db,-query=>"select 1 from master..sysdatabases where name='".$db."' and status&4=0",
            					-die_on_error=>$die_on_error);
            foreach (@is_selectinto) { print dbi_decode_row($_),"\n"; }
            #print "DB=$db IsSelectInto=",$#is_selectinto,"\n";

            dbi_use_database($db) or install_die("CANT USE DATABASE $db\n");

            # GET TYPES OF EXISTING OBJECTS INTO objtype
            undef %objtype;
	    print "Line: ",__LINE__,": sysobjects\n" if defined $DO_ARGS{debug};
            my(@rc)=dbi_query(-db=>$db,-query=>"select name,type from sysobjects where type!='S' and uid=1",-die_on_error=>$die_on_error);
	 	  print "Line: ",__LINE__,": ","Get Objects\n" if defined $DO_ARGS{debug};
            foreach (@rc) {
                    my($nm,$ty)=dbi_decode_row($_);
                    $nm=~s/\s//g;
                    $ty=~s/\s//g;
                    $objtype{$nm}=$ty;
            }

            # GET NUM ROWS OF TABLES INTO TBL_ROWS
            undef %TBL_ROWS;
            %TBL_ROWS=get_rows($srvtype,$db);

            my($cnt)=1;
            my($err)=0;
            foreach $file (@filelist) {
                    $file =~ s/\s//g;
                    next if $file =~ /^#/ or $file =~ /^$/;
                    $cnt++;
                    next if -e $file;

                    warn "\n  File #".$cnt.") WARNING: CANT FIND FILE $file\n";
                    $err++;
            }

            install_die("\nERROR: CANT FIND REQUIRED FILES in ".$DO_ARGS{"cur_directory"}."\n")
                    if $err>0 and ! defined $DO_ARGS{"batch_nodie"};

            $cnt=1;
            foreach $file (@filelist) {

                    $file =~ s/\s//g;
                    next if $file =~ /^#/ or $file =~ /^$/;

                    # get object name from file without extension
                    my($object_name)=basename($file);
                    $object_name=~s/\.\w*$//;

                    my($ok)=1;
                    my $reason="";

                    # START & STOP CONDITIONS
                    $ok=0 if defined $DO_ARGS{"start_at_fileno"}
                                    and $cnt < $DO_ARGS{"start_at_fileno"};
                    $ok=0 if defined $DO_ARGS{"end_at_fileno"}
                                    and $cnt > $DO_ARGS{"end_at_fileno"};
                    $reason="Start/Stop" if $ok==0;

                    my($object_type) = get_type($file);
                    # Handle procs_only by resetting $ok
                    if( $ok and defined $DO_ARGS{"procs_only"} ) {
                            $ok=0 unless $object_type =~ /PROC/;
                            $reason="Procs Only" if $ok==0;
                    }

                    # Handle new_obj_only by resetting $ok if file
                    if( $ok and defined $DO_ARGS{"new_obj_only"} ) {
                            $ok = 0 unless $object_type =~ /NEW/;
                            $reason="New Obj Only" if $ok==0;
                    }

                    # RULES & DEFAULTS
                    if( $ok and defined $DO_ARGS{"norules"} ) {
                            $ok=0 if $object_type =~ /RULE/;
                            $ok=0 if $object_type =~ /DFLT/;
                            $reason="No Rules" if $ok==0;
                    }

                    # BACKUP FILE if it is table
                    if( $ok == 1 ) {
            	            die "ERROR CAN NOT LOAD $file of type $object_type because \n\tSELECT INTO APPEARS TO NOT BE SET FOR $db\n"
            	            	if $object_type eq "TABLE" and $#is_selectinto>= 0;
                            install_file( $db, $file, $object_name, $object_type );
                    } else {
                            printf( "    ignoring   %-30s (user control - %s)\n",$file,$reason);
                    }
                    $cnt++;
            }

            # GET NEW TABLE COUNTS
            my(%NEW_TBL_ROWS)=get_rows($srvtype,$db);

            # CHECK ROWCOUNTS!
            foreach $file (@filelist) {

                    # get object name from file without extension
                    my($object_name)=basename($file);
                    $object_name=~s/\.\w*$//;

                    next unless defined $TBL_ROWS{$object_name};
                    next unless defined $NEW_TBL_ROWS{$object_name};

                    print "    Table Rows in $object_name used to be ".$TBL_ROWS{$object_name}." and now is $NEW_TBL_ROWS{$object_name}\n" if $NEW_TBL_ROWS{$object_name} != $TBL_ROWS{$object_name} and $TBL_ROWS{$object_name} > 0;
            }
    }
}

my(%type_map) = (
'S' , 'System' ,
'D' , 'Default',
'V' , 'View'   ,
'U' , 'Table'  ,
'P' , 'Proc'   ,
'TR', 'Trigger',
'R' , 'Rule'
);

# YOU HAVE EXISTING TABLE - BACK IT UP
sub     backup_table
{
    my($db,$table)  = @_;
    my($tbl_bak)    = $table."_BAK";

    my($msg)="";
    $msg=squery($db,"select name from sysobjects where name=\'".$table."\' and uid=1" );
#print "xxx that returned $msg\n";
    install_die("Cant Backup Table $table.  $table does not exist! |$msg|$table|\n") if $msg ne $table;

    $msg="";
    $msg=squery($db,"select name from sysobjects where name=\'".$tbl_bak."\' and uid=1" );
#print "xxx that returned $msg\n";
    install_die("Cant Backup Table $table.  $tbl_bak allready exists! This indicates that a prior release failed.  $tbl_bak is a backup of $table prior to the upgrade - make sure that the data is ok, drop $tbl_bak, and rerun this command\n") if $msg ne "";

    $msg="";
    $msg=squery($db,"select * into $tbl_bak from $table" );
    install_die("ERROR SAVING DATA $tbl_bak: $msg\n") if $msg ne "";

    return 1;
}

sub squery
{
    my($db,$query)=@_;
    my($msg)="";
	 print "Line: ",__LINE__,": $query\n" if defined $DO_ARGS{debug};
    #foreach (dbi_query(-db=>$db,-query=>$query,-die_on_error=>$die_on_error )) {
    foreach (dbi_query(-db=>$db,-query=>$query,-die_on_error=>0 )) {
			$msg.=join(" ",dbi_decode_row($_));
			#$msg .= $_;
		}
    $msg;
}

sub     restore_table
{
    my($db,$table)  = @_;
    my($tbl_bak)=$table."_BAK";

    my($msg)="";
    $msg=squery($db,"select count(*) from $tbl_bak" );

    my(%old_column,%new_column);
    my($svsql)="";

    # GET NAME, COLUMN, IDENTITY FIELDS FOR THE OLD TABLE!
    #  - SET %column{num} = name
    #  - set $table_has_identity.
    my($table_has_identity)="";
    my($query)="select name,colid,status&0x80 from syscolumns where id=object_id('".$tbl_bak."')\n";
	 print "Line: ",__LINE__,": ",$query if defined $DO_ARGS{debug};

    foreach (dbi_query(-db=>$db,-query=>$query,-die_on_error=>$die_on_error)) {
        my(@rc)=dbi_decode_row($_);
        $old_column{$rc[0]} = $rc[1];
        $table_has_identity=$rc[0] if $rc[2] ne "0";
    }

    # GET NAME, COLUMN, IDENTITY FIELDS FOR THE TABLE!
    $query="select name,colid,status&0x80 from syscolumns where id=object_id('".$table."')\n";
	 print "Line: ",__LINE__,": ",$query if defined $DO_ARGS{debug};
    foreach (dbi_query(-db=>$db,-query=>$query,-die_on_error=>$die_on_error)) {
        my(@rc)=dbi_decode_row($_);
        if( defined $old_column{$rc[0]} ) {
            $svsql.= $rc[0].",";
            delete $old_column{$rc[0]};
        }  else {
            $new_column{$rc[0]} = $rc[1];
        }
    }
    $svsql =~ s/,$//;                       # remove trailing comma
    if( $svsql =~ /^\s*$/ ) {
        print "CANT GET RESTORE INFORMATION - ARCHIVE TABLE $table BY HAND\n";
        foreach ( keys %old_column ) {
            print "\told table column $old_column{$_} name $_ not in new table\n";
        }
        foreach ( keys %new_column ) {
            print "\tnew table column $new_column{$_} name $_ not in new table\n";
        }
        install_die("Can Not Continue - DATA SAVED IN $tbl_bak\n");
    }

    my($hdr) = "set identity_insert $table on\n"
            if $table_has_identity ne ""
            and ! defined $old_column{$table_has_identity};
    my($ftr) = "\nset identity_insert $table off\n"
            if $table_has_identity ne ""
            and ! defined $old_column{$table_has_identity};

    $svsql = "$hdr insert $table ( $svsql ) select $svsql from $tbl_bak $ftr";
    $msg="";
    $msg=squery($db,$svsql );
    install_die("ERROR RUNNING $svsql: $msg\n") if $msg ne "";

    $msg="";
    $msg=squery($db,"drop table $tbl_bak" );
    install_die("ERROR DROPPING $tbl_bak: $msg\n") if $msg ne "";

    return 1;
}

sub install_file
{
    my($db,$file,$obj_nm,$type)=@_;

    return if $file =~ /^#/ or $file =~ /^$/;

    install_die("ERROR: CANT FIND FILE $file in ".$DO_ARGS{"cur_directory"})
            unless -e $file or defined $DO_ARGS{"batch_nodie"};
    install_die("ERROR: CANT READ FILE $file in ".$DO_ARGS{"cur_directory"})
            unless -r $file or defined $DO_ARGS{"batch_nodie"};

    my($num_rows_bkedup)="";

    if($type eq "TABLE"){
        # ITS AN EXISTING TABLE SO CHECK NUM ROWS
        if( $TBL_ROWS{$obj_nm}==0 ) {
           $num_rows_bkedup=0;
        } else {
           $num_rows_bkedup = backup_table($db,$obj_nm);
        }
    }

    if( $type =~ /NEW/ ) {
        printf( "    %-10s %-35s ",$type,$obj_nm);
    } elsif( $num_rows_bkedup eq "" ) {
        printf( "    %-10s %-30s      ",$type,$obj_nm);
    } elsif( $num_rows_bkedup eq "0" ) {
        printf( "    %-10s %-30s EMPTY",$type,$obj_nm);
    } elsif( $type eq "OTHER" or $type eq "SCRIPT" ) {
        printf("    %-10s %-30s SAVE ",$type,$file,$num_rows_bkedup);
    } else {
        printf("    %-10s %-30s SAVE ",$type,$obj_nm,$num_rows_bkedup);
    }

    my(@rc);
    if( $type eq "TABLE" or $type eq "VIEW" or $type eq "PROC" ) {
        print " drop ";
        my($str) = "if exists ( select * from sysobjects where name='".$obj_nm."' and uid=1) DROP ".$type." ".$obj_nm;
	 	  print "Line: ",__LINE__,": ",$str,"\n" if defined $DO_ARGS{debug};
        @rc=dbi_query(-db=>$db, -query=>$str ,-die_on_error=>$die_on_error);
        if( $#rc >= 0 ) {
           print "\nERROR: DROP PRODUCED OUTPUT\n";
           foreach (@rc) { print dbi_decode_row($_),"\n"; }
           install_die("exiting\n") unless defined $DO_ARGS{"batch_nodie"};
        }
    } else {
        print "      ";
    }

    print " Install ";

    open(SQL_IN, $file) || install_die("Can't open input $file: $!\n");
    my($batch_sql)="";
    my($batch_id)=1;
    my($rc);

    # Do a Use DB Here
    dbi_use_database($db);

	 my($linecount)=0;
    while (<SQL_IN>) {
		 $linecount++;
       if( /^go/ ) {
          # YOU NOW HAVE A BATCH SO RUN IT
          $batch_sql .="\n";
          if( ! defined $DO_ARGS{"noexec"} ) {
              if( $batch_sql =~ /DROP\s+TABLE\s+[a-z]/i
				  and $linecount<10
              and $batch_sql !~ /\w\s*DROP\s+TABLE\s+[a-z]/i ) {
                  # print "\n\t\tIgnoring drop table statement\n";
              } else {
                  if( $batch_sql =~ /^\s*use[ \t]+[\w_]+[ \t]*\n/i
                  or  $batch_sql =~   /\nuse[ \t]+[\w_]+[ \t]*\n/i ) {
                      print "\nPOSSIBLE USE DATABASE STATEMENT FOUND!!!\n$batch_sql";
                      print "< HIT ENTER KEY IF YOU WISH TO CONTINUE (RUNNING STMT) OR ctrl-C to ABORT>";
                      <STDIN>;
                  }
	 print "Line: ",__LINE__,": $batch_sql\n" if defined $DO_ARGS{debug};
                  @rc=dbi_query(-db=>"",-query=>$batch_sql,-die_on_error=>$die_on_error);
                  foreach (@rc) { print "\nBATCH $batch_id OUTPUT:",dbi_decode_row($_),"\n";}
              }
          } else {
              print "\t\tBatch ",$batch_id++," (noexec) $type\n";
          }
			$linecount=0;
          $batch_sql="";
       }  else {
          if( substr($_,0,2) eq ":r" ) {
              next if $_ =~ /:r database/;
              $_ = "dump tran sybsystemprocs with truncate_only"
                 if $_ =~ /:r dumpdb/;
          }

          # WAITING FOR GO - ADD TO SQL TO RUN
          $batch_sql .= $_;
       }
    }

    if( $num_rows_bkedup ne "" and $num_rows_bkedup ne "0" ) {
       print "Restore ";
       restore_table($db,$obj_nm)
    }

    print "\n";
    close(SQL_IN);
}

sub get_type
{
    my($filename)=@_;
    $filename = basename($filename);

    my($obj,$ext)=split(/\./,$filename);
    return "TABLE" if $objtype{$obj} eq "U" and $ext eq "tbl";
    return "PROC" if  $objtype{$obj} eq "P" and $ext eq "prc";
    return "RULE" if  $objtype{$obj} eq "R" and $ext eq "rul";
    return "DFLT" if  $objtype{$obj} eq "D" and $ext eq "def";
    return "TRIGGER" if  $objtype{$obj} eq "TR" and $ext eq "trg";
    return "VIEW" if  $objtype{$obj} eq "V" and $ext eq "vie";
    return "INDEX" if $ext eq "idx";
    return "NEW TABLE" if $ext eq "tbl";
    return "NEW PROC" if  $ext eq "prc";
    return "NEW RULE" if  $ext eq "rul";
    return "NEW DFLT" if  $ext eq "def";
    return "NEW TRIG" if  $ext eq "trg";
    return "NEW VIEW" if  $ext eq "vie";
    return "PROCEDURE" if  $ext eq "10";
    return "PROCEDURE" if  $ext eq "492";
    return "CONSTRAINT" if $ext eq "cst";
    return "INSRT" if $ext eq "ins";
    return "SCRIPT" if $ext eq "sql";
    return "OTHER";
}

sub install_die
{
    print "\n*****************************************************\n";
    print @_;
    print "*****************************************************\n";
    exit(2);
}

sub get_rows {
	my($srvtype,$db)=@_;
	my(%TBL_ROWS);
	print "Line: ",__LINE__,": ","Get Table Counts\n" if defined $DO_ARGS{debug};

	my($q)= "select distinct object_name(id),sum(rowcnt(i.doampg)) from sysindexes i group by id";
	$q= "select distinct object_name(id),sum(rowcnt) from sysindexes i where indid in (0,1,255) group by id"
	if $srvtype eq "SQLSERVER";
	$q= "select object_name(id),row_count(db_id('".$db."'),id) from sysobjects"
	if $optionpacks{IS_SYBASE_15} eq "TRUE";

	print "Line: ",__LINE__,": ","$q\n" if defined $DO_ARGS{debug};
	foreach (dbi_query(-db=>$db,-query=>$q,-die_on_error=>$die_on_error )) {
		my($tbl,$cnt)=dbi_decode_row($_);
		$TBL_ROWS{$tbl}=$cnt;
	}

	#foreach (keys %TBL_ROWS) { print $_,"\t",$TBL_ROWS{$_},"\n"; }
	#die "WOAH - SYSTEM 15" if $optionpacks{IS_SYBASE_15} eq "TRUE";
	return %TBL_ROWS;
}

1;

__END__


=head1 NAME

Sybase::Release Module - release management library for sybase and sql server

=head2 DESCRIPTION

Release Manager - perl interface to load database ddl.

=head2

This library loads object ddl the right way.  It manages drops and will copy the
data back.  It does this by ensuring a strict naming conventions.  Files must have the
appropriate extensions.  For example, x.tbl indicates that a script for the table x.  You
need a go in the file, but do not need to do any thing else.
The library manages will drop and recreate the table by archiving table data
to a permanent temporary table (via select into) and then copying it back.
Output is in a decently pretty format.

The module is semi intelligent.  It can handle reordering of column names and the addition
of columns.  If you change a column name, that column data will be lost (sadly).  The program
simply cant figure out how to map the saved data to the new data.  You will need
to do your own "save" and "repopulate" of the data in this case.

Requires DBI.pm and the appropriate DBD driver module

Files are installed in the order they are passed in - so another program
will need to manage file installation order.

Common messages are filtered and ignored - these include sysdepends etc...

=head2 USAGE

do_release( ARG=>VALUE, ARG=>VALUE... );

 objects             - space separated list of file names to install (in order)
 batch_die           - its a batch (no confirmation) and it should die
                cleanly if there are any errors
 batch_nodie    		- its a batch (no confirmation) and you want to
                                    continue if there are any errors

 username            - server login
 servername     		- server DSQUERY
 password            - server password
 dbname              - database to install into.  (include sybase wild cards)

 ctlfile             - optional control file
 dir                 - default directory objects will be found in
 start_at_fileno  	- opt. object # to start install at (restartablility)
 end_at_fileno  		- opt. object # to end install at (restartablility)
 noexec              - dont actually run anything
 norules             - ignore rules and defaults (.rul and .def) files
 nosave              - dont save table data
 procs_only     		- only stored procedures
 new_obj_only   		- only objects that dont exist

=head2 EXAMPLE CODE

    use Sybase::Release;
    do_release( batch_die => $opt_x,
                username => $opt_U,
                servername => $opt_S,
                password => $opt_P,
                dbname => $opt_D,
                ctlfile => $opt_c,
                dir => $opt_V,
                start_at_fileno => $opt_s,
                end_at_fileno => $opt_e,
                batch_nodie => $opt_b,
                noexec => $opt_n,
                norules => $opt_r,
                nosave => $opt_s,
                procs_only => $opt_T,
                new_obj_only => $opt_N,
                objects => join(" ",@ARGV),
    );

=cut
