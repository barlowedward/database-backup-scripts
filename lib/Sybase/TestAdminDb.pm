# Copyright (c) 2000 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package Sybase::SybAdminDb;

require 5.000;
use     Exporter;
use     Carp;
use     DBIFunc;

$VERSION = 1.02;

@ISA    = qw(Exporter);

@EXPORT = qw(db_open db_param db_close db_readfile db_readserver);

use     strict;
my(%DBM_DATA,@SYBASE_SERVERS,@BACKUP_SERVERS);
my($database_opened)=0;

sub db_open {
	my(%OPT)=@_;
	validate_params("db_open",%OPT);

	dbmopen(%DBM_DATA,$OPT{-dbmname},0666)
		or croak "Cant Open File ",$OPT{-dbmname},": $!\n";

	%DBM_DATA = () if defined $OPT{-clear};

	$database_opened=1;

	@SYBASE_SERVERS=split(/\s+/,$DBM_DATA{sybase_servers})
				if defined $DBM_DATA{sybase_servers};
	@BACKUP_SERVERS=split(/\s+/,$DBM_DATA{backup_servers})
				if defined $DBM_DATA{backup_servers};
}

sub db_close {
	my(%OPT)=@_;
	validate_params("db_close",%OPT);
	dbmclose %DBM_DATA;
	$database_opened=0;
}

sub db_param {
	my(%OPT)=@_;
	validate_params("db_param",%OPT);
	croak "Database Not Opened\n" unless $database_opened==1;

	my($keymod)="|".$OPT{-dbname} if defined $OPT{-dbname};

	if( defined $OPT{-keys} ) {
		# return the keys
		# you must pass type&server or type&name
		my(@return_keys);
		if( defined $OPT{-type} and defined $OPT{-server} ) {
			foreach( keys %DBM_DATA ) {
				my($pat)= "^".$OPT{-type}."\|[\w\_]+\|".$OPT{-server}.$keymod."\$";
				push @return_keys,$_ if /$pat/;
			}
		} elsif( defined $OPT{-type} and defined $OPT{-name} ) {
			#my($pat)= "^".$OPT{-type}."\|".$OPT{-name}."[\|\w]+".$keymod."\$";
			foreach( keys %DBM_DATA ) {
				next unless /^$OPT{-type}\|$OPT{-name}/o;
				next if defined $OPT{-dbname} and ! /\|${OPT{-dbname}}$/;
				push @return_keys,$_;
				#push @return_keys,$_ if $_ =~ /$pat/o;
			}
		} elsif( defined $OPT{-type} ) {
			foreach( keys %DBM_DATA ) {
				next unless /^$OPT{-type}\|/;
				next if defined $keymod and ! /$keymod\$/o;
				push @return_keys,$_;
			}
		} else {
			# return just a list of the types
			my(%types)=();
			foreach( keys %DBM_DATA ) {
 				my($t,$k,$s,$d)=split(/\|/,$_);
				$types{$t}++;
			}
			return keys %types;
			#  croak "Must Pass -name&-type or -type&-server or -type to retrieve keys\nKeys Passed: ".join(" ",keys %OPT)."\n";
		}
		return @return_keys;

	} elsif( defined $OPT{-value} ) {

		# set it
		if( $OPT{-name} eq "unix_systems"
		or  $OPT{-name} eq "unix_discover_date"
		or  $OPT{-name} eq "backup_servers"
		or  $OPT{-name} eq "sybase_servers" ) {
			$DBM_DATA{$OPT{-name}}=$OPT{-value};
		} else {
			$DBM_DATA{$OPT{-type}."|".$OPT{-name}."|".$OPT{-server}.$keymod}=$OPT{-value};
		}

	} else {

		if( $OPT{-name} eq "unix_systems"
		or  $OPT{-name} eq "unix_discover_date"
		or  $OPT{-name} eq "backup_servers"
		or  $OPT{-name} eq "sybase_servers" ) {
			croak "Serious Error - No Value For $OPT{-name} Found in Database\nHas Discovery run successful?" unless defined $DBM_DATA{$OPT{-name}};
			return $DBM_DATA{$OPT{-name}};
		} elsif( defined $OPT{-type} and defined $OPT{-name} and defined $OPT{-server} ) {
			return $DBM_DATA{$OPT{-type}."|".$OPT{-name}."|".$OPT{-server}.$keymod};
		} elsif( defined $DBM_DATA{$OPT{-name}}) {
			return $DBM_DATA{$OPT{-name}};
		} else {
			croak "$OPT{-name} is not legal key in database\n";
		}
	}
}

sub db_readfile {

	my(%OPT)=@_;
	validate_params("db_readfile",%OPT);
	croak "Database Not Opened\n" unless $database_opened==1;
	# print "reading file ",$OPT{-filename}," of type ",$OPT{-type},"\n";

	if( $OPT{-type} eq "I" ) {
		#       I - From Sybase Server Interfaces File

		# Get list of other interfaces files on this host and get id number of
		# the one you care about
		my($id_of_interfaces_file);
		if( ! defined $DBM_DATA{"H|Interfaces_File_Id|".$OPT{-host}} ) {
			# add it to the hash
			#$DBM_DATA{"H|Interfaces_File_Id|".$OPT{-host}} = $OPT{-filename}.":1";
			$DBM_DATA{"H|Interfaces_File_Id|".$OPT{-host}} = $OPT{-orig_filename}.":1";
			$id_of_interfaces_file=1;
		} else {
		my $maxid = -100;
		foreach (split(/\|/, $DBM_DATA{"H|Interfaces_File_Id|".$OPT{-host}})){
			my($fname,$id)=split /:/;
			$maxid=$id if $maxid<$id;
			next unless $fname eq $OPT{-orig_filename};
			$id_of_interfaces_file=$id;
			last;
		}
		die "This shouldnt happen" if $maxid == -100;
		if( ! defined $id_of_interfaces_file ) {
			# append it to the hash
			$maxid++;
			$DBM_DATA{"H|Interfaces_File_Id|".$OPT{-host}} .= "|".$OPT{-orig_filename}.":".$maxid;
		}
		$id_of_interfaces_file=$maxid;
		}

		#
		# ok delete records
		#
		my $srch_str = "_".$id_of_interfaces_file."|".$OPT{-host}."\$";
		foreach ( keys %DBM_DATA ) {
			next unless /^I/;
			my($typ,$srv_and_id,$host) = split /\|/;
			next unless $host eq $OPT{-host};
			next unless $srv_and_id=~ /_$id_of_interfaces_file$/;
			# print "DBG - DELETING KEY $_ - search str is $srch_str \n";
			undef $DBM_DATA{$_};
		}

		#
		# ok now we have an id number store interfaces entries in the db
		#
		foreach (db_get_interfaces(undef,$OPT{-filename})){
		my($srv )=$_->[0];
		my($host)=$_->[1];
		my($port)=$_->[2];
		$DBM_DATA{"I|".$srv."_".$id_of_interfaces_file."|".$OPT{-host}}=$_->[1]."::".$_->[2];
		}

	} elsif( $OPT{-type} eq "R"
		or      $OPT{-type} eq "B"
		or      $OPT{-type} eq "S" ) {

		#       R - From RUN_FILE
		#       B - From Backup Server RUN_FILE
		#       S - From Sybase Server RUN_FILE
		if( ! open(RUNFILE,$OPT{-filename})) {
			warn("Cant Open RUN FILE ".$OPT{-filename}."\n");
			return;
		}

		my(%INFO);

      $INFO{"Run_File"}=$OPT{-orig_filename};

		# Interpolate The Sybase Directory From Run File Name
		my($tmp)=$OPT{-orig_filename};
		$tmp =~ s/\/[\w\_\.]+\/RUN_.+$//;

		$INFO{Sybase_Dir}=$tmp;

      $INFO{"Hostname"}=$OPT{-host};
      $INFO{"Run_File_Line_Count"}=0;
      $INFO{"Run_File_Contents"}="";

		my($command);
		my($in_command)=0;
		my($found_cmd_line)=0;
		while(<RUNFILE>) {
		$INFO{"Run_File_Contents"}.=$_;
		if( /SQL Server Information:/ ) {
			$INFO{type}="SQL_SERVER";
		} elsif( /Backup Server Information:/ ) {
			$INFO{type}="BACKUP_SERVER";
		} elsif( /^#\s+([\s\w]+):\s+(\w+)$/  ) {
			# my($key,$val)=($1,$2);
			# print "===> KEY $key --- VALUE => $val \n";
		} elsif( /^\s*#/ ) {
			# comment
		} elsif( /^\s*$/ ) {
			# blank line
		} else {
			$INFO{"Run_File_Line_Count"}++ unless /\\\s$/;

			# command
			if( /dataserver/ or /backupserver/ or /sqlsrvr.exe/ or /bcksrvr.exe/){
				$INFO{"Run_File_Line_Count"}++ if /\\\s$/;
				$found_cmd_line=1;
				# doublecheck server type
				if( /dataserver/ or /sqlsrvr.exe/ ) {
					$INFO{type}="SQL_SERVER" if ! defined $INFO{type};
					croak "Confused RUNSERVER type"
						if $INFO{type} ne "SQL_SERVER";
				} else {
					$INFO{type}="BACKUP_SERVER" if ! defined $INFO{type};
					croak "Confused RUNSERVER type"
						if $INFO{type} ne "BACKUP_SERVER";
				}
				$in_command=1;
				s/\\\s*$//;
				$command=$_;
			} elsif ( $in_command ) {
				$command.=$_;
				$command=~s/\\$/ /;
				$in_command=0 unless /\\$/;
			}
		}
		}
		return undef unless $found_cmd_line;

		my(@args)=split(/\s+/,$command);
		foreach (@args) {
		if( $INFO{type} eq "SQL_SERVER" ) {
			$INFO{prefix}="S";
			if( /-d/ ) {
				s/^-\w//;
				$INFO{"Master_Device"}=$_;
			} elsif( /-c/ ) {
				s/^-\w//;
				$INFO{"Configuration_File"}=$_;
			} elsif( /-e/ ) {
				s/^-\w//;
				$INFO{"Error_Log"}=$_;
			} elsif( /-m/ ) {
				s/^-\w//;
				$INFO{"Single_User_Mode"}=$_;
			} elsif( /-r/ ) {
				s/^-\w//;
				$INFO{"Master_Mirror_Device"}=$_;
			} elsif( /-M/ ) {
				s/^-\w//;
				$INFO{"Shmem_Dir"}=$_;
			} elsif( /-i/ ) {
				s/^-\w//;
				$INFO{"Interfaces_File_Dir"}=$_;
			} elsif( /-s/i ) {
				s/^-\w//;
				$INFO{Server_Name}=$_;
			} elsif( /-p/ ) {
				s/^-\w//;
				$INFO{"SSO"}=$_;
			}
		} elsif( $INFO{type} eq "BACKUP_SERVER" ) {
			$INFO{prefix}="B";
			if( /-S/ ) {
				s/^-\w//;
				$INFO{Server_Name}=$_;
			} elsif( /-e/ ) {
				s/^-\w//;
				$INFO{"Error_Log"}=$_;
			}
		}
		}
		close(RUNFILE);

		#
		# QA The info u just got before u save it
		#
		croak "Server_Name Not Defined in Run File ",$OPT{-orig_filename}," on host ",$OPT{-host},"\n" unless defined $INFO{"Server_Name"};

		# A RUN FILE FOR A SERVER CAN ONLY EXIST ONE PLACE.
		if( defined $DBM_DATA{$INFO{prefix}."|Run_File|". $INFO{"Server_Name"}}
		and defined $DBM_DATA{$INFO{prefix}."|Run_File_Host|". $INFO{"Server_Name"}}
		and ( $DBM_DATA{$INFO{prefix}."|Run_File|". $INFO{"Server_Name"}}  ne $OPT{-orig_filename}
		or $DBM_DATA{$INFO{prefix}."|Run_File_Host|". $INFO{"Server_Name"}}  ne $OPT{-hostname})) {
		croak "ERROR: Multiple RUN_XXX files contain information for server".
			$INFO{"Server_Name"}."\n\t".
			"Found on host " .
			$DBM_DATA{$INFO{prefix}."|Run_File_Host|". $INFO{"Server_Name"}}  .
			"\n\tIn File\n\t".
			$DBM_DATA{$INFO{prefix}."|Run_File|". $INFO{"Server_Name"}}  .
			"\n\tAnd On Host\n\t".
			$OPT{-host}.
			"\n\tIn File\n\t".
			$OPT{-orig_filename}.
			"\n\tPlease Add full path of one of these files to your\nIGNORE_RUNFILE entry in your passwordfile\n";
		}

		foreach ( keys %INFO ) {
			next unless /$INFO{Server_Name}$/;
			next unless /^$INFO{prefix}/;
			undef $DBM_DATA{$_};
		}

		foreach ( keys %INFO ) {
			next if $_ eq "Server_Name";
#print "DBG: key=",$INFO{prefix}."|$_|". $INFO{"Server_Name"}," VAL=",$INFO{$_},"\n";
			my($key)= $INFO{prefix}."|$_|". $INFO{"Server_Name"};

			# bug must be < 1008 bytes
			if( length($key) + length($INFO{$_}) > 1008 )  {
				$INFO{$_} = substr( $INFO{$_}, 0, 940-length($key) );
			}
			#$DBM_DATA{$INFO{prefix}."|$_|". $INFO{"Server_Name"}}=$INFO{$_};
			$DBM_DATA{$key}=$INFO{$_};
		}

		if( $INFO{prefix} eq "B" ) {
			my($found)=0;
			foreach (@BACKUP_SERVERS) {
				next unless $INFO{Server_Name} eq $_;
				$found++;
				last;
			}
			if( $found==0 ) {
				push @BACKUP_SERVERS, $INFO{Server_Name};
				$DBM_DATA{"backup_servers"}=join(" ",@BACKUP_SERVERS);
				# print "Adding Backup Server : $INFO{Server_Name} \n";
			}
		} else {
			my($found)=0;
			foreach (@SYBASE_SERVERS) {
				next unless $INFO{Server_Name} eq $_;
				$found++;
				last;
			}

			if( $found==0 ) {
				push @SYBASE_SERVERS,$INFO{Server_Name};
				$DBM_DATA{sybase_servers}=join(" ",@SYBASE_SERVERS);
			}
		}

		my($found)=0;
		my(@thishostservers);

		foreach (split(/\s+/,$DBM_DATA{"H|SYBASE_SRV_LIST|".$OPT{-host}} )) {
			push @thishostservers,$_;
			next unless $INFO{Server_Name} eq $_;
			$found++;
			last;
		}

		if( $found==0 ) {
			push @thishostservers,$INFO{Server_Name};
			$DBM_DATA{"H|SYBASE_SRV_LIST|".$OPT{-host}}=join(" ",@thishostservers);
		}

		return $INFO{Server_Name};

	} elsif( $OPT{-type} eq "H" ) {
		#       H - From Unix Password File (via GetPassword.pm)
	} elsif( $OPT{-type} eq "s" ) {
		#       s - From Sybase Password File (via GetPassword.pm)
	} else {
		croak "Invalid File Type: ".$OPT{-type}."\n";
	}
}

sub db_readserver {
	my(%OPT)=@_;
	croak "Database Not Opened\n" unless $database_opened==1;
	validate_params("db_readserver",%OPT);
	my($srvr,$login,$password)=($OPT{-server},$OPT{-login},$OPT{-password});

	my($found)=0;
	foreach (@SYBASE_SERVERS) {
		next unless $OPT{-server} eq $_;
		$found++;
		last;
	}

	if( $found==0 ) {
		push @SYBASE_SERVERS,$OPT{-server};
		$DBM_DATA{sybase_servers}=join(" ",@SYBASE_SERVERS);
	}

	if( db_syb_ping($srvr,$login,$password) ne "0" ) {
		print "Cant connect to $srvr as $login - continuing\n";
		return 0;
	}

	if( ! db_connect($srvr,$login,$password) ) {
		print "Cant connect to $srvr as $login - continuing\n";
		return 0;
	}

	my(@d)=db_query("master","select srvnetname from master..sysservers where srvname='SYB_BACKUP'");
	$DBM_DATA{"s|BackupServer|".$OPT{-server}} = $d[0] if $#d>=0;

	@d=db_query("master","select \@\@servername");
	$DBM_DATA{"s|ServerName|".$OPT{-server}} = $d[0] if $#d>=0;

	@d=db_query("master","select name,status,status2 from sysdatabases");
	my(@databaselist);
	foreach (@d) {
		my($n,$s1,$s2)=db_decode_row($_);
		my($logsame)=1 if $s2<0;
		$s2 += 32768 if $s2<0;
		push @databaselist,$n;
		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} = "";
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} = "";

		# select into
		if ($s1 & 4) {
		$DBM_DATA{"s|SelectInto|".$OPT{-server}."|".$n}=1;
		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "Si ";
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "Si ";
		}

		# trunc log
		if ($s1 & 8) {
		$DBM_DATA{"s|TruncLog|".$OPT{-server}."|".$n}=1;
		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "Tl ";
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "Tl ";
		}

		if ($s1 & 16) {
			$DBM_DATA{"s|NoChkptRcvry|".$OPT{-server}."|".$n}=1;
			$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "NoChkRcvry ";
			$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=BLUE>NoChkRcvry</FONT> ";
		}

		if ($s1 & 32) {
			$DBM_DATA{"s|ForLoad|".$OPT{-server}."|".$n}=1;
			$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "ForLoad ";
			$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>ForLoad</FONT> ";
		}

		if ($s1 & 256) {
			$DBM_DATA{"s|Suspect|".$OPT{-server}."|".$n}=1;
			$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "Suspect ";
			$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>Suspect</FONT> ";
		}

		if ($s1 & 1024) {
			$DBM_DATA{"s|ReadOnly|".$OPT{-server}."|".$n}=1;
			$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "ReadOnly ";
			$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=BLUE>ReadOnly</FONT> ";
		}

		if ($s1 & 2048) {
			$DBM_DATA{"s|DboOnly|".$OPT{-server}."|".$n}=1;
			$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "DboOnly ";
			$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>DboOnly</FONT> ";
		}

		if ($s1 & 4096) {
			$DBM_DATA{"s|SglUser|".$OPT{-server}."|".$n}=1;
			$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "SglUser ";
			$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>SglUser</FONT> ";
		}

		if ($s2 & 16) {
		$DBM_DATA{"s|Offline|".$OPT{-server}."|".$n}=1;
		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "Offline ";
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>Offline</FONT> ";
		}

		if ($s2 & 32) {
		$DBM_DATA{"s|OfflineToLoad|".$OPT{-server}."|".$n}= 1;
		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "OfflineToLoad ";
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>OfflineToLoad</FONT> ";
		}

		if ( $logsame==1 and ! $s1 & 8 ) {
		$DBM_DATA{"s|NoSepLog|".$OPT{-server}."|".$n}= 1;
		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n} .= "NoSepLog ";
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} .= "<FONT COLOR=RED>NoSepLog</FONT> ";
		}

		if( ($s1 & 32) or ($s1 & 256 ) or ($s1  & 4096 )
			or ($s2  & 16 ) or ($s2  & 32 ) ) {
			$DBM_DATA{"s|BadDatabases|".$OPT{-server}} = ""
				unless defined $DBM_DATA{"s|BadDatabases|".$OPT{-server}};
			$DBM_DATA{"s|BadDatabases|".$OPT{-server}} .= $n." "
		};

		$DBM_DATA{"s|DBoptions|".$OPT{-server}."|".$n}     =~ s/\s+$//;
		$DBM_DATA{"s|DBoptionsHtml|".$OPT{-server}."|".$n} =~ s/\s+$//;
	}
	$DBM_DATA{"s|BadDatabases|".$OPT{-server}} =~ s/ $//;
	$DBM_DATA{"s|Databases|".$OPT{-server}}= join(" ",@databaselist);

	#
	# Get Version & Date Installed Information
	#
	my(@rc)=db_query("master","select \@\@VERSION");
	my(@ver)=split("\/",$rc[0]);
	$DBM_DATA{"s|DateInstalled|".$OPT{-server}}= pop @ver;
	$DBM_DATA{"s|Version|".$OPT{-server}}= join("\/",@ver);

	# Find pages per megabyte
	(@rc)=db_query("master","  select 1048576. / low
									from master.dbo.spt_values
									where number = 1 and type = 'E' ");
	my($numpgsmb)=int($rc[0]);
	$DBM_DATA{"s|NumPgsMb|".$OPT{-server}}= $numpgsmb;

	# Find out how much space was given to sybase
	(@rc)=db_query("master","select sum(1+high-low)/".$numpgsmb." from sysdevices where status&2=2 ");
	$DBM_DATA{"s|SpaceForServer|".$OPT{-server}}= int($rc[0]);

	$DBM_DATA{"s|SpaceAllocated|".$OPT{-server}}= 0;
	foreach (@databaselist ) {
		(@rc)=db_query("master","select sum(size) from master.dbo.sysusages u where u.dbid = db_id(\"$_\") and   u.segmap != 4");
		$DBM_DATA{"s|DataSpaceByDb|".$OPT{-server}."|".$_} = $rc[0]/$numpgsmb;
		$DBM_DATA{"s|SpaceAllocated|".$OPT{-server}} += $rc[0]/$numpgsmb;

		(@rc)=db_query("master","select sum(size) from master.dbo.sysusages u where u.dbid = db_id(\"$_\") and   u.segmap = 4");
		$DBM_DATA{"s|LogSpaceByDb|".$OPT{-server}."|".$_} = $rc[0]/$numpgsmb;
		$DBM_DATA{"s|SpaceAllocated|".$OPT{-server}} += $rc[0]/$numpgsmb;
	}

	my(%device_size);
	foreach ( dbi_query("master","select phyname,high-low from sysdevices where status & 2 = 2") ) {
		my($p,$s)=db_decode_row($_);
		$device_size{$p}=int((1+$s)/$numpgsmb);
	}

	my($count)=0;
	foreach ( db_query("master","
			select distinct db_name(dbid), dv.phyname, sum(size) , b.name
			from  master..sysusages u,
				master..sysdevices dv,
					master..spt_values b
			where dv.low <= size + vstart
				and dv.high >= size + vstart - 1
				and dv.status & 2 = 2
				and b.type = \"S\"
				and u.segmap & 7 = b.number
			group by dbid,dv.phyname
			having dv.low <= size + vstart
				and dv.high >= size + vstart - 1
				and dv.status & 2 = 2
				and b.type = \"S\"
				and u.segmap & 7 = b.number
" ) ) {
		my(@x)=db_decode_row($_);
		$DBM_DATA{"s|DeviceInfo|".$OPT{-server}."|".$count}    =
										$x[1]."||".
										$srvr."||".
										$x[0]."||".
										$device_size{$x[1]}."||".
										int(($x[2]+1)/$numpgsmb)."||".
										$x[3];
		$count++;
	}

	foreach ( db_query("master",
	"select c.config,
		cmt=substring(x.name,1,40),
		c.value,
		dflt=substring(c.defvalue,1,10)
	from    master..syscurconfigs c,master..sysconfigures x
	where   c.config = x.config
	and     ((      c.value2 != c.defvalue
	and     c.config != 114 ) or c.config=122 or c.config=123
				or c.config=124 )
		order by x.name") ) {

		my($id,$cmt,$v,$d)=db_decode_row($_);
		$d=~s/\s//g;
		$DBM_DATA{"s|ConfigName|".$OPT{-server}."|".$id}    = $cmt;
		$DBM_DATA{"s|ConfigValue|".$OPT{-server}."|".$id}   = $v;
		$DBM_DATA{"s|ConfigDefault|".$OPT{-server}."|".$id} = $d;
	}

	# character sets
	@rc=db_query("master","select id,name from master..syscharsets");
	$DBM_DATA{"s|Charset|".$OPT{-server}} = "";
	foreach (@rc) {
		my($lid,$lname)=db_decode_row($_);
		$DBM_DATA{"s|CharsetDef|".$lid} = $lname;
		$DBM_DATA{"s|Charset|".$OPT{-server}} .= $lid." ";
	}
	$DBM_DATA{"s|Charset|".$OPT{-server}} =~ s/ $//;

	# languages
	@rc=db_query("master","select langid,name from master..syslanguages");
	$DBM_DATA{"s|Languages|".$OPT{-server}} = "";
	foreach (@rc) {
		my($lid,$lname)=db_decode_row($_);
		$DBM_DATA{"s|LanguageDef|".$lid} = $lname;
		$DBM_DATA{"s|Languages|".$OPT{-server}} .= $lid." ";
	}
	$DBM_DATA{"s|Languages|".$OPT{-server}} =~ s/ $//;
	db_close_sybase();
	return 1;
}

sub validate_params
{
	my($func,%OPT)=@_;

	#
	# Required if 1, optional if 0
	#
	my(%OK_OPTS)= (
		"db_open-dbmname"		       => 1,
		"db_open-clear"		       => 0,
		"db_param-name"		 => 0,
		"db_param-value"			=> 0,
		"db_param-type"		 => 0,
		"db_param-server"		       => 0,
		"db_param-keys"		 => 0,
		"db_param-dbname"		       => 0,
		"db_readfile-type"	      => 1,
		"db_readfile-filename"  => 1,
		"db_readfile-orig_filename"=> 1,
		"db_readfile-host"	      => 1,
		"db_readserver-server"  => 1,
		"db_readserver-login"   => 1,
		"db_readserver-password"	=> 1
	);

	# check bad keys
	foreach (keys %OPT) {
		next if defined $OK_OPTS{$func.$_};
		croak "ERROR: Incorrect $func Arguments - Change $_ argument to -$_\n"
			if defined $OK_OPTS{$func."-".$_};
		croak "ERROR: Function $func Does Not Accept Parameter Named |$_|\n";
	}

	# check missing keys
	foreach (keys %OK_OPTS) {
		next unless /$func/;
		next unless $OK_OPTS{$_} == 1;
		$_=~s/$func//;
		next if defined $OPT{$_};
		croak "ERROR: Function $func Requires Parameter Named $_\n";
	}
}
1;

__END__

=head1 NAME

Sybase::SybAdminDb Module - Sybase Management Database

=head2 DESCRIPTION

This module provides an interface to a DBM Administrative Database for sybase.  This database will contain all kinds of hard to get information regarding your
server.  You open this DBM database with db_open, retrieve parameters with db_param() and close it when u are done with db_close().  This DBM database is populated using the db_readfile() and db_readserver() methods.  A utility to populate the database from all servers in your environment is part
of perl toolkit.

This database contains information from your run files, interfaces files, and
from your sql servers.  It will perhaps be expanded to contain information
from your unix servers.

=head2 AUTHOR

   Edward Barlow
   mail: edbarlow@mad.scientist.com
   url : http://www.edbarlow.com
   All Rights Reserved

=head2 TYPES

Data is stored by type.  The type indicates the SOURCE of the data.  The
following types are currently recognized:

=over 4

=item * G - General Information (server names etc)

=item * H - Host specific information (list of files found)

=item * B - From Backup Server RUN_FILE

=item * S - From Sybase Server RUN_FILE

=item * I - From Sybase Server Interfaces File

=item * M - Space Information

=item * s - server information (from catalogs)

=back

=head2 FUNCTIONS

=over 4

=item * db_open(-dbmname=>filename)

Open a New Database .  The name is the root of the dbm file (no .pag/.dir
extension. pass -clear to delete data from the dbm database.

=item * db_close()

Close The database.

=item * db_param(
   -name=>KeyName,
   -value=>KeyValue,
   -type=>WhereReadFrom,
   -server=>ServerName,
   -keys,
   -dbname=>DbName)

 -name:     KeyName
 -value:    if defined then set value, if not then return it
 -type:     See Type Section Above
 -server:   server of interest if type B/S or host if type H/I
 -keys:     if defined returns a list of keys.  The keys are returned in
            a pipe separated string - type|KeyName|Server|Dbname
 -dbname:   database name (only for certain key values)

=item * db_readfile(
   -type=>FileType,
   -filename=>FileToRead,
   -host=>Hostname,
   -orig_filename=>OriginalFilename)

Read and save information from a file.  Files may be RUN files, INTERFACE files as per the -type parameter to db_param(). Results are saved with key 'B' or 'S'.

=item * db_readserver(
   -server=>SERVERNAME,
   -login=>LOGIN,
   -password=>PASSWORD )

Get Values from a server by connecting and looking in the system catelogs.  Results are saved in the database with a key 's'.

=back

=head2 KEYS

The following are a list of keys by type of program

=over 4

=item * GENERAL (TYPE G)

 unix_discover_date
 unix_systems
 sybase_server
 backup_server

=item * PER HOST   (TYPE H)   (server should be a unix host)

 SYBASE_SRV_LIST      List of the Server on this host
 Interfaces_File_Id   The Id # of the interfaces file
 Format File:Count|File:Count...

=item * RUN_SCRIPT - ALL SERVERS (TYPE B or S)

The following are available for both sybase servers and backup servers

 Run_File        Path Name To Run File
 Sybase_Dir      Directory to Sybase (from run file name)
 Hostname        Host the servers Run File Was Found On
 Run_File_Line_Count  Num Non Comment Lines in Run File
 Run_File_Contents    Contents of the Run File
 type    SQL_SERVER or BACKUP_SERVER
 prefix          S;

=item * RUN_SCRIPT - BACKUPSERVER - (TYPE B)

Contains the general per server keys plus

 Server_Name
 Error_Log

=item * RUN_SCRIPT - SQL SERVER - (TYPE S)

Contains the general per server keys plus

 Master_Device
 Configuration_File
 Error_Log
 Single_User_Mode
 Master_Mirror_Device
 Shmem_Dir
 Interfaces_File_Dir
 Server_Name
 SSO

=item * INTERFACES FILE - (TYPE I)

 $SERVERNAME_##       ## is the number of the interfaces file
 values are host:port.  Duplicate entries in interfaces
 file are ignored (last one is used) although a
 warning is printed.

=item * SERVER DISCOVERY (TYPE M)

populated by space_monitor.pl

The following items require a secondary parameter to be passed using
the -dbname=>xxx argument

 TimeMonitored    - a time() value
 DbSpaceUsed      - last space used (mb)
 LogSpaceUsed     - last log space used (mb)
 DbSpaceUsedPct   - last space used (mb)
 LogSpaceUsedPct  - last log space used (mb)

=item * SERVER DISCOVERY (TYPE s)

populated by db_readserver()

The following items require a secondary parameter to be passed using
the -dbname=>xxx argument

 DBoptionsHtml  html string of the database options
 DBoptions      regular string of the database options
 DataSpaceByDb  data space allocated to this database
 LogSpaceByDb   log space allocated to this database
 DeviceInfo     sec param is a sequential number.  values are || separated list
                of phy,srvr,db,dev,size,alloc,usage
 ConfigDefault  number is config value.  Default for sp_configure value
 ConfigName     number is config value.  Name of sp_configure value
 ConfigValue    number is config value.  Value of sp_configure value

DATABASE OPTIONS (aka sp_dboption)

 Offline
 OfflineToLoad
 NoSepLog
 ForLoad
 NoChkptRcvry
 Suspect
 DboOnly
 ReadOnly
 SelectInto
 SglUser
 TruncLog

ITEMS WITHOUT DBNAME REQUIRED

 BackupServer    Name of the backup server
 ServerName      Name of the local server
 BadDatabases    space sep list of databases with problems
 CharsetDef
 Databases       space sep list of databases
 DateInstalled
 NumPgsMb        pages per MB
 SpaceAllocated  sum of pages allocated from sysusages
 SpaceForServer
 Version         @@version
 Languages       space separated list of id of languages installed
 Charset         space separated list of id of character sets installed

ITEMS WITH JUST A NUMBER INSTEAD OF SERVER

 LanguageDef   Language name for language id
 CharsetDef    Character Set name for id

=back

=cut
