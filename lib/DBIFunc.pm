# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.
#

package 		DBIFunc;

require	  	5.000;
use	     	Exporter;
use	     	Carp qw/cluck confess croak/;
use 			DBI;
use			Socket;			 # needed for gethostbyaddr


BEGIN {
	if( ! defined $ENV{SYBASE} or $ENV{SYBASE}=~/^\s*$/ or ! -d $ENV{SYBASE} ) {
		if( 1==2 ) {
     	# *****************************
   	# WARNING - DO NOT REMOVE THIS COMMENT
	#******************
		} elsif( -d "/export/home/sybase" ) {
			$ENV{SYBASE}="/export/home/sybase";
		} elsif( -d "/apps/sybase" ) {
			$ENV{SYBASE}="/apps/sybase";
		} elsif( -d "C:/sybase" ) {
			$ENV{SYBASE}="C:/sybase";
	#******************
   	# WARNING - THANK YOU FOR NOT REMOVING THIS EITHER
   	# *****************************
   	# EVERYTHING BELOW HERE IS GRAVY - JUST IN CASE YOU DIDNT SET THINGS UP VIA GEM!
  		} elsif( -d "/export/home/sybase" ) {
  			$ENV{SYBASE}="/export/home/sybase";
  		} elsif( -d "/opt/sybase" ) {
  			$ENV{SYBASE}="/opt/sybase";
  		} elsif( -d "/apps/sybase" ) {
  			$ENV{SYBASE}="/apps/sybase";
  		} elsif( -d "C:/sybase" ) {
  			$ENV{SYBASE}="C:/sybase";
  		} elsif( -d "D:/sybase" ) {
  			$ENV{SYBASE}="D:/sybase";
  		}
  	}
}

my($VERSION) = 1.11;
use subs qw(myprint mycroak);

@ISA	    	= qw(Exporter);
@EXPORT 	= qw(dbi_reformat_results dbi_encode_row dbi_set_ignore dbi_add_line_number dbi_set_web_page
dbi_colorize_sql dbi_get_dirlist dbi_set_mode  dbi_set_debug dbi_decode_row dbi_query_to_table
dbi_msg_exclude dbi_msg_ok add_line_number dbi_disconnect dbi_has_login_failed dbi_connect dbi_use_database
dbi_query dbi_set_option dbi_quote dbi_getdbh dbi_parse_opt_D dbi_optionpacks dbi_eachobject dbi_get_interfaces
dbi_db_version);

use		strict;

# Method Arguments.  Set to 1 if Required, optional if 0
#    key is func:value
my(%OK_OPTS)= (
		"dbi_connect:-srv"		       	=> 1,
		"dbi_connect:-login"		       	=> 1,
		"dbi_connect:-password"		    	=> 1,
		"dbi_connect:-quiet"		    	=> 0,
		"dbi_connect:-type"		       	=> 0,
		"dbi_connect:-allow_bcp_in"	 	=> 0,
		"dbi_connect:-die_on_error"	 	=> 0,
		"dbi_connect:-debug"		       	=> 0,
		"dbi_connect:-connection"		       	=> 0,

		"dbi_disconnect:-connection"		       	=> 0,

		"dbi_eachobject:-db"	=> 1,
		"dbi_eachobject:-cmd"	=> 1,
		"dbi_eachobject:-cmd1"	=> 0,
		"dbi_eachobject:-cmd2"	=> 0,
		"dbi_eachobject:-cmd3"	=> 0,
		"dbi_eachobject:-cmd4"	=> 0,
		"dbi_eachobject:-type"				=> 0,
		"dbi_eachobject:-debug"				=> 0,
		"dbi_eachobject:-do_print"			=> 0,
		"dbi_eachobject:-die_on_error"			=> 0,
		"dbi_eachobject:-no_results"			=> 0,
		"dbi_eachobject:-no_exec"			=> 0,
		"dbi_eachobject:-exclude"			=> 0,
		"dbi_eachobject:-include"			=> 0,

		"dbi_query:-db"					=> 1,
		"dbi_query:-query"				=> 1,
		"dbi_query:-print_hdr"				=> 0,
		"dbi_query:-add_batchid"			=> 0,
		"dbi_query:-srv"				=> 0,
		"dbi_query:-login"				=> 0,
		"dbi_query:-password"				=> 0,
		"dbi_query:-type"				=> 0,
		"dbi_query:-nullstr"				=> 0,
		"dbi_query:-debug"				=> 0,
		"dbi_query:-die_on_error"			=> 0,
		"dbi_query:-no_results"				=> 0,
		"dbi_query:-connection"				=> 0,

		"dbi_query_to_table:-db"			=> 1,
		"dbi_query_to_table:-query"			=> 1,
		"dbi_query_to_table:-print_hdr"			=> 0,
		"dbi_query_to_table:-table_options"		=> 0,
		"dbi_query_to_table:-die_on_error"		=> 0,
		"dbi_query_to_table:-TBLHDRCOLOR"		=> 0,
		"dbi_query_to_table:-TBLCELLCOLOR"		=> 0,
		"dbi_query_to_table:-TBLCELLFONT"		=> 0,
		"dbi_query_to_table:-TBLHDRFONT"		=> 0,
		"dbi_query_to_table:-connection"		=> 0,
);

# the 17001,183,14024 messages are to stop errors from rep server
my(@syb_exclude_msgs)=(911,5701,5703,20019,5704,4002,913,918,919,916,20012,20014,20018,20009,2409,17001,183,14024);
my(@mssql_exclude_msgs)=(1000);


my($NL) = "\n";
my($more_results) = "syb_more_results";
my($DEBUG);
my($mode)="NORMAL";	     		# MODE= NORMAL|INLINE for how to show error
					#    messages during a query
my($die_on_error)=1;		    	# croak if you get serious error
my(@message_stack);		     	# internal message stack
my($ignore_error_message)=0;		# Ignore a failure condition.  Required for
					# 2007 messages from the server
my($print_pre_on_msg)=0;
my($login_failed_reason);	   	# SET TO A VALUE IF YOU FAILED TO LOGIN
my($dbproc);

my($print_handler,$croak_handler)=(\&myprint,\&mycroak);

sub mycroak { die(@_); }
sub myprint { print @_; }

sub dbi_getdbh {
	print "DBPROC undefined\n" unless defined $dbproc;
	return $dbproc;
}

sub dbi_quote {
	my(%args)=@_;

	return $args{-connection}->quote($args{-text})
			if defined $args{-connection};
	return $dbproc->quote($args{-text})
			if defined $dbproc;
	return $args{-text};
}

sub dbi_set_option {
	my(%OPT)=@_;
	$print_handler=$OPT{-print} if defined $OPT{-print};
	$croak_handler=$OPT{-croak} if defined $OPT{-croak};
}

# returns (Sybase|SQL, Version);
sub dbi_db_version
{
	my(%args)=@_;
	my($sql2)="select \@\@version";
	my($DBTYPE,$DBVERSION,$PATCHLEVEL);
	foreach( dbi_query(-db=>"master",-query=>$sql2,%args)) {
		
		#Adaptive Server Enterprise/11.9.2.6/1290/P/EBF 10491 ESD1/Sun_svr4/OS 5.5.1/FBO/Mon Jul 15 06:54:22 2002
		#Microsoft SQL Server  2000 - 8.00.2039 (Intel X86)   May  3 2005 23:18:38   Copyright (c) 1988-2003 Microsoft Corporation  Enterprise Edition on Windows NT 5.2 (Build 3790: Service Pack 1) 
		#Microsoft SQL Server 2005 - 9.00.1399.06 (Intel X86)   Oct 14 2005 00:33:37   Copyright (c) 1988-2005 Microsoft Corporation  Enterprise Edition on Windows NT 5.2 (Build 3790: Service Pack 1) 
		my($str)=join("",dbi_decode_row($_));
		$str=~s/^\s+//;
		if( $str =~ /^Microsoft SQL Server/ ) {
			$DBTYPE="SQL SERVER";
			
			my(@ver) = split(/\n\s*/,$str);
			
			$ver[0] =~ s/^\s+//;
			$ver[0] =~ s/\s+$//;			
			my(@y) = split(/\s*-\s*/,$ver[0]);
			#my(@z) = split(/\./,$y[5]);
			$DBVERSION	=$y[0];			
			$PATCHLEVEL	=$y[1]
			
			#$DBVERSION=$z[0].".".$z[1].$z[2].$z[3];
			#$PATCHLEVEL=$ver[2];
			
		} elsif( $str =~ /Adaptive Server Enterprise/ ) {
			$DBTYPE="SYBASE";
			my(@x) = split("\/",$str);
			my(@y) = split(/\./,$x[1]);
			$DBVERSION=$y[0].".".$y[1].$y[2].$y[3];
			$PATCHLEVEL=$x[2];
		} else {
			$DBVERSION=$str;
		}
	}	
	return($DBTYPE,$DBVERSION,$PATCHLEVEL);
}

# =========================
# PROC:		 	dbi_disconnect
# DESCRIPTION:	  	cleans up and exits the server
# PARAMETERS:   	none
# RETURNS:  		no return code
# =========================
sub dbi_disconnect
{
	my(%args)=@_;
	validate_params("dbi_disconnect",%args);
	debugmsg("Exiting Database") ;
	if( defined $args{-connection} ) {
		$args{-connection}->disconnect();
		delete $args{-connection};
	} else {
		if( defined $dbproc ) {
			$dbproc->disconnect();
			$dbproc=undef;
		};
	}
}

sub dbi_has_login_failed { return $login_failed_reason; }

# =========================
# PROC:		dbi_connect($server,$login,$password,$type)
# DESCRIPTION:	connects to sybase using login & password
# PARAMETERS:   server, login, password as per normal
# RETURNS:  	1/0
# =========================
sub dbi_connect
{
	my(%args)=@_;
	if( defined $args{-setconnection} ) {
		$dbproc = $args{-connection};
		return;
	}
	my($str)="";
	foreach ( keys %args) {
		if( defined $args{$_} ) {
			$str.= $_."=>".$args{$_}." ";
		} else {
			$str.= $_."=> not defined ";
		}
	}
	#print "DEBUG LINE ",__LINE__," $str\n";

	validate_params("dbi_connect",%args);

	my($srvr)		= $args{-srv};
	my($login)		= $args{-login};
	my($passwd)		= $args{-password};
	my($type)		= $args{-type};
	$type = "ODBC"   if defined $ENV{PROCESSOR_LEVEL};
	$type = "Sybase" if ! defined $type or $type eq "sybase";
	$type = "Oracle" if $type eq "oracle";
	$type = "Sybase" if $type eq "ODBC" and ! defined $ENV{PROCESSOR_LEVEL};

	my($allow_bcp_in)	= $args{-allow_bcp_in};

	$die_on_error=$args{-die_on_error} if defined $args{-die_on_error};

	if( $args{-debug} ) {
		DBI->trace(2) if $args{-debug}>=2;
		debugmsg( "dbi_connect() called in debug mode\n" );
	}
	$DEBUG=$args{-debug};
	debugmsg("Connecting To $type Database") ;

	&$croak_handler("Sybase Environment Must Be Set")
		if ! defined $ENV{"SYBASE"} and $type =~ /Sybase/i;

	undef $login_failed_reason;

	my($dbprocess);
	if( $type eq "ODBC" ) {
		debugmsg("DSN=dbi:$type:$srvr login=$login\n") ;
		$dbprocess = DBI->connect("dbi:$type:$srvr",$login,$passwd,
					{RaiseError=>0,PrintError=>0,AutoCommit=>1,odbc_err_handler=>\&ms_error_handler});
	} else {
		debugmsg("DSN=dbi:$type:$srvr login=$login\n") ;
		$dbprocess = DBI->connect("dbi:$type:$srvr",$login,$passwd,
					{RaiseError=>0,PrintError=>0,AutoCommit=>1,syb_err_handler=>\&sybase_handler});
	}
	debugmsg("dbi_connect done\n") ;
#	my($dbprocess) = DBI->connect("dbi:$type:$srvr",$login,$passwd,
#					{RaiseError=>0,PrintError=>0,AutoCommit=>1});

	if( ! defined $dbprocess ) {
		if( ! defined $args{-quiet} ) {
			&$print_handler( "Can't connect to dbi:$type:$srvr as $login\n");
			&$print_handler( $login_failed_reason ) if defined $login_failed_reason;
		}
		return 0;
	}
	#print "DBG: Process is $dbprocess\n";

#	$dbprocess->{odbc_err_handler} = \&ms_error_handler  if $type eq "ODBC";
#	$dbprocess->{syb_err_handler}  = \&sybase_handler    if $type =~ /Sybase/i;

	$more_results = "odbc_more_results" if $type eq "ODBC";

	debugmsg( "Successful Login To $type as $login (dbi_connect() finished" );
	return $dbprocess if defined $args{-connection};
	$dbproc=$dbprocess;

	1;
}

# =========================
# PROC:		 dbi_use_database($db)
# DESCRIPTION:	  use a database
# PARAMETERS:   $db - database name
# RETURNS:  returns 1 (success) or 0 (fail)
# =========================
sub dbi_use_database{
	my($db,$dbprocess)=@_;
	$dbprocess=$dbproc unless defined $dbprocess;
	return $dbprocess->do("use $db");
}

# =========================
# PROC:		dbi_query
# DESCRIPTION:	run a command
# PARAMETERS:   $db - database name
#	 	$query - what to run
#	 	$die_on_err (optional) - if defined then mycroak if an error occurrs
#	 	$print_hdr - if defined first row will be header info
# RETURNS:  	returns array (success) or undef (fail). array must be decoded
# =========================
sub dbi_query
{
	my(%args)=@_;
	validate_params("dbi_query",%args);

	my($db)			= $args{-db};
	my($query)		= $args{-query};
	my($print_hdr)		= $args{-print_hdr};
	my($add_batchid)	= $args{-add_batchid};
	my($srv)			= $args{-srv};
	my($login)		= $args{-login};
	my($pass)		= $args{-password};
	my($type)		= $args{-type};
	my($nullstr)	= $args{-nullstr};

	$type = "Sybase" if defined $type and $type eq "ODBC" and ! defined $ENV{PROCESSOR_LEVEL};

	DBI->trace(2) if $args{-debug} and $args{-debug}>=2;

	my($old_debug)=$DEBUG;
	my($old_die_on_err) = $die_on_error;

	$DEBUG=$args{-debug};
	debugmsg( "dbi_query() called in debug mode" );
	$die_on_error       = $args{-die_on_error};

	my(@output_results)=();
	@message_stack=();

	debugmsg("dbi_query($db,$query)") ;

	my($dbprocess);
	undef $login_failed_reason;
	if( defined $srv ) {
		debugmsg("NEW LOGIN JUST FOR THIS COMMAND");
		debugmsg( "CONNECTING TO SERVER $srv LOGIN $login");
		&$croak_handler("Sybase Environment Must Be Set")
			if ! defined $ENV{"SYBASE"} and $args{-type} =~ /Sybase/i;
		&$croak_handler("Login Not Set")
			unless defined $login and $login ne "";
		&$croak_handler("Password Not Set")
			unless defined $pass and $pass ne "";
		&$croak_handler("Server Not Set")
			unless defined $srv and $srv ne "";
		$dbprocess = DBI->connect("dbi:$type:$srv",$login,$pass,{RaiseError=>0,PrintError=>0,AutoCommit=>1});
	} elsif( defined $args{-connection} ) {
		$dbprocess=$args{-connection};
	} else {
		$dbprocess=$dbproc;
	}

	# When u return, mycroak if u want to, reset die on error, and return either
	# the message stack or an error
	if( ! defined $dbprocess ) {
		debugmsg("No Connection For Query $query" );
		if( defined $srv or defined $login or defined $pass ) {
			&$croak_handler("No Connection to $type - You tried to connect to server=$srv using login=$login") if $die_on_error;
		} else {
			&$croak_handler("No Connection Available to $type - please connect before calling this function.") if $die_on_error;
		}
		$die_on_error = $old_die_on_err;
		$DEBUG=$old_debug;
		if( $mode eq "INLINE" ) {
			push @message_stack,"No Connection For $query";
			push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
			@message_stack=();
			return @output_results;
		} else {
			print "No Connection For $query\n";
		}
		return ();
	}

	if( $db ne "" ) {
		my($rc)=$dbprocess->do("use $db");
		if( ! defined $rc or $DBI::err ) {
			$dbprocess->disconnect() if defined $srv;
			if( $die_on_error ) {
				&$croak_handler("Cant Use Database $db $DBI::err $DBI::errstr\n") ;
			} elsif( $mode eq "INLINE" ) {
				push @message_stack,"Cant Use Database $db $DBI::err $DBI::errstr\n";
			} else {
				&$print_handler("Cant Use Database $db $DBI::err $DBI::errstr\n") ;
			}
			$die_on_error = $old_die_on_err;
			$DEBUG=$old_debug;
			if( $mode eq "INLINE" ) {
				push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
				@message_stack=();
				return @output_results;
			}
			return undef;
		}
	}

	debugmsg("Preparing Query");
	my($sth)=$dbprocess->prepare($query);
	if(( ! defined $sth or defined $sth->err  ) and $ignore_error_message==0 ) {
		debugmsg("No statement handle - Prepare failed");
		$dbprocess->disconnect() if defined $srv;
		&$croak_handler("\nERROR: prepare Failed ($DBI::errstr $ignore_error_message)")
			if $die_on_error;
		$die_on_error = $old_die_on_err;
		$DEBUG=$old_debug;
		if( $mode eq "INLINE" ) {
			push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
			@message_stack=();
			debugmsg("Returning From Preparing Query (with results)\n");
			return @output_results;
		}
		debugmsg("Returning From Preparing Query\n");
		return undef;
	}

	debugmsg("Preparing Completed - Executing Query");
	my($rv)=$sth->execute();
	if( (! $rv or defined $sth->err) and $ignore_error_message==0 ) {
		debugmsg("Execute Failed ($sth->errstr $ignore_error_message)");
		$dbprocess->disconnect() if defined $srv;
		&$croak_handler("\nERROR: execute Failed (".$sth->errstr." $ignore_error_message)")
			if $die_on_error;
		$die_on_error = $old_die_on_err;
		$DEBUG=$old_debug;
		if( $mode eq "INLINE" ) {
			push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
			@message_stack=();
			return @output_results;
		}
		return undef;
	}

	debugmsg("Processing Result Set.");
	debugmsg("Message Stack has ".($#message_stack+1)." Rows.");
	# push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
	# @message_stack=();
	my($batchid);
	$batchid=1 if defined $add_batchid;
	my( $header_count );
	if( ! defined $args{-no_results} ) {
 	do {
		debugmsg("In Main Loop");
		$header_count=1 if defined $print_hdr;
		while ( my($aref) = $sth->fetchrow_arrayref() ) {
			last if ! defined $aref or $aref==0 or $#$aref<0 ;
			
			if( $more_results eq "syb_more_results"
				and  defined $sth->{syb_result_type}
				and  $sth->{syb_result_type} != 4040
				and  $sth->{syb_result_type} != 4041
				and  $sth->{syb_result_type} != 4044 ) {
				debugmsg("Skipping Sybase Row of type ".$sth->{syb_result_type}."\n");
				next;
			}
			
			#next if $#aref==0 and ! defined $$aref[0];		# stupid syntax errors... how does this work
			#print __FILE__," ",__LINE__," Ref = ",ref $aref," num=$#$aref,",join("~",@$aref),"\n";
			debugmsg("Fetched Row: (",join("~",@$aref),").");
			debugmsg("More Results=$more_results RowType=",$sth->{syb_result_type} ,". " );

			# ASSUME PRINTS SHOULD COME BEFORE ANYTHING ELSE
			if( $#message_stack>=0 ) {
				foreach (@message_stack) {
					debugmsg(" -------> $_ \n");
				}
				push @output_results, encode_stack(@message_stack) ;
				@message_stack=();
			}

			if( defined $header_count ) {
				# Populate Header
				my($ncol) = $sth->{NUM_OF_FIELDS};
				my($ptr_colnames)=$sth->{NAME};
				my(@header)= @$ptr_colnames;
				unshift @header,"BATCH ".$batchid if defined $add_batchid;
				debugmsg("adding header ".join("~",@header));
				push(@output_results,\@header);
				undef $header_count;
			}
			my(@d);
			push @d,"BATCH ".$batchid if defined $add_batchid;
			foreach( @$aref) { push @d,$_; }
			#debugmsg("adding row ".join("~",@d));
			push @output_results, \@d;
		}

		# If there were no rows returned
		push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
		@message_stack=();

		$batchid++ if defined $add_batchid;
	} while( $dbprocess->{$more_results} );
	}

	###$sth->finish();

	#foreach (@output_results) {
		#debugmsg("Row: ",(ref $_)," ",join("~",@$_),"\n");
	#}
	debugmsg("Completed Query ",($#output_results+1)," rows...");

	$dbprocess->disconnect() if defined $srv;
	$die_on_error = $old_die_on_err;

	push @output_results, encode_stack(@message_stack) if $#message_stack>=0;
	@message_stack=();

	debugmsg("Returning ",($#output_results+1)," rows...");
	$DEBUG=$old_debug;
	return @output_results;
}

sub dbi_encode_row {
	my(@dat)=@_;
	return \@dat;
}

# =========================
# PROC:		 dbi_decode_row
# DESCRIPTION:	  decodes packed row returned by the functions
# PARAMETERS:   packed row
# RETURNS:  array - each element is a cell for row
# =========================
sub dbi_decode_row {
	my($rc)=@_;
	return undef unless defined $rc;
	if( ! ref $rc ) {
		confess( "Warning $rc is not a reference\n" );
		cluck "Warning $rc is not a reference\n";
		return $rc;
	}
	if( ref $rc eq "ARRAY" ) {
		return @$rc;
	} elsif( ref $rc eq "SCALAR" ) {
		print "Warning $$rc is not a reference to an array\n";
		return $$rc;
	}
	print "Warning $rc is a reference to a ",(ref $rc),"\n";
	return undef;
}
# =========================
# PROC:		 dbi_query_to_table
# =========================
sub dbi_query_to_table
{
	my(%args)=@_;
	# foreach (@_) { print __LINE__," Arg=$_ \n" };
	validate_params("dbi_query_to_table",%args);

	my($db)				= $args{-db};
	my($query)			= $args{-query};
	my($print_hdr)		= $args{-print_hdr};
	my($table_options)= $args{-table_options};
	my($die_on_error)	= $args{-die_on_error};

	my($old_mode) = dbi_set_mode("INLINE");

	my($TBLHDRCOLOR,$TBLCELLCOLOR);
	if( defined $args{-TBLHDRCOLOR} ) {
		$TBLHDRCOLOR = "BGCOLOR=".$args{-TBLHDRCOLOR};
	} else {
		$TBLHDRCOLOR = "BGCOLOR=#FFFFBB";
	}

	if( defined $args{-TBLCELLCOLOR} ) {
		$TBLCELLCOLOR = "BGCOLOR=".$args{-TBLCELLCOLOR};
	} else {
		$TBLCELLCOLOR = "BGCOLOR=#FFFFEE";
	}

	debugmsg( "Running Command To Table: $query" );

	# WE HAVE DEFINED BATCHES HERE - OUTPUT OF PRINTS NORMAL
	# OUTPUT OF BATCHES START WITH "BATCH #" IN FIRST FIELD
	my(@rc)=dbi_query(
				-db=>$db,
				-query=>$query,
				-die_on_error=>$die_on_error,
				-connection=>$args{-connection},
				-print_hdr=>$print_hdr,
				-add_batchid=>1 );

	my(@output);
	my($current_batch) = "GARBAGE STRING!";
	my($in_table)      = 0;
	my($in_pre_block)  = 0;
	my($table_count)   = 0;

	foreach (@rc) {
		my(@x)=dbi_decode_row($_);
		if( $current_batch ne $x[0] ) {

			# NEW BATCH OR PRINT STATEMENT SO START NEW TABLE
			if( $in_table ) {
					push @output,"</TABLE><p>\n";
					$table_count--;
					$in_table=0;
			}

			# DIFFERENT FIRST FIELD
			# - is either a print
			# - or the first row of batch - header if it is set
			if( $x[0] =~ /BATCH/ ) {

				if( $in_pre_block ) {
					push @output,"</b></PRE>\n";
					$in_pre_block=0;
				}

				push @output,"<TABLE $table_options>\n";
				$in_table   =1;
				$table_count++;
				$current_batch=$x[0];
				shift @x;
				if( defined $print_hdr )  {
					$args{-TBLHDRFONT}="" unless defined $args{-TBLHDRFONT};
					push @output,
						"<TR><TH $TBLHDRCOLOR><b>".$args{-TBLHDRFONT}.join("</b></TH><TH $TBLHDRCOLOR><b>".$args{-TBLHDRFONT},@x)."</b></TH></TR>\n";
				} else {
					$args{-TBLCELLFONT}="" unless defined $args{-TBLCELLFONT};
					push @output,"<TR><TD $TBLCELLCOLOR>&nbsp;".$args{-TBLCELLFONT}.join("</TD><TD $TBLCELLCOLOR>&nbsp;".$args{-TBLCELLFONT},@x)."</TD></TR>\n";
				}
			} else {

				# YUP ITS A PRINT SO JUST PRINT IT!
				# UNLESS IT IS A PLAIN LINE - THEN IGNORE IT
				my $pr_stmt=join(" ",@x);
				next if $pr_stmt =~ /^\s*$/;    # ignore blank lines
				$pr_stmt =~ s/\s*$//;		   # rtrim line

				$pr_stmt = "<PRE><b>".$pr_stmt if ! $in_pre_block;
				$in_pre_block=1;
				push @output,$pr_stmt."\n";
				next;
			}
		} else {
			if( $in_pre_block ) {
				push @output, "</b></PRE>\n";
				$in_pre_block=0;
			}
			# REGULAR ROW IN TABLE SO PRINT IT AS A ROW
			shift @x;
			push @output,"<TR><TD $TBLCELLCOLOR>&nbsp;".join("</TD><TD $TBLCELLCOLOR>&nbsp;",@x)."</TD></TR>\n";
		}
	}
	if( $in_pre_block ) {
		push @output,"</b></PRE>\n";
		$in_pre_block=0;
	}
	push @output,"</TABLE><p>\n" while $table_count-- > 0;
	dbi_set_mode($old_mode);
	@output;
}

# =========================
# PROC:		 dbi_msg_exclude
# DESCRIPTION:	  adds to list of messages to exclude
# PARAMETERS:   message number
# RETURNS:  N.A.
# NOTES:  Currently list of messages is
#	       5701,3,4 - use database successful message
#	       4002       - login incorrect message
#	       911,13,16,18,19				 - Unable To Use Db
#    20012     - srv name not in interfaces file
#    20018     - general message
#    20019     - results pending
# =========================

sub dbi_msg_exclude
{
	my($msg)=@_;
	push @syb_exclude_msgs,$msg   if defined $msg;
	push @mssql_exclude_msgs,$msg if defined $msg;
	return @syb_exclude_msgs;
}

# =========================
# PROC:		 dbi_msg_ok
# DESCRIPTION:	  remove message from list of messages to exclude
# PARAMETERS:   message number
# RETURNS:  N.A.
# =========================
sub dbi_msg_ok
{
	my($msg)=@_;
	my($cnt) = -1;
	while( defined $msg and $cnt++ < $#syb_exclude_msgs ) {
		if( $syb_exclude_msgs[$cnt] eq $msg ) {
			undef $syb_exclude_msgs[$cnt];
			return;
		}
	}

	$cnt = -1;
	while( defined $msg and $cnt++ < $#mssql_exclude_msgs ) {
		if( $mssql_exclude_msgs[$cnt] eq $msg ) {
			undef $mssql_exclude_msgs[$cnt];
			return;
		}
	}

	return @syb_exclude_msgs;
}

sub ms_error_handler
{
	my($state,$msg,$h)=@_;
	#print "dbg: ms_error_handler( $state, $msg ) $h\n";
	$msg =~ s/^(\[[\w\s]*\])+//;

	foreach (@mssql_exclude_msgs) {
		next if /^\s$/ or ! defined $_;
		#print "DBG $_\n";
		next unless $state == $_ or $msg eq $_;
		debugmsg( "IGNORING ERROR NUMBER $_" );
		&$print_handler( "dbg: MESAGE IGNORED: ms_error_handler( $state, $msg ) MESSAGE IGNORED\n" )
			if defined $DEBUG;
		return 1;
	}

	#print "DBG $mode:$state:$msg\n";
	if( $mode eq "NORMAL" ) {
		debugmsg( "PRINTING MESSAGE $msg\n" );
		&$print_handler($state.": ".$msg);
	} else {
		debugmsg( "PUSHING MESSAGE $msg onto stack\n" );
		push @message_stack, $state.": ".$msg;
	}
	return 0;
}

sub encode_stack {
	my(@in)=@_;
	my(@out);
	# debugmsg("DBG encode_stack(): Line ".__LINE__." Message Stack=");
	foreach (@in) {
		# debugmsg("  DBG:".$_);
		my(@x);
		push @x,$_;
		push @out, \@x;
	}
	return @out;
}

# =========================
# PROC:		 		DEFAULT Error Handler
# DESCRIPTION:	  	Print Error sent from server
# NOTES:				Handles messages on exclude list
# =========================
sub sybase_handler {
	my ($error, $severity, $state, $line, $server, $proc, $error_msg) = @_;
	$ignore_error_message=0;
	foreach (@syb_exclude_msgs) {
		next unless defined $_;
		next unless $error == $_ and $_ ne $error_msg;
		debugmsg( "IGNORING ERROR ($error)" );
		$login_failed_reason  = "REASON: LOGIN INCORRECT\n" if $error==4002;
		$login_failed_reason  = "REASON: SERVER NOT FOUND IN INTERFACES ($error_msg)\n" if $error==20012;
		$login_failed_reason  = "SERVER DOWN\n" if $error==20009;
		$ignore_error_message = 1 if $error == 2007;	     # dependencies
		$ignore_error_message = 1 if $error == 20018;	     # general
		return 0;
	}

	# added 4/1/06 this was slipping through and hosing some apps...
	if( $error==3 and $severity==5 and $error_msg=~/Requested server name not found.$/ ) {
		$login_failed_reason="Server Name Not Found\n";
		return;
	}
	if( $error==4 and $severity==5 and $error_msg=~/call to connect two endpoints failed$/ ) {
		$login_failed_reason="Server Not Available\n";
		return;
	}

#print "CALLED sybase_handler( ",join(" ",@_)," ) \n";
	debugmsg( "SYBASE ERROR HANDLER (error=$error severity=$severity mode=$mode)" );

	if( $error == 0 ) {
		if( $mode eq "NORMAL" ) {
			debugmsg("DBG: printing $error_msg ");
			&$print_handler($error_msg);
		} else {
			debugmsg("DBG: Adding $error_msg to Message Stack ");
			push @message_stack, "$error_msg";
		}
	} elsif( $mode eq "NORMAL" or $die_on_error ) {
			my $str="";
			$str.= "<PRE>" if $print_pre_on_msg==1;
			$str.= "sybase_handler() ERROR # $error: $error_msg";
			$str.= "</PRE>" if $print_pre_on_msg==1;
			debugmsg("DBG: Print/Die $str ");
			&$croak_handler($str."$NL FATAL ERROR\n") if $die_on_error;
			&$print_handler($str);
	} else {
			debugmsg("DBG: Adding MESSAGE $error: $error_msg to Message Stack ");
			push @message_stack, "$error: $error_msg";
	}
	return 0;
}

# =========================
# PROC:					 dbi_add_line_number
# DESCRIPTION:	  adds line number to text
# =========================
sub add_line_number
{
	my $txt = shift;
	my $out = "";
	my $count=1;
	foreach ( split /\n/,$txt ) {
		$out .= sprintf("%5d %s",$count,$_);
		$count++;
	}
	$out;
}

# =========================
# PROC:		 dbi_colorize_sql
# DESCRIPTION:	  formats sql text
# =========================
sub dbi_colorize_sql {

	 my $html = shift;
	 $html =~ s/\n/<br>\n/g;

	 # comments
	 $html =~ s/(\/\*.*?\*\/)/<FONTCOLOR="BLUE">$1<\/FONT>/g;

	 # use FONTCOLOR and then respace out later to FONT COLOR
	 $html =~ s/(\@\w+)/<FONTCOLOR="GREEN">$1<\/FONT>/ig;
	 $html =~ s/\b(not null|null)\b/<i>$1<\/i>/ig;
	 $html =~ s/\b(as|begin|between|declare|delete|drop|else|end|exec|exists|go|if|insert|procedure|return|set|update|values|from|into|select|where|and|or|create|order by)\b/<b>$1<\/b>/ig;
	 $html =~ s/\b(tinyint|smallint|int|char|varchar|float|double|datetime|smalldatetime|money|smallmoney|numeric|decimal|text|binary|varbinary|image)\b/<FONTCOLOR=\"DARKVIOLET\">$1<\/FONT>/gi;

	 $html =~ s/\t/\&nbsp;\&nbsp;\&nbsp;\&nbsp;/g;
	 $html =~ s/ /\&nbsp;/sg;
	 $html =~ s/FONTCOLOR/FONT COLOR/g;

	 "<CODE>".$html."</CODE>";
}

# =========================
# dbi_get_dirlist(): Returns list of files in directory
# =========================
sub dbi_get_dirlist {
	my($dir) = @_;
	$dir = '.' unless defined($dir);
	return () unless -d $dir;

	# Get list of files in current directory
	opendir(DIR,$dir)
		|| &$croak_handler("Can't open(directory) $dir for reading\n");
	my(@dirlist) = grep(!/^\./,readdir(DIR));
	closedir(DIR);
	return @dirlist;
}

# =========================
# PROC:		 debugmsg
# DESCRIPTION:	  debug message - only prints in debug mode
# =========================
sub debugmsg
{
	my($msg);
	foreach ( @_) { $msg.=$_ if $_; }
	return unless defined $DEBUG;
	return if $msg eq "";
	chomp $msg;

	if( $NL eq "\n" ) {
		$msg="debug: $msg";
	} else {
		$msg="<FONT COLOR=BLUE>debug: $msg</FONT>";
	}
	print $msg,$NL;
	# &$print_handler($msg,$NL);
}

# =========================
# PROC: dbi_set_mode
# DESCRIPTION: sets the mode to NORMAL or INLINE
#		       returns current mode (before set)
# =========================
sub dbi_set_mode {
	my($rc)=$mode;
	($mode)=@_ if $#_>=0;
	return $rc;
}

# =========================
# PROC: dbi_set_debug
# DESCRIPTION: Sets Debug Flag
# =========================
sub dbi_set_debug { ($DEBUG)=@_; $dbproc->trace(2) if defined $dbproc; }

# =========================
# PROC: dbi_set_web_page
# DESCRIPTION: 1 = web page 0 if not
# =========================
sub dbi_set_web_page {
	my($webpg)=@_;
	if( $webpg ) {
		$NL="<br>\n";
		$print_pre_on_msg=1;
	} else {
		$NL="\n";
		$print_pre_on_msg=0;
	}
}

sub dbi_set_ignore { my($i)=@_; $ignore_error_message=$i; }

# Returns Reformatted Query
# expects queries to have been run in batch mode so as to remove prints
#
# example:
#  @rc=dbi_query(-print_hdr=>1,...);
#  print join("\n",dbi_reformat_results( @rc )) ;
#
# EXAMPLE NUM 2
#my($dohdr)=1;my(@results);
#foreach my $db (@databaselist) {
#	my(@rc)=dbi_query(-db=>$db,-query=>'sp_helpthreshold',-print_hdr=>$dohdr );
#	if( $dohdr) {
#		my(@y)=dbi_decode_row(shift @rc);
#		unshift @y,"Database";
#		push @results, dbi_encode_row(@y);;
#		$dohdr=undef;
#	}
#	foreach (@rc) {
#		my(@z)=dbi_decode_row($_);
#		push @results, dbi_encode_row($db,@z);
#	}
#}
#print join("\n",dbi_reformat_results( @results )),"\n" ;
sub dbi_reformat_results
{
	my(@indat)=@_;
	my(@output);

	# BUILD FORMAT STRING
	my(@widths)=(-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1);
	foreach my $dat ( @indat ) {
		my @rcdat=dbi_decode_row($dat);
		next if $#rcdat==0;
		my $count=0;
		foreach ( @rcdat ) {
			s/\s+$//;
			$widths[$count] = length($_) if $widths[$count] < length($_);
			$count++;
		}
	}

	my($fmt)="";
	foreach (@widths) {
		last if $_ == -1;
		$fmt.="%-".$_."s ";
	}

	foreach( @indat ) {
		# chomp;
		# next if /^\s*$/;
		my(@rcdat)=dbi_decode_row($_);
		if( $#rcdat==0 ){
			push @output,$rcdat[0];
			next;
		}
		my($str)=sprintf($fmt,grep(s/\s*$//,@rcdat));
		push @output,$str;
	}
	return @output;
}

sub validate_params
{
	my($func,%OPT)=@_;

	# check bad keys
	foreach (keys %OPT) {
		next if defined $OK_OPTS{$func.":".$_};

		# did you mix up name=>x with -name=>x
		croak("ERROR: Incorrect $func Arguments - Change $_ argument to -$_\n")
			if defined $OK_OPTS{$func.":-".$_};
		# foreach my $x (keys %OPT) {
			# print __LINE__," ARG ~".$x."~  = ~".$OPT{$x}."~ \n";
		# }

		foreach my $x (keys %OPT) {
			print "-- $x $OPT{$x} \n";
		}
		croak("ERROR: Function $func Does Not Accept Parameter Named $_\nAllowed Arguments: ".  join(" ",grep(s/$func://,keys %OK_OPTS)));
	}

	# check missing required keys
	foreach (keys %OK_OPTS) {
		next unless /^$func:/;
		next unless $OK_OPTS{$_} == 1;
		s/$func://;
		next if defined $OPT{$_};
		my($str)="";
		#foreach ( keys %OPT) { 	$str.=$_." "; }
		foreach ( keys %OPT) { 	$str.=$_."=>".$OPT{$_}." "; }
		croak("ERROR: Function $func Requires Parameter Named $_\nPassed Args: $str\n");
	}

	if( defined $OPT{-debug} ) {
		foreach ( keys %OPT ) {
			debugmsg( "$func key=$_ val=$OPT{$_} \n") unless /^-password/;
		}
	}
}

sub dbi_eachobject
{
	my(%args)=@_;
	validate_params("dbi_eachobject",%args);

	#print "DBG: dbi_eachobject()\n";
	&$print_handler("Database $args{-db}",$NL) if defined $args{-do_print};
	&$croak_handler("Cant Use Database $args{-db}".$NL)
		unless dbi_use_database($args{-db});

	# CHECK IF GROUP EXISTS
	my($query)="select name from sysobjects where type=\"$args{-type}\" and uid=1";
	if( defined $args{-include} and $args{-include} !~ /^\s*$/) {
		my(@l)=split(/,/,$args{-include});
		$query.= " and name in (";
		foreach (@l) {
			$query.= "'".$_."',";
		}
		$query=~s/,$//;
		$query.=")";
	}

	if( defined $args{-exclude} and $args{-exclude} !~ /^\s*$/) {
		my(@l)=split(/,/,$args{-exclude});
		$query.= " and name not in (";
		foreach (@l) {
			$query.= "'".$_."',";
		}
		$query=~s/,$//;
		$query.=")";
	}

	print "Running $query in db=$args{-db}" if $args{-debug};
	my(@obj)=dbi_query(-db=>$args{-db}, -query=>$query);
	print("Query Done") if  $args{-debug};

	my($object);
	foreach (@obj) {
		my($object)=dbi_decode_row($_);
		#print "DBG: ",$object,"\n";

		#&$print_handler("\t$args{-cmd} $object\n") if defined $args{-do_print};
		#if( ! defined $args{-no_exec} ) {
		#my(@rc)=dbi_query(-db=>$args{-db},-query=>"$args{-cmd} $object",-die_on_error=>$args{die_on_error},-no_results=>$args{-no_results});
		#foreach (@rc) { my(@d)=dbi_decode_row($_); &$print_handler(@d,$NL); }
		#}

		foreach my $d ( $args{-cmd}, $args{-cmd1}, $args{-cmd2}, $args{-cmd3}, $args{-cmd4} ) {
			my($c)=$d;
			if( defined $c and $c !~ /^\s*$/ ) {
				if( $c =~ /OBJECT/ ) {
					$c =~ s/OBJECT/$object/g;
					_runit( %args, -query=>$c);
				} else {
					_runit( %args, -query=>$c." ".$object);
				}
			}
		}
	}
}

sub _runit {
	my(%args)=@_;
  	&$print_handler("\t$args{-query}\n") if defined $args{-do_print};
	#print "DBG: ",$args{-query},"\n";
	if( ! defined $args{-no_exec} ) {
     		my(@rc)=dbi_query(-db=>$args{-db},-query=>"$args{-query}",
				-die_on_error=>$args{die_on_error},
				-no_results=>$args{-no_results});
    		foreach (@rc) { my(@d)=dbi_decode_row($_); &$print_handler(@d,$NL); }
	}
}

sub dbi_optionpacks {
	my($dbtype)=@_;

	my(%options);
	if( $dbtype eq "SYBASE" ) {
		$options{IS_SYBASE_15}="FALSE";
		$options{MDA_ENABLED}="FALSE";
		foreach( dbi_query(-db=>"master",-query=>"select * from sysconfigures where config=356 and value=1" ) ) {
	         # well enable monitoring on... check number of tables
	        	foreach( dbi_query(-db=>"master",
	            		-query=>"select count(*) from master..sysobjects where type='U' and name like 'mon%'" ) ) {
		            my($num)=dbi_decode_row($_);
		            if( $num > 15 ) {  # there are ~20 of these things so this will work
		               $options{MDA_ENABLED}="TRUE";
		            }
	         	}
	      	};
	      	foreach( dbi_query(-db=>"master",-query=>"select * from sysconfigures where config=122 and value>=15000") ) {
	        	 $options{IS_SYBASE_15}="TRUE";
	      	};
	}
      	return %options;
}

# =========================
# PROC:      db_parse_opt_D
# DESCRIPTION:   given opt_D return all matching databases
# =========================
sub dbi_parse_opt_D
{
   	my($db,$usable_only,$exclude_sys_db,$get_tempdb_only)=@_;
   	$usable_only=0 unless defined $usable_only;
   	$db='%' unless $db;
   	my($server_type)="SQLSERVER";
   	foreach( dbi_query(-db=>"master", -query=>"select 'SYBASE' from master..sysdatabases where name='sybsystemprocs'" ) ) {
	    	($server_type)=dbi_decode_row($_);
   	}

		# HANDLE MULTIPLE TEMPDB's = PATCH 5/1/06
		my(@tempdb_list);
		if( $server_type eq "SYBASE" ) {
			my($sp__tempdb_exists);
			foreach( dbi_query(-db=>"sybsystemprocs", -query=>"select name from sysobjects where name='sp_tempdb'" ) ) {
		    	($sp__tempdb_exists)=dbi_decode_row($_);
   		}

   		if($sp__tempdb_exists) {
				foreach( dbi_query(-db=>"sybsystemprocs", -query=>"exec sp_tempdb 'show','db'" ) ) {
		    		my(@x)=dbi_decode_row($_);
		    		push @tempdb_list,$x[0];
   			}
   		} else {
   			push @tempdb_list,"tempdb";
   		}
   		return ($server_type,@tempdb_list) if $get_tempdb_only;
   	}

   	my($cmd)='select name from sysdatabases where (';
   	my(@list)=split(/[,\|]/,$db);
   	foreach (@list) {
      		$cmd .= "name like \'$_\' or ";
   	}
   	$cmd =~ s/or $/ \)/;

	if( $usable_only ) {
           	my( $status1bits, $status2bits );
        	if( $server_type eq "SQLSERVER" ) {
           		$status1bits = 32+64+128+256+512+1024+4096+32768;
           		$status2bits = 0x0000;
        	} else {
        		# 1120= 1000 (single usr) + 100 (suspect) + 20 (for load)
        		#   30= 20 (offline until recovery) + 10 (offline)
           		$status1bits = 0x1120;
           		$status2bits = 0x0030;
        	}
        	$cmd.=" and status&$status1bits=0 and status2&$status2bits=0";
	}
	if( defined $exclude_sys_db ) {
		$cmd .= " and name not in ('master','syblogins','model','tempdb','pubs','pubs2','msdb','Northwind',
			'sybsystemdb','sybsecurity','sybsystemprocs'".") ";
		foreach ( @tempdb_list ) {
			$cmd .= " and name != '".$_."' ";
		}
	}
	$cmd .= " and has_dbaccess(name)=1" if $server_type eq "SQLSERVER";

	my(@db);

	my(@rc)=dbi_query(-db=>'master',-query=>$cmd);
	foreach (@rc) {
		push @db,dbi_decode_row($_);
	}
   	return ($server_type,@db);
}

my(%found_name);
sub dbi_get_interfaces
{
	my($search_string,$filename)=@_;

	die "ERROR: SYBASE ENV not defined" unless defined $ENV{"SYBASE"};

	my(@srvlist,%port,%host,%found_svr);

	print "Warning: File $filename Not Found\n" if defined $filename and ! -e $filename;
	$filename=$ENV{"SYBASE"}."/interfaces" if ! defined $filename and -e $ENV{"SYBASE"}."/interfaces";
	if( defined $filename and -e $filename ) {
				
		# UNIX
		open FILE,$filename or die "ERROR: Cant open $filename : $!";
		my($srv,$dummy);
		while( <FILE> ){
			next if /^\s*#/;
			chomp;

			if( /^\s/ ) {
				next unless /query/;
				next if $srv eq "";
				chomp;

				my(@vals)=split;

				# Sun Binary Format
				if( $vals[4] =~ /^\\x/ ) {
					#format \x
					$vals[4] =~ s/^\\x0002//;

					my($p) = hex(substr($vals[4],0,4));
					my($pk_ip) = pack('C4',
						hex(substr($vals[4],4,2)),
						hex(substr($vals[4],6,2)),
						hex(substr($vals[4],8,2)),
						hex(substr($vals[4],10,2)));

					my($name,$aliases,$addrtype,$len,@addrs);
					if( defined $found_name{$pk_ip} ) {
						$name=$found_name{$pk_ip};
					} else {
						($name,$aliases,$addrtype,$len,@addrs) = gethostbyaddr($pk_ip,AF_INET);
						$found_name{$pk_ip}=$name if defined $name and $name !~ /^\s*$/;
					}

					my($h) =$name ||
						hex(substr($vals[4],4,2)).".".
						hex(substr($vals[4],6,2)).".".
						hex(substr($vals[4],8,2)).".".
						hex(substr($vals[4],10,2));

					if( defined $host{$srv}  ) {
						$host{$srv}.="+".$h;
						$port{$srv}.="+".$p;
					} else {
						$host{$srv}=$h;
						$port{$srv}=$p;
					}
				} else {
					$port{$srv} = $vals[4];
					$host{$srv} = $vals[3];
				}

				push @srvlist,$srv unless defined $found_svr{$srv};
				$found_svr{$srv} = 1;
				# $srv="";
			} else {
				($srv,$dummy)=split;
			}
		}
		close FILE;

	} elsif( -e $ENV{"SYBASE"}."/ini/sql.ini"  ) {
		# DOS
		# fix up sybase env vbl
		$ENV{"SYBASE"} =~ s#\\#/#g;
		open FILE,$ENV{"SYBASE"}."/ini/sql.ini"
			or die "ERROR: Cant open file ".$ENV{"SYBASE"}."/ini/sql.ini: $!\n";
		my($cursvr)="";
		while( <FILE> ){
			next if /^#/;
			next if /^;/;
			next if /^\s*$/;
			chop;

	#
	# MUST ALSO HANDLE BIZARROS LIKE THE FOLLOWING
	#
	#[NYSISW0035]
	#$BASE$00=NLMSNMP,\pipe\sybase\query
	#$BASE$01=NLWNSCK,nysisw0035,5000
	#MASTER=$BASE$00;$BASE$01;
	#$BASE$02=NLMSNMP,\pipe\sybase\query
	#$BASE$03=NLWNSCK,nysisw0035,5000
	#QUERY=$BASE$02;$BASE$03;

			if( /^\[/ ) {
				# IT A SERVER LINE
				s/^\[//;
				s/\]\s*$//;
				s/\s//g;
				$cursvr=$_;
				push @srvlist,$_ unless defined $found_svr{$_};
				$found_svr{$_} = 1;
			} else {
				# IT A DATA LINE
				next if /^master=/i;
				next if /QUERY=\$BASE/i;
				next if /\,.pipe/i;
				next if /NLMSNMP/i;
				(undef,$host{$cursvr},$port{$cursvr})=split /,/;
			}
		}
		close FILE;
	} elsif( defined $filename ) {
		die "ERROR: $filename error";
	} else {
		die "ERROR: UNABLE TO DETERMINE INTERACE FILE";
	}

	my(@rc);
	foreach (sort @srvlist ) {

		if(defined $search_string) {
			next unless /$search_string/i
				or $host{$_} =~ /$search_string/i
				or $port{$_} =~ /$search_string/i;
		}
		push @rc,[ $_,$host{$_},$port{$_} ];
	}
	return @rc;
}

1;

__END__

=head1 NAME

DBIFunc.pm - DBI Database Subroutines

=head2 DESCRIPTION

This is my cut at a generic subroutine library for sybase perl applications.
I have used it for a variety of packages and it seems quite adequate.
It currently uses dblib.
These routines are useful if you only need a single connection to sybase.

=head2 AUTHOR

	Edward Barlow
	mail: edbarlow@mad.scientist.com
	url : http://www.edbarlow.com
	All Rights Reserved

=head2 FUNCTION OVERVIEW

 dbi_add_line_number
 dbi_colorize_sql     - format sql text
 dbi_eachobject
 dbi_get_sql_text     - get sql text for an object
 dbi_get_dirlist
 dbi_has_login_failed
 dbi_msg_exclude      - tell message handler to ignore this message
 dbi_msg_ok	   - tell message handler to error out on this message
 dbi_set_ignore
 dbi_disconnect     - close you connection
 dbi_connect	  - connect to your server
 dbi_use_database     - use database
 dbi_query	    - the main query
 dbi_decode_row       - interpret output from dbi_query()
 dbi_query_to_table   - command - output html formatted
 dbi_set_debug	- set debug option
 dbi_set_web_page      - set html_on option
 dbi_get_errorlogs    - return a list of error log files
 dbi_set_mode	 - set run mode

=head2 LIBRARY FUNCTIONS

=over 4

=item * dbi_disconnect()

 DESCRIPTION: cleans up and exits the server
 RETURNS:     no return code

=item * dbi_connect($server,$login,$password)

 DESCRIPTION: connects to sybase using login & password.
 RETURNS:     1/0

=item * dbi_use_database($db)

 DESCRIPTION: use a database
 PARAMETERS:  $db - database name
 RETURNS:     returns 1 (success) or 0 (fail)

=item * dbi_query($db,$query,$die_on_error,$print_hdr,$add_batchid,$srv,$login,$pass)

 DESCRIPTION:   run a command
 PARAMETERS:
   $db	  - database name
   $query       - what to run
   $die_on_err (optional) - if defined then die if an error
   $print_hdr   - if defined first row will be header info
   $add_batchid - batch id should be added
   $srv	 - OPTIONAL SERVER ONLY FOR THIS COMMAND
   $login       - OPTIONAL LOGIN TO SERVER ONLY FOR THIS COMMAND
   $pass	- OPTIONAL PASSWORD TO SERVER ONLY FOR THIS COMMAND
 RETURNS:  returns array (success) or undef (fail). The array must
	     be decoded

=item * dbi_decode_row($row)

 DESCRIPTION:  decodes packed row returned by the functions
 PARAMETERS:  $row - packed row
 RETURNS:  array - each element is a cell for row

=item * dbi_query_to_table($db,$query,$print_hdr, $table_options)

 DESCRIPTION: Run sybase query to table (or multiple tables)
 RETURNS: table formatted output

=item * dbi_msg_exclude()

 DESCRIPTION:  adds to list of messages to exclude
 PARAMETERS:  message number
 RETURNS:  N.A.
 NOTES:  Currently list of messages is

   5701,3,4 - use database successful message
   4002    - login incorrect message
   911,13,16,18,19     - Unable To Use Db
	2409		 - char set conversion
   20012     - srv name not in interfaces file
   20018     - general message
   20019     - results pending

=item * dbi_msg_ok($msg)

 DESCRIPTION:  remove message from list of messages to exclude
 PARAMETERS:  message number
 RETURNS:  N.A.

=item * dbi_get_sql_text($objname,$print_go)

 DESCRIPTION:  return text for an object
 NOTES:  handles any object type in current db
 PARAMETERS:
   $objname: object name in current db
   $print_go:  do you wish to include a line with the word go

=item * dbi_colorize_sql($html)

 DESCRIPTION:  formats sql text

=item * dbi_get_dirlist($directory)

 DESCRIPTION:  Returns list of files in directory

=item * dbi_get_errorlogs($errorlog_type,$SYBASE)

 DESCRIPTION: returns a list of sybase errorlogs.
 PARAMETERS:    errorlog_type:
   if type=0 SYBASE
   if type=1 Backupserver
   if type=2 Both

=item * dbi_eachobject($db,$cmd,$type,$do_print)

 DESCRIPTION: runs cmd on each object of type and print output
 PARAMETERS:
   $do_print: print text of what you are doing too
   $type: object type

=item *  dbi_set_mode($mode)

 DESCRIPTION: sets the mode to NORMAL or INLINE.
 RETURNS: Returns current mode (before set).

=item *  dbi_set_debug($debug_mode)

 DESCRIPTION: Sets Debug Flag

=item *  dbi_set_web_page($NL)

 DESCRIPTION: Sets NEWLINE  & controls printing of pre on errors

=back

=cut
