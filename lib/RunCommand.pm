# Copyright (c) 2004 by Edward Barlow. All rights reserved.

package RunCommand;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use Tk::ROText;
use Do_Time;

$VERSION= 1.0;
@ISA      = ( 'Exporter' );
@EXPORT   = ( 'RunCommand', "show_html","get_win32_disks");

sub RunCommand
{
   my(%OPT)=@_;
   chomp $OPT{-cmd};

   my($starttime)=time;

   $OPT{-err}="" unless $OPT{-err};
   $OPT{-statusfunc}->( "[RunCommand] Unix RunCommand Called\n" )
   	if defined $OPT{-statusfunc};

   $OPT{-cmd}=$^X." ".$OPT{-cmd} if $OPT{-cmd} =~ /^[\~\:\\\/\w_\.]+\.pl\s/;
   my $cmd_no_password=$OPT{-cmd};
   $OPT{-cmd} .= " 2>&1 |";

   if( $cmd_no_password=~/-PASSWORD=/ ) {
   	$cmd_no_password=~s/-PASSWORD=\w+/-PASSWORD=XXX/;
   } elsif( $cmd_no_password=~/-PASS=/ ) {
   	$cmd_no_password=~s/-PASS=\w+/-PASS=XXX/;
   } else {
   	$cmd_no_password=~s/-P\w+/-PXXX/;
   }

   if( length($cmd_no_password)>70 ) {
   	my(@words)=split(/ /,$cmd_no_password);
   	my($curlen)=0;
   	$cmd_no_password="";
   	foreach(@words) {
   		if( $curlen+length() > 70 ) {
   			if( $curlen+length()>75 ) {
   				$cmd_no_password.="\n\t".$_;
   				$curlen=length();
   			} else {
   				$cmd_no_password.=" ".$_."\n\t";
   				$curlen=0;
   			}
   		} else {
   			$curlen+=length()+1;
   			$cmd_no_password.=" ".$_;
   		}
   	}
		$cmd_no_password=~s/^\s+//;
		$cmd_no_password=~s/\s+$//;
   }
	
   $OPT{-statusfunc}->( "[RunCommand] ".$cmd_no_password."\n" ) 
   	if  defined $OPT{-statusfunc};
   	
   if( ! open( SYS2STD,$OPT{-cmd} )) {
		print "debug: Cant Run $OPT{-cmd}\n" if $OPT{-debug};		
		my($tmsg)= $OPT{-err}."Command Failed To Run\n".
      				$cmd_no_password."\n".
						"Error: $!\n";		
		my($xwidth)=20;
		foreach ( split(/\n/,$tmsg )) { 
			my($l2)=length; 
			$xwidth=$l2 if $l2>$xwidth; 
		}		
		$OPT{-mainwindow}->messageBox(
			-title => 	'Command Failed',
			-width => 	$xwidth, 
			-message =>	$tmsg,
				-type => "OK" ) if defined $OPT{-mainwindow};
		
		return(0,undef);
	}
	
	print "debug: Processing Output For $OPT{-cmd}\n" if $OPT{-debug};
	my(@output);	
   while ( <SYS2STD> ) {
   	chomp; chomp;
		$OPT{-printfunc}->($OPT{-printhdr}.$_."\n") if defined $OPT{-printfunc};
		push @output, $_;
   }
   
   close(SYS2STD);   
	print "debug: Done Processing Output\n" if $OPT{-debug};
	
	my($cmd_rc)=$?;
   if( $cmd_rc != 0 ) {   	
   	my($tmsg)= $OPT{-err}."Command Failed - return code=$cmd_rc\n".
      							"Command: ".$cmd_no_password."\n".
									"\n\nResults:\n".join("\n",@output);
		my($width)=40;
		foreach ( split(/\n/,$tmsg )) { my($l)=length; $width=$l if $l>$width; }
		$OPT{-mainwindow}->messageBox(
					-title => 'Command Failed', 
					-width=>$width,
					-message => $tmsg,
					-type => "OK" ) if defined $OPT{-mainwindow};
		return(0,\@output);
	}
	
	my($dur);

	if( defined $OPT{-showinwin} ) 	{
		$OPT{-title} = $OPT{-title} || $OPT{-showinwin};
		my(@output2);
		my($ignore_count)=0;
		foreach (@output){
			if( $OPT{-ignore_string} and /$OPT{-ignore_string}/) {
				push @output2,".";
				$ignore_count++;
				if( $ignore_count>40 ) {
					push @output2,"\n";
					$ignore_count=0;
				}
			} else {
				if( $ignore_count>0 ) {
					push @output2,"\n";
					$ignore_count=0;
				}
				push @output2,$_."\n";
			};
		}
	
		# ORIGINAL MECHANISM TO SHOW RESULTS
		#$OPT{-mainwindow}->messageBox(
		#	-title => $OPT{-title},
		#	-message => $OPT{-showinwin}." (Succeeded)\n\nResults:\n".join("",@output2),
		#	-type => "OK" );

		# CURRENT MECHANISM
		my $d = $OPT{-mainwindow}->DialogBox(-title=>$OPT{-title}, -buttons=>["OK"]);
		$d->add('Frame')->grid;
		my $frame = $d->Subwidget('frame');

		$frame->Label(-font=>"large",-text=>"COMMAND:", -justify=>'left', -bg=>'beige',
			-relief=>'groove')->grid(-column=>1,-row=>1,-sticky=>'ew');
			
		$frame->Label(-bg=>'white',-font=>"large",-textvariable=>\$OPT{-showinwin},
			-width=>35, -relief=>'groove')->grid(-column=>2,-row=>1, -sticky=>'ew');

		$frame->Label(-font=>"large",-text=>"DURATION:", -justify=>'left', -bg=>'beige',
			-relief=>'groove')->grid(-column=>1,-row=>2,-sticky=>'ew');
			
		my($dur)=do_diff(time - $starttime, 1);
		$frame->Label(-bg=>'white',-font=>"large",-text=>$dur,
				-width=>35, -relief=>'groove')->grid(-column=>2,-row=>2, -sticky=>'ew');


		my($textObject) = $frame->Scrolled("ROText",
			-relief => "sunken", -wrap=>'none',
			-bg=>'white',#-font=>'code',
			-scrollbars => "osoe")->grid(-column=>1,-row=>3, -sticky=>'ew',-columnspan=>2, -rowspan=>3);
		#$textObject->tagConfigure("t_red",   -foreground=>'red');
		$textObject->insert("end",$OPT{-showinwin}." (Succeeded)\n\nResults:\n".join("",@output2));
		$OPT{-statusfunc}->( "[RunCommand] "."Creating Window With Results... Click Ok To Continue\n" )
   			if  defined $OPT{-statusfunc};
		$d->Show();
	
	}
	
	$OPT{-statusfunc}->( "[RunCommand] "."Command completed at ".localtime(time)." (Duration=$dur)\n")
		if defined $OPT{-statusfunc};
	print "debug: Returning ",$#output, " Rows\n" if $OPT{-debug};
	
	return(1,\@output);
}

sub show_html {
	my($file)=@_;

	if( ! defined $main::CONFIG{UNIX_BROWSER_LOCATION} or ! -x $main::CONFIG{UNIX_BROWSER_LOCATION} ) {
	$main::XMLDATA{mainWindow}->messageBox(
		-title => 'Show Html Unimplemented',
		-message => "WARNING: Note Web Browser Not Specified. \n\nUnix GEM installs must specify a Web Browser Executable on the Paths tab of Configure.pl.\n\nYou were trying to browse to\n$file\n",
		-type => "OK" );
	} else {
		system($main::CONFIG{UNIX_BROWSER_LOCATION}." ".$file."&");
	}
}

sub get_win32_disks {
	my($system)=@_;
	my(@rc);
	if( $ENV{PROCESSOR_LEVEL} ) {
		die "Library Path Mixup : You are running RunCommand.pm from the lib subdirectory instead of from the appropriate Win32* subdirectory and are running the unix version.\n";
	} else {
		die "Can Not Check Win32 Disks From UNIX.  If you are on windows, you are running RunCommand.pm from the lib subdirectory instead of from the Win32* subdirectory and are running the unix version.\n";
	}

	return @rc;
}

1;

__END__

=head1 NAME

RunCommand.pm - Architecture Independant External Command Execution

=head2 DESCRIPTION

This is an attempt at an architecture independent mechanism to run external commands.

On Win32 Architectures, Spawn a separate process window.  On unix, run
job in the background window with a fileevent manager.  This is only
useful in a Tk architecture.

=head2 USAGE

RunCommand(-key=>value...);

	-cmd		   - The system command to run.  If the first word in the command ends in .pl, then the
	              current version of perl will be prepended to it
	-err			- prefix for alarm messages
	-mainwindow - main window - if you pass this an error box will appear if
					 the command errors out
	-printfunc  - optional function ref to print ouput messages.  
	-printhdr	- prefix for -printfunc commands 
	-title		- window title
	
	-statusfunc - function ref to print status messages to.  
					 
	-showinwin	- if defined it will create an output window with the results
						will use -title||-showinwin for the title
						will ignore output lines matching -ignore_string
						

get_win32_disks($system)
	
	returns an array of disks
	
=head2 FLOW

	-statusmsg version info
   -statusmsg $cmd without password
   if ! open( CMD )
   	print debug: ...
   	-mainwindow -> messagebox() if -mainwindow
   foreach <results>
   	-printfunc( -printhdr.$_ 
   	push @output,$_
   if $?!=0	# command returncode
   	messagebox ( -err.msg.$cmd_no_pass.@output )
   	return (0,\@output)
   if -showinwin
   	
=head2 NOTES

If the command is a perl command, the current running perl will be used

Returns two args - $status, $outputref.  $status is 1 for success and 0 for failure.  $outputref is a ptr to an array of return data.

=cut
