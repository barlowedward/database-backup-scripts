package Win32::TaskScheduler;

use 5.006;
use strict;
use warnings;

require Exporter;

our @ISA = qw(Exporter);

# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.

# This allows declaration	use TaskScheduler ':all';
# If you do not need this, moving things directly into @EXPORT or @EXPORT_OK
# will save memory.
our %EXPORT_TAGS = ( 'all' => [ qw(
) ] );

our @EXPORT_OK = ( @{ $EXPORT_TAGS{'all'} } );

our @EXPORT = qw(
	
);

our $VERSION = '2.0.3a';

#variables exported to user scope
use constant TASK_SUNDAY=>1;
use constant TASK_MONDAY=>2;
use constant TASK_TUESDAY=>4;
use constant TASK_WEDNESDAY=>8;
use constant TASK_THURSDAY=>16;
use constant TASK_FRIDAY=>32;
use constant TASK_SATURDAY=>64;
use constant TASK_FIRST_WEEK=>1;
use constant TASK_SECOND_WEEK=>2;
use constant TASK_THIRD_WEEK=>3;
use constant TASK_FOURTH_WEEK=>4;
use constant TASK_LAST_WEEK=>5;
use constant TASK_JANUARY=>1;
use constant TASK_FEBRUARY=>2;
use constant TASK_MARCH=>4;
use constant TASK_APRIL=>8;
use constant TASK_MAY=>16;
use constant TASK_JUNE=>32;
use constant TASK_JULY=>64;
use constant TASK_AUGUST=>128;
use constant TASK_SEPTEMBER=>256;
use constant TASK_OCTOBER=>512;
use constant TASK_NOVEMBER=>1024;
use constant TASK_DECEMBER=>2048;
use constant TASK_FLAG_INTERACTIVE=>1;
use constant TASK_FLAG_DELETE_WHEN_DONE=>2;
use constant TASK_FLAG_DISABLED=>4;
use constant TASK_FLAG_START_ONLY_IF_IDLE=>16;
use constant TASK_FLAG_KILL_ON_IDLE_END=>32;
use constant TASK_FLAG_DONT_START_IF_ON_BATTERIES=>64;
use constant TASK_FLAG_KILL_IF_GOING_ON_BATTERIES=>128;
use constant TASK_FLAG_RUN_ONLY_IF_DOCKED=>256;
use constant TASK_FLAG_HIDDEN=>512;
use constant TASK_FLAG_RUN_IF_CONNECTED_TO_INTERNET=>1024;
use constant TASK_FLAG_RESTART_ON_IDLE_RESUME=>2048;
use constant TASK_FLAG_SYSTEM_REQUIRED=>4096;
use constant TASK_TRIGGER_FLAG_HAS_END_DATE=>1;
use constant TASK_TRIGGER_FLAG_KILL_AT_DURATION_END=>2;
use constant TASK_TRIGGER_FLAG_DISABLED=>4;
use constant TASK_MAX_RUN_TIMES=>1440;
use constant REALTIME_PRIORITY_CLASS=>256;
use constant HIGH_PRIORITY_CLASS=>128;
use constant NORMAL_PRIORITY_CLASS=>32;
use constant IDLE_PRIORITY_CLASS=>64;
use constant INFINITE=>-1;
use constant TASK_TIME_TRIGGER_ONCE=>0;
use constant TASK_TIME_TRIGGER_DAILY=>1;
use constant TASK_TIME_TRIGGER_WEEKLY=>2;
use constant TASK_TIME_TRIGGER_MONTHLYDATE=>3;
use constant TASK_TIME_TRIGGER_MONTHLYDOW=>4;
use constant TASK_EVENT_TRIGGER_ON_IDLE=>5;
use constant TASK_EVENT_TRIGGER_AT_SYSTEMSTART=>6;
use constant TASK_EVENT_TRIGGER_AT_LOGON=>7;

sub New {
	my($class)=shift;
	my(%x);
	my($self) = bless { %x }, $class;
	return $self;
}

1;
